package com.aquatic.av.activities.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.DashboardRepository
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentResponse
import com.google.gson.JsonObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val dashboardRepository: DashboardRepository) : ViewModel() {
    private val _userEnvironmentResponse = MutableLiveData<Event<State<MyEnvironmentResponse>>>()
    val userEnvironmentResponse: LiveData<Event<State<MyEnvironmentResponse>>> get() = _userEnvironmentResponse

    fun getUserEnvironment(token: String, userEmail: JsonObject) {
//        viewModelScope.launch {
//            dashboardRepository.getUserEnvironments(token, userEmail).collect {
//                _userEnvironmentResponse.value = Event(it)
//            }
//        }
        viewModelScope.launch {
            dashboardRepository.getMyEnvironmentInfo(token, userEmail).collect {
                _userEnvironmentResponse.value = Event(it)
            }
        }

    }

}