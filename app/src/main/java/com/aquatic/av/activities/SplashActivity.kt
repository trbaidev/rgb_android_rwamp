package com.aquatic.av.activities

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.VideoView
import com.aquatic.av.R
import com.aquatic.av.activities.base.AppActivityController
import com.aquatic.av.utils.launchActivityAndFinish

/**
 * - Initial screen for the app, This screen will not have any layout, This screen will appear till app load the dependency libs
 * - This screen will just show the background with logo at the centre of the screen which defined in style
 * - Will check the user auth and move the screen to auth page or app home page
 */
class SplashActivity : AppActivityController() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)

        //hideSystemUI()

        packageManager.getPackageInfo(packageName, PackageManager.GET_META_DATA).apply {
            findViewById<TextView>(R.id.version).text = getString(R.string.splash_lbl_version) + " " + versionName
        }

        val videoView = findViewById<View>(R.id.videoView) as VideoView

        val video = Uri.parse("android.resource://" + packageName + "/" + R.raw.aav_logo_anim_splash)
        videoView.setVideoURI(video)
        videoView.setZOrderOnTop(true)

        videoView.setOnCompletionListener { mp ->
            mp.isLooping = false
            launchActivityAndFinish<MainActivity>()

        }

        videoView.start()

    }


//    private fun hideSystemUI() {
//        // Enables regular immersive mode.
//        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
//        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//        val decorView = window.decorView
//        decorView.systemUiVisibility =
//                (View.SYSTEM_UI_FLAG_IMMERSIVE // Set the content to appear under the system bars so that the
//                        // content doesn't resize when the system bars hide and show.
//                        or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN // Hide the nav bar and status bar
//                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                        or View.SYSTEM_UI_FLAG_FULLSCREEN)
//    }
}
