package com.aquatic.av.activities.model


import com.google.gson.annotations.SerializedName

data class UserEnvironmentModel(
    @SerializedName("device_id")
    var deviceId: Int? = 0,
    @SerializedName("device_name")
    var deviceName: String? = "",
    @SerializedName("device_type")
    var deviceType: Any? = Any(),
    @SerializedName("env_icon_name")
    var envIconName: String? = "",
    @SerializedName("env_name")
    var envName: String? = "",
    @SerializedName("serial_no")
    var serialNo: String? = "",
    var purchase_location: String?="",
    var purchase_date: String?="",
    var product_id: Int=0,
    var product_type_id: Int=0,
    var product_name: String?="",
    var product_type: String?="",
    var purchase_location_dealer_id: Int=0,
    var dealer_name: String?="",
    var base_path: String?="",
    var receipt: String?="",
    var receipt_status: String?="",
    var product_code: String?="",
    var expiry_date: String?="",
    var remaining_warranty: String?="",
    var product_image: String?="",
    var receipt_path: String?=""
)