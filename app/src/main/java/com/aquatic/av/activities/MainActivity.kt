package com.aquatic.av.activities

import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.aquatic.av.R
import com.aquatic.av.activities.base.AppActivityController
import com.aquatic.av.activities.viewModel.MainViewModel
import com.aquatic.av.databinding.ActivityMainBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.accountAuthentication.AccountFragment
import com.aquatic.av.screens.dashboard.DashboardFragment
import com.aquatic.av.screens.rgb.ColorPickerFragment
import com.aquatic.av.screens.setting.SettingFragment
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentModel
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.getUserDetailModel
import com.aquatic.av.utils.BasePreferenceHelper.isLogin
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.dialogs.DialogFactory
import com.google.android.material.navigation.NavigationBarView
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppActivityController() {
    private var viewBinding: ActivityMainBinding? = null
    val viewModel by viewModels<MainViewModel>()

    val mTitleBar
        get() = viewBinding?.headerMain
    val bottomNavigation
        get() = viewBinding?.navigation

    var isRGBMode = false

    var device_id: Int = 841

    private val bottomNavigationListener =
        NavigationBarView.OnItemSelectedListener { item: MenuItem ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    popBackStackTillEntry(0)
                    navigateTo(DashboardFragment())
                    return@OnItemSelectedListener true
                }
                R.id.navigation_lights -> {
                    popBackStackTillEntry(0)
                    navigateTo(ColorPickerFragment())
                    return@OnItemSelectedListener true
                }
                R.id.navigation_settings -> {
                    popBackStackTillEntry(0)
                    navigateTo(SettingFragment())
                    return@OnItemSelectedListener true
                }
                else -> return@OnItemSelectedListener false
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewBinding?.isDataLoading = false
        initListeners()
        initApp()
    }


    private fun setEnvironmentlistner() {
        viewModel.userEnvironmentResponse.observe(this, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.let {
                        if (!it.getResponseData().isNullOrEmpty()) {
                            it.getResponseData()?.let { it1 -> mTitleBar?.adapter?.setData(it1) }
                        }
                    }
                }
            }
        })
    }


//
//    fun setEnvironmentSelectionListener(id:Int) {
//        mTitleBar?.showEnvironments(id)
//        setEnvironmentlistner()
//    }


    fun setEnvironmentSelectionListener(itemSelectedListener: (item: MyEnvironmentModel) -> Unit) {
        mTitleBar?.showEnvironments(itemSelectedListener)
        setEnvironmentlistner()
    }

    fun initApp(isRGBMode: Boolean = false) {
        this.isRGBMode = isRGBMode
        when {
            isLogin() -> {
                viewBinding?.apply {
                    navigation.setOnItemSelectedListener(bottomNavigationListener)
                    navigation.selectedItemId = R.id.navigation_home
                    navigation.menu.findItem(R.id.navigation_home).isVisible = true
                    mTitleBar?.binding?.spnEnvironment?.setSelection(-1)
                    mTitleBar?.adapter?.setData(listOf())
                }
                var jObj = JsonObject()
                jObj.addProperty("user_id", this.getUserDetailModel()?.user_id)
                viewModel.getUserEnvironment(this.getAccessToken(), jObj)
            }
            isRGBMode -> {
                viewBinding?.apply {
                    navigation.setOnItemSelectedListener(bottomNavigationListener)
                    navigation.selectedItemId = R.id.navigation_lights
                    navigation.menu.findItem(R.id.navigation_home).isVisible = false
                }
                mTitleBar?.adapter?.setData(listOf(MyEnvironmentModel(envName = "RGB Mode")))

            }
            else -> {
                navigateTo(AccountFragment())
            }
        }

    }

    fun showLoader(loading: Boolean) {
        viewBinding?.isDataLoading = loading
        if (viewBinding?.isDataLoading == true) {
            blockUserInterAction()
        } else {
            unBlockUserInterAction()
        }
    }

    fun blockUserInterAction() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    fun unBlockUserInterAction() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    private fun initListeners() {
        mTitleBar?.let {
            it.setBackButtonListener {
                onBackPressed()
            }
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) super.onBackPressed() else DialogFactory.createQuitDialog(this,
            { _, _ -> finish() },
            R.string.message_quit
        ).show()
    }

    fun navigateTo(destination: Fragment) {
        val transaction = supportFragmentManager
            .beginTransaction()
        transaction
            .replace(R.id.fragmentContainer, destination)
            .addToBackStack(if (supportFragmentManager.backStackEntryCount == 0) "firstFragment" else null)
            .commit()
    }

    /**
     * @param entryIndex is the index of fragment to be popped to, for example the
     * first fragment will have a index 0;
     */
    fun popBackStackTillEntry(entryIndex: Int) {
        if (supportFragmentManager.backStackEntryCount <= entryIndex) return
        val entry = supportFragmentManager.getBackStackEntryAt(
            entryIndex
        )
        supportFragmentManager.popBackStack(
            entry.id,
            FragmentManager.POP_BACK_STACK_INCLUSIVE
        )
    }

    fun popFragment() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStack()
    }

    fun canShowRGBIcon(canShow: Boolean) {
        viewBinding?.navigation?.menu?.findItem(R.id.navigation_lights)?.isVisible = canShow
    }


}
