package com.aquatic.av.player

import android.graphics.Color
import android.media.MediaPlayer
import android.media.audiofx.Visualizer
import android.media.audiofx.Visualizer.MEASUREMENT_MODE_PEAK_RMS
import android.media.audiofx.Visualizer.MeasurementPeakRms
import android.util.Log
import java.util.*

object AAVPlayer {
    private var playStatusListener: PlayStatusListener? = null

    private var mediaPlayer: MediaPlayer = MediaPlayer()
    private val songs: ArrayList<Song> = arrayListOf()
    private var shuffleSongs: ArrayList<Song> = arrayListOf()

    private var TAG: String = "MPlayer"
    private var isPaused: Boolean = false
    private var isPlaying: Boolean = false
    private var repeat: Boolean = false
    private var currentPosition: Int = 0

    private var visualizer: Visualizer? = null

    private var oldRms: Int = 0
    const val MAXCOLOR = 200
    private var mColorChangeCount = 0
    private var cRed = 150
    private var cAlpha = 0
    private var cBlue = 0
    private var cGreen = 50
    private var changenum = 5
    private var blue = 0
    private var green = 50
    private var red = 150

    var mMeasurementPeakRms =
        MeasurementPeakRms()

    init {
        mediaPlayer.setOnCompletionListener {
            visualizer?.enabled = false

            if (repeat) {
                play()
                return@setOnCompletionListener
            }
            next()
        }
    }

    private fun handleShuffle(): Boolean {
        if (shuffleSongs.size > 0) {
            updateSongs(shuffleSongs)
            shuffleSongs.clear()
            return true
        }

        return false
    }

    fun updateSongs(songs: ArrayList<Song>) {
        this.songs.clear()

        if (songs.size > 0) {
            this.songs.addAll(songs)
            playStatusListener?.newSongList(songs)
        }
    }

    fun setListener(listener: PlayStatusListener) {
        playStatusListener = listener
    }

    fun getSongs() = songs


    fun play() {
        if (isPaused) {
            if (shuffleEnable()) {
                handleShuffle()
            } else {
                mediaPlayer.start()
                visualizer?.enabled = true
                playStatusListener?.onPlayStateChanged(true)
                return
            }
        }

        if (songs.size > 0) {
            val song = songs[currentPosition]
            try {
                mediaPlayer.reset()
                mediaPlayer.setDataSource(song.path)
                mediaPlayer.prepare()
                mediaPlayer.start()
                playStatusListener?.onPlayStateChanged(true)

                visualizer = Visualizer(mediaPlayer.audioSessionId)
                visualizer?.enabled = false
                visualizer?.measurementMode = MEASUREMENT_MODE_PEAK_RMS
                visualizer?.captureSize = 512
                isPlaying = true

                visualizer?.setDataCaptureListener(object : Visualizer.OnDataCaptureListener {

                    override fun onWaveFormDataCapture(
                        visualizer: Visualizer, waveform: ByteArray,
                        samplingRate: Int
                    ) {

                        if (isPlaying()) {
                            try {
                                val measurementPeakRms =
                                    visualizer.getMeasurementPeakRms(mMeasurementPeakRms)
                            } catch (e: Exception) {
                            }
                            if (mMeasurementPeakRms.mRms == -9600) {
                                return
                            }
                            if (mMeasurementPeakRms.mRms >= -9000) {
                                convertRMStoHexColor(
                                    (mMeasurementPeakRms.mRms + 1.5 * (mMeasurementPeakRms.mRms - oldRms)).toInt(),
                                    (mMeasurementPeakRms.mRms - oldRms) * 2.5
                                )
                                oldRms = mMeasurementPeakRms.mRms
                            }
                        } else {
                            Log.d(TAG, "Media player is not playing...")
                            isPlaying = false
                        }
                    }

                    override fun onFftDataCapture(
                        visualizer: Visualizer,
                        fft: ByteArray,
                        samplingRate: Int
                    ) {
                        //Do Nothing
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false)

                visualizer?.enabled = true

            } catch (e: Exception) {
                e.printStackTrace()
                isPlaying = false
                playStatusListener?.onPlayStateChanged(false)
                next()
            }
        }
    }

    fun play(song: Song) {
        val pos = songs.indexOfFirst { s -> s.id == song.id }

        isPaused = false
        currentPosition = pos

        play()
    }

    fun isPlaying() = mediaPlayer.isPlaying

    fun pause() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
            isPaused = true
            isPlaying = false
            visualizer?.enabled = false

            playStatusListener?.onPlayStateChanged(false)
        }
    }

    fun getProgress() = mediaPlayer.currentPosition

    fun getCurrentPlayingSongPosition() = currentPosition

    fun seekTo(process: Int) {
        if (songs.size == 0) {
            return
        }

        val currentSong = songs[currentPosition]

        if (currentSong.duration <= process) {
            onCompletion()
        } else {
            mediaPlayer.seekTo(process)
            playStatusListener?.onPlayStateChanged(isPlaying())
        }
    }

    fun onCompletion() {
        playStatusListener?.onPlayStateChanged(false)
        //  Check with team we need to play next song or just we should leave like that
        next()
    }

    fun hasNext() = currentPosition < (songs.size - 1)

    fun releasePlayer() {
        songs.clear()
        mediaPlayer.reset()
        mediaPlayer.release()
    }

    fun next() {
        val isShuffled = handleShuffle()

        if (isShuffled) {
            currentPosition = 0
            play()
            return
        }

        if (!hasNext()) {
            if (!repeat) {
                if (isPlaying()) {
                    mediaPlayer.reset()
                }
                return
            }
            currentPosition = 0
        } else {
            currentPosition++
            isPaused = false
        }

        play()
    }

    fun prev() {
        val isShuffled = handleShuffle()

        if (isShuffled) {
            currentPosition = 0
            play()
            return
        }

        if (currentPosition == 0) return

        currentPosition--
        isPaused = false
        play()
    }

    fun repeat() {
        repeat = !repeat
    }

    fun isRepeat() = repeat

    fun shuffleEnable() = shuffleSongs.size > 0

    fun shuffleSongs() {
        if (shuffleEnable()) {
            shuffleSongs.clear()
            return
        }

        val rand = Random()
        val speed = System.nanoTime()
        rand.setSeed(speed)

        shuffleSongs.clear()
        shuffleSongs.addAll(getSongs())
        shuffleSongs.shuffle(rand)

        if (isPlaying()) {
            return
        }
        currentPosition = 0
        isPaused = false
        playStatusListener?.onShuffleChanged()
        handleShuffle()
    }


    @Synchronized
    fun convertRMStoHexColor(newRms: Int, lastRMS: Double) {
        cRed = 0
        cGreen = 0
        cBlue = 0
        cAlpha = 0
        if (newRms > -8000 && newRms <= -3000) {
            val cache = (0.05 * (Math.abs(newRms) - 3000).toDouble()).toInt()
            cRed =
                (cache.toDouble() / 255.0 * red.toDouble()).toInt()
            cGreen =
                (cache.toDouble() / 255.0 * green.toDouble()).toInt()
            cBlue =
                (cache.toDouble() / 255.0 * blue.toDouble()).toInt()
            cRed = addcolor(
                red,
                cRed + (lastRMS / 255.0 * red.toDouble()).toInt()
            )
            cGreen = addcolor(
                green,
                cGreen + (lastRMS / 255.0 * green.toDouble()).toInt()
            )
            cBlue = addcolor(
                blue,
                cBlue + (lastRMS / 255.0 * blue.toDouble()).toInt()
            )
        }
        if (newRms > -3000 && newRms <= -2000) {
            val cache2 =
                (0.0667 * (Math.abs(newRms) - 1500).toDouble()).toInt() + 100
            cRed =
                (cache2.toDouble() / 255.0 * red.toDouble()).toInt()
            cGreen =
                (cache2.toDouble() / 255.0 * green.toDouble()).toInt()
            cBlue =
                (cache2.toDouble() / 255.0 * blue.toDouble()).toInt()
            cRed = addcolor(
                red,
                cRed + (1.25 * lastRMS / 255.0 * red.toDouble()).toInt()
            )
            cGreen = addcolor(
                green,
                cGreen + (1.25 * lastRMS / 255.0 * green.toDouble()).toInt()
            )
            cBlue = addcolor(
                blue,
                cBlue + (1.25 * lastRMS / 255.0 * blue.toDouble()).toInt()
            )
        }
        if (newRms > -2000 && newRms <= -1500) {
            val cache3 =
                (0.0667 * (Math.abs(newRms) - 1500).toDouble()).toInt() + 100
            cRed =
                (cache3.toDouble() / 255.0 * red.toDouble()).toInt()
            cGreen =
                (cache3.toDouble() / 255.0 * green.toDouble()).toInt()
            cBlue =
                (cache3.toDouble() / 255.0 * blue.toDouble()).toInt()
            cRed = addcolor(
                red,
                cRed + (lastRMS / 255.0 * red.toDouble()).toInt()
            )
            cGreen = addcolor(
                green,
                cGreen + (lastRMS / 255.0 * green.toDouble()).toInt()
            )
            cBlue = addcolor(
                blue,
                cBlue + (lastRMS / 255.0 * blue.toDouble()).toInt()
            )
        }
        if (newRms > -1500 && newRms <= -880) {
            val cache4 =
                (0.07 * (Math.abs(newRms) - 880).toDouble()).toInt() + 50
            cRed =
                (cache4.toDouble() / 255.0 * red.toDouble()).toInt()
            cGreen =
                (cache4.toDouble() / 255.0 * green.toDouble()).toInt()
            cBlue =
                (cache4.toDouble() / 255.0 * blue.toDouble()).toInt()
            cRed = addcolor(
                red,
                cRed + (1.5 * lastRMS / 255.0 * red.toDouble()).toInt()
            )
            cGreen = addcolor(
                green,
                cGreen + (1.5 * lastRMS / 255.0 * green.toDouble()).toInt()
            )
            cBlue = addcolor(
                blue,
                cBlue + (1.5 * lastRMS / 255.0 * blue.toDouble()).toInt()
            )
        }
        if (newRms > -880 && newRms <= -800) {
            val cache5 =
                (0.9325 * (Math.abs(newRms) - 800).toDouble()).toInt() + 120
            cRed =
                (cache5.toDouble() / 255.0 * red.toDouble()).toInt()
            cGreen =
                (cache5.toDouble() / 255.0 * green.toDouble()).toInt()
            cBlue =
                (cache5.toDouble() / 255.0 * blue.toDouble()).toInt()
            cRed = addcolor(
                red + 2,
                cRed + (1.5 * lastRMS / 255.0 * red.toDouble()).toInt()
            )
            cGreen = addcolor(
                green + 2,
                cGreen + (1.5 * lastRMS / 255.0 * green.toDouble()).toInt()
            )
            cBlue = addcolor(
                blue + 2,
                cBlue + (1.5 * lastRMS / 255.0 * blue.toDouble()).toInt()
            )
        }
        if (newRms > -800 && newRms <= -700) {
            val cache6 =
                (1.5 * (Math.abs(newRms) - 700).toDouble()).toInt() + 100
            cRed =
                (cache6.toDouble() / 255.0 * red.toDouble()).toInt()
            cGreen =
                (cache6.toDouble() / 255.0 * green.toDouble()).toInt()
            cBlue =
                (cache6.toDouble() / 255.0 * blue.toDouble()).toInt()
            cRed = addcolor(
                red + 5,
                cRed + (1.5 * lastRMS / 255.0 * red.toDouble()).toInt()
            )
            cGreen = addcolor(
                green + 5,
                cGreen + (1.5 * lastRMS / 255.0 * green.toDouble()).toInt()
            )
            cBlue = addcolor(
                blue + 5,
                cBlue + (1.5 * lastRMS / 255.0 * blue.toDouble()).toInt()
            )
        }
        if (newRms > -700) {
            cRed = (red.toDouble() * 1.53).toInt()
            cGreen = (green.toDouble() * 1.53).toInt()
            cBlue = (blue.toDouble() * 1.53).toInt()
            cRed = addcolor(
                red + 10,
                cRed + (lastRMS / 255.0 * red.toDouble()).toInt()
            )
            cGreen = addcolor(
                green + 10,
                cGreen + (lastRMS / 255.0 * green.toDouble()).toInt()
            )
            cBlue = addcolor(
                blue + 10,
                cBlue + (lastRMS / 255.0 * blue.toDouble()).toInt()
            )
        }

        playStatusListener?.newColor(
            Color.argb(
                255,
                cRed,
                cGreen,
                cBlue
            )
        )
        calculateColorChange()
    }

    private fun calculateColorChange() {
        if (this.mColorChangeCount == 0) {
            red = MAXCOLOR
            blue = 0
            green += changenum
            if (green >= 200) {
                green = MAXCOLOR
                this.mColorChangeCount = 1
            }
        } else if (this.mColorChangeCount == 1) {
            green = MAXCOLOR
            red -= changenum
            if (red <= 0) {
                red = 0
                this.mColorChangeCount = 2
            }
        } else if (this.mColorChangeCount == 2) {
            green = MAXCOLOR
            blue += changenum
            if (blue >= 200) {
                blue = MAXCOLOR
                this.mColorChangeCount = 3
            }
        } else if (this.mColorChangeCount == 3) {
            blue = MAXCOLOR
            green -= changenum
            if (green <= 0) {
                green = 0
                this.mColorChangeCount = 4
            }
        } else if (this.mColorChangeCount == 4) {
            blue = MAXCOLOR
            red += changenum
            if (red >= 200) {
                red = MAXCOLOR
                this.mColorChangeCount = 5
            }
        } else if (this.mColorChangeCount == 5) {
            red = MAXCOLOR
            blue -= changenum
            if (blue <= 0) {
                blue = 0
                this.mColorChangeCount = 0
            }
        }
    }

    private fun addcolor(src: Int, add2: Int): Int {
        var color = src + add2
        if (color > 255) {
            color = 255
        }
        return if (color < 0) {
            0
        } else color
    }
}
