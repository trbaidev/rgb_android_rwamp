package com.aquatic.av.player;


import java.util.ArrayList;

public interface PlayStatusListener {
    void onPlayStateChanged(boolean isPlaying);

    void newSongList(ArrayList<Song> songs);

    void newColor(int color);

    void onShuffleChanged();
}
