package com.aquatic.av.player

/**
 * Project           : rgb-android
 * File Name         : Song
 * Description       :
 * Revision History  : version 1
 * Date              : 2020-01-29
 * Original author   : Kannappan
 * Description       : Initial version
 */
data class Song(
    val id: Int,
    val title: String,
    val artist: String,
    val album: String,
    val path: String,
    val duration: Int
) {
}