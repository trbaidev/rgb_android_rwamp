package com.aquatic.av.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.provider.Settings
import android.util.Base64
import com.aquatic.av.BuildConfig
import java.io.ByteArrayOutputStream
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

object Utils {

    fun getSha256HMACFromID(id: String): String {
        try {
            val sha256Mac: Mac = Mac.getInstance("HmacSHA256")
            val secretKey = SecretKeySpec(BuildConfig.APP_SECRET.toByteArray(), "HmacSHA256")
            sha256Mac.init(secretKey)
            val hash: ByteArray = sha256Mac.doFinal(id.toByteArray())
            val result = StringBuffer()
            for (b in hash) {
                result.append(
                        String.format(
                                "%02x",
                                b
                        )
                )
                // thanks sachins! https://gist.github.com/thewheat/7342c76ade46e7322c3e#gistcomment-1863031
            }
            return result.toString() // aa747c502a898200f9e4fa21bac68136f886a0e27aec70ba06daf2e2a5cb5597
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    @SuppressLint("HardwareIds")
    fun getDeviceId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)

    }

    fun encodeBitmapToBase64(bm: Bitmap): String? {
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    @SuppressLint("MissingPermission", "HardwareIds")
    fun getDeviceUniqueID(): String {
        val uniquePseudoID = "35" + Build.BOARD.length % 10 + Build.BRAND.length % 10 + Build.DEVICE.length % 10 + Build.DISPLAY.length % 10 + Build.HOST.length % 10 + Build.ID.length % 10 + Build.MANUFACTURER.length % 10 + Build.MODEL.length % 10 + Build.PRODUCT.length % 10 + Build.TAGS.length % 10 + Build.TYPE.length % 10 + Build.USER.length % 10
        return UUID(uniquePseudoID.hashCode().toLong(), Build.getRadioVersion().hashCode().toLong()).toString()
    }

}