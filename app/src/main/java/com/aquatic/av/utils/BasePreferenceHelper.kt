package com.aquatic.av.utils

import android.content.Context
import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginResponse
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressResponse
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.DeviceRegInfo
import com.invision.simpill1.utils.PreferenceHelper

object BasePreferenceHelper : PreferenceHelper() {


    fun Context.setLoginStatus(isLogin: Boolean) {
        putBooleanPreference(this, "isLogin", isLogin)
    }

    fun Context.isLogin(): Boolean {
        return getBooleanPreference(this, "isLogin")
    }

    fun Context.setUserPinStatus(isLogin: Boolean) {
        putBooleanPreference(this, "userPinOnOff", isLogin)
    }

    fun Context.getUserPinStatus(): Boolean {
        return getBooleanPreference(this, "userPinOnOff")
    }

    fun Context.setMedicationAlertStatus(isLogin: Boolean) {
        putBooleanPreference(this, "medicationAlert", isLogin)
    }

    fun Context.getMedicationAlertStatus(): Boolean {
        return getBooleanPreference(this, "medicationAlert")
    }

    fun Context.putUserToken(token: String) {
        putStringPreference(this, "userPin", token)
    }

    fun Context.getUserToken(): String {
        return getStringPreference(this, "userPin")
    }

    fun Context.putUserDetailModel(isLogin: LoginResponse.UserInfo?) {
        putParcelableModel(this, "userDetailModel", isLogin)
    }
    fun Context.getUserDetailModel(): LoginResponse.UserInfo? {
        return getParcelableModel(this, "userDetailModel")
    }

    fun Context.getUserAddressModel():UserAddressResponse ?{
        return getParcelableModel(this, "getUserAddressModel")
    }

    fun Context.putUserAddressModel(isDeviceReg: UserAddressResponse) {
        putParcelableModel(this, "getUserAddressModel", isDeviceReg)
    }

    fun Context.putUserDeviceDetailModel(isDeviceReg: DeviceRegInfo) {
        putUserDeviceDetailModel(this, "putUserDeviceDetailModel", isDeviceReg)
    }



    fun Context.canDownloadDrugList(canDownload: Boolean) {
        putBooleanPreference(this, "drugList", canDownload)
    }

    fun Context.shouldDownloadDrugList(): Boolean {
        return getBooleanPreference(this, "drugList")
    }

    fun Context.putAccessToken(token: String) {
        putStringPreference(this, "token", token)
    }

    fun Context.getAccessToken(): String {
        return getStringPreference(this, "token")
    }

    fun Context.putUserEmail(email: String) {
        putStringPreference(this, "email", email)
    }

    fun Context.getUserEmaiil(): String {
        return getStringPreference(this, "email")
    }

    fun Context.setPaymentDueStatus(isLogin: Boolean) {
        putBooleanPreference(this, "paymentDue", isLogin)
    }

    fun Context.isPaymentDue(): Boolean {
        return getBooleanPreference(this, "paymentDue")
    }

    fun Context.clearAllPreferences() {
        return removeAllPreference(this)
    }

    fun Context.getConnectedDeviceName(): String {
        return getStringPreference(this, "connected_device_name")
    }
    fun Context.getConnectedDeviceId(): Int {
        return getIntPreference(this, "connected_device_id")
    }
}