package com.aquatic.av.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.aquatic.av.bluetoothScanner.ble.RGBManager
import com.aquatic.av.bluetoothScanner.ble.utils.BLEUtils
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.setting.myEnvironments.addNewDeviceProductType.dataModels.AddNewDeviceProductTypeResponse
import com.google.common.hash.Hashing
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

inline fun <reified T> String.fromJson(): T = Gson().fromJson(this, T::class.java)

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun String.capitalizeWords(): String = split(" ", "-").joinToString(" ") { it.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() } }


fun Fragment.showToast(message: String?, length: Int = Toast.LENGTH_SHORT) =
        message?.let {
            Toast.makeText(requireContext(), message, length).show()
        }

fun Fragment.isDeviceConnected(): Boolean {
    return BLEUtils.isBleEnabled(requireContext()) && RGBManager.getInstance().getBLEManager(requireContext()).isConnected
}

fun Activity.showToast(message: String?, length: Int = Toast.LENGTH_SHORT) =
        message?.let {
            Toast.makeText(this, message, length).show()
        }

fun Date.formatTo(dateFormat: String): String {
    val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
    return formatter.format(this)
}

fun Date.isBetweenDays(days: Int): Boolean {
    return Date().compareTo(this) * this.compareTo(Calendar.getInstance().getDaysSubtracted(days)) > 0;
}

fun Calendar.getDaysSubtracted(days: Int): Date {
    set(Calendar.DAY_OF_MONTH, get(Calendar.DAY_OF_MONTH) - days)
    return time
}


inline fun <reified T : Activity> Activity.launchActivityAndFinish(option: ActivityOptionsCompat? = null) {

    val intent = Intent(this, T::class.java)
    if (option == null) {
        startActivity(intent)
    } else {
        ActivityCompat.startActivity(this, intent, option.toBundle())
    }

    finish()
}

inline fun <reified T : Activity> Activity.launchActivity(option: ActivityOptionsCompat? = null) {

    val intent = Intent(this, T::class.java)
    if (option == null) {
        startActivity(intent)
    } else {
        ActivityCompat.startActivity(this, intent, option.toBundle())
    }
}

inline fun <reified T : Activity> Activity.launchActivityWithResult(
        requestCode: Int,
        option: ActivityOptionsCompat? = null
) {

    val intent = Intent(this, T::class.java)
    if (option == null) {
        startActivity(intent)
    } else {
        ActivityCompat.startActivityForResult(this, intent, requestCode, option.toBundle())
    }
}

inline fun <reified T : Activity> Fragment.launchActivityWithResult(
        requestCode: Int,
        option: Bundle? = null
) {

    val intent = Intent(this.context, T::class.java)
    if (option == null) {
        startActivityForResult(intent, requestCode)
    } else {
        intent.putExtras(option)
        startActivityForResult(intent, requestCode)
    }
}

inline fun <reified T : Activity> Activity.launchActivityWithResult(
        requestCode: Int,
        option: Bundle? = null
) {

    val intent = Intent(this, T::class.java)
    if (option == null) {
        startActivityForResult(intent, requestCode)
    } else {
        intent.putExtras(option)
        startActivityForResult(intent, requestCode)
    }
}

inline fun <reified T : Activity> Activity.launchActivity(id: String, name: String? = "") {
    val intent = Intent(this, T::class.java)
    intent.putExtra("id", id)
    intent.putExtra("name", name)
    startActivity(intent)
}

inline fun <reified T : Activity> Activity.launchActivity(key: String, value: Boolean) {
    val intent = Intent(this, T::class.java)
    intent.putExtra(key, value)
    startActivity(intent)
}

inline fun <reified T : Activity> Activity.launchActivity(bundle: Bundle) {
    val intent = Intent(this, T::class.java)
    intent.putExtras(bundle)
    startActivity(intent)
}


fun getRGBAFromHexInt(hex: Int): Array<Int> {
    /*val a = hex and -0x1000000 shr 24
    val r = hex and 0xFF0000 shr 16
    val g = hex and 0xFF00 shr 8
    val b = hex and 0xFF*/
    val r: Int = Color.red(hex)
    val g: Int = Color.green(hex)
    val b: Int = Color.blue(hex)
    val a: Int = Color.alpha(hex)
    return arrayOf(r, g, b, a)
}

fun toHexARGB(intColor: Int): String {
    return "#${"%08x".format(intColor)}"
}

object EditTextExtension {
    fun EditText.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(this.windowToken, 0)
    }

    fun EditText.getEncryptPassword(): String {
        return Hashing.sha512().hashString(
                this.text.toString().trim { it <= ' ' },
                StandardCharsets.UTF_8
        ).toString()
    }

    inline fun EditText.doAfterTextChangedDelayed(
            delay: Long = 400,
            crossinline onTextChangedDelayed: (text: String) -> Unit
    ) = onTextChangeListener(delay, onTextChangedDelayed)

    fun <T, R : ViewDataBinding> Fragment.getGenericAdapter(collection: ArrayList<T> = arrayListOf(), viewBinder: BaseRecyclerViewBinder<T, R>): BaseRecyclerViewAdapter<T, R> {
        return BaseRecyclerViewAdapter(
                collection,
                viewBinder,
                this.requireContext()
        )
    }

    /**
     * Add an action which will be invoked after the text changed.
     *
     * @return the [EditText.onTextChangeListener] added to the [EditText]
     */
    inline fun EditText.onTextChangeListener(
            delay: Long,
            crossinline onTextChangedDelayed: (text: String) -> Unit
    ): TextWatcher {
        val listener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                handlerPostDelayed(delay) { onTextChangedDelayed.invoke(s?.toString() ?: "") }
            }
        }
        this.addTextChangedListener(listener)
        return listener
    }

    var handlerDelayTimer: Timer = Timer()

    inline fun handlerPostDelayed(delay: Long, crossinline onSuccess: () -> Unit) {
        handlerDelayTimer.cancel()
        handlerDelayTimer = Timer()
        handlerDelayTimer.schedule(object : TimerTask() {
            override fun run() {
                Handler(Looper.getMainLooper()).post {
                    onSuccess.invoke()
                }
            }
        }, delay)
    }

    inline fun <reified T> Gson.fromJsonString(json: String):T{
        return fromJson(json, object : TypeToken<T>() {}.type)

    }

    inline fun <T> Gson.mapToObject(map: Map<String, Any?>?, type: Class<T>): T? {
        if (map == null) return null

        val json = toJson(map)
        return fromJson(json, type)
    }

    inline fun <reified T> Gson.getGenericList(json: String,type: Class<T>): List<T> {
        val list: ArrayList<Map<String, Any?>>? = fromJsonString(json)
        val result = list?.mapNotNull {
            mapToObject(it, type)
        }
        return  result ?: listOf()
    }
}
