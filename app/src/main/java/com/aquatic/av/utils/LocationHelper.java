package com.aquatic.av.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

public class LocationHelper {
    private final Activity activity;
    private final int numOfUpdates;
    private final int priority;
    private final long interval;
    private final long fastestInterval;
    private LocationListener listener;
    private LocationCallback locationCallback;
    private final FusedLocationProviderClient fusedLocationClient;
    @Nullable
    private Location currentLastLocation;

    @Nullable
    public Location getCurrentLastLocation() {
        return currentLastLocation;
    }

    public LocationHelper(Activity activity, int numOfUpdates, long interval, long fastestInterval,
                          int priority, LocationListener listener) {
        this.activity = activity;
        this.numOfUpdates = numOfUpdates;
        this.interval = interval;
        this.fastestInterval = fastestInterval;
        this.priority = priority;
        this.listener = listener;

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
    }

    public void getLocationAsync() {
        Dexter.withContext(activity)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(permissionsListener)
                .withErrorListener(dexterError -> Log.e("Error", dexterError.toString()))
                .check();
    }

    public void stop() {
        if(locationCallback != null)
            fusedLocationClient.removeLocationUpdates(locationCallback);
        listener = null;
    }


    public interface LocationListener {
        void onLocationResult(@NonNull Location location);
        void onLocationAvailability(LocationAvailability availability);
    }
    private final MultiplePermissionsListener permissionsListener = new MultiplePermissionsListener() {
        @SuppressLint("MissingPermission")
        @Override
        public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
            if (multiplePermissionsReport.areAllPermissionsGranted()) {

                // Location permission check
                if (ActivityCompat.checkSelfPermission(activity,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(activity,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                fusedLocationClient.getLastLocation()
                        .addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                            @SuppressLint("MissingPermission")
                            @Override
                            public void onSuccess(Location location) {
                                // Checking Location Provider is enabled
                                LocationManager locationManager =
                                        (LocationManager) activity.getSystemService(Activity.LOCATION_SERVICE);
                                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                                }

                                // Checking if location received is not null then will be passed
                                if (location != null) {
                                    currentLastLocation = location;
                                    if(listener != null)
                                        listener.onLocationResult(location);
                                    //return;
                                }

                                // if the location is null starting location request for one time to
                                // fetch one location update and will pass the location when fetched
                                LocationRequest request = LocationRequest.create();
                                request.setPriority(priority)
                                        .setInterval(interval)
                                        .setFastestInterval(fastestInterval);
                                if (numOfUpdates > -1)
                                    request.setNumUpdates(numOfUpdates);
                                locationCallback = new LocationCallback() {
                                    @Override
                                    public void onLocationResult(@NonNull LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        listener.onLocationResult(locationResult.getLastLocation());
                                        currentLastLocation = locationResult.getLastLocation();
                                    }

                                    @Override
                                    public void onLocationAvailability(@NonNull LocationAvailability locationAvailability) {
                                        super.onLocationAvailability(locationAvailability);
                                        listener.onLocationAvailability(locationAvailability);
                                    }
                                };
                                fusedLocationClient.requestLocationUpdates(
                                        request,
                                        locationCallback,
                                        Looper.myLooper()
                                );
                            }
                        });
            }
        }

        @Override
        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
            new AlertDialog.Builder(activity)
                    .setTitle("Location Permission")
                    .setMessage("Please allow location permission to get weather details")
                    .setNegativeButton("Cancel", (dialog, which) -> {
                        dialog.dismiss();
                        permissionToken.cancelPermissionRequest();
                    })
                    .setPositiveButton("Ok", (dialog, which) -> {
                        dialog.dismiss();
                        permissionToken.continuePermissionRequest();
                    })
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            permissionToken.cancelPermissionRequest();
                        }
                    })
                    .show();
        }
    };
}
