package com.invision.simpill1.utils

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

/**
 * Class that can be extended to make available simple preference
 * setter/getters.
 *
 * Should be extended to provide Facades.
 *
 */
open class PreferenceHelper {
    private val prefsName = "preferences"
    fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE)
    }

    protected inline fun <reified T> putParcelableModel(context: Context, key: String?, value: T?) {
        context.getSharedPreferences("preferences", Activity.MODE_PRIVATE).edit().apply {
            putString(key, Gson().toJson(value))
            apply()
        }
    }
    protected inline fun <reified T> putUserDeviceDetailModel(context: Context, key: String?, value: T?) {
        context.getSharedPreferences("preferences", Activity.MODE_PRIVATE).edit().apply {
            putString(key, Gson().toJson(value))
            apply()
        }
    }

    protected inline fun <reified T> getParcelableModel(context: Context, key: String?): T {
        val preferences = context.getSharedPreferences("preferences", Activity.MODE_PRIVATE)
        return Gson().fromJson(preferences.getString(key, "") ?: "", T::class.java)
    }

    protected fun putStringPreference(context: Context, key: String?, value: String?) {
        context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).edit().apply {
            putString(key, value)
            apply()
        }
    }
    protected fun getIntPreference(context: Context, key: String?): Int {
        val preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE)
        return preferences.getInt(key, 0)
    }


    protected fun getStringPreference(context: Context, key: String?): String {
        val preferences = context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE)
        return preferences.getString(key, "") ?: ""
    }

    protected fun putBooleanPreference(context: Context, key: String?, value: Boolean) {
        context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).edit().apply {
            putBoolean(key, value)
            apply()
        }
    }

    protected fun getBooleanPreference(context: Context, key: String?): Boolean {
        return context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).getBoolean(key, false)
    }

    protected fun putIntegerPreference(context: Context,
                                       key: String?, value: Int) {
        context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).edit().apply {
            putInt(key, value)
            apply()
        }

    }

    /**
     * Get a integer under a particular key and filename
     *
     * @param context
     * @param the
     * filename of preferences
     * @param key
     * name of preference
     * @return -1 if key is not found
     */
    protected fun getIntegerPreference(context: Context, key: String?): Int {
        return context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).getInt(key, -1)
    }

    protected fun putLongPreference(context: Context, key: String?, value: Long) {
        context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).edit().apply {
            putLong(key, value)
            apply()
        }

    }

    /**
     * Get a integer under a particular key and filename
     *
     * @param context
     * @param the
     * filename of preferences
     * @param key
     * name of preference
     * @return Integer.MIN if key is not found
     */
    protected fun getLongPreference(context: Context,
                                    key: String?): Long {
        return context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).getLong(key, Int.MIN_VALUE.toLong())
    }

    protected fun putFloatPreference(context: Context, key: String?, value: Float) {
        context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).edit().apply {
            putFloat(key, value)
            apply()
        }
    }

    protected fun getFloatPreference(context: Context, key: String?): Float {
        return context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).getFloat(key, 0f)
    }

    private fun removePreference(context: Context, key: String?) {
        context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).edit().apply {
            remove(key)
            apply()
        }

    }

    protected fun removeAllPreference(context: Context) {
        context.getSharedPreferences(prefsName, Activity.MODE_PRIVATE).all.map { it.key }.forEach {
            removePreference(context, it)
        }
    }
}