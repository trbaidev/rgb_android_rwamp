package com.aquatic.av.utils

import android.view.View
import android.widget.AdapterView

abstract class UserSelectedItemSelectedListener : AdapterView.OnItemSelectedListener {
    abstract fun onUserItemSelected( position: Int)
    private var check = 0
    override fun onItemSelected(
            parent: AdapterView<*>?,
            view: View,
            position: Int,
            id: Long
    ) {
        if (++check > 1) {
            onUserItemSelected(position)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}
}