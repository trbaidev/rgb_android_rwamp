package com.aquatic.av.utils

import com.aquatic.av.screens.rgb.dataModel.FavColorData

const val BRIGHTNESS_DEFAULT_VALUE: Int = 3
const val BRIGHTNESS_MAX_VALUE: Int = 7
const val DEFAULT_CELL_COLOR = "#808080"
const val TYPE_WHEEL: Int = 0
const val TYPE_HUE = 1
const val TYPE_SQUARE = 2
const val APP_ZOOM_LEVEL = 16f


val presetColors = listOf("#EB5757", "#66DE64", "#015DFE", "#EF64B3", "#E8EDEF")
val standardColors = mutableListOf("#EB5757", "#66DE64", "#015DFE", "#EF64B3", "#E8EDEF")
val favColors = mutableListOf(
        FavColorData(-1, "#015DFE", 0),
        FavColorData(-1, "#00FFC1", 1),
        FavColorData(-1, "#F9B900", 2),
        FavColorData(-1, "#FF0092", 3),
        FavColorData(-1, "#49B8EF", 4)
)

fun getColorIntToRGB(color: Int): Array<Int> {
    val rValue = color shr 16 and 0xFF
    val gValue = color shr 8 and 0xFF
    val bValue = color shr 0 and 0xFF
    return arrayOf(rValue, gValue, bValue)
}
