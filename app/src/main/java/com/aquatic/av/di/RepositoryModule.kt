package com.aquatic.av.di

import com.aquatic.av.repository.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindAuthRepository(imp: AuthRepositoryImp): AuthRepository

    @Binds
    abstract fun bindDealerRepository(imp: DealerLocationRepositoryImp): DealerLocatorRepository

    @Binds
    abstract fun bindDashboardRepository(imp: DashboardRepositoryImp): DashboardRepository

    @Binds
    abstract fun bindRGBRepository(imp: RGBRepositoryImp): RGBRepository
}
