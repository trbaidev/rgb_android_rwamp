package com.aquatic.av.di

import com.aquatic.av.BuildConfig
import com.aquatic.av.network.retrofit.GoogleWebService
import com.aquatic.av.network.retrofit.OKHttpClientCreator
import com.aquatic.av.network.retrofit.WebService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun providesWebService(): WebService {
        val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BaseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(OKHttpClientCreator.createCustomInterceptorClient())
                .build()
        return retrofit.create(WebService::class.java)
    }

    @Provides
    @Singleton
    fun providesGoogleApiWebService(): GoogleWebService {
        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.AUTOCOMPLETE_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OKHttpClientCreator.createCustomInterceptorClient())
            .build()
        return retrofit.create(GoogleWebService::class.java)
    }


}