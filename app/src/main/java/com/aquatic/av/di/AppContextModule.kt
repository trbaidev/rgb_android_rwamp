package com.aquatic.av.di


import android.content.Context
import com.aquatic.av.AquaticAVApplication
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import javax.inject.Singleton

@InstallIn(ActivityComponent::class)
@Module
object AppContextModule {

    @Singleton
    @Provides
    fun getApplicationContext(): Context = AquaticAVApplication.getAppContext().applicationContext

}