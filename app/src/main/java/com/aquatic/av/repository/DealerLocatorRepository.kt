package com.aquatic.av.repository

import com.aquatic.av.network.State
import com.aquatic.av.screens.dealerLocator.model.DealerFilterResponse
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionRequest
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionResponse
import com.aquatic.av.screens.dealerLocator.model.GoogleAutocompleteSearch
import kotlinx.coroutines.flow.Flow
import retrofit2.http.Query

interface DealerLocatorRepository {
    fun getDealerSuggestion(token: String, dealerSuggestionRequest: DealerSuggestionRequest): Flow<State<DealerSuggestionResponse>>
    fun getDealerFilters(token: String): Flow<State<DealerFilterResponse>>
    fun getNearbyDealers(input:String,types: String,key:String):Flow<State<GoogleAutocompleteSearch>>

}