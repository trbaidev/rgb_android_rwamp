package com.aquatic.av.repository

import com.aquatic.av.network.NetworkResultParser
import com.aquatic.av.network.State
import com.aquatic.av.screens.accountAuthentication.createAccount.dataModels.EmailCheckModel
import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginModel
import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginResponse
import com.aquatic.av.screens.accountAuthentication.resetPassword.dataModels.ResetPasswordModel
import com.aquatic.av.screens.accountAuthentication.resetPassword.dataModels.ResetPasswordResponse
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressResponse
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedModel
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedResponse
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.*
import com.aquatic.av.screens.setting.contactInformation.dataModels.ContactInfoModel
import com.aquatic.av.screens.setting.contactInformation.dataModels.ContactInfoResponse
import com.aquatic.av.screens.setting.myDevices.dataModels.MyDeviceModel
import com.aquatic.av.screens.setting.myDevices.dataModels.MyDeviceResponse
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentResponse
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentResponse
import com.aquatic.av.screens.setting.personalInformation.dataModels.PersonalInfoModel
import com.aquatic.av.screens.setting.personalInformation.dataModels.PersonalInfoResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import retrofit2.Response
import kotlin.collections.ArrayList


interface AuthRepository {
    fun emailCheck(name: String): Flow<State<EmailCheckModel>>

    fun verifyUserEmail(email: String): Flow<State<EmailCheckModel>>

    fun resetPassword(resetPasswordModel: ResetPasswordModel): Flow<State<ResetPasswordResponse>>

    fun createUser(userAddressModel: UserAddressModel?): Flow<State<UserAddressResponse>>

    fun loginUser(loginModel: LoginModel): Flow<State<LoginResponse>>

    fun getAllEnvironments(userOnboardingEnvironmentModel: Any): Flow<State<Any>>

    fun getAllProductTypes(): Flow<State<ArrayList<Any>>>

    fun getAllProductsByTypeId(userRegSelectedDeviceModel: Any): Flow<State<ArrayList<Any>>>

    fun getAllDealers(): Flow<State<ArrayList<DealerModel>>>

    fun verifySerialNo(serial_no: String): Flow<State<SerialVerificationModel>>

    fun postReceipt(token: String, file: MultipartBody.Part): Flow<State<ReceiptUploadResponse>>

    fun registerDevice(
        token: String,
        addDeviceModel: AddDeviceModel
    ): Flow<State<AddDeviceResponse>>

    fun addUpdateEnvironments(token: String, userCustomEnvironmentModel: Any): Flow<State<Any>>

    fun updateUserPersonalInfo(
        token: String,
        personalInfoModel: PersonalInfoModel
    ): Flow<State<PersonalInfoResponse>>

    fun updateUserContactInfo(
        token: String,
        contactInfoModel: ContactInfoModel
    ): Flow<State<ContactInfoResponse>>

    fun getMyEnvironmentInfo(token: String, user_id: JsonObject): Flow<State<MyEnvironmentResponse>>

    fun getDevicesInfoDash(
        token: String,
        user_email: String,
        env_name: String
    ): Flow<State<MyDeviceResponse>>

    fun updateDeviceName(token: String, my_device: MyDeviceModel): Flow<State<Any>>

    fun updateDeviceConnectedStatus(
        token: String,
        body: UpdateDeviceConnectedModel
    ): Flow<State<UpdateDeviceConnectedResponse>>

}