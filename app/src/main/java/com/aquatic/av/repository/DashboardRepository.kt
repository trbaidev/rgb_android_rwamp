package com.aquatic.av.repository

import com.aquatic.av.network.State
import com.aquatic.av.activities.model.UserEnvironmentModel
import com.aquatic.av.network.NetworkResultParser
import com.aquatic.av.screens.dashboard.dataModel.DashboardItemModel
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface DashboardRepository {
    fun getUserEnvironments(
        token: String,
        userEmail: String
    ): Flow<State<ArrayList<UserEnvironmentModel>>>

    fun getUserDashboard(
        token: String,
        userEmail: String,
        environmentName: String
    ): Flow<State<ArrayList<DashboardItemModel>>>

    fun getMyEnvironmentInfo(token: String, user_id: JsonObject): Flow<State<MyEnvironmentResponse>>

}