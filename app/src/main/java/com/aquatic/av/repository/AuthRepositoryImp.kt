package com.aquatic.av.repository

import com.aquatic.av.network.NetworkResultParser
import com.aquatic.av.network.State
import com.aquatic.av.network.retrofit.WebService
import com.aquatic.av.screens.accountAuthentication.createAccount.dataModels.EmailCheckModel
import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginModel
import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginResponse
import com.aquatic.av.screens.accountAuthentication.resetPassword.dataModels.ResetPasswordModel
import com.aquatic.av.screens.accountAuthentication.resetPassword.dataModels.ResetPasswordResponse
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressResponse
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedModel
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedResponse
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.*
import com.aquatic.av.screens.setting.contactInformation.dataModels.ContactInfoModel
import com.aquatic.av.screens.setting.contactInformation.dataModels.ContactInfoResponse
import com.aquatic.av.screens.setting.myDevices.dataModels.MyDeviceModel
import com.aquatic.av.screens.setting.myDevices.dataModels.MyDeviceResponse
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentResponse
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentResponse
import com.aquatic.av.screens.setting.personalInformation.dataModels.PersonalInfoModel
import com.aquatic.av.screens.setting.personalInformation.dataModels.PersonalInfoResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList

@Singleton
class AuthRepositoryImp @Inject constructor(private val apiService: WebService) : AuthRepository {

    @ExperimentalCoroutinesApi
    override fun emailCheck(name: String): Flow<State<EmailCheckModel>> {
        return object : NetworkResultParser<EmailCheckModel>() {
            override suspend fun fetchFromRemote(): Response<EmailCheckModel> {
                return apiService.emailCheck(name)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun verifyUserEmail(email: String): Flow<State<EmailCheckModel>> {
        return object : NetworkResultParser<EmailCheckModel>() {
            override suspend fun fetchFromRemote(): Response<EmailCheckModel> {
                return apiService.verifyUserEmail(email)
            }
        }.asFlow()
    }


    @ExperimentalCoroutinesApi
    override fun createUser(userAddressModel: UserAddressModel?): Flow<State<UserAddressResponse>> {
        return object : NetworkResultParser<UserAddressResponse>() {
            override suspend fun fetchFromRemote(): Response<UserAddressResponse> {
                return apiService.createUser(userAddressModel)
            }
        }.asFlow()
    }


    @ExperimentalCoroutinesApi
    override fun loginUser(loginModel: LoginModel): Flow<State<LoginResponse>> {
        return object : NetworkResultParser<LoginResponse>() {
            override suspend fun fetchFromRemote(): Response<LoginResponse> {
                return apiService.loginUser(loginModel)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun resetPassword(resetPasswordModel: ResetPasswordModel): Flow<State<ResetPasswordResponse>> {
        return object : NetworkResultParser<ResetPasswordResponse>() {
            override suspend fun fetchFromRemote(): Response<ResetPasswordResponse> {
                return apiService.resetPassword(resetPasswordModel)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun getAllEnvironments(userOnboardingEnvironmentModel: Any): Flow<State<Any>> {
        return object : NetworkResultParser<Any>() {
            override suspend fun fetchFromRemote(): Response<Any> {
                return apiService.getAllEnvironments(userOnboardingEnvironmentModel)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun getAllProductTypes(): Flow<State<ArrayList<Any>>> {
        return object : NetworkResultParser<ArrayList<Any>>() {
            override suspend fun fetchFromRemote(): Response<ArrayList<Any>> {
                return apiService.getAllProductTypes()
            }
        }.asFlow()
    }


    @ExperimentalCoroutinesApi
    override fun getAllProductsByTypeId(userRegSelectedDeviceModel: Any): Flow<State<ArrayList<Any>>> {
        return object : NetworkResultParser<ArrayList<Any>>() {
            override suspend fun fetchFromRemote(): Response<ArrayList<Any>> {
                return apiService.getAllProductsByTypeId(userRegSelectedDeviceModel)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun getAllDealers(): Flow<State<ArrayList<DealerModel>>> {
        return object : NetworkResultParser<ArrayList<DealerModel>>() {
            override suspend fun fetchFromRemote(): Response<ArrayList<DealerModel>> {
                return apiService.getAllDealers()
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun verifySerialNo(serial_no:String): Flow<State<SerialVerificationModel>> {
        return object : NetworkResultParser<SerialVerificationModel>() {
            override suspend fun fetchFromRemote(): Response<SerialVerificationModel> {
                return apiService.verifySerialNo(serial_no)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun postReceipt(token:String,file: MultipartBody.Part): Flow<State<ReceiptUploadResponse>> {
        return object : NetworkResultParser<ReceiptUploadResponse>() {
            override suspend fun fetchFromRemote(): Response<ReceiptUploadResponse> {
                return apiService.postReceipt(token,file)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun registerDevice(token:String,addDeviceModel: AddDeviceModel): Flow<State<AddDeviceResponse>> {
        return object : NetworkResultParser<AddDeviceResponse>() {
            override suspend fun fetchFromRemote(): Response<AddDeviceResponse> {
                return apiService.registerDevice(token ,addDeviceModel)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun addUpdateEnvironments(token: String,userCustomEnvironmentModel: Any): Flow<State<Any>> {
        return object : NetworkResultParser<Any>() {
            override suspend fun fetchFromRemote(): Response<Any> {
                return apiService.addUpdateEnvironments(token,userCustomEnvironmentModel)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun updateUserPersonalInfo(token: String, personalInfoModel: PersonalInfoModel): Flow<State<PersonalInfoResponse>> {
        return object : NetworkResultParser<PersonalInfoResponse>() {
            override suspend fun fetchFromRemote(): Response<PersonalInfoResponse> {
                return apiService.updateUserPersonalInfo(token,personalInfoModel)
            }
        }.asFlow()
    }


    @ExperimentalCoroutinesApi
    override fun updateUserContactInfo(token: String, contactInfoModel: ContactInfoModel): Flow<State<ContactInfoResponse>> {
        return object : NetworkResultParser<ContactInfoResponse>() {
            override suspend fun fetchFromRemote(): Response<ContactInfoResponse> {
                return apiService.updateUserContactInfo(token,contactInfoModel)
            }
        }.asFlow()
    }


    @ExperimentalCoroutinesApi
    override fun getMyEnvironmentInfo(token: String, user_id: JsonObject): Flow<State<MyEnvironmentResponse>> {
        return object : NetworkResultParser<MyEnvironmentResponse>() {
            override suspend fun fetchFromRemote(): Response<MyEnvironmentResponse> {
                return apiService.getMyEnvironmentInfo(token,user_id)
            }
        }.asFlow()
    }


    @ExperimentalCoroutinesApi
    override fun getDevicesInfoDash(token:String,user_email: String, env_name: String): Flow<State<MyDeviceResponse>> {
        return object : NetworkResultParser<MyDeviceResponse>() {
            override suspend fun fetchFromRemote(): Response<MyDeviceResponse> {
                return apiService.getDevicesInfoDash(token ,user_email,env_name)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun updateDeviceName(token:String,my_device: MyDeviceModel): Flow<State<Any>> {
        return object : NetworkResultParser<Any>() {
            override suspend fun fetchFromRemote(): Response<Any> {
                return apiService.updateDeviceName(token ,my_device)
            }
        }.asFlow()
    }

    override fun updateDeviceConnectedStatus(token: String, body : UpdateDeviceConnectedModel): Flow<State<UpdateDeviceConnectedResponse>> {
        return object : NetworkResultParser<UpdateDeviceConnectedResponse>() {
            override suspend fun fetchFromRemote(): Response<UpdateDeviceConnectedResponse> {
                return apiService.updateDeviceConnectedStatus(token,body)
            }
        }.asFlow()
    }
}