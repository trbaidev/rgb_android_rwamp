package com.aquatic.av.repository

import com.aquatic.av.network.NetworkResultParser
import com.aquatic.av.network.State
import com.aquatic.av.screens.rgb.dataModel.RGBColorModel
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentModel
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface RGBRepository {
    fun getAllUserColors(token: String, param: HashMap<String, Int>): Flow<State<RGBColorModel>>
    fun addUpdateColor(token: String, param: HashMap<String, Any>): Flow<State<Any>>
    fun deleteColors(token: String, param: HashMap<String, Any>): Flow<State<Any>>
    fun removeDeviceFavoritesColor(token: String, param: HashMap<String, List<Int>>): Flow<State<RGBColorModel>>
    fun updateEnvironment(token: String, body : UpdateEnvironmentModel): Flow<State<UpdateEnvironmentResponse>>
}