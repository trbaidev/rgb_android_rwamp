package com.aquatic.av.repository

import com.aquatic.av.network.NetworkResultParser
import com.aquatic.av.network.State
import com.aquatic.av.network.retrofit.WebService
import com.aquatic.av.activities.model.UserEnvironmentModel
import com.aquatic.av.screens.dashboard.dataModel.DashboardItemModel
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DashboardRepositoryImp @Inject constructor(private val apiService: WebService) : DashboardRepository {

    override fun getUserEnvironments(token:String,userEmail: String): Flow<State<ArrayList<UserEnvironmentModel>>> {
        return object : NetworkResultParser<ArrayList<UserEnvironmentModel>>() {
            override suspend fun fetchFromRemote(): Response<ArrayList<UserEnvironmentModel>> {
                return apiService.getUserEnvironments(token,userEmail)
            }
        }.asFlow()
    }

    override fun getUserDashboard(token:String,userEmail: String, environmentName: String): Flow<State<ArrayList<DashboardItemModel>>> {
        return object : NetworkResultParser<ArrayList<DashboardItemModel>>() {
            override suspend fun fetchFromRemote(): Response<ArrayList<DashboardItemModel>> {
                return apiService.getUserDashboard(token,userEmail, environmentName)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun getMyEnvironmentInfo(token: String, user_id: JsonObject): Flow<State<MyEnvironmentResponse>> {
        return object : NetworkResultParser<MyEnvironmentResponse>() {
            override suspend fun fetchFromRemote(): Response<MyEnvironmentResponse> {
                return apiService.getMyEnvironmentInfo(token,user_id)
            }
        }.asFlow()
    }

}