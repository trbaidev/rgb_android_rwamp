package com.aquatic.av.repository

import com.aquatic.av.network.NetworkResultParser
import com.aquatic.av.network.State
import com.aquatic.av.network.retrofit.WebService
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedModel
import com.aquatic.av.screens.rgb.dataModel.RGBColorModel
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentModel
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RGBRepositoryImp @Inject constructor(private val apiService: WebService):RGBRepository {
    override fun getAllUserColors(token: String, param: HashMap<String, Int>): Flow<State<RGBColorModel>> {
        return object : NetworkResultParser<RGBColorModel>() {
            override suspend fun fetchFromRemote(): Response<RGBColorModel> {
                return apiService.getAllUserColors(token,param)
            }
        }.asFlow()
    }

    override fun addUpdateColor(token: String, param: HashMap<String, Any>): Flow<State<Any>> {
        return object : NetworkResultParser<Any>() {
            override suspend fun fetchFromRemote(): Response<Any> {
                return apiService.addUpdateDeviceColor(token, param)
            }
        }.asFlow()
    }

    override fun deleteColors(token: String, param: HashMap<String, Any>): Flow<State<Any>> {
        return object : NetworkResultParser<Any>() {
            override suspend fun fetchFromRemote(): Response<Any> {
                return apiService.addUpdateDeviceColor(token, param)
            }
        }.asFlow()
    }

    override fun removeDeviceFavoritesColor(token: String, param: HashMap<String, List<Int>>): Flow<State<RGBColorModel>> {
        return object : NetworkResultParser<RGBColorModel>() {
            override suspend fun fetchFromRemote(): Response<RGBColorModel> {
                return apiService.removeDeviceFavoritesColor(token,param)
            }
        }.asFlow()
    }

    override fun updateEnvironment(token: String, body : UpdateEnvironmentModel): Flow<State<UpdateEnvironmentResponse>> {
        return object : NetworkResultParser<UpdateEnvironmentResponse>() {
            override suspend fun fetchFromRemote(): Response<UpdateEnvironmentResponse> {
                return apiService.updateEnvironment(token,body)
            }
        }.asFlow()
    }
}