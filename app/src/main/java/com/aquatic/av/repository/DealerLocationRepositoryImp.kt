package com.aquatic.av.repository

import com.aquatic.av.network.NetworkResultParser
import com.aquatic.av.network.State
import com.aquatic.av.network.retrofit.GoogleWebService
import com.aquatic.av.network.retrofit.WebService
import com.aquatic.av.screens.dealerLocator.model.DealerFilterResponse
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionRequest
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionResponse
import com.aquatic.av.screens.dealerLocator.model.GoogleAutocompleteSearch
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DealerLocationRepositoryImp @Inject constructor(private val apiService: WebService,private val googleApiService: GoogleWebService) : DealerLocatorRepository {
    @ExperimentalCoroutinesApi
    override fun getDealerSuggestion(token: String, dealerSuggestionRequest: DealerSuggestionRequest): Flow<State<DealerSuggestionResponse>> {
        return object : NetworkResultParser<DealerSuggestionResponse>() {
            override suspend fun fetchFromRemote(): Response<DealerSuggestionResponse> {
                return apiService.getDealerSuggestions(token, dealerSuggestionRequest)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun getDealerFilters(token: String): Flow<State<DealerFilterResponse>> {
        return object : NetworkResultParser<DealerFilterResponse>() {
            override suspend fun fetchFromRemote(): Response<DealerFilterResponse> {
                return apiService.getDealerFilters(token)
            }
        }.asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun getNearbyDealers(input:String,types: String,key:String): Flow<State<GoogleAutocompleteSearch>> {
        return object : NetworkResultParser<GoogleAutocompleteSearch>() {
            override suspend fun fetchFromRemote(): Response<GoogleAutocompleteSearch> {
                return googleApiService.getNearbyDealers(input,types,key)
            }
        }.asFlow()
    }



}