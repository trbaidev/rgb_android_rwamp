package com.aquatic.av.screens.setting.myDevices.dataModels

data class UpdateEnvironmentResponse(
    var responseCode: Int? = -1,
    var responseMessage: String? = "",
    var responseData: Int? = -1
)