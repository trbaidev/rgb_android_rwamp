package com.aquatic.av.screens.bluetooth

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.RGBManager
import com.aquatic.av.bluetoothScanner.ble.adapter.OnItemClickListener
import com.aquatic.av.bluetoothScanner.ble.adapter.RGBBluetoothDevice
import com.aquatic.av.bluetoothScanner.ble.adapter.ScannerDeviceAdapter
import com.aquatic.av.bluetoothScanner.ble.profile.BTConnectionListener
import com.aquatic.av.bluetoothScanner.ble.utils.BLEUtils
import com.aquatic.av.bluetoothScanner.ble.utils.BTCon
import com.aquatic.av.databinding.ActivityScannerBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedModel
import com.aquatic.av.screens.bluetooth.viewModel.BleConnectionViewModel
import com.aquatic.av.screens.bluetooth.viewModel.ScannerLiveData
import com.aquatic.av.screens.bluetooth.viewModel.ScannerLiveData.SCANSTATE
import com.aquatic.av.screens.bluetooth.viewModel.ScannerViewModel
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.invisible
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ScannerFragment : BaseFragment<ActivityScannerBinding>(R.layout.activity_scanner),
    OnItemClickListener {
    private val mScannerViewModel by viewModels<ScannerViewModel>()
    private val preferences by lazy {
        requireContext().getSharedPreferences(mainActivity.packageName, Context.MODE_PRIVATE)
    }
    private val viewModel by viewModels<BleConnectionViewModel>()
    var adapter: ScannerDeviceAdapter? = null

    private var deviceName: String = ""
    private var deviceID: Int = -1

    companion object {
        const val REQUEST_CODE_BLE_CONNECTION = 1003
        private const val REQUEST_ACCESS_COARSE_LOCATION = 1022
        private const val REQUEST_WRITE_EXTERNAL_STORAGE = 1001
        fun newInstance(deviceName: String, deviceID: Int): ScannerFragment {

            val fragment = ScannerFragment()
            fragment.deviceID = deviceID
            fragment.deviceName = deviceName
            return fragment
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        RGBManager.getInstance().registerConnectionListener(object : BTConnectionListener {
            override fun onDeviceConnected(device: BluetoothDevice) {
                adapter?.apply {
                    val currentDevice = RGBBluetoothDevice(device.name, device, true)
                    if (mDevices.contains(currentDevice)) {
                        mDevices.remove(currentDevice)
                        mDevices.add(currentDevice)
                    }
                    notifyDataSetChanged()
                }

            }

            override fun onDeviceDisconnected(device: BluetoothDevice) {
                adapter?.apply {
                    val currentDevice = RGBBluetoothDevice(device.name, device, false)
                    if (mDevices.contains(currentDevice)) {
                        mDevices.remove(currentDevice)
                        mDevices.add(currentDevice)
                    }
                    notifyDataSetChanged()
                }

            }

            override fun onDataSent() {}
            override fun onSuccess(data: String) {}
            override fun onFailed(data: String) {}
            override fun onDataReceived(
                characteristic: BluetoothGattCharacteristic,
                bytes: ByteArray
            ) {
            }
        })

        // Create view model containing utility methods for scanning
        mScannerViewModel.scannerState.observe(
            viewLifecycleOwner,
            { scannerLiveData: ScannerLiveData ->
                when (scannerLiveData.scanState) {
                    SCANSTATE.NONE -> startScan(scannerLiveData)
                    SCANSTATE.STARTED -> onScanStarted()
                    SCANSTATE.STOPPED -> onScanStopped(scannerLiveData)
                    null -> return@observe
                }
                if (scannerLiveData.scanState != SCANSTATE.NONE && !scannerLiveData.isBluetoothEnabled) {
                    onBluetoothDisabled()
                }
            })

        // Configure the recycler view
        binding.recyclerViewBleDevices.layoutManager = LinearLayoutManager(requireContext())
        val dividerItemDecoration =
            DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        binding.recyclerViewBleDevices.addItemDecoration(dividerItemDecoration)
        (binding.recyclerViewBleDevices.itemAnimator as SimpleItemAnimator?)!!.supportsChangeAnimations =
            false
        adapter = ScannerDeviceAdapter(
            requireContext(),
            viewLifecycleOwner,
            mScannerViewModel.scannerState
        )
        adapter!!.setOnItemClickListener(this)
        binding.recyclerViewBleDevices.adapter = adapter
        binding.noLocationPermission.actionGrantLocationPermission.setOnClickListener { onGrantLocationPermissionClicked() }
        binding.noLocationPermission.actionPermissionSettings.setOnClickListener { onPermissionSettingsClicked() }
        binding.bluetoothOff.actionEnableBluetooth.setOnClickListener { onEnableBluetoothClicked() }
        binding.noDevices.actionEnableLocation.setOnClickListener { onEnableLocationClicked() }
        binding.bleSwipeContainer.setOnRefreshListener {
            stopScan()
            mScannerViewModel.restartScan()
        }
        viewModel.isConnected.observe(viewLifecycleOwner, { connected: Boolean? ->
            if (!connected!!) {
                BTCon.setConnectionState(false)
                val editor = preferences.edit()
                editor.putString("connected_device_name", "")
                editor.putInt("connected_device_id", -1)
                editor.apply()
                Toast.makeText(requireContext(), R.string.state_disconnected, Toast.LENGTH_SHORT)
                    .show()
            } else {
                BTCon.setConnectionState(true)
                val editor = preferences.edit()
                editor.putString("connected_device_name", deviceName)
                editor.putInt("connected_device_id", deviceID)
                editor.apply()
            }
        })
        viewModel.isSupported.observe(viewLifecycleOwner, { supported: Boolean? ->
            if (!supported!!) {
                Toast.makeText(requireContext(), R.string.state_not_supported, Toast.LENGTH_SHORT)
                    .show()
            }
        })
        viewModel.isDeviceReady.observe(
            viewLifecycleOwner,
            { binding.connectionStateLayout.gone() })
        viewModel.connectionState.observe(
            viewLifecycleOwner,
            { message -> binding.connectionState.text = message })
        binding.dmgContainer.setOnClickListener { mainActivity.onBackPressed() }
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
    }

    override fun onStop() {
        super.onStop()
        stopScan()
    }

    override fun onItemClick(device: RGBBluetoothDevice) {
        binding.connectionStateLayout.visible()
        if (device.isConnectionState) {
            viewModel.disconnect()
        } else {
            deviceName=device.name
            viewModel.connect(device)
            mScannerViewModel.updateDeviceConnectedStatus(
                requireContext().getAccessToken(),
                UpdateDeviceConnectedModel(
                    device_id = deviceID,
                    uuid = device.name
                )
            )
        }
    }

    @Suppress("DEPRECATION")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_ACCESS_COARSE_LOCATION -> mScannerViewModel.refresh()
        }
    }

    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_BLE_CONNECTION && resultCode == RESULT_OK) {
            mainActivity.popBackStackTillEntry(1)
        }
    }

    private fun onEnableLocationClicked() {
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivity(intent)
    }

    private fun onEnableBluetoothClicked() {
        val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivity(enableIntent)
    }

    @Suppress("DEPRECATION")
    private fun onGrantLocationPermissionClicked() {
        BLEUtils.markLocationPermissionRequested(requireContext())
        requestPermissions(
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_ACCESS_COARSE_LOCATION
        )
    }

    private fun onPermissionSettingsClicked() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.data = Uri.fromParts("package", mainActivity.packageName, null)
        startActivity(intent)
    }

    /**
     * Start scanning for Bluetooth devices or displays a message based on the scanner state.
     *
     * @param state
     */
    private fun startScan(state: ScannerLiveData) {
        // First, check the Location permission. This is required on Marshmallow onwards in order to scan for Bluetooth LE devices.
        if (BLEUtils.isLocationPermissionsGranted(requireContext())) {
            binding.noLocationPermission.root.gone()

            // Bluetooth must be enabled
            if (state.isBluetoothEnabled) {
                binding.bluetoothOff.root.gone()

                // We are now OK to start scanning
                mScannerViewModel.startScan()
                binding.connectionStateLayout.visible()
                if (state.isEmpty) {
                    binding.noDevices.root.visible()
                    if (!BLEUtils.isLocationRequired(requireContext()) || BLEUtils.isLocationEnabled(
                            requireContext()
                        )
                    ) {
                        binding.noDevices.noLocation.invisible()
                    } else {
                        binding.noDevices.noLocation.visible()
                    }
                } else {
                    binding.noDevices.root.gone()
                }
            } else {
                binding.bluetoothOff.root.visible()
                binding.connectionStateLayout.invisible()
                binding.noDevices.root.gone()
            }
        } else {
            binding.noLocationPermission.root.visible()
            binding.bluetoothOff.root.gone()
            binding.connectionStateLayout.invisible()
            binding.noDevices.root.gone()
            val deniedForever = BLEUtils.isLocationPermissionDeniedForever(requireActivity())
            binding.bluetoothOff.actionEnableBluetooth.visibility =
                if (deniedForever) View.GONE else View.VISIBLE
            binding.noLocationPermission.actionPermissionSettings.visibility =
                if (deniedForever) View.VISIBLE else View.GONE
        }
    }

    /**
     * stop scanning for bluetooth devices.
     */
    private fun stopScan() {
        binding.connectionStateLayout.invisible()
        mScannerViewModel.stopScan()
    }

    private fun onScanStarted() {
        binding.noDevices.root.gone()
        binding.bluetoothOff.root.gone()
        binding.noLocationPermission.root.gone()
        binding.connectionState.text = getString(R.string.scanning)
        binding.connectionStateLayout.visible()
    }

    private fun onScanStopped(scannerLiveData: ScannerLiveData) {
        binding.bleSwipeContainer.isRefreshing = false
        binding.bluetoothOff.root.gone()
        binding.connectionStateLayout.invisible()

        //if there is no device then show info
        if (scannerLiveData.isEmpty) {
            binding.noDevices.root.visible()
            if (!BLEUtils.isLocationRequired(requireContext()) || BLEUtils.isLocationEnabled(
                    requireContext()
                )
            ) {
                binding.noDevices.noLocation.invisible()
            } else {
                binding.noDevices.noLocation.visible()
            }
        }
        if (!BLEUtils.isWriteStoragePermissionsGranted(requireContext()) && BLEUtils.isMarshmallowOrAbove()) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_WRITE_EXTERNAL_STORAGE
            )
        }
    }

    private fun onBluetoothDisabled() {
        binding.bluetoothOff.root.visible()
        mScannerViewModel.restartScan()
    }

}