package com.aquatic.av.screens.setting.myEnvironments.addNewUploadReceipt

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentAddDeviceReceiptUploadBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.binders.CustomSpinnerDealers
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.AddDeviceModel
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.DealerModel
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.SerialVerificationModel
import com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice.dataModels.AddNewSelectDeviceResponse
import com.aquatic.av.screens.setting.myEnvironments.addNewUploadReceipt.ViewModel.*
import com.aquatic.av.screens.setting.myEnvironments.dataModels.EnvironmentReferenceModel
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.getUserDetailModel
import com.aquatic.av.utils.Utils.encodeBitmapToBase64
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.esafirm.imagepicker.features.*
import com.esafirm.imagepicker.features.cameraonly.CameraOnlyConfig
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class AddDeviceUploadReceiptFragment :
    BaseFragment<FragmentAddDeviceReceiptUploadBinding>(R.layout.fragment_add_device_receipt_upload),
    AdapterView.OnItemSelectedListener {

    var launcher: ImagePickerLauncher? = null
    private lateinit var dealersModel: List<DealerModel>
    lateinit var addNewDeviceResponse: AddNewSelectDeviceResponse
    lateinit var environment_ref: EnvironmentReferenceModel
    lateinit var serialVerificationModel: SerialVerificationModel
    private val dealerViewModel by viewModels<UploadReceiptDealerViewModel>()
    private val serialVerificationViewModel by viewModels<UploadReceiptSerialVerificationViewModel>()
    private val receiptUploadViewModel by viewModels<ReceiptUploadViewModel>()
    private val receiptUploadRegDeviceViewModel by viewModels<UploadReceiptRegDeviceViewModel>()
    var receiptUrl = ""
    var purchaseDate = ""
    var is_serial_duplicate = false
    var bitmap: Bitmap? = null

    var dealer_id: Int? = 0
    var dealer_location: String? = ""
    val userModel by lazy {
        requireContext().getUserDetailModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setDealersListeners()
        setSerialVerificationListeners()
        setImageUploadListeners()
        setRegDeviceListeners()
        binding.apply {
            spinnerPurchaseLocation.setOnItemSelectedListener(this@AddDeviceUploadReceiptFragment)
            btnBack.setOnClickListener { mainActivity.popFragment() }
            actextNamaprov.threshold = 0
            actextNamaprov.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                override fun onItemClick(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    showToast(parent?.getItemAtPosition(position).toString())
                    var dealerModel = dealersModel.find {
                        it.dealer_name == parent?.getItemAtPosition(position).toString()
                    }
                    dealer_id = dealerModel?.dealer_id
                    dealer_location = dealerModel?.dealer_name
                }
            })
            edtSerialNo.onFocusChangeListener =
                View.OnFocusChangeListener { v, hasFocus ->
                    if (!hasFocus) {
                        serialVerificationViewModel.getAllProductTypes(
                            edtSerialNo.text.trim().toString()
                        )
                    }
                }
            ivBtnUpload.setOnClickListener {
             //   checkGalleryPermission()
                selectImage(mainActivity)
            }
            btnNext.setOnClickListener {
                if (checkFieldsValidation()) {
                    var deviceViewModel = AddDeviceModel(
                        device_name = environment_ref.deviceName,
                        device_type = environment_ref.device_type,
                        env_icon_name = environment_ref.env_icon,
                        env_name = environment_ref.env_name,
                        user_email = userModel?.user_email,
                        created_by = userModel?.user_email,
                        modified_by = userModel?.user_email,
                        product_id = environment_ref.product_id,
                        product_type_id = environment_ref.product_type_id!!,
                        purchase_location_dealer_id = dealer_id!!,
                        purchase_location = dealer_location,
                        serial_no = edtSerialNo.text.trim().toString(),
                        purchase_date = purchaseDate,
                        receipt = receiptUrl,
                        env_id = environment_ref.env_id!!,
                        user_id = userModel?.user_id!!,
                        dealer_id = dealer_id!!,
                        device_id = 0,
                        software_version = "Android " + Build.VERSION.RELEASE,
                        sku = "0000",
                        last_connected = ""
                    )
                    receiptUploadRegDeviceViewModel.registerDevice(
                    requireContext().getAccessToken(),
                        deviceViewModel
                    )
                }
            }
            edtPurchaseDate.setOnClickListener {
                val mCalendar: Calendar = GregorianCalendar()
                mCalendar.time = Date()

                DatePickerDialog(
                    mainActivity, R.style.my_dialog_theme,
                    { view, year, monthOfYear, dayOfMonth ->
                        val newDate = Calendar.getInstance()
                        newDate[year, monthOfYear] = dayOfMonth

                        val simpleDateFormat = SimpleDateFormat("yyyy/MM/dd")
                        purchaseDate = simpleDateFormat.format(newDate.time)
                        edtPurchaseDate.setText(purchaseDate)
                    },
                    mCalendar[Calendar.YEAR],
                    mCalendar[Calendar.MONTH],
                    mCalendar[Calendar.DAY_OF_MONTH]
                ).show()
            }

            btnViewReceipt.setOnClickListener {
                showReceipt.visibility = View.VISIBLE
            }

            ivClose.setOnClickListener(View.OnClickListener { showReceipt.visibility = View.GONE })


            btnReplaceReceipt.setOnClickListener { selectImage(mainActivity) }
        }

        dealerViewModel.getAllProductTypes()
    }

    private fun setDealersListeners() {
        dealerViewModel.userRegUploadReceiptDealerResponseResults.observe(
            viewLifecycleOwner,
            EventObserver {
                when (it) {
                    is State.Error -> showToast(it.message)
                    is State.Exception -> {
                        it.exception.printStackTrace()
                        showToast(it.exception.message)
                    }
                    is State.Loading -> mainActivity.showLoader(it.isLoading)
                    is State.Success -> {
                        it.body?.apply {
                            it.body?.apply {
                                dealersModel = it.body
                                val mAdapter = CustomSpinnerDealers(dealersModel)
                                binding.spinnerPurchaseLocation.setAdapter(mAdapter)
                                var name = ArrayList<String>()
                                for (item in dealersModel) {
                                    name.add(item.dealer_name)
                                }
                                val adapter: ArrayAdapter<*> = ArrayAdapter<Any?>(
                                    mainActivity,
                                    android.R.layout.simple_list_item_1,
                                    name as List<String>
                                )
                                binding.actextNamaprov.setAdapter(adapter)
                            }
                        }
                    }
                }
            })
    }

    private fun setSerialVerificationListeners() {
        serialVerificationViewModel.userRegUploadReceiptSerialVerificationModelResponseResults.observe(
            viewLifecycleOwner,
            EventObserver {
                when (it) {
                    is State.Error -> showToast(it.message)
                    is State.Exception -> {
                        it.exception.printStackTrace()
                        showToast(it.exception.message)
                    }
                    is State.Loading -> mainActivity.showLoader(it.isLoading)
                    is State.Success -> {
                        it.body?.apply {
                            it.body?.apply {
                                serialVerificationModel = it.body
                                if (serialVerificationModel.serial_exist.equals("true", true)) {
                                    is_serial_duplicate = true
                                    binding.edtSerialNo.error =
                                        "The serial number (" + binding.edtSerialNo.text.trim()
                                            .toString() + ") is already registered."
                                } else {
                                    is_serial_duplicate = false
                                }
                            }
                        }
                    }
                }
            })
    }

    private fun setImageUploadListeners() {
        receiptUploadViewModel.receiptUploadResults.observe(
            viewLifecycleOwner,
            EventObserver {
                when (it) {
                    is State.Error -> showToast(it.message)
                    is State.Exception -> {
                        it.exception.printStackTrace()
                        showToast(it.exception.message)
                    }
                    is State.Loading -> mainActivity.showLoader(it.isLoading)
                    is State.Success -> {
                        it.body?.apply {
                            receiptUrl = it.body.responseData
                            showToast("Receipt uploaded successfully!")
                            if (bitmap != null) {
                                binding.ivReceipt.setImageBitmap(bitmap)
                            }
                            binding.btnReplaceReceipt.setVisibility(View.VISIBLE)
                            binding.btnViewReceipt.setVisibility(View.VISIBLE)
                            binding.ivBtnUpload.setVisibility(View.GONE)
                        }
                    }
                }
            })
    }

    private fun setRegDeviceListeners() {
        receiptUploadRegDeviceViewModel.userRegUploadReceiptRegDeviceResponseResults.observe(
            viewLifecycleOwner,
            EventObserver {
                when (it) {
                    is State.Error -> showToast(it.message)
                    is State.Exception -> {
                        it.exception.printStackTrace()
                        showToast(it.exception.message)
                    }
                    is State.Loading -> mainActivity.showLoader(it.isLoading)
                    is State.Success -> {
                        it.body?.apply {
                            if (it.body.responseMessage.equals("success", true)) {
                                showToast("Device has been added to your account!")
                                mainActivity.popBackStackTillEntry(3)
                            } else {
                                showToast("Failed to add device into your account, please try later.")
                            }
                        }
                    }
                }
            })
    }

    private fun checkCameraPermission() {
        Dexter.withContext(requireContext())
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(response: MultiplePermissionsReport?) {
                    if (response?.areAllPermissionsGranted() == true) {
                        val imagepicker = CameraOnlyConfig()
                        imagepicker.returnMode = ReturnMode.CAMERA_ONLY
                        imagepicker.isSaveImage = true
                        imagepicker.savePath = ImagePickerSavePath(
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                                .toString(),
                            isRelative = false
                        )
                        launcher?.launch(imagepicker)
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                    Toast.makeText(
                        requireContext(),
                        "Please allow permission to move forward",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }).check()
    }


    private fun checkGalleryPermission() {
        Dexter.withContext(requireContext())
            .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    var imagePicker = ImagePickerConfig()
                    imagePicker.mode = ImagePickerMode.SINGLE
                    imagePicker.limit = 1
                    imagePicker.isShowCamera = false
                    launcher?.launch(imagePicker)
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    Toast.makeText(
                        requireContext(),
                        "Image is required",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                }
            }).check()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        launcher = registerImagePicker {
            val firstImage = it.firstOrNull() ?: return@registerImagePicker
            bitmap =
                MediaStore.Images.Media.getBitmap(requireContext().contentResolver, firstImage.uri)
            binding.ivReceipt.setImageBitmap(bitmap)
            var encodeReceipt = bitmap?.let { it1 -> encodeBitmapToBase64(it1) }
            receiptUploadViewModel.postReceipt(
                 requireContext().getAccessToken(),
                File(firstImage.path)
            )
        }
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }

    companion object {
        fun getInstance(
            environment_ref: EnvironmentReferenceModel,
            addNewDeviceResponse: AddNewSelectDeviceResponse
        ): AddDeviceUploadReceiptFragment {
            val UploadReceiptFragment = AddDeviceUploadReceiptFragment()
            UploadReceiptFragment.addNewDeviceResponse = addNewDeviceResponse
            UploadReceiptFragment.environment_ref = environment_ref
            return UploadReceiptFragment
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        dealer_id = dealersModel.get(position).dealer_id
        dealer_location = dealersModel.get(position).dealer_name
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    private fun selectImage(context: Context) {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Choose your profile picture")
        builder.setItems(options) { dialog, item ->
            if (options[item] == "Take Photo") {
                checkCameraPermission()
            } else if (options[item] == "Choose from Gallery") {
                checkGalleryPermission()
            } else if (options[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    private fun checkFieldsValidation(): Boolean {
        val serialNo: String = binding.edtSerialNo.text.trim().toString()
        val edtPurchaseLocation: String = binding.actextNamaprov.text.trim().toString()
        val edtPurchaseDate: String = binding.edtPurchaseDate.text.trim().toString()
        when {
            is_serial_duplicate -> {
                showToast("The serial number is already registered.")
                return false
            }
            TextUtils.isEmpty(serialNo) -> {
                binding.edtSerialNo.error = "Please provide serial number"
                return false
            }
            TextUtils.isEmpty(edtPurchaseLocation) -> {
                binding.actextNamaprov.error = "Please provide the location of purchase"
                return false
            }
            TextUtils.isEmpty(edtPurchaseDate) -> {
                binding.edtPurchaseDate.error = "Please provide purchase date"
                return false
            }
            else -> {
                return true
            }
        }
        return true
    }
}