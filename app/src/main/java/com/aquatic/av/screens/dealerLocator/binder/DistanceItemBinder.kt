package com.aquatic.av.screens.dealerLocator.binder

import android.annotation.SuppressLint
import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.RowItemRadioBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.dealerLocator.model.DealerFilterResponse

class DistanceItemBinder(var lastSelectedItem: Int, private val itemClickListener: (item: DealerFilterResponse.MilesData, position: Int) -> Unit) : BaseRecyclerViewBinder<DealerFilterResponse.MilesData, RowItemRadioBinding>(R.layout.row_item_radio) {
    @SuppressLint("NotifyDataSetChanged")
    override fun bindView(entity: DealerFilterResponse.MilesData, position: Int, viewHolder: BaseViewHolder<RowItemRadioBinding>?, context: Context, collections: MutableList<DealerFilterResponse.MilesData>) {
        viewHolder?.viewBinding?.apply {
            val item = entity.value.toIntOrNull()
            btnItem.text = if (item == 0) "Any Distance" else "${entity.value} Miles"
            btnItem.setOnCheckedChangeListener(null)
            btnItem.isChecked = item == lastSelectedItem
            btnItem.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    lastSelectedItem = item ?: 0
                    itemClickListener.invoke(entity, position)
                }
            }
        }
    }
}