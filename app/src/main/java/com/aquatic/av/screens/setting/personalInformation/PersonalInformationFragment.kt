package com.aquatic.av.screens.setting.personalInformation

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentPersonalInformationBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.setting.personalInformation.dataModels.PersonalInfoModel
import com.aquatic.av.screens.setting.personalInformation.viewModel.PersonalInfoViewModel
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.getUserDetailModel
import com.aquatic.av.utils.BasePreferenceHelper.putUserDetailModel
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.aquatic.av.views.dialogs.DialogFactory.showDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.common.hash.Hashing
import dagger.hilt.android.AndroidEntryPoint
import java.nio.charset.StandardCharsets

@AndroidEntryPoint
class PersonalInformationFragment :
    BaseFragment<FragmentPersonalInformationBinding>(R.layout.fragment_personal_information) {

    val userInfoModel by lazy { requireContext().getUserDetailModel() }

    private val personalInfoViewModel by viewModels<PersonalInfoViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        binding.apply {
            dataItem = requireContext().getUserDetailModel()
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnSave.setOnClickListener {
                if (!visibleStatus) {
                    visibleStatus = !visibleStatus
                    fieldsEnable(true)
                } else {
                    if (checkFieldsValidations()) {
                        var pwd= if(edtNewPwd.text.trim().toString().isNullOrEmpty()) edtUserPwd.text.trim().toString() else edtNewPwd.text.trim().toString()
                        val user_existing_email =
                            userInfoModel?.user_email
                        val personalInformationModel = PersonalInfoModel()
                        personalInformationModel.first_name =
                            edtUserFirstName.text.trim().toString()
                        personalInformationModel.last_name = edtUserLastName.text.trim().toString()
                        personalInformationModel.user_email = edtEmail.text.trim().toString()
                        personalInformationModel.isEmailModified =
                            if (user_existing_email == edtEmail.text.toString()
                                    .trim()
                            ) "false" else "true"
                        personalInformationModel.user_password = Hashing.sha512()
                            .hashString(pwd, StandardCharsets.UTF_8)
                            .toString()
                        personalInformationModel.new_email =
                            if (user_existing_email == edtEmail.text.toString()
                                    .trim()
                            ) "" else edtEmail.text.toString().trim()
                        personalInfoViewModel.updateUserPersonalInfo(requireContext().getAccessToken(),personalInformationModel)
                    }
                }
            }
        }
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        titleBar?.hideButtons()
    }

    private fun setListeners() {
        personalInfoViewModel.personalInfoResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        if (it.body.resp_msg.equals("true", true)) {
                            userInfoModel?.first_name =
                                binding.edtUserFirstName.text.trim().toString()
                            userInfoModel?.last_name =
                                binding.edtUserLastName.text.trim().toString()
                            userInfoModel?.user_email =
                                binding.edtEmail.text.trim().toString()
                            userInfoModel?.user_pwd =  if(binding.edtNewPwd.text.trim().toString().isNullOrEmpty()) binding.edtUserPwd.text.trim().toString() else binding.edtNewPwd.text.trim().toString()
                            requireContext().putUserDetailModel(userInfoModel)
                            binding.dataItem = userInfoModel
                            binding.edtNewPwd.text.clear()
                            binding.edtCnfrmPwd.text.clear()
                          binding.visibleStatus = !binding.visibleStatus
                            fieldsEnable(false)
                            showDialog("","Information updated successfully!",true)
                        } else {
                            showDialog("", "Please verify your credentials.", true)
                        }
                    }
                }
            }
        })
    }


    private fun fieldsEnable(enable: Boolean) {
        binding.edtUserFirstName.isEnabled = enable
        binding.edtUserLastName.isEnabled = enable
        binding.edtEmail.isEnabled = enable
        binding.edtCnfrmPwd.isEnabled = enable
        binding.edtNewPwd.isEnabled = enable
        binding.edtUserPwd.isEnabled = enable
    }


    private fun checkFieldsValidations(): Boolean {
        val firstName = binding.edtUserFirstName.text.trim().toString()
        val lastName = binding.edtUserLastName.text.trim().toString()
        val email = binding.edtEmail.text.trim().toString()
        val pwd = binding.edtUserPwd.text.trim().toString()
        val newPwd = binding.edtNewPwd.text.trim().toString()
        val conPwd = binding.edtCnfrmPwd.text.trim().toString()
        when {
            TextUtils.isEmpty(firstName) -> {
                binding.edtUserFirstName.error = "Please enter first name "
                return false
            }
            TextUtils.isEmpty(lastName) -> {
                binding.edtUserLastName.error = "Please enter last name"
                return false
            }
            TextUtils.isEmpty(email) -> {
                binding.edtEmail.error = "Please enter email"
                return false
            }
            TextUtils.isEmpty(pwd) -> {
                binding.edtUserPwd.error = "Please enter password"
                return false
            }
//            TextUtils.isEmpty(newPwd) -> {
//                binding.edtNewPwd.error = "Please new password"
//                return false
//            }
            !TextUtils.isEmpty(newPwd) &&  TextUtils.isEmpty(conPwd) -> {
                binding.edtCnfrmPwd.error = "Please enter confirm password"
                return false
            }
            conPwd != newPwd -> {
                binding.edtCnfrmPwd.error = "Password doesn't match"
                return false
            }
            else -> {
                binding.edtCnfrmPwd.error=null
                return true
            }
        }
    }


}