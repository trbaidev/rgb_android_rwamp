package com.aquatic.av.screens.setting.myDevices.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.RGBRepositoryImp
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentModel
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UpdateEnvironmentViewModel  @Inject constructor(private val authRepository: RGBRepositoryImp): ViewModel() {
    private val _envResults = MutableLiveData<Event<State<UpdateEnvironmentResponse>>>()
    val envResults: LiveData<Event<State<UpdateEnvironmentResponse>>> get() = _envResults

    fun updateEnvironment(token:String, body : UpdateEnvironmentModel) {
        viewModelScope.launch {
            authRepository.updateEnvironment(token,body).collect {
                _envResults.value = (Event(it))
            }
        }
    }
}