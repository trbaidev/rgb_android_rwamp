package com.aquatic.av.screens.rgb.dataModel

data class FavColorData(var device_id: Int = -1,
                        var color: String = "#ffc4c4c4",
                        var dcid: Int = -1,
                        var dcidServer: Int = -1,
                        var isChecked: Boolean = false,
                        var isSelectionMode: Boolean = false)
