package com.aquatic.av.screens.accountAuthentication.resetPassword

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentResetPasswordBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.accountAuthentication.AccountFragment
import com.aquatic.av.screens.accountAuthentication.resetPassword.dataModels.ResetPasswordModel
import com.aquatic.av.screens.accountAuthentication.resetPassword.viewModel.ResetPwdViewModel
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.utils.EditTextExtension.getEncryptPassword
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.aquatic.av.views.dialogs.DialogFactory.showDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ResetPasswordFragment :
        BaseFragment<FragmentResetPasswordBinding>(R.layout.fragment_reset_password) {

    private val viewModel by viewModels<ResetPwdViewModel>()
    lateinit var email: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        binding.apply {
            btnContinue.setOnClickListener {
                if (checkFieldsValidation()) {
                    val resetPasswordModel =
                            ResetPasswordModel(email, edtCnfrmPwd.getEncryptPassword())
                    viewModel.resetPassword(resetPasswordModel)
                }
            }
            ivBack.setOnClickListener {
                mainActivity.popFragment()
            }
        }
    }

    override fun setHeaderFooterViews(
            titleBar: TitleBar?,
            bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }

    private fun setListeners() {
        viewModel.resetPwdResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        if (it.body.resp_msg.equals("true", true)) {
                            showDialog("Update", "Password reset successfully!") { dialogView, _ ->
                                dialogView.cancel()
                                mainActivity.popBackStackTillEntry(0)
                                mainActivity.navigateTo(AccountFragment())
                            }
                        }else {
                            showDialog("Update", "Password reset unsuccessful!") { dialogView, _ ->
                                dialogView.cancel()
                                mainActivity.popBackStackTillEntry(0)
                                mainActivity.navigateTo(AccountFragment())
                            }
                        }
                    }
                }
            }
        })
    }

    private fun checkFieldsValidation(): Boolean {
        val password: String = binding.edtPassword.text.trim().toString()
        val cnfPassword: String = binding.edtCnfrmPwd.text.trim().toString()
        when {
            TextUtils.isEmpty(password) -> {
                binding.edtPassword.error = "Please provide password"
                return false
            }
            TextUtils.isEmpty(cnfPassword) -> {
                binding.edtCnfrmPwd.error = "Please re-enter password"
                return false
            }
            password != cnfPassword -> {
                binding.edtCnfrmPwd.error = "Password doesn't match"
            }
            else -> {
                return true
            }
        }
        return true
    }


    companion object {
        fun getInstance(email: String): ResetPasswordFragment {
            val resetPasswordFragment = ResetPasswordFragment()
            resetPasswordFragment.email = email
            return resetPasswordFragment
        }
    }
}