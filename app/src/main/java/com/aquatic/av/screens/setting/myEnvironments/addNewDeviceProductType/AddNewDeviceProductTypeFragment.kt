package com.aquatic.av.screens.setting.myEnvironments.addNewDeviceProductType

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentAddNewDeviceProductTypeBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.setting.myEnvironments.addNewDeviceProductType.binders.AddNewDeviceProductTypeBinder
import com.aquatic.av.screens.setting.myEnvironments.addNewDeviceProductType.dataModels.AddNewDeviceProductTypeResponse
import com.aquatic.av.screens.setting.myEnvironments.addNewDeviceProductType.viewModel.AddNewDeviceProductTypeViewModel
import com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice.AddNewSelectDeviceFragment
import com.aquatic.av.screens.setting.myEnvironments.dataModels.EnvironmentReferenceModel
import com.aquatic.av.utils.EditTextExtension.getGenericList

import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddNewDeviceProductTypeFragment :
    BaseFragment<FragmentAddNewDeviceProductTypeBinding>(R.layout.fragment_add_new_device_product_type) {
    lateinit var userEnvironmentReferenceModel: EnvironmentReferenceModel
    private lateinit var userRegTypeProductTypeResponses: List<AddNewDeviceProductTypeResponse>
    private val viewModel by viewModels<AddNewDeviceProductTypeViewModel>()

    private val addNewDeviceProductTypeItemAdapter by lazy {
        BaseRecyclerViewAdapter(
            arrayListOf(),
            addNewDeviceProductTypeItemBinder,
            requireContext()
        )
    }

    private val addNewDeviceProductTypeItemBinder by lazy {
        AddNewDeviceProductTypeBinder { entity, position ->
            updateData(position)
        }
    }

    private fun updateData(position: Int) {
        var item = userRegTypeProductTypeResponses.get(position)
        userEnvironmentReferenceModel.product_type_id= item.product_type_id
        userEnvironmentReferenceModel.product_type_img=item.product_type_image
        mainActivity.navigateTo(
            AddNewSelectDeviceFragment.getInstance(
                userEnvironmentReferenceModel
            )
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        binding.ptRecyclerView.adapter = addNewDeviceProductTypeItemAdapter
        binding.ptRecyclerView.layoutManager = LinearLayoutManager(mainActivity)
        binding.apply {
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnNext.setOnClickListener {
                showToast("Please select a product type to continue")
            }
        }
        viewModel.getAllProductTypes()
    }


    private fun setListeners() {
        viewModel.addNewDeviceProductTypeResponseResults.observe(
            viewLifecycleOwner,
            EventObserver {
                when (it) {
                    is State.Error -> showToast(it.message)
                    is State.Exception -> {
                        it.exception.printStackTrace()
                        showToast(it.exception.message)
                    }
                    is State.Loading -> mainActivity.showLoader(it.isLoading)
                    is State.Success -> {
                        it.body?.apply {
                            var userCustomEnvironmentResponse = Gson().getGenericList(
                                Gson().toJson(it.body),
                                AddNewDeviceProductTypeResponse::class.java
                            )
                            userRegTypeProductTypeResponses = userCustomEnvironmentResponse
                            addNewDeviceProductTypeItemAdapter.addAll(
                                userRegTypeProductTypeResponses
                            )
                        }
                    }
                }
            })
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }

    companion object {
        fun getInstance(
            userEnvironmentReferenceModel: EnvironmentReferenceModel
        ): AddNewDeviceProductTypeFragment {
            val addNewDeviceProductTypeFragment = AddNewDeviceProductTypeFragment()
            addNewDeviceProductTypeFragment.userEnvironmentReferenceModel = userEnvironmentReferenceModel
            return addNewDeviceProductTypeFragment
        }
    }

}