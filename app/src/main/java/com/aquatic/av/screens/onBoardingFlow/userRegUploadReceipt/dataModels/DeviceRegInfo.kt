package com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels

class DeviceRegInfo {
    var user_email: String? = ""
    var user_pwd: String? = ""
    var user_pwd_encrypted: String? = ""
    var first_name: String? = ""
    var last_name: String? = ""
    var address: String? = ""
    var city: String? = ""
    var state: String? = ""
    var zip_code: String? = ""
    var phone: String? = ""
    var country: String? = ""
    var user_id: Int? = -1
    var is_rgb_only: Boolean? = false
}