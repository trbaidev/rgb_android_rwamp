package com.aquatic.av.screens.setting.myAccount

import android.os.Bundle
import android.view.View
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentMyAccountBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.setting.contactInformation.ContactInformationFragment
import com.aquatic.av.screens.setting.myDevices.MyDevicesFragment
import com.aquatic.av.screens.setting.myEnvironments.MyEnvironmentsFragment
import com.aquatic.av.screens.setting.personalInformation.PersonalInformationFragment
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView

class MyAccountFragment : BaseFragment<FragmentMyAccountBinding>(R.layout.fragment_my_account) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            personalInfoCnt.setOnClickListener {
                mainActivity.navigateTo(PersonalInformationFragment())
            }
            contactInfoCnt.setOnClickListener {
                mainActivity.navigateTo(ContactInformationFragment())
            }
            myEnvironment.setOnClickListener {
                mainActivity.navigateTo(MyEnvironmentsFragment())
            }
            myDevice.setOnClickListener {
                mainActivity.navigateTo(MyDevicesFragment())
            }
        }
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        titleBar?.hideButtons()
    }
}