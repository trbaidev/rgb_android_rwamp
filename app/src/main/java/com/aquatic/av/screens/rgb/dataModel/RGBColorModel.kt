package com.aquatic.av.screens.rgb.dataModel


import com.aquatic.av.utils.DEFAULT_CELL_COLOR
import com.google.gson.annotations.SerializedName

data class RGBColorModel(
        var responseCode: Int? = 0,
        var responseData: List<ResponseData>? = listOf(),
        var responseMessage: String? = ""
) {

    data class ResponseData(
            var color: String? = "",
            var dcid: Int? = 0,
            @SerializedName("device_id")
            var deviceId: Int? = 0,
            @SerializedName("is_favourite")
            var isFavourite: Boolean? = false,
            var localItemPosition: Int = -1,
            var is_top:Boolean?=false
    ) {
        fun getColorItem(): String {
            return color ?: DEFAULT_CELL_COLOR
        }
    }
}