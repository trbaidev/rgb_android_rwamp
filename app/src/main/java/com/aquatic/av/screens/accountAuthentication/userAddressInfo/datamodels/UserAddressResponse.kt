package com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels

import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginResponse


data class UserAddressResponse(
    var responseCode: Int,
    var responseMessage: String?,
    var responseData: UserResponseData
) {

    data class UserResponseData(
        var user_info: LoginResponse.UserInfo,
        var tokens: Token
    )


    data class Token(
        var token: String,
        var refreshToken: String
    )
}