package com.aquatic.av.screens.dashboard

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentDashboardBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.bluetooth.ConnectedDeviceFragment
import com.aquatic.av.screens.bluetooth.ScannerFragment
import com.aquatic.av.screens.dashboard.binder.DashboardItemBinder
import com.aquatic.av.screens.dashboard.dataModel.DashboardItemModel
import com.aquatic.av.screens.rgb.viewModel.RGBViewModel
import com.aquatic.av.screens.setting.deviceinformation.DeviceDetailFragment
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.getUserDetailModel
import com.aquatic.av.utils.BasePreferenceHelper.getUserEmaiil
import com.aquatic.av.utils.isDeviceConnected
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : BaseFragment<FragmentDashboardBinding>(R.layout.fragment_dashboard) {
    private val viewModel by viewModels<DashboardViewModel>()
    private val mRGBViewModel by viewModels<RGBViewModel>()
    private val preferences by lazy {
        requireContext().getSharedPreferences(mainActivity.packageName, Context.MODE_PRIVATE)
    }
    private val dashboardItemBinder by lazy {
        DashboardItemBinder(
            isDeviceConnected = mRGBViewModel.isDeviceConnected,
            connectedDeviceID = preferences.getInt("connected_device_id", 0),
            connectedDeviceName = preferences.getString("connected_device_name", "") ?: "",
            itemClickListener =  {
                navigateToDeviceInfo(it)
            },
            statusConnectedItemClickListener = {
                if (isDeviceConnected()) {
                    it.deviceId?.let { it1 ->
                        preferences.getString("connected_device_name", "")?.let { it2 ->
                            ConnectedDeviceFragment.newInstance(
                                it2,
                                it1
                            )
                        }
                    }?.let { it2 -> mainActivity.navigateTo(it2) }
                } else {
                    it.deviceId?.let { it1 ->
                        preferences.getString("connected_device_name", "")?.let { it2 ->
                            ScannerFragment.newInstance(
                                it2,
                                it1
                            )
                        }
                    }?.let { it2 -> mainActivity.navigateTo(it2) }
                }
            }
        )
    }

    private fun navigateToDeviceInfo(dashboardItemModel: DashboardItemModel) {
        mainActivity.navigateTo(DeviceDetailFragment.getInstance(dashboardItemModel))
    }

    private val dashboardItemAdapter by lazy {
        BaseRecyclerViewAdapter(
            arrayListOf(),
            dashboardItemBinder,
            this.requireContext()
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()

        titleBar?.let {
            it.binding?.apply {
                if (spnEnvironment.selectedItemPosition != -1) {
                    getUserDashboard(
                        it.adapter.getItem(spnEnvironment.selectedItemPosition).getEnvName() ?: ""
                    )
                }
                var jObj = JsonObject()
                jObj.addProperty("user_id", requireContext().getUserDetailModel()?.user_id)
                mainActivity.viewModel.getUserEnvironment(
                    requireContext().getAccessToken(),
                    jObj
                )
            }
        }
        mainActivity.setEnvironmentSelectionListener {
            getUserDashboard(it.getEnvName() ?: "")
        }
        viewModel.userDashboardResponse.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.let {
                        dashboardItemAdapter.addAll(it)
                        var item = it.find { it.isBleDevice == true && it.isMobileApp == true }
                        if (item != null)
                            mainActivity.device_id = item?.deviceId!!
                        mainActivity.canShowRGBIcon(item != null)
                    }
                }
            }
        })
    }

    private fun getUserDashboard(environmentName: String) {
        viewModel.getUserDashboard(
            token = requireContext().getAccessToken(),
            userEmail = requireContext().getUserEmaiil(),
            environmentName = environmentName
        )
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        bottomNavigationView?.visibility = View.VISIBLE
        titleBar?.hideButtons()
    }

    private fun setListeners() {
        binding.apply {
            rvDashboardItems.layoutManager = LinearLayoutManager(requireContext())
            rvDashboardItems.itemAnimator = DefaultItemAnimator()
            rvDashboardItems.adapter = dashboardItemAdapter

        }
    }
}