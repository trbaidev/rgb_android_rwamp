package com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentAddNewEnvironmentBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.binder.MyEnvironmentBinder
import com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.dataModels.MyCustomEnvironmentModel
import com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.dataModels.MyCustomEnvironmentResponse
import com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.dataModels.MyEnvironmentModel
import com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.dataModels.MyEnvironmentResponse
import com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.viewModels.MyCustomEnvironmentViewModel
import com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.viewModels.MyEnvironmentViewModel
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.getUserDetailModel
import com.aquatic.av.utils.EditTextExtension.mapToObject
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.aquatic.av.views.dialogs.DialogFactory.showDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddNewEnvironmentFragment :
    BaseFragment<FragmentAddNewEnvironmentBinding>(R.layout.fragment_add_new_environment) {


    private var environmentResponse: MyEnvironmentResponse? = null
    private var environment: MyEnvironmentResponse.Environment? = null
    private val viewModel by viewModels<MyEnvironmentViewModel>()
    private val customEnvViewModel by viewModels<MyCustomEnvironmentViewModel>()
    private var cusEnvName: String? = ""
    private var cusEnvironmentimage: String? = ""
    private val userEnvironmentItemAdapter by lazy {
        BaseRecyclerViewAdapter(
            arrayListOf(),
            userEnvironmentBinder,
            requireContext()
        )
    }

    private val userEnvironmentBinder by lazy {
        MyEnvironmentBinder { entity, position ->
            updateData(entity, position)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun updateData(
        selectedEnvironment: MyEnvironmentResponse.Environment?,
        position: Int
    ) {
        binding.customEnvironmentName = ""
        binding.isCustomEnvironment = false
        cusEnvName = ""
        cusEnvironmentimage = ""
        val env = environmentResponse?.responseData?.static_env?.find { it.selected }
        env?.selected = false
        environmentResponse?.responseData?.static_env?.get(position)?.selected = true
        updateEnvironment(selectedEnvironment)
        userEnvironmentItemAdapter.notifyDataSetChanged()
    }


    private fun updateEnvironment(selectedEnvironment: MyEnvironmentResponse.Environment?) {
        environment = selectedEnvironment
    }

    private fun setCustomEnvironment(envName: String,env_type:String,env_id:Int) {
        cusEnvName=envName
        val cusEnv = MyCustomEnvironmentModel()
        cusEnv.env_Icon_Name = binding.customEnvironmentName
        cusEnv.environmentId =  env_id
        cusEnv.environmentName = envName
        cusEnv.environmentType = env_type
        cusEnv.isActive = environment?.is_active ?: 1
        cusEnv.userIds = arrayOf(requireContext().getUserDetailModel()?.user_id ?: -1)
        customEnvViewModel.addUpdateEnvironments(
            requireContext().getAccessToken(),
            cusEnv
        )
    }

    private fun resetEnvirSelection(){
        val env = environmentResponse?.responseData?.static_env?.find { it.selected }
        env?.selected = false
        userEnvironmentBinder.lastSelectedIndex=-1
        userEnvironmentItemAdapter.notifyDataSetChanged()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        seCustomEnvListeners()
        binding.rvEnvironments.adapter = userEnvironmentItemAdapter
        binding.rvEnvironments.layoutManager = LinearLayoutManager(mainActivity)
        if (environmentResponse?.responseData?.static_env != null) {
            environmentResponse?.let {
                userEnvironmentItemAdapter.addAll(
                    it.responseData.static_env ?: arrayListOf()
                )
            }
        }
        binding.apply {
            btnCancel.setOnClickListener { mainActivity.popFragment() }
            btnSave.setOnClickListener {
                if (isCustomEnvironment) {
                    cusEnvName = edtEnv.text.trim().toString()
                    if (cusEnvName?.isEmpty()!!) {
                        showToast("PLease enter Environment name")
                    } else if (cusEnvironmentimage?.isEmpty()!!) {
                        showToast("Please select an icon for the environment.")
                    } else {
                        setCustomEnvironment(cusEnvName!!,"d",0)
                    }
                } else {
                    if (environment == null) {
                        showToast("Please select an environment.")
                    } else {
                        setCustomEnvironment(
                            environment?.environment_name!!,"s",0
                        )
                    }
                }

            }
            tvCustomEnv.setOnClickListener {
                isCustomEnvironment = !isCustomEnvironment
                resetEnvirSelection()
            }
            ivOne.setOnClickListener {
                cusEnvironmentimage = "vector_0"
                binding.customEnvironmentName = cusEnvironmentimage
                resetEnvirSelection()
            }
            ivTwo.setOnClickListener {
                cusEnvironmentimage = "vector_1"
                binding.customEnvironmentName = cusEnvironmentimage
                resetEnvirSelection()
            }
            ivThree.setOnClickListener {
                cusEnvironmentimage = "vector_2"
                binding.customEnvironmentName = cusEnvironmentimage
                resetEnvirSelection()
            }
            ivFour.setOnClickListener {
                cusEnvironmentimage = "vector_3"
                binding.customEnvironmentName = cusEnvironmentimage
                resetEnvirSelection()
            }
            ivFive.setOnClickListener {
                cusEnvironmentimage = "vector_4"
                binding.customEnvironmentName = cusEnvironmentimage
                resetEnvirSelection()
            }
            ivSix.setOnClickListener {
                cusEnvironmentimage = "vector_5"
                binding.customEnvironmentName = cusEnvironmentimage
                resetEnvirSelection()
            }
            ivSeven.setOnClickListener {
                cusEnvironmentimage = "vector_6"
                binding.customEnvironmentName = cusEnvironmentimage
                resetEnvirSelection()
            }
            ivEight.setOnClickListener {
                cusEnvironmentimage = "vector_7"
                binding.customEnvironmentName = cusEnvironmentimage
                resetEnvirSelection()
            }
        }
        if (environmentResponse == null) {
            viewModel.getAllEnvironments(
                MyEnvironmentModel(
                    requireContext().getUserDetailModel()?.user_id,
                    arrayOf("S")
                )
            )
        }
    }


    private fun setListeners() {
        viewModel.userEnvironmentResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        val enviResponse = Gson().mapToObject(
                            it.body as Map<String, Any?>?,
                            MyEnvironmentResponse::class.java
                        )
                        if (enviResponse != null) {
                            environmentResponse = enviResponse
                        }
                        userEnvironmentItemAdapter.addAll(
                            environmentResponse?.responseData?.static_env ?: arrayListOf()
                        )
                    }
                }
            }
        })
    }

    private fun seCustomEnvListeners() {
        customEnvViewModel.userCustomEnvironmentResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        val userCustomEnvironmentResponse = Gson().mapToObject(
                            it.body as Map<String, Any?>?,
                            MyCustomEnvironmentResponse::class.java
                        )
                        if (userCustomEnvironmentResponse?.responseCode == 1) {
                            showDialog(
                                "",
                                "Environment " + "\"" + cusEnvName + "\"" + " has been created",
                                true
                            )
                            mainActivity.popFragment()
                        }
                    }
                }
            }
        })
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }

}