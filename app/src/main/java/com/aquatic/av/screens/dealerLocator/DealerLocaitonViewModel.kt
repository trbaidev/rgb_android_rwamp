package com.aquatic.av.screens.dealerLocator

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.DealerLocatorRepository
import com.aquatic.av.screens.dealerLocator.model.DealerFilterResponse
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionRequest
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionResponse
import com.aquatic.av.screens.dealerLocator.model.GoogleAutocompleteSearch
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DealerLocatorViewModel @Inject constructor(private val dealerLocatorRepository: DealerLocatorRepository) : ViewModel() {

    private val _dealerFilterResponse = MutableLiveData<Event<State<DealerFilterResponse>>>()
    val dealerFilterResponse: LiveData<Event<State<DealerFilterResponse>>> get() = _dealerFilterResponse

    private val _dealerLocatorResponse = MutableLiveData<Event<State<DealerSuggestionResponse>>>()
    val dealerLocatorResponse: LiveData<Event<State<DealerSuggestionResponse>>> get() = _dealerLocatorResponse

    private val _dealerLocatorFilteredResponse = MutableLiveData<Event<State<DealerSuggestionResponse>>>()
    val dealerLocatorFilteredResponse: LiveData<Event<State<DealerSuggestionResponse>>> get() = _dealerLocatorFilteredResponse


    private val _googlResult = MutableLiveData<Event<State<GoogleAutocompleteSearch>>>()
    val googlResult: LiveData<Event<State<GoogleAutocompleteSearch>>> get() = _googlResult


    fun getDealerSuggestions(token: String, dealerSuggestionRequest: DealerSuggestionRequest) {
        viewModelScope.launch {
            dealerLocatorRepository.getDealerSuggestion(token, dealerSuggestionRequest).collect {
                _dealerLocatorResponse.value = Event(it)
            }
        }
    }

    fun getFilteredDealerSuggestion(token: String, dealerSuggestionRequest: DealerSuggestionRequest) {
        viewModelScope.launch {
            dealerLocatorRepository.getDealerSuggestion(token, dealerSuggestionRequest).collect {
                _dealerLocatorFilteredResponse.value = Event(it)
            }
        }
    }

    fun getDealerFilters(token: String) {
        viewModelScope.launch {
            dealerLocatorRepository.getDealerFilters(token).collect {
                _dealerFilterResponse.value = Event(it)
            }
        }
    }


    fun getNearbyDealers(input:String,types: String,key:String) {
        viewModelScope.launch {
            dealerLocatorRepository.getNearbyDealers(input,types,key).collect {
                _googlResult.value = (Event(it))
            }
        }
    }
}
