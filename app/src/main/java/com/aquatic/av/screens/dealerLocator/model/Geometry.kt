package com.aquatic.av.screens.dealerLocator.model

data class Geometry(
    var location: Location,
    var viewport: Viewport
) {


    data class Location(
        var lat: Double? = 0.0,
        var lng: Double? = 0.0
    )

    data class Viewport(
        var northeast: Northeast,
        var southwest: Southwest
    ) {

        data class Northeast(
            var lat: Double? = 0.0,
            var lng: Double? = 0.0
        )

        data class Southwest(
            var lat: Double? = 0.0,
            var lng: Double? = 0.0
        )
    }
}