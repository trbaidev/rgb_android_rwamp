package com.aquatic.av.screens.setting.myDevices.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.setting.myDevices.dataModels.MyDeviceModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyDeviceUpdateViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _myUpdateDeviceResults = MutableLiveData<Event<State<Any>>>()
    val myUpdateDeviceResults: LiveData<Event<State<Any>>> get() = _myUpdateDeviceResults

    fun updateDeviceName(token:String,model: MyDeviceModel) {
        viewModelScope.launch {
            authRepository.updateDeviceName(token,model).collect {
                _myUpdateDeviceResults.value = (Event(it))
            }
        }
    }
}