package com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels

data class DealerModel(
    var dealer_id: Int,
    var dealer_name: String
)