package com.aquatic.av.screens.setting.personalInformation.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.setting.personalInformation.dataModels.PersonalInfoModel
import com.aquatic.av.screens.setting.personalInformation.dataModels.PersonalInfoResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PersonalInfoViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _personalInfoResults = MutableLiveData<Event<State<PersonalInfoResponse>>>()
    val personalInfoResults: LiveData<Event<State<PersonalInfoResponse>>> get() = _personalInfoResults

    fun updateUserPersonalInfo(token:String,userCustomEnvironmentModel: PersonalInfoModel) {
        viewModelScope.launch {
            authRepository.updateUserPersonalInfo(token,userCustomEnvironmentModel).collect {
                _personalInfoResults.value = (Event(it))
            }
        }
    }
}