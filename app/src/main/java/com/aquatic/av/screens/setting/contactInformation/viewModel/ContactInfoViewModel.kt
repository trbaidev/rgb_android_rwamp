package com.aquatic.av.screens.setting.contactInformation.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.setting.contactInformation.dataModels.ContactInfoModel
import com.aquatic.av.screens.setting.contactInformation.dataModels.ContactInfoResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ContactInfoViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _contactInfoResults = MutableLiveData<Event<State<ContactInfoResponse>>>()
    val contactInfoResults: LiveData<Event<State<ContactInfoResponse>>> get() = _contactInfoResults

    fun updateUserContactInfo(token:String,contactInfoModel: ContactInfoModel) {
        viewModelScope.launch {
            authRepository.updateUserContactInfo(token,contactInfoModel).collect {
                _contactInfoResults.value = (Event(it))
            }
        }
    }
}