package com.aquatic.av.screens.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.aquatic.av.activities.MainActivity
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView

@Suppress("UNCHECKED_CAST")
abstract class BaseFragment<T : ViewDataBinding>(@LayoutRes private val layoutId: Int) : Fragment() {


    protected lateinit var binding: T

    protected val mainActivity by lazy {
        requireActivity() as MainActivity
    }

    protected val titleBar by lazy {
        mainActivity.mTitleBar
    }

    protected val bottomNavigation by lazy {
        mainActivity.bottomNavigation
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }


    override fun onResume() {
        super.onResume()
        fragmentResume()
    }

    private fun fragmentResume() {
        setHeaderFooterViews(titleBar, bottomNavigation)
    }


    /**
     * This is called in the end to modify titlebar. after all changes.
     *
     * @param
     */
    open fun setHeaderFooterViews(titleBar: TitleBar?, bottomNavigationView: BottomNavigationView?) {
        titleBar?.showTitleBar()
        // titleBar.refreshListener();
    }

}