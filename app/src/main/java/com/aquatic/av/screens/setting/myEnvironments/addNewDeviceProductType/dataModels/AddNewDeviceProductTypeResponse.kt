package com.aquatic.av.screens.setting.myEnvironments.addNewDeviceProductType.dataModels

data class AddNewDeviceProductTypeResponse(
    var product_type_id: Int,
    var product_type: String,
    var base_path: String,
    var product_type_image: String
)