package com.aquatic.av.screens.setting.myEnvironments.dataModels

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MyEnvironmentResponse(
    @SerializedName("responseCode")
    @Expose
    private var responseCode: Int? = -1,

    @SerializedName("responseMessage")
    @Expose
    private var responseMessage: String? = "",

    @SerializedName("responseData")
    @Expose
    private var responseData: List<MyEnvironmentModel>? = null
) {

    fun getResponseCode(): Int? {
        return responseCode
    }

    fun setResponseCode(responseCode: Int?) {
        this.responseCode = responseCode
    }

    fun getResponseMessage(): String? {
        return responseMessage
    }

    fun setResponseMessage(responseMessage: String?) {
        this.responseMessage = responseMessage
    }

    fun getResponseData(): List<MyEnvironmentModel>? {
        return responseData
    }

    fun setResponseData(responseData: List<MyEnvironmentModel>?) {
        this.responseData = responseData
    }
}