package com.aquatic.av.screens.setting.myDevices.dataModels

data class MyDeviceModel(var device_id:Int,
var device_name:String,
var modified_by:String)
