package com.aquatic.av.screens.setting.contactInformation

import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.TextUtils
import android.view.View
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentContactInformationBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.setting.contactInformation.dataModels.ContactInfoModel
import com.aquatic.av.screens.setting.contactInformation.viewModel.ContactInfoViewModel

import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.getUserDetailModel
import com.aquatic.av.utils.BasePreferenceHelper.putUserDetailModel
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.aquatic.av.views.dialogs.DialogFactory.showDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ContactInformationFragment :
    BaseFragment<FragmentContactInformationBinding>(R.layout.fragment_contact_information) {

    val userInfoModel by lazy { requireContext().getUserDetailModel() }

    private val contactInfoViewModel by viewModels<ContactInfoViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        binding.apply {
            edtPhone.addTextChangedListener(PhoneNumberFormattingTextWatcher())
            dataItem = requireContext().getUserDetailModel()
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnSave.setOnClickListener {
                if (!visibleStatus) {
                    visibleStatus = !visibleStatus
                    fieldsEnable(true)
                }else{
                    if (checkFieldsValidations()) {
                        val contactInformationModel = ContactInfoModel(
                            address = edtAddress.text.trim().toString(),
                            city = edtCity.text.trim().toString(),
                            state = edtState.text.trim().toString(),
                            zip_code = edtState.text.trim().toString(),
                            country = edtCountry.text.trim().toString(),
                            phone = edtPhone.text.trim().toString(),
                            user_email = userInfoModel?.user_email
                        )
                        contactInfoViewModel.updateUserContactInfo(requireContext().getAccessToken(),contactInformationModel)
                    }
                }
            }
        }
    }

    private fun setListeners() {
        contactInfoViewModel.contactInfoResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        if (it.body.resp_msg.equals("true", true)) {
                            userInfoModel?.address =binding.edtAddress.text.trim().toString()
                            userInfoModel?.city =binding.edtCity.text.trim().toString()
                            userInfoModel?.state =binding.edtState.text.trim().toString()
                            userInfoModel?.zip_code =binding.edtZipCode.text.trim().toString()
                            userInfoModel?.phone=binding.edtPhone.text.trim().toString()
                            userInfoModel?.country=binding.edtCountry.text.trim().toString()

                            requireContext().putUserDetailModel(userInfoModel)
                            binding.dataItem = userInfoModel
                            binding.visibleStatus = !binding.visibleStatus
                            fieldsEnable(false)
                            showDialog("","Information updated successfully!",true)
                        } else {
                            showDialog("", "Please verify your credentials.", true)
                        }
                    }
                }
            }
        })
    }

    private fun fieldsEnable(enable: Boolean) {
        binding.edtAddress.isEnabled = enable
        binding.edtCity.isEnabled = enable
        binding.edtState.isEnabled = enable
        binding.edtZipCode.isEnabled = enable
        binding.edtCountry.isEnabled = enable
        binding.edtPhone.isEnabled = enable
    }


    private fun checkFieldsValidations(): Boolean {
        val address = binding.edtAddress.text.trim().toString()
        val city = binding.edtCity.text.trim().toString()
        val state = binding.edtState.text.trim().toString()
        val zip = binding.edtZipCode.text.trim().toString()
        val country = binding.edtCountry.text.trim().toString()
        val phone = binding.edtPhone.text.trim().toString()
        when {
            TextUtils.isEmpty(address) -> {
                binding.edtAddress.error = "Please enter Address"
                return false
            }
            TextUtils.isEmpty(city) -> {
                binding.edtCity.error = "Please enter City name"
                return false
            }
            TextUtils.isEmpty(state) -> {
                binding.edtState.error = "Please enter Email"
                return false
            }
            TextUtils.isEmpty(zip) -> {
                binding.edtZipCode.error = "Please enter Zip code"
                return false
            }
            TextUtils.isEmpty(country) -> {
                binding.edtCountry.error = "Please enter Country name"
                return false
            }
            TextUtils.isEmpty(phone) -> {
                binding.edtPhone.error = "Please enter phone number"
                return false
            }
            phone.length < 14 -> {
                binding.edtPhone.error = "Please enter correct phone number"
                return false
            }
            else -> {
                return true
            }
        }
    }


    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        titleBar?.hideButtons()
    }
}