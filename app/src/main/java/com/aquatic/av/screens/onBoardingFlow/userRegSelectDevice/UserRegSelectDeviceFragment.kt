package com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.UserRegSelectDeviceFragmentBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressResponse
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.onBoardingFlow.dataModels.DevicesInformation
import com.aquatic.av.screens.onBoardingFlow.userDeviceName.UserDeviceNameFragment
import com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice.binders.UserRegSelectDeviceBinder
import com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice.dataModels.UserRegSelectDeviceResponse
import com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice.dataModels.UserRegSelectedDeviceModel
import com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice.viewModel.UserRegSelectDeviceViewModel
import com.aquatic.av.screens.onBoardingFlow.userRegSelectProductType.dataModels.UserRegSelectProductTypeResponse
import com.aquatic.av.utils.EditTextExtension.getGenericList
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserRegSelectDeviceFragment :
        BaseFragment<UserRegSelectDeviceFragmentBinding>(R.layout.user_reg_select_device_fragment) {

    var userRegTypeProductTypeResponse: UserRegSelectProductTypeResponse? = null
    private val viewModel by viewModels<UserRegSelectDeviceViewModel>()
    private var userRegSelectDeviceResponses: List<UserRegSelectDeviceResponse>? = null
    private var devicesInformation: DevicesInformation? = null
    private var userAddressModel: UserAddressModel? = null
    private var userAddressResponse: UserAddressResponse? = null
    private val userRegSelectDeviceItemAdapter by lazy {
        BaseRecyclerViewAdapter(
                arrayListOf(),
                userRegSelectDeviceBinder,
                requireContext()
        )
    }

    private val userRegSelectDeviceBinder by lazy {
        UserRegSelectDeviceBinder { entity, position ->
            updateData(position)
        }
    }

    private fun updateData(position: Int) {
        var userRegSelectDeviceResponse = userRegSelectDeviceResponses?.get(position)
        devicesInformation?.product_id = userRegSelectDeviceResponse?.product_id
        mainActivity.navigateTo(
                UserDeviceNameFragment.getInstance(
                        userRegSelectDeviceResponse,
                        userAddressModel,
                        devicesInformation,
                        userAddressResponse
                )
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.prRecyclerView.adapter = userRegSelectDeviceItemAdapter
        binding.prRecyclerView.layoutManager = GridLayoutManager(mainActivity, 2)
        setListeners()
        binding.apply {
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnNext.setOnClickListener {
                showToast("Please select a device to continue")
            }
        }
        viewModel.getAllProductsByTypeId(UserRegSelectedDeviceModel(userRegTypeProductTypeResponse?.product_type_id))
    }


    private fun setListeners() {
        viewModel.userRegSelectDeviceResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        it.body?.apply {
                            var list = Gson().getGenericList(
                                    Gson().toJson(it.body),
                                    UserRegSelectDeviceResponse::class.java
                            )
                            userRegSelectDeviceResponses = list
                            userRegSelectDeviceItemAdapter.addAll(
                                    userRegSelectDeviceResponses?: arrayListOf()
                            )
                        }
                    }
                }
            }
        })
    }

    override fun setHeaderFooterViews(
            titleBar: TitleBar?,
            bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }

    companion object {
        fun getInstance(
                userRegTypeProductTypeResponse: UserRegSelectProductTypeResponse?,
                userAddressResponse: UserAddressResponse?,
                userAddressModel: UserAddressModel?,
                devicesInformation: DevicesInformation?
        ): UserRegSelectDeviceFragment {
            val userRegSelectDeviceFragment = UserRegSelectDeviceFragment()
            userRegSelectDeviceFragment.userRegTypeProductTypeResponse =
                    userRegTypeProductTypeResponse
            userRegSelectDeviceFragment.userAddressResponse = userAddressResponse
            userRegSelectDeviceFragment.userAddressModel = userAddressModel
            userRegSelectDeviceFragment.devicesInformation = devicesInformation
            return userRegSelectDeviceFragment
        }
    }

}