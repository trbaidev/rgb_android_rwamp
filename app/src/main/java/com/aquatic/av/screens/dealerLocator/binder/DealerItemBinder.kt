package com.aquatic.av.screens.dealerLocator.binder

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.RowItemDealerBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionResponse

class DealerItemBinder : BaseRecyclerViewBinder<DealerSuggestionResponse.DealerItem, RowItemDealerBinding>(R.layout.row_item_dealer) {
    override fun bindView(entity: DealerSuggestionResponse.DealerItem, position: Int, viewHolder: BaseViewHolder<RowItemDealerBinding>?, context: Context, collections: MutableList<DealerSuggestionResponse.DealerItem>) {
        viewHolder?.let {
            it.viewBinding?.dealerItem = entity
        }
    }
}