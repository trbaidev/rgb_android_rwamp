package com.aquatic.av.screens.rgb


import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.Toast
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.BLE_CONNECTION_INTERVAL_DELAY_TIME
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.COLOR_WHEEL_INTERVAL_DELAY_TIME
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.PREF_BRIGHTNESS
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.PREF_ON_OFF_CONTROL
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.fav1Value
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.fav2Value
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.fav3Value
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.fav4Value
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.fav5Value
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.preset1Value
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.preset2Value
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.preset3Value
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.preset4Value
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.preset5Value
import com.aquatic.av.bluetoothScanner.ble.preference.getBoolean
import com.aquatic.av.bluetoothScanner.ble.preference.getInt
import com.aquatic.av.bluetoothScanner.ble.preference.putBoolean
import com.aquatic.av.bluetoothScanner.ble.preference.putInt
import com.aquatic.av.bluetoothScanner.ble.utils.BLEUtils
import com.aquatic.av.bluetoothScanner.ble.utils.Transporter
import com.aquatic.av.databinding.FragmentAdjustColorBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.rgb.dataModel.FavColorData
import com.aquatic.av.utils.*
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.views.CircleImage
import com.aquatic.av.views.ColorChangeListener
import com.skydoves.colorpickerview.listeners.ColorListener
import dagger.hilt.android.AndroidEntryPoint
import net.margaritov.preference.colorpicker.ColorPickerView

@AndroidEntryPoint
class AdjustColorFragment :
    BaseFragment<FragmentAdjustColorBinding>(R.layout.fragment_adjust_color), View.OnClickListener {
    private val TAG = "AdjustColorFragment"
    private val mRGBViewModel by lazy {
        (parentFragment as ColorPickerFragment).mRGBViewModel
    }
    private var selectedPickerType = 0

    private var selectedColor = Color.parseColor(presetColors[0])
    private var selectedFavIndex = -1
    private var canShowColor: Int = 0
    private var selectedId = -1
    private var brightnessReceivedTimeStamp: Long = 0
    private var rgbColorWheelReceivedTimeStamp: Long = 0
    private var rgbColorTapReceivedTimeStamp: Long = 0
    private var selectedFavColorData: FavColorData? = null
    private var favSelected = false
    private var favSelectedTop=false

    //Default brightness value
    private var mBrightness: Int = BRIGHTNESS_DEFAULT_VALUE


    var delay: Long = 1000 // 1 seconds after color selection stops
    var lastSelectedColor: Long = 0
    var handler = Handler(Looper.myLooper()!!)
    private val runnableColorSelection = Runnable {
        if (System.currentTimeMillis() > lastSelectedColor + delay - 500) {

            val rgb = getColorIntToRGB(selectedColor)
            mRGBViewModel.sendColorAdjustment(mBrightness, rgb[0], rgb[1], rgb[2])

            if (activity != null) {
//                (activity as MainActivity).onColorPicked(selectedColor)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mRGBViewModel.isConnected.observe(viewLifecycleOwner, {
            showConnectionStatus()
        })

        //mHomeViewModel.startAutoConnection()
        showONOFFStatus()
        mRGBViewModel.isONOFF.observe(viewLifecycleOwner, {
            showONOFFStatus()
        })
        saveFavColors()

        if (!mainActivity.isRGBMode) {
            mRGBViewModel.getUserFavoritesColor(
                requireContext().getAccessToken(),
                mainActivity.device_id
            )
        }

        mRGBViewModel.updateRGBColorResponse.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    mRGBViewModel.getUserFavoritesColor(
                        requireContext().getAccessToken(),
                        mainActivity.device_id
                    )
                }
            }
        })

        mRGBViewModel.allColorsResponse.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.let { it ->
                        it.responseData?.let {
                            if (it.size > 5) {
                                var sortedList = it.filter { !it.color.equals("#808080") }
                                favColors[0].dcidServer =
                                    if (sortedList.getOrNull(0) != null) sortedList[0].dcid!! else -1
                                favColors[1].dcidServer =
                                    if (sortedList.getOrNull(1) != null) sortedList[1].dcid!! else -1
                                favColors[2].dcidServer =
                                    if (sortedList.getOrNull(2) != null) sortedList[2].dcid!! else -1
                                favColors[3].dcidServer =
                                    if (sortedList.getOrNull(3) != null) sortedList[3].dcid!! else -1
                                favColors[4].dcidServer =
                                    if (sortedList.getOrNull(4) != null) sortedList[4].dcid!! else -1
                                fav1Value.putInt(
                                    Color.parseColor(
                                        if (sortedList.getOrNull(0) != null) sortedList[0].color!! else DEFAULT_CELL_COLOR
                                    )
                                )
                                fav2Value.putInt(
                                    Color.parseColor(
                                        if (sortedList.getOrNull(1) != null) sortedList[1].color!! else DEFAULT_CELL_COLOR
                                    )
                                )
                                fav3Value.putInt(
                                    Color.parseColor(
                                        if (sortedList.getOrNull(2) != null) sortedList[2].color!! else DEFAULT_CELL_COLOR
                                    )
                                )
                                fav4Value.putInt(
                                    Color.parseColor(
                                        if (sortedList.getOrNull(3) != null) sortedList[3].color!! else DEFAULT_CELL_COLOR
                                    )
                                )
                                fav5Value.putInt(
                                    Color.parseColor(
                                        if (sortedList.getOrNull(4) != null) sortedList[4].color!! else DEFAULT_CELL_COLOR
                                    )
                                )
                                Handler(Looper.getMainLooper()).postDelayed({
                                    updateFavColorByIds()
                                }, 200)
                            }
                        }

                    }
                }
            }
        })

        //TODO Should remove later
        //   selectedId = R.id.preset1
        bindColorPickerViews()

        bindEditableFieldsViews()

        bindBrightnessViews()
        mRGBViewModel.onServerFavoriteItemSelected.observe(viewLifecycleOwner, {

            var selectedColor: Int = Color.parseColor(DEFAULT_CELL_COLOR)
            try {
                selectedColor = Color.parseColor(
                    if (it.getColorItem()
                            .contains("#")
                    ) it.getColorItem() else "#" + it.getColorItem()
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
            binding.colorPickerView.setInitialColor(selectedColor)
            binding.colorPickerRd.setColor(selectedColor)
            binding.colorPickerSq.setColor(selectedColor, false)
            selectedFavColorData = FavColorData(
                device_id = it.deviceId ?: 0,
                color = it.color ?: DEFAULT_CELL_COLOR,
                dcidServer = it.dcid ?: 0
            )
            val rgb = getColorIntToRGB(selectedColor)
            sendColorTapData(rgb)
            updateFavIconDataAndUI(true)
            when (it.localItemPosition) {
                0 -> binding.fav1.selectedColor(selectedColor)
                1 -> binding.fav2.selectedColor(selectedColor)
                2 -> binding.fav3.selectedColor(selectedColor)
                3 -> binding.fav4.selectedColor(selectedColor)
                4 -> binding.fav5.selectedColor(selectedColor)
            }
        })
        mRGBViewModel.onLocalFavoriteItemSelected.observe(viewLifecycleOwner, {
            Handler(Looper.getMainLooper()).postDelayed({
                when (it) {
                    0 -> onClick(binding.fav1)
                    1 -> onClick(binding.fav2)
                    2 -> onClick(binding.fav3)
                    3 -> onClick(binding.fav4)
                    4 -> onClick(binding.fav5)
                }
            }, 100)
        })
    }

    private fun bindBrightnessViews() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            binding.brightnessSeekBar.min = 0
        }
        binding.brightnessSeekBar.max = BRIGHTNESS_MAX_VALUE

        binding.brightnessSeekBar.progress = PREF_BRIGHTNESS.getInt(mBrightness)

        binding.brightnessSeekBar.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                mBrightness = progress
                Log.d(TAG, "Brightness progress changed: $mBrightness")
                PREF_BRIGHTNESS.putInt(mBrightness)
                if ((System.currentTimeMillis() - brightnessReceivedTimeStamp) > BLE_CONNECTION_INTERVAL_DELAY_TIME) {
                    mRGBViewModel.sendBrightnessData(mBrightness)
                    Log.d(TAG, "Brightness: $mBrightness")
                    brightnessReceivedTimeStamp = System.currentTimeMillis()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                Log.d(TAG, "Brightness: onStartTrackingTouch")
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                Log.d(TAG, "onStopTrackingTouch Brightness: $mBrightness")
                mRGBViewModel.sendBrightnessData(mBrightness)
            }

        })
    }

    private fun bindEditableFieldsViews() {
        binding.etA.setOnEditorActionListener { _, actionID, _ ->
            if (actionID == EditorInfo.IME_ACTION_DONE) {
                updateEditFieldsColors()
                return@setOnEditorActionListener true
            }
            false
        }
        binding.etR.setOnEditorActionListener { _, actionID, _ ->
            if (actionID == EditorInfo.IME_ACTION_DONE) {
                updateEditFieldsColors()
                return@setOnEditorActionListener true
            }
            false
        }
        binding.etB.setOnEditorActionListener { _, actionID, _ ->
            if (actionID == EditorInfo.IME_ACTION_DONE) {
                updateEditFieldsColors()
                return@setOnEditorActionListener true
            }
            false
        }
        binding.etG.setOnEditorActionListener { _, actionID, _ ->
            if (actionID == EditorInfo.IME_ACTION_DONE) {
                updateEditFieldsColors()
                return@setOnEditorActionListener true
            }
            false
        }
        binding.etHex.setOnEditorActionListener { viewID, actionID, _ ->
            if (actionID == EditorInfo.IME_ACTION_DONE) {
                try {
                    val hex = Color.parseColor(viewID.text.toString())
                    setPickerColors(hex)
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), "Invalid Color", Toast.LENGTH_SHORT).show()
                }
                return@setOnEditorActionListener true
            }
            false
        }
    }

    private fun bindColorPickerViews() {
        binding.colorPickerSq.alphaSliderVisible = true
        binding.colorPickerSq.color = selectedColor
        binding.colorPickerView.setColorListener(object : ColorListener {
            override fun onColorSelected(newColor: Int, fromUser: Boolean) {
                if (Transporter.isMusicEnabled()) {
                    return
                }

                // Unselecting favorite icon
                updateFavIconDataAndUI(false)

                selectedColor = newColor
                val rgb = getColorIntToRGB(selectedColor)

                sendColorWheelAdjustmentData(rgb)
                //updateStandardColor()


                // Updating Advanced Palette data here
                val hexColor = toHexARGB(newColor)
                val hexIntColor: Int = Color.parseColor(hexColor)
                val rgba = getRGBAFromHexInt(hexIntColor)
                binding.etHex.setText(hexColor)
                binding.etR.setText(rgba[0].toString())
                binding.etG.setText(rgba[1].toString())
                binding.etB.setText(rgba[2].toString())
                binding.etA.setText(rgba[3].toString())

                //You need to remove this to run only once
                handler.removeCallbacks(runnableColorSelection)
                // if selectedColor then runnable will execute after delay
                lastSelectedColor = System.currentTimeMillis()
                handler.postDelayed(runnableColorSelection, delay)
            }

        })
        binding.colorPickerSq.setOnColorChangedListener(object :
            ColorPickerView.OnColorChangedListener {
            override fun onColorChanged(newColor: Int) {
                if (Transporter.isMusicEnabled()) {
                    return
                }

                // Unselecting favorite icon
                updateFavIconDataAndUI(false)

                selectedColor = newColor
                val rgb = getColorIntToRGB(selectedColor)

                sendColorWheelAdjustmentData(rgb)
                //updateStandardColor()

                // Updating Advanced Palette data here
                val hexColor = toHexARGB(newColor)
                val hexIntColor: Int = Color.parseColor(hexColor)
                val rgba = getRGBAFromHexInt(hexIntColor)
                //hexColor = "#${"%02x%02x%02x%02x".format(rgba[0], rgba[1], rgba[2], rgba[3])}"
                binding.etHex.setText(hexColor)
                binding.etR.setText(rgba[0].toString())
                binding.etG.setText(rgba[1].toString())
                binding.etB.setText(rgba[2].toString())
                binding.etA.setText(rgba[3].toString())

                //You need to remove this to run only once
                handler.removeCallbacks(runnableColorSelection)
                // if selectedColor then runnable will execute after delay
                lastSelectedColor = System.currentTimeMillis()
                handler.postDelayed(runnableColorSelection, delay)
            }

        })

        binding.colorPickerRd.colorChangeListener = object :
            ColorChangeListener {
            override fun onColorUpdated(newColor: Int) {
                if (Transporter.isMusicEnabled()) {
                    return
                }

                // Unselecting favorite icon
                updateFavIconDataAndUI(false)

                selectedColor = newColor
                val rgb = getColorIntToRGB(selectedColor)

                sendColorWheelAdjustmentData(rgb)
                //updateStandardColor()


                // Updating Advanced Palette data here
                val hexColor = toHexARGB(newColor)
                val hexIntColor: Int = Color.parseColor(hexColor)
                val rgba = getRGBAFromHexInt(hexIntColor)
                binding.etHex.setText(hexColor)
                binding.etR.setText(rgba[0].toString())
                binding.etG.setText(rgba[1].toString())
                binding.etB.setText(rgba[2].toString())
                binding.etA.setText(rgba[3].toString())

                //You need to remove this to run only once
                handler.removeCallbacks(runnableColorSelection)
                // if selectedColor then runnable will execute after delay
                lastSelectedColor = System.currentTimeMillis()
                handler.postDelayed(runnableColorSelection, delay)
            }

            override fun onStopTrackingTouch() {
                if (Transporter.isMusicEnabled()) {
                    return
                }
                /*val rgb = getColorIntToRGB(selectedColor)
                mHomeViewModel.sendColorAdjustment(mBrightness, rgb[0], rgb[1], rgb[2])*/
            }
        }
    }

    private fun updateEditFieldsColors() {
        try {
            val r =
                if (binding.etR.text.toString().trim().isEmpty()) 0 else binding.etR.text.toString()
                    .toInt()
            val g =
                if (binding.etG.text.toString().trim().isEmpty()) 0 else binding.etG.text.toString()
                    .toInt()
            val b =
                if (binding.etB.text.toString().trim().isEmpty()) 0 else binding.etB.text.toString()
                    .toInt()
            val hex = Color.parseColor(String.format("#%02x%02x%02x", r, g, b))
            setPickerColors(hex)

        } catch (e: Exception) {
            Toast.makeText(requireContext(), "Invalid Color", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setPickerColors(hex: Int) {
        binding.colorPickerSq.setColor(hex, true)
        binding.colorPickerRd.setColor(hex)
        binding.colorPickerView.setInitialColor(hex)
    }

    override fun onResume() {
        super.onResume()
        showConnectionStatus()
        showONOFFStatus()

        updateStandardColor()
        updatePresetColor()
        updateFavColorByIds()
        //getFavColorsForDevice(false)
        updateBrightness()

    }

    private fun addFavouriteColorForDevice(color: String) {
        if (color == DEFAULT_CELL_COLOR)
            return

        if (selectedFavIndex != -1) {
            favColors[selectedFavIndex] = FavColorData(-1, color, -1)
            saveFavColors()
            updateFavColorByIds()
        }

    }

    private fun showConnectionStatus() {
        if (mRGBViewModel.isDeviceConnected) {
            binding.txtDeviceStatus.gone()
        } else if (!BLEUtils.isBleEnabled(requireContext())) {
            binding.txtDeviceStatus.visible()
            binding.txtDeviceStatus.setText(R.string.alert_error_rgb_device_is_not_connected_please_connect_bluetooth_and_connect)
        } else {
            binding.txtDeviceStatus.visible()
            binding.txtDeviceStatus.setText(R.string.alert_error_rgb_device_is_not_connected_please_connect_rgb_device)
        }
    }

    private fun showONOFFStatus() {
        val isSelected = PREF_ON_OFF_CONTROL.getBoolean(false)
        if (isSelected) {
            mainActivity.mTitleBar?.setHeading(R.drawable.aquatic_av_on) {
                mRGBViewModel.sendONOFFControlData(false)
                PREF_ON_OFF_CONTROL.putBoolean(false)
            }
        } else {
            mainActivity.mTitleBar?.setHeading(R.drawable.aquatic_av_off) {
                mRGBViewModel.sendONOFFControlData(true)
                PREF_ON_OFF_CONTROL.putBoolean(true)
            }
        }
    }

    private fun updateBrightness() {
        binding.brightnessSeekBar.progress = PREF_BRIGHTNESS.getInt(mBrightness)
    }

    private fun updateStandardColor() {
        updateColor(binding.preset1, Color.parseColor(standardColors[0]))
        updateColor(binding.preset2, Color.parseColor(standardColors[1]))
        updateColor(binding.preset3, Color.parseColor(standardColors[2]))
        updateColor(binding.preset4, Color.parseColor(standardColors[3]))
        updateColor(binding.preset5, Color.parseColor(standardColors[4]))

        binding.ivLightsRdColor.setOnClickListener(this)
        binding.ivLightsFav.setOnClickListener(this)
        binding.ivAdvancedColorPalette.setOnClickListener(this)
        binding.btnBrightness.setOnClickListener(this)
    }

    private fun updatePresetColor() {
        updateColor(binding.preset1, preset1Value.getInt(Color.parseColor(presetColors[0])))
        updateColor(binding.preset2, preset2Value.getInt(Color.parseColor(presetColors[1])))
        updateColor(binding.preset3, preset3Value.getInt(Color.parseColor(presetColors[2])))
        updateColor(binding.preset4, preset4Value.getInt(Color.parseColor(presetColors[3])))
        updateColor(binding.preset5, preset5Value.getInt(Color.parseColor(presetColors[4])))
    }

    private fun updateFavColorByIds() {
        updateColor(binding.fav1, fav1Value.getInt(Color.parseColor(DEFAULT_CELL_COLOR)), true)
        updateColor(binding.fav2, fav2Value.getInt(Color.parseColor(DEFAULT_CELL_COLOR)), true)
        updateColor(binding.fav3, fav3Value.getInt(Color.parseColor(DEFAULT_CELL_COLOR)), true)
        updateColor(binding.fav4, fav4Value.getInt(Color.parseColor(DEFAULT_CELL_COLOR)), true)
        updateColor(binding.fav5, fav5Value.getInt(Color.parseColor(DEFAULT_CELL_COLOR)), true)

    }

    private fun saveFavColors() {
        if (favColors.size < 5 && fav1Value.getInt(-1) != -1)
            return
        fav1Value.putInt(Color.parseColor(favColors[0].color))
        fav2Value.putInt(Color.parseColor(favColors[1].color))
        fav3Value.putInt(Color.parseColor(favColors[2].color))
        fav4Value.putInt(Color.parseColor(favColors[3].color))
        fav5Value.putInt(Color.parseColor(favColors[4].color))
    }

    private fun updateColor(circleImage: CircleImage, color: Int, isFavorite: Boolean = false) {
        if (selectedId == circleImage.id) {
            circleImage.selectedColor(selectedColor)

            if (!isFavorite) {
                updatePresetColor(circleImage.id)
            } else {
                updateFavoriteColor(circleImage.id)
            }
        } else {
            circleImage.setColor(color)
        }
        circleImage.setOnClickListener(this)
    }

    private fun updatePresetColor(id: Int) {
        when (id) {
            R.id.preset1 -> {
                preset1Value.putInt(selectedColor)
            }
            R.id.preset2 -> {
                preset2Value.putInt(selectedColor)
            }
            R.id.preset3 -> {
                preset3Value.putInt(selectedColor)
            }
            R.id.preset4 -> {
                preset4Value.putInt(selectedColor)
            }
            R.id.preset5 -> {
                preset5Value.putInt(selectedColor)
            }
        }
    }

    private fun updateFavoriteColor(id: Int) {
        when (id) {
            R.id.fav1 -> {
                fav1Value.putInt(selectedColor)
            }
            R.id.fav2 -> {
                fav2Value.putInt(selectedColor)
            }
            R.id.fav3 -> {
                fav3Value.putInt(selectedColor)
            }
            R.id.fav4 -> {
                fav4Value.putInt(selectedColor)
            }
            R.id.fav5 -> {
                fav5Value.putInt(selectedColor)
            }
        }
    }

    override fun onClick(v: View) {
        selectedFavIndex = -1
        if (R.id.ivLightsRdColor == v.id) {
            canShowColor = if (canShowColor < 3) ++canShowColor else 0
            changePickerViews()
            return
        }
        selectedId = v.id

        when (selectedId) {
            R.id.ivLightsRdColor -> {
                canShowColor = if (canShowColor < 3) ++canShowColor else 0
                changePickerViews()
            }
            R.id.ivLightsFav -> {
                val tag = (v as ImageView).tag as String?
                if (tag != null && tag == "s") {
                    v.setImageResource(R.drawable.ic_baseline_favorite_us)
                    v.tag = "us"
                    val hexColor = toHexARGB(selectedColor)
//                    removeFavColorsForDevice(hexColor)
                } else {
                    v.setImageResource(R.drawable.ic_baseline_favorite_s)
                    v.tag = "s"
                    val hexColor = toHexARGB(selectedColor)

                    if (selectedFavColorData != null) {
                        if (mainActivity.isRGBMode) {
                            updateFavColorByIds(
                                selectedFavColorData?.dcid ?: -1,
                                hexColor
                            )
                        } else {
                            mRGBViewModel.updateRGBColor(
                                requireContext().getAccessToken(),
                                selectedFavColorData?.dcidServer ?: 0,
                                hexColor,
                                1,
                                if(favSelectedTop){ 1 } else if (mRGBViewModel?.onServerFavoriteItemSelected?.value?.is_top == true){ 1 }else {0},
                                mainActivity.device_id
                            )
                        }
                        favSelectedTop=false
                    } /*else {
//                        addFavouriteColorForDevice(hexColor)
                    }*/
                }
            }
            R.id.ivAdvancedColorPalette -> {
                val advPaletteVisible = binding.clAdvancedPalette.visibility == View.VISIBLE
                binding.clAdvancedPalette.visibility =
                    if (advPaletteVisible) View.GONE else View.VISIBLE
                binding.clBrightness.gone()
            }
            R.id.btnBrightness -> {
                val advPaletteVisible = binding.clBrightness.visibility == View.VISIBLE
                binding.clAdvancedPalette.gone()
                binding.clBrightness.visibility = if (advPaletteVisible) View.GONE else View.VISIBLE
            }

            R.id.preset1 -> {
                updateFavIconDataAndUI(false)
                selectedColor = preset1Value.getInt(Color.parseColor(presetColors[0]))
            }
            R.id.preset2 -> {
                updateFavIconDataAndUI(false)
                selectedColor = preset2Value.getInt(Color.parseColor(presetColors[1]))
            }
            R.id.preset3 -> {
                updateFavIconDataAndUI(false)
                selectedColor = preset3Value.getInt(Color.parseColor(presetColors[2]))
            }
            R.id.preset4 -> {
                updateFavIconDataAndUI(false)
                selectedColor = preset4Value.getInt(Color.parseColor(presetColors[3]))
            }
            R.id.preset5 -> {
                updateFavIconDataAndUI(false)
                selectedColor = preset5Value.getInt(Color.parseColor(presetColors[4]))
            }

            R.id.fav1 -> {
                favSelectedTop=true
                selectedFavIndex = 0
                selectedColor =
                    fav1Value.getInt(Color.parseColor(favColors[selectedFavIndex].color))
                if (toHexARGB(selectedColor) != DEFAULT_CELL_COLOR)
                    updateFavIconDataAndUI(true, selectedFavIndex)
            }
            R.id.fav2 -> {
                favSelectedTop=true
                selectedFavIndex = 1
                selectedColor =
                    fav2Value.getInt(Color.parseColor(favColors[selectedFavIndex].color))
                if (toHexARGB(selectedColor) != DEFAULT_CELL_COLOR)
                    updateFavIconDataAndUI(true, selectedFavIndex)
            }
            R.id.fav3 -> {
                favSelectedTop=true
                selectedFavIndex = 2
                selectedColor =
                    fav3Value.getInt(Color.parseColor(favColors[selectedFavIndex].color))
                if (toHexARGB(selectedColor) != DEFAULT_CELL_COLOR)
                    updateFavIconDataAndUI(true, selectedFavIndex)
            }
            R.id.fav4 -> {
                favSelectedTop=true
                selectedFavIndex = 3
                selectedColor =
                    fav4Value.getInt(Color.parseColor(favColors[selectedFavIndex].color))
                if (toHexARGB(selectedColor) != DEFAULT_CELL_COLOR)
                    updateFavIconDataAndUI(true, selectedFavIndex)
            }
            R.id.fav5 -> {
                favSelectedTop=true
                selectedFavIndex = 4
                selectedColor =
                    fav5Value.getInt(Color.parseColor(favColors[selectedFavIndex].color))
                if (toHexARGB(selectedColor) != DEFAULT_CELL_COLOR)
                    updateFavIconDataAndUI(true, selectedFavIndex)
            }
        }

        binding.colorPickerView.setInitialColor(selectedColor)
        binding.colorPickerRd.setColor(selectedColor)
        binding.colorPickerSq.setColor(selectedColor, false)
        val rgb = getColorIntToRGB(selectedColor)
        sendColorTapData(rgb)
        updateStandardColor()
        updateFavColorByIds()
        //updatePresetColor()
        //updateFavColor()
    }

    private fun changePickerViews() {
        if (selectedPickerType == 2) {
            selectedPickerType = 0
        } else {
            selectedPickerType++
        }
        when (selectedPickerType) {
            TYPE_WHEEL -> {
                binding.colorPickerRd.visible()
                binding.colorPickerView.gone()
                binding.colorPickerSq.gone()
            }
            TYPE_HUE -> {
                binding.colorPickerRd.gone()
                binding.colorPickerView.visible()
                binding.colorPickerSq.gone()
            }
            TYPE_SQUARE -> {
                binding.colorPickerRd.gone()
                binding.colorPickerView.gone()
                binding.colorPickerSq.visible()
            }
        }
    }

    fun updateFavIconDataAndUI(favorite: Boolean, position: Int = -1) {
        if (position > -1) {
            selectedFavColorData = favColors[position]
        } else if (selectedFavIndex > -1) {
            var data = mRGBViewModel.onServerFavoriteItemSelected.value
            selectedFavColorData = FavColorData(
                device_id = data?.deviceId ?: 0,
                color = data?.color ?: DEFAULT_CELL_COLOR,
                dcidServer = data?.dcid ?: 0
            )
        }
        favSelected = favorite
        binding.ivLightsFav.setImageResource(if (favSelected) R.drawable.ic_baseline_favorite_s else R.drawable.ic_baseline_favorite_us)
        binding.ivLightsFav.tag = if (favSelected) "s" else "us"
    }

    private fun updateFavColorByIds(dcid: Int, updateColor: String) {
//        if (mainActivity.isRGBMode)
//            return
        if (dcid != -1) {
            favColors[dcid] = FavColorData(-1, updateColor, dcid)
            saveFavColors()
            updateFavColorByIds()
        }
    }


    private fun sendColorWheelAdjustmentData(rgb: Array<Int>) {
        if ((System.currentTimeMillis() - rgbColorWheelReceivedTimeStamp) >= COLOR_WHEEL_INTERVAL_DELAY_TIME) {
            mRGBViewModel.sendColorAdjustment(mBrightness, rgb[0], rgb[1], rgb[2])
            rgbColorWheelReceivedTimeStamp = System.currentTimeMillis()
        }
    }

    private fun sendColorTapData(rgb: Array<Int>) {
        if ((System.currentTimeMillis() - rgbColorTapReceivedTimeStamp) > BLE_CONNECTION_INTERVAL_DELAY_TIME) {
            mRGBViewModel.sendColorAdjustment(mBrightness, rgb[0], rgb[1], rgb[2])
            rgbColorTapReceivedTimeStamp = System.currentTimeMillis()
        }
    }
}
