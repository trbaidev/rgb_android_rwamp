package com.aquatic.av.screens.dealerLocator.model


import com.google.gson.annotations.SerializedName

data class DealerSuggestionResponse(
    var responseCode: Int? = 0,
    var responseData: ArrayList<DealerItem>? = arrayListOf(),
    var responseMessage: String? = ""
) {

    data class DealerItem(
        @SerializedName("dealer_id")
        var dealerId: Int = 0,
        @SerializedName("dealer_name")
        var dealerName: String = "",
        @SerializedName("dealer_type")
        var dealerType: String = "",
        var email: String = "",
        var fulladdress: String = "",
        var lat: Double = 0.0,
        var longitude: Double = 0.0,
        var miles: Int = 0,
        var phone: String = "",
        var tags: String = "",
        var website: String = "",
        var  distance:Float = 0f
    )
    {
        fun getMilesAway(): String {
            return "$miles miles away"
        }
    }
}