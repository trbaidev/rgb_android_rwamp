package com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels

import com.google.gson.JsonArray

class AddDeviceModel(
    var device_name: String? = "",
    var device_type: String? = "",
    var env_icon_name: String? = "",
    var env_name: String? = "",
    var user_email: String? = "",
    var created_by: String? = "",
    var modified_by: String? = "",
    var product_id: Int? = -1,
    var product_type_id: Int? = -1,
    var purchase_location_dealer_id: Int? = 0,
    var purchase_location: String? = "",
    var serial_no: String? = "",
    var purchase_date: String? = "",
    var receipt: String? = "",
    var env_id: Int? = 0,
    var user_id: Int? = 0,
    var dealer_id: Int? = 0,
    var device_id: Int? = 0,
    var software_version: String? = "",
    var sku: String? = "",
    var last_connected: String? = "",
    var favColors: JsonArray? = JsonArray(0)
)