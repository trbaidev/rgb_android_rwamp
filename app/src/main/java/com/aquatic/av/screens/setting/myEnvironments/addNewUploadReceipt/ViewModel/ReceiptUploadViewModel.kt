package com.aquatic.av.screens.setting.myEnvironments.addNewUploadReceipt.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.ReceiptUploadResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File


@HiltViewModel
class ReceiptUploadViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel() {

    private val _receiptUploadResults = MutableLiveData<Event<State<ReceiptUploadResponse>>>()
    val receiptUploadResults: LiveData<Event<State<ReceiptUploadResponse>>> get() = _receiptUploadResults


    fun postReceipt(token: String, file: File) {
        val body = file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
        val part = MultipartBody.Part.createFormData("", file.name, body)
        viewModelScope.launch {
            authRepository.postReceipt(token, part).collect {
                _receiptUploadResults.value = Event(it)
            }
        }
    }
}