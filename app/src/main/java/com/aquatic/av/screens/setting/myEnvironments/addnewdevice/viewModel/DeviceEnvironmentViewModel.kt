package com.aquatic.av.screens.setting.myEnvironments.addnewdevice.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DeviceEnvironmentViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _deviceEnvironmentResults = MutableLiveData<Event<State<Any>>>()
    val deviceEnvironmentResults: LiveData<Event<State<Any>>> get() = _deviceEnvironmentResults

    fun getAllEnvironments(userOnboardingEnvironmentModel: Any) {
        viewModelScope.launch {
            authRepository.getAllEnvironments(userOnboardingEnvironmentModel).collect {
                _deviceEnvironmentResults.value = (Event(it))
            }
        }
    }
}