package com.aquatic.av.screens.setting.myDevices.dataModels

import com.aquatic.av.screens.dashboard.dataModel.DashboardItemModel

data class MyDeviceResponse(
    var responseCode: Int,
    var responseMessage: String,
    var responseData: List<DashboardItemModel>
)