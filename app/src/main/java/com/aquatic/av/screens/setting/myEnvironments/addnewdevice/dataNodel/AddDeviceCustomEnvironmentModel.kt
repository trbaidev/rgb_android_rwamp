package com.aquatic.av.screens.setting.myEnvironments.addnewdevice.dataNodel

data class AddDeviceCustomEnvironmentModel (   var environmentId: Int = -1,
                                               var environmentName: String = "",
                                               var environmentType: String = "",
                                               var env_Icon_Name: String? = "",
                                               var isActive: Int = 0,
                                               var userIds: Array<Int>? = null)