package com.aquatic.av.screens.setting.myEnvironments.addnewdevice.dataNodel

data class AddDeviceCustomEnvironmentResponse(var responseCode: Int,
                                              var responseData: Int,
                                              var responseMessage: String)