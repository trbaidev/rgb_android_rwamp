package com.aquatic.av.screens.onBoardingFlow.RegisterScanner

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceUtil
import com.aquatic.av.bluetoothScanner.ble.utils.BLEUtils
import com.aquatic.av.bluetoothScanner.ble.utils.BTCon
import com.aquatic.av.databinding.*
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.bluetooth.viewModel.BleConnectionViewModel
import com.aquatic.av.screens.bluetooth.viewModel.ScannerLiveData
import com.aquatic.av.screens.bluetooth.viewModel.ScannerViewModel
import com.aquatic.av.screens.onBoardingFlow.RegisterScanner.binder.RegisterScannerAdapter
import com.aquatic.av.utils.BasePreferenceHelper.setLoginStatus
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterScannerFragment :
    BaseFragment<FragmentRegisterScannerBinding>(R.layout.fragment_register_scanner) {

    private val REQUEST_CODE_BLE_CONNECTION = 1003
    private val REQUEST_ACCESS_COARSE_LOCATION = 1022
    private val REQUEST_WRITE_EXTERNAL_STORAGE = 1001
    private val mScannerViewModel by viewModels<ScannerViewModel>()
    private val bleViewModel by viewModels<BleConnectionViewModel>()
     var device_name: String? = ""
    var device_id = -1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setScannerListenr()
        setBleScannerListner()
        binding.recyclerViewBleDevices.layoutManager = LinearLayoutManager(mainActivity)
        val dividerItemDecoration =
            DividerItemDecoration(mainActivity, DividerItemDecoration.VERTICAL)
        binding.recyclerViewBleDevices.addItemDecoration(dividerItemDecoration)
        (binding.recyclerViewBleDevices.itemAnimator as SimpleItemAnimator?)!!.supportsChangeAnimations =
            false
        binding.recyclerViewBleDevices.adapter = rehisterScannerAdapter
        binding.apply {
            noLocationPermission.actionGrantLocationPermission.setOnClickListener {
                BLEUtils.markLocationPermissionRequested(mainActivity)
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_ACCESS_COARSE_LOCATION
                )
            }
            noLocationPermission.actionPermissionSettings.setOnClickListener {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                intent.data = Uri.fromParts("package", requireContext()?.packageName, null)
                startActivity(intent)
            }
            noDevices.actionEnableLocation.setOnClickListener {
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            bluetoothOff.actionEnableBluetooth.setOnClickListener {
                val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivity(enableIntent)
            }

            bleSwipeContainer.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
                override fun onRefresh() {
                    stopScan();
                    mScannerViewModel.restartScan();
                }
            })
            btnNext.setOnClickListener {
                requireContext().setLoginStatus(true)
                bottomNavigation?.visible()
                mainActivity.initApp(isRGBMode = false)
            }

            btnBack.setOnClickListener(View.OnClickListener { mainActivity.popFragment() })
        }
    }

    private val rehisterScannerAdapter by lazy {
        BaseRecyclerViewAdapter(
            arrayListOf(),
            registerScannerBinder,
            requireContext()
        )
    }

    private val registerScannerBinder by lazy {
        RegisterScannerAdapter { entity, position ->
            updateData(position)
        }
    }

    private fun updateData(position: Int) {
        binding.connectionStateLayout!!.visibility = View.VISIBLE
        bleViewModel.connect(mScannerViewModel.scannerState.devices[position])
    }

    private fun setScannerListenr() {
        rehisterScannerAdapter.addAll(mScannerViewModel.scannerState.devices)
        mScannerViewModel.scannerState.observe(viewLifecycleOwner) { devices ->
            val i: Int? = devices.getUpdatedDeviceIndex()
            i?.let { rehisterScannerAdapter.notifyItemChanged(it) }
                ?: rehisterScannerAdapter.notifyDataSetChanged()
        }

        mScannerViewModel.scannerState.observe(viewLifecycleOwner) { scannerLiveData ->
            val scanstate: ScannerLiveData.SCANSTATE = scannerLiveData.getScanState()
            when (scanstate) {
                ScannerLiveData.SCANSTATE.NONE -> startScan(scannerLiveData)
                ScannerLiveData.SCANSTATE.STARTED -> onScanStarted()
                ScannerLiveData.SCANSTATE.STOPPED -> onScanStopped(scannerLiveData)
            }
            if (scanstate !== ScannerLiveData.SCANSTATE.NONE && !scannerLiveData.isBluetoothEnabled()) {
                onBluetoothDisabled()
            }
        }
    }

    private fun setBleScannerListner() {
        bleViewModel.isConnected.observe(viewLifecycleOwner) { connected ->
            if (!connected) {
                BTCon.setConnectionState(false)
                val prefUtils = PreferenceUtil.getInstance()
                prefUtils.setStringValue("connected_device_name", "")
                prefUtils.setIntValue("connected_device_id", -1)
                Toast.makeText(mainActivity, R.string.state_disconnected, Toast.LENGTH_SHORT).show()
                mainActivity.popFragment()
            } else {
                BTCon.setConnectionState(true)
                val prefUtils = PreferenceUtil.getInstance()
                prefUtils.setStringValue("connected_device_name", device_name)
                prefUtils.setIntValue("connected_device_id", device_id)
                mainActivity.popFragment()
            }
        }

        bleViewModel.isSupported.observe(viewLifecycleOwner) { supported ->
            if (!supported) {
                Toast.makeText(mainActivity, R.string.state_not_supported, Toast.LENGTH_SHORT)
                    .show()
            }
        }

        bleViewModel.isDeviceReady.observe(viewLifecycleOwner) { deviceReady ->
            binding.connectionStateLayout!!.visibility = View.GONE
        }
        bleViewModel.connectionState.observe(viewLifecycleOwner, object : Observer<String?> {
            override fun onChanged(message: String?) {
                binding.connectionState!!.text = message
            }
        })
    }


    override fun onStop() {
        super.onStop()
        stopScan()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_ACCESS_COARSE_LOCATION -> mScannerViewModel!!.refresh()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_BLE_CONNECTION && resultCode == Activity.RESULT_OK) {
            mainActivity.popFragment()
        }
    }

    private fun startScan(state: ScannerLiveData) {
        if (BLEUtils.isLocationPermissionsGranted(mainActivity)) {
            binding.noLocationPermission!!.root.visibility = View.GONE
            if (state.isBluetoothEnabled()) {
                binding.bluetoothOff!!.root.visibility = View.GONE
                mScannerViewModel!!.startScan()
                binding.connectionStateLayout!!.visibility = View.VISIBLE
                if (state.isEmpty()) {
                    binding.noDevices!!.root.visibility = View.VISIBLE
                    if (!BLEUtils.isLocationRequired(mainActivity) || BLEUtils.isLocationEnabled(
                            mainActivity
                        )
                    ) {
                        binding.noDevices.noLocation!!.visibility = View.INVISIBLE
                    } else {
                        binding.noDevices.noLocation!!.visibility = View.VISIBLE
                    }
                } else {
                    binding.noDevices!!.root.visibility = View.GONE
                }
            } else {
                binding.bluetoothOff!!.root.visibility = View.VISIBLE
                binding.connectionStateLayout!!.visibility = View.INVISIBLE
                binding.noDevices!!.root.visibility = View.GONE
            }
        } else {
            binding.noLocationPermission!!.root.visibility = View.VISIBLE
            binding.bluetoothOff!!.root.visibility = View.GONE
            binding.connectionStateLayout!!.visibility = View.INVISIBLE
            binding.noDevices!!.root.visibility = View.GONE
            val deniedForever: Boolean = BLEUtils.isLocationPermissionDeniedForever(mainActivity)
            binding.bluetoothOff.actionEnableBluetooth!!.visibility =
                if (deniedForever) View.GONE else View.VISIBLE
            binding.noLocationPermission.actionPermissionSettings!!.visibility =
                if (deniedForever) View.VISIBLE else View.GONE
        }
    }

    private fun stopScan() {
        binding.connectionStateLayout!!.visibility = View.INVISIBLE
        mScannerViewModel!!.stopScan()
    }

    private fun onScanStarted() {
        binding.noDevices!!.root.visibility = View.GONE
        binding.bluetoothOff!!.root.visibility = View.GONE
        binding.noLocationPermission!!.root.visibility = View.GONE
        binding.connectionState!!.text = getString(R.string.scanning)
        binding.connectionStateLayout!!.visibility = View.VISIBLE
    }

    private fun onScanStopped(scannerLiveData: ScannerLiveData) {
        binding!!.bleSwipeContainer.isRefreshing = false
        binding.bluetoothOff!!.root.visibility = View.GONE
        binding.connectionStateLayout!!.visibility = View.INVISIBLE
        if (scannerLiveData.isEmpty()) {
            binding.noDevices!!.root.visibility = View.VISIBLE
            if (!BLEUtils.isLocationRequired(mainActivity) || BLEUtils.isLocationEnabled(
                    mainActivity
                )
            ) {
                binding.noDevices.noLocation!!.visibility = View.INVISIBLE
            } else {
                binding.noDevices.noLocation!!.visibility = View.VISIBLE

            }
        }
        if (!BLEUtils.isWriteStoragePermissionsGranted(mainActivity) && BLEUtils.isMarshmallowOrAbove()) {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_WRITE_EXTERNAL_STORAGE
            )
        }
    }

    private fun onBluetoothDisabled() {
        binding.bluetoothOff!!.root.visibility = View.VISIBLE
        mScannerViewModel!!.restartScan()
    }


    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }


    companion object {
        fun getInstance(
            device_name: String?,
            device_id: Int
        ): RegisterScannerFragment {
            val registerScannerFragment = RegisterScannerFragment()
            registerScannerFragment.device_name = device_name
            registerScannerFragment.device_id = device_id
            return registerScannerFragment
        }
    }
}