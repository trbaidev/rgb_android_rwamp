package com.aquatic.av.screens.setting

import android.os.Bundle
import android.view.View
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentHelpGuideBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView

class HelpGuideFragment: BaseFragment<FragmentHelpGuideBinding>(R.layout.fragment_help_guide) {
    var isTapped = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnBack.setOnClickListener { mainActivity.onBackPressed() }
            btnNextBack.setOnClickListener {
                if (isTapped) {
                    isTapped = false
                    ivLeftArrow.visible()
                    ivRightArrow.gone()
                    ivTutorial.setImageResource(R.drawable.help_two)
                } else {
                    isTapped = true
                    ivLeftArrow.gone()
                    ivRightArrow.visible()
                    ivTutorial.setImageResource(R.drawable.help_one)
                }
            }
        }
    }
     override fun setHeaderFooterViews(titleBar: TitleBar?, bottomNavigationView: BottomNavigationView?) {
        super.setHeaderFooterViews(titleBar,bottomNavigationView)
        titleBar?.hideTitleBar()
    }
}