package com.aquatic.av.screens.onBoardingFlow.userEnvironment.dataModels

data class UserCustomEnvironmentResponse(var responseCode: Int,
                                         var responseData: Int,
                                         var responseMessage: String)