package com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentAddNewDeviceBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.setting.myEnvironments.addNewDeviceName.AddNewDeviceNameFragment
import com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice.binders.AddNewSelectDeviceBinder
import com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice.dataModels.AddNewSelectDeviceResponse
import com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice.dataModels.AddNewSelectedDeviceModel
import com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice.viewModel.AddNewSelectDeviceViewModel
import com.aquatic.av.screens.setting.myEnvironments.dataModels.EnvironmentReferenceModel
import com.aquatic.av.utils.EditTextExtension.getGenericList
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddNewSelectDeviceFragment :
    BaseFragment<FragmentAddNewDeviceBinding>(R.layout.fragment_add_new_device) {
    lateinit var environment_ref: EnvironmentReferenceModel
    private lateinit var addNewDeviceResponses: List<AddNewSelectDeviceResponse>
    private val viewModel by viewModels<AddNewSelectDeviceViewModel>()
    private val addNewDeviceItemAdapter by lazy {
        BaseRecyclerViewAdapter(
            arrayListOf(),
            addNewDeviceBinder,
            requireContext()
        )
    }

    private val addNewDeviceBinder by lazy {
        AddNewSelectDeviceBinder { entity, position ->
            updateData(position)
        }
    }

    private fun updateData(position: Int) {
        var item = addNewDeviceResponses.get(position)
        environment_ref.product_id = item.product_id
        environment_ref.product_img=item.product_image
        environment_ref.product_code="(" + item.product_code + ")"
        mainActivity.navigateTo(
            AddNewDeviceNameFragment.getInstance(
                item,environment_ref
            )
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.prRecyclerView.adapter = addNewDeviceItemAdapter
        binding.prRecyclerView.layoutManager = GridLayoutManager(mainActivity, 2)
        setListeners()
        binding.apply {
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnNext.setOnClickListener {
                showToast("Please select a device to continue")
            }
        }
        viewModel.getAllProductsByTypeId(AddNewSelectedDeviceModel(environment_ref.product_type_id!!))
    }


    private fun setListeners() {
        viewModel.userRegSelectDeviceResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        it.body?.apply {
                            var list = Gson().getGenericList(
                                Gson().toJson(it.body),
                                AddNewSelectDeviceResponse::class.java
                            )
                            addNewDeviceResponses = list
                            addNewDeviceItemAdapter.addAll(
                                addNewDeviceResponses
                            )
                        }
                    }
                }
            }
        })
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }

    companion object {
        fun getInstance(
            environment_ref: EnvironmentReferenceModel
        ): AddNewSelectDeviceFragment {
            val add_new_device = AddNewSelectDeviceFragment()
            add_new_device.environment_ref = environment_ref

            return add_new_device
        }
    }

}