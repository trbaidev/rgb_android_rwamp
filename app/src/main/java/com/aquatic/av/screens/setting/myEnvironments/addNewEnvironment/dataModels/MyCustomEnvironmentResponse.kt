package com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.dataModels

data class MyCustomEnvironmentResponse(var responseCode: Int,
                                       var responseData: Int,
                                       var responseMessage: String)