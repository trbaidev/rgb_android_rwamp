package com.aquatic.av.screens.onBoardingFlow.userRegSelectProductType.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserRegSelectProductTypeViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _userRegSelectProductTypeResponseResults = MutableLiveData<Event<State<ArrayList<Any>>>>()
    val userRegSelectProductTypeResponseResults: LiveData<Event<State<ArrayList<Any>>>> get() = _userRegSelectProductTypeResponseResults

    fun getAllProductTypes() {
        viewModelScope.launch {
            authRepository.getAllProductTypes().collect {
                _userRegSelectProductTypeResponseResults.value = (Event(it))
            }
        }
    }
}