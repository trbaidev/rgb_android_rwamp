package com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice.dataModels

data class AddNewSelectDeviceResponse(
    var product_id: Int,
    var product_name: String,
    var product_code: String,
    var base_path: String,
    var product_image: String,
    var is_serial_number: Boolean,
    var is_ble_device: Boolean,
    var is_mobile_app: Boolean
)

