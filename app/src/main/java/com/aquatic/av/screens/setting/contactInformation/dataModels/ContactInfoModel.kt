package com.aquatic.av.screens.setting.contactInformation.dataModels

data  class ContactInfoModel(
    var address: String?,
    var city: String?,
    var state: String?,
    var zip_code: String?,
    var country: String?,
    var phone: String?,
    var user_email:String?
)