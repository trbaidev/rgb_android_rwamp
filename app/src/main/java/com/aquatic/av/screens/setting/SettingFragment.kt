package com.aquatic.av.screens.setting

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentSettingBinding
import com.aquatic.av.screens.accountAuthentication.AccountFragment
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.bluetooth.ConnectedDeviceFragment
import com.aquatic.av.screens.bluetooth.ScannerFragment
import com.aquatic.av.screens.dashboard.DashboardViewModel
import com.aquatic.av.screens.dealerLocator.DealerLocatorFragment
import com.aquatic.av.screens.setting.myAccount.MyAccountFragment
import com.aquatic.av.utils.BasePreferenceHelper.clearAllPreferences
import com.aquatic.av.utils.BasePreferenceHelper.getUserDetailModel
import com.aquatic.av.utils.BasePreferenceHelper.isLogin
import com.aquatic.av.utils.BasePreferenceHelper.setLoginStatus
import com.aquatic.av.utils.Utils
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.isDeviceConnected
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import io.intercom.android.sdk.Intercom
import io.intercom.android.sdk.identity.Registration

class SettingFragment : BaseFragment<FragmentSettingBinding>(R.layout.fragment_setting) {
    private val userModule by lazy {
        requireContext().getUserDetailModel()
    }
    private val viewModel by viewModels<DashboardViewModel>()

    private val onClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.btnUserDetail -> {

            }
            R.id.btnAccount -> {
                mainActivity.navigateTo(MyAccountFragment())
            }
            R.id.btnDealerLocator, R.id.btnDealerLocators -> {
                mainActivity.navigateTo(DealerLocatorFragment())
            }
            R.id.btnSupport -> {
                navigateIntercom()
            }
            R.id.btnLogout -> {
                mainActivity.isRGBMode = false
                requireContext().setLoginStatus(false)
                requireContext().clearAllPreferences()
                mainActivity.popBackStackTillEntry(0)
                mainActivity.navigateTo(AccountFragment())
            }
            R.id.btnNearbyDevices -> {
                if (isDeviceConnected()) {
                    mainActivity.navigateTo(ConnectedDeviceFragment.newInstance("", -1))
                } else {
                    mainActivity.navigateTo(ScannerFragment.newInstance("", -1))
                }
            }
            R.id.btnSwitchAccount -> {
                mainActivity.popBackStackTillEntry(0)
                mainActivity.navigateTo(AccountFragment())
            }
            R.id.btnAbout, R.id.btnAboutApp -> {
                mainActivity.navigateTo(AboutUsFragment())
            }
            R.id.btnHelp, R.id.btnHelpGuide -> {
                mainActivity.navigateTo(HelpGuideFragment())
            }
        }
    }

    private fun navigateIntercom() {
        if (!requireContext().isLogin()) {
            Intercom.client().setUserHash(Utils.getSha256HMACFromID(Utils.getDeviceUniqueID()))
            Intercom.client().registerIdentifiedUser(
                Registration.create()
                    .withEmail(Utils.getSha256HMACFromID(Utils.getDeviceUniqueID()))
            )

//            FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
//                if (task.isSuccessful) {
//                    task.result?.let {
//                        Intercom.client().setUserHash(Utils.getSha256HMACFromID(it.id))
//                    }
//                }
//            }
        } else {
            var userData = requireContext().getUserDetailModel()
            Intercom.client()
                .setUserHash(Utils.getSha256HMACFromID(userData?.user_id.toString() + "_" + userData?.user_email))
            Intercom.client()
                .registerIdentifiedUser(Registration.create().withUserId(userData?.user_id.toString() + "_" + userData?.user_email))
        }

        Intercom.client().displayMessenger()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            dataItem = userModule
            txtUserName.text = userModule?.first_name ?: "N/A"
            btnNearbyDevices.setOnClickListener(onClickListener)
            btnHelp.setOnClickListener(onClickListener)
            btnHelpGuide.setOnClickListener(onClickListener)
            btnAbout.setOnClickListener(onClickListener)
            btnAboutApp.setOnClickListener(onClickListener)
            btnSwitchAccount.setOnClickListener(onClickListener)

            btnUserDetail.setOnClickListener(onClickListener)
            btnAccount.setOnClickListener(onClickListener)
            btnDealerLocator.setOnClickListener(onClickListener)
            btnDealerLocators.setOnClickListener(onClickListener)
            btnSupport.setOnClickListener(onClickListener)
            btnLogout.setOnClickListener(onClickListener)
            if (!requireContext().isLogin()) {
                containerRGBMode.visible()
                containerAuthSetting.gone()
            } else {
                containerRGBMode.gone()
                containerAuthSetting.visible()
            }
        }

        mainActivity.setEnvironmentSelectionListener {
            mainActivity.bottomNavigation?.selectedItemId = R.id.navigation_home
            mainActivity.bottomNavigation?.menu?.findItem(R.id.navigation_home)?.isVisible = true
        }

    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.showTitleBar()
        bottomNavigationView?.visible()
        titleBar?.hideButtons()
    }

}