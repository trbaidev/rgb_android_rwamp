package com.aquatic.av.screens.dealerLocator.model

data class GoogleAutoCompleteResult(
    var business_status: String? = "",
    var formatted_address: String? = "",
    var icon: String? = "",
    var icon_background_color: String? = "",
    var icon_mask_base_uri: String? = "",
    var name: String? = "",
    var place_id: String? = "",
    var rating: Double? = 0.0,
    var reference: String? = "",
    var user_ratings_total: Double? = 0.0,
    var types: ArrayList<String>,
    var opening_hours: Openinghours,
    var plus_code: PlusCode,
    var geometry: Geometry
)