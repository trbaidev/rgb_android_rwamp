package com.aquatic.av.screens.accountAuthentication.resetPassword.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.accountAuthentication.createAccount.dataModels.EmailCheckModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ForgotPwdViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel() {

    private val _emailPwdResults = MutableLiveData<Event<State<EmailCheckModel>>>()
    val emailPwdResults: LiveData<Event<State<EmailCheckModel>>> get() = _emailPwdResults

    fun verifyUserEmail(query: String) {
        viewModelScope.launch {
            authRepository.verifyUserEmail(query).collect {
                _emailPwdResults.value = (Event(it))
            }
        }
    }


}