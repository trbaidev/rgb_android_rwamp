package com.aquatic.av.screens.accountAuthentication.createAccount

import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentCreateAccountBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.accountAuthentication.createAccount.viewModel.AccountAuthenticationViewModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.UserAddressInfoFragment
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.utils.EditTextExtension.getEncryptPassword
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.aquatic.av.views.dialogs.DialogFactory.showDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateAccountFragment :
        BaseFragment<FragmentCreateAccountBinding>(R.layout.fragment_create_account) {

    private val viewModel by viewModels<AccountAuthenticationViewModel>()
    private var userAddressModel: UserAddressModel? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setListeners()
        binding.apply {
            btnNext.isSelected = false
            btnNext.setOnClickListener {
                if (checkFieldsValidation()) {
                    if (Patterns.EMAIL_ADDRESS.matcher(binding.edtUserEmail.text.trim().toString()).matches()) {
                        viewModel.emailCheck(binding.edtUserEmail.text.trim().toString())
                    }
                }
            }
            btnBack.setOnClickListener {
                mainActivity.onBackPressed()
            }
        }
    }

    private fun checkFieldsValidation(): Boolean {
        val fName: String = binding.edtUserFirstName.text.trim().toString()
        val lName: String = binding.edtUserLast.text.trim().toString()
        val email: String = binding.edtUserEmail.text.trim().toString()
        val password: String = binding.edtUserPwd.text.trim().toString()
        val cnfPassword: String = binding.edtCnfrmPwd.text.trim().toString()
        when {
            TextUtils.isEmpty(fName) -> {
                binding.edtUserFirstName.error = "Please enter first name"
                return false
            }
            TextUtils.isEmpty(lName) -> {
                binding.edtUserLast.error = "Please enter last name"
                return false
            }
            TextUtils.isEmpty(email) -> {
                binding.edtUserEmail.error = "Please enter email"
                return false
            }
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                binding.edtUserEmail.error = "Please enter valid email"
                return false
            }
            TextUtils.isEmpty(password) -> {
                binding.edtUserPwd.error = "Please provide password"
                return false
            }
            TextUtils.isEmpty(cnfPassword) -> {
                binding.edtCnfrmPwd.error = "Please re-enter password"
                return false
            }
            password != cnfPassword -> {
                binding.edtCnfrmPwd.error = "Password doesn't match"
                return false
            }
            else -> {
                return true
            }
        }
        return true
    }

    private fun setListeners() {
        viewModel.emailCheckResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    //   showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        if (it.body.user_exist.equals("true", true)) {
                            binding.btnNext.isSelected = false
                            showDialog(
                                    "User already exists",
                                    "Please login with your credentials or reset your password if you don't recall it.", true
                            )
                        }else{
                            binding.apply {
                                userAddressModel = UserAddressModel(
                                        edtUserFirstName.text.trim().toString(),
                                        edtUserLast.text.trim().toString(),
                                        edtUserEmail.text.trim().toString(),
                                        edtUserPwd.getEncryptPassword(),
                                        edtUserPwd.text.trim().toString()
                                )
                                mainActivity.navigateTo(UserAddressInfoFragment.getInstance(userAddressModel))
                            }

                        }
                    }
                }
            }
        })
    }


    override fun setHeaderFooterViews(
            titleBar: TitleBar?,
            bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
    }
}