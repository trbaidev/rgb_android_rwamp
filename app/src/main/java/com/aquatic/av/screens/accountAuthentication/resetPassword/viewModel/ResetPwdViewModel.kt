package com.aquatic.av.screens.accountAuthentication.resetPassword.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.accountAuthentication.resetPassword.dataModels.ResetPasswordModel
import com.aquatic.av.screens.accountAuthentication.resetPassword.dataModels.ResetPasswordResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ResetPwdViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel() {

    private val _resetPwdResults = MutableLiveData<Event<State<ResetPasswordResponse>>>()
    val resetPwdResults: LiveData<Event<State<ResetPasswordResponse>>> get() = _resetPwdResults

    fun resetPassword(resetPasswordModel: ResetPasswordModel) {
        viewModelScope.launch {
            authRepository.resetPassword(resetPasswordModel).collect {
                _resetPwdResults.value = (Event(it))
            }
        }
    }
}