package com.aquatic.av.screens.dealerLocator.model


data class DealerFilterResponse(
        var responseCode: Int? = 0,
        var responseData: DealerFilterItem? = DealerFilterItem(),
        var responseMessage: String? = ""
) {

    data class DealerFilterItem(
            var dealertypedata: ArrayList<DealerTypeData> = arrayListOf(),
            var milesdata: ArrayList<MilesData> = arrayListOf(),
            var productssdata: ArrayList<ProductsData> = arrayListOf()
    )

    data class DealerTypeData(
            var value: String = ""
    )

    data class MilesData(
            var value: String = ""
    )

    data class ProductsData(
            var value: String = ""
    )
}