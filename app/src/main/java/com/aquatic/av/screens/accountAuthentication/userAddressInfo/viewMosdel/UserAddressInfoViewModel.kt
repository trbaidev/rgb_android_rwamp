package com.aquatic.av.screens.accountAuthentication.userAddressInfo.viewMosdel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserAddressInfoViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel() {

    private val _userResults = MutableLiveData<Event<State<UserAddressResponse>>>()
    val userAddressResults: LiveData<Event<State<UserAddressResponse>>> get() = _userResults

    fun createUser(userAddressModel: UserAddressModel?) {
        viewModelScope.launch {
            authRepository.createUser(userAddressModel).collect {
                _userResults.value = (Event(it))
            }
        }
    }
}