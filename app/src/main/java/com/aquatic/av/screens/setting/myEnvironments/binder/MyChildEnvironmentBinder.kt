package com.aquatic.av.screens.setting.myEnvironments.binder

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.MyChildEnvironmentRowItemBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.setting.myEnvironments.dataModels.Device

class MyChildEnvironmentBinder (private var itemClickLister: (entity: Device, position: Int) -> Unit) :
    BaseRecyclerViewBinder<Device, MyChildEnvironmentRowItemBinding>(
        R.layout.my_child_environment_row_item
    ) {
    override fun bindView(
        entity: Device,
        position: Int,
        viewHolder: BaseViewHolder<MyChildEnvironmentRowItemBinding>?,
        context: Context,
        collections: MutableList<Device>
    ) {
        viewHolder?.viewBinding?.let {
            it.dataItem = entity
            viewHolder.itemView.setOnClickListener {
                itemClickLister.invoke(entity, position)
            }
        }
    }
}