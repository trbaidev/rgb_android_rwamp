package com.aquatic.av.screens.onBoardingFlow.RegisterScanner.binder

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.adapter.RGBBluetoothDevice
import com.aquatic.av.databinding.RegisterScannerRowItemBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder


class RegisterScannerAdapter(private var itemClickLister: (entity: RGBBluetoothDevice, position: Int) -> Unit) :
    BaseRecyclerViewBinder<RGBBluetoothDevice, RegisterScannerRowItemBinding>(R.layout.register_scanner_row_item) {
    override fun bindView(
        entity: RGBBluetoothDevice,
        position: Int,
        viewHolder: BaseViewHolder<RegisterScannerRowItemBinding>?,
        context: Context,
        collections: MutableList<RGBBluetoothDevice>
    ) {
        viewHolder?.viewBinding?.let {
            it.dataItem = entity
            viewHolder
            viewHolder.itemView.setOnClickListener {
                itemClickLister.invoke(entity, position)
            }
        }
    }

}
