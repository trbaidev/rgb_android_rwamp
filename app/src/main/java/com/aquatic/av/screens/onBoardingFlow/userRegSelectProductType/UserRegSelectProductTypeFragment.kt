package com.aquatic.av.screens.onBoardingFlow.userRegSelectProductType

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentUserRegProductTypeBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressResponse
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.onBoardingFlow.dataModels.DevicesInformation
import com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice.UserRegSelectDeviceFragment
import com.aquatic.av.screens.onBoardingFlow.userRegSelectProductType.binders.UserRegSelectProductTypeBinder
import com.aquatic.av.screens.onBoardingFlow.userRegSelectProductType.dataModels.UserRegSelectProductTypeResponse
import com.aquatic.av.screens.onBoardingFlow.userRegSelectProductType.viewModel.UserRegSelectProductTypeViewModel
import com.aquatic.av.utils.EditTextExtension.getGenericList
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserRegSelectProductTypeFragment :
        BaseFragment<FragmentUserRegProductTypeBinding>(R.layout.fragment_user_reg_product_type) {

    private var userRegTypeProductTypeResponses: List<UserRegSelectProductTypeResponse>? = null
    private var userAddressResponse: UserAddressResponse? = null
    private var devicesInformation: DevicesInformation? = null
    private var userAddressModel: UserAddressModel? = null
    private val viewModel by viewModels<UserRegSelectProductTypeViewModel>()

    private val userRegSelectProductTypeItemAdapter by lazy {
        BaseRecyclerViewAdapter(
                arrayListOf(),
                userRegSelectProductTypeItemBinder,
                requireContext()
        )
    }

    private val userRegSelectProductTypeItemBinder by lazy {
        UserRegSelectProductTypeBinder { entity, position ->
            updateData(position)
        }
    }

    private fun updateData(position: Int) {
        var userRegTypeProductTypeResponse = userRegTypeProductTypeResponses?.get(position)
        devicesInformation?.product_type_id = userRegTypeProductTypeResponse?.product_type_id
        mainActivity.navigateTo(
                UserRegSelectDeviceFragment.getInstance(
                        userRegTypeProductTypeResponse,
                        userAddressResponse,
                        userAddressModel,
                        devicesInformation
                )
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        binding.ptRecyclerView.adapter = userRegSelectProductTypeItemAdapter
        binding.ptRecyclerView.layoutManager = LinearLayoutManager(mainActivity)
        binding.apply {
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnNext.setOnClickListener {
                showToast("Please select a product type to continue")
            }
        }
        viewModel.getAllProductTypes()
    }


    private fun setListeners() {
        viewModel.userRegSelectProductTypeResponseResults.observe(
                viewLifecycleOwner,
                EventObserver {
                    when (it) {
                        is State.Error -> showToast(it.message)
                        is State.Exception -> {
                            it.exception.printStackTrace()
                            showToast(it.exception.message)
                        }
                        is State.Loading -> mainActivity.showLoader(it.isLoading)
                        is State.Success -> {
                            it.body?.apply {
                                var list = Gson().getGenericList(
                                        Gson().toJson(it.body),
                                        UserRegSelectProductTypeResponse::class.java
                                )
                                userRegTypeProductTypeResponses = list
                                userRegSelectProductTypeItemAdapter.addAll(userRegTypeProductTypeResponses ?: arrayListOf())
                            }
                        }
                    }
                })
    }

    override fun setHeaderFooterViews(
            titleBar: TitleBar?,
            bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }

    companion object {
        fun getInstance(
                userAddressResponse: UserAddressResponse?,
                userAddressModel: UserAddressModel?,
                devicesInformation: DevicesInformation?
        ): UserRegSelectProductTypeFragment {
            val userRegSelectProductTypeFragment = UserRegSelectProductTypeFragment()
            userRegSelectProductTypeFragment.userAddressResponse = userAddressResponse
            userRegSelectProductTypeFragment.userAddressModel = userAddressModel
            userRegSelectProductTypeFragment.devicesInformation = devicesInformation
            return userRegSelectProductTypeFragment
        }
    }

}