package com.aquatic.av.screens.setting.myEnvironments.addNewDeviceProductType.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddNewDeviceProductTypeViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _addnewDeviceProductTypeResponseResults = MutableLiveData<Event<State<ArrayList<Any>>>>()
    val addNewDeviceProductTypeResponseResults: LiveData<Event<State<ArrayList<Any>>>> get() = _addnewDeviceProductTypeResponseResults

    fun getAllProductTypes() {
        viewModelScope.launch {
            authRepository.getAllProductTypes().collect {
                _addnewDeviceProductTypeResponseResults.value = (Event(it))
            }
        }
    }
}