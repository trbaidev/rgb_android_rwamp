package com.aquatic.av.screens.onBoardingFlow.userDeviceName

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentUserDeviceNameBinding
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressResponse
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.onBoardingFlow.dataModels.DevicesInformation
import com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice.dataModels.UserRegSelectDeviceResponse
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.UserRegUploadReceiptFragment
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView

class UserDeviceNameFragment :
        BaseFragment<FragmentUserDeviceNameBinding>(R.layout.fragment_user_device_name) {

     var userRegSelectDeviceResponse: UserRegSelectDeviceResponse? = null
    private var devicesInformation: DevicesInformation? = null
    private var userAddressModel: UserAddressModel? = null
    private var userAddressResponse: UserAddressResponse? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            dataItem = userRegSelectDeviceResponse
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnNext.setOnClickListener {
                var deviceName = edtDeviceName.text.trim().toString()
                if (!deviceName.isNullOrEmpty()) {
                    devicesInformation?.device_name = deviceName
                    mainActivity.navigateTo(
                            UserRegUploadReceiptFragment.getInstance(
                                    userAddressResponse,
                                    userRegSelectDeviceResponse,
                                    userAddressModel,
                                    devicesInformation
                            )
                    )
                } else {
                    showToast("Device name require.")
                }
            }
            edtDeviceName.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable?) {
                    tvDName.text = s.toString()
                }

            })
        }
    }


    override fun setHeaderFooterViews(
            titleBar: TitleBar?,
            bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }


    companion object {
        fun getInstance(
                userRegSelectDeviceResponse: UserRegSelectDeviceResponse?,
                userAddressModel: UserAddressModel?,
                devicesInformation: DevicesInformation?,
                userAddressResponse: UserAddressResponse?
        ): UserDeviceNameFragment {
            val userRegDeviceNameFragment = UserDeviceNameFragment()
            userRegDeviceNameFragment.userRegSelectDeviceResponse = userRegSelectDeviceResponse
            userRegDeviceNameFragment.userAddressModel = userAddressModel
            userRegDeviceNameFragment.devicesInformation = devicesInformation
            userRegDeviceNameFragment.userAddressResponse = userAddressResponse
            return userRegDeviceNameFragment
        }
    }
}