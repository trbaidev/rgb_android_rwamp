package com.aquatic.av.screens.onBoardingFlow.userRegSelectProductType.binders

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.UserRegSeletctProductTypeRowItemBinding


import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.onBoardingFlow.userRegSelectProductType.dataModels.UserRegSelectProductTypeResponse

class UserRegSelectProductTypeBinder(private var itemClickLister: (entity: UserRegSelectProductTypeResponse, position: Int) -> Unit) :
    BaseRecyclerViewBinder<UserRegSelectProductTypeResponse, UserRegSeletctProductTypeRowItemBinding>(
        R.layout.user_reg_seletct_product_type_row_item
    ) {
    override fun bindView(
        entity: UserRegSelectProductTypeResponse,
        position: Int,
        viewHolder: BaseViewHolder<UserRegSeletctProductTypeRowItemBinding>?,
        context: Context,
        collections: MutableList<UserRegSelectProductTypeResponse>
    ) {
        viewHolder?.viewBinding?.let {
            it.dataItem = entity
            viewHolder
            viewHolder.itemView.setOnClickListener {
                itemClickLister.invoke(entity, position)
            }
        }
    }
}