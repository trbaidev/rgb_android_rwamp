package com.aquatic.av.screens.setting.myEnvironments.dataModels


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Device(
    @SerializedName("device_id")
    @Expose
    private var deviceId: Int? = -1,

    @SerializedName("device_name")
    @Expose
    private var deviceName: String? = "",

    @SerializedName("base_path")
    @Expose
    private var basePath: String? = "",

    @SerializedName("product_image")
    @Expose
    private var productImage: String? = null,

    public var sku:String?="",

    public var serial_no:String?="",

    public var last_connected:String?="",

    public var warranty_status:String?="",

    public var product_name:String?="",

){


    fun getDeviceId(): Int? {
        return deviceId
    }

    fun setDeviceId(deviceId: Int?) {
        this.deviceId = deviceId
    }

    fun getDeviceName(): String? {
        return deviceName
    }

    fun setDeviceName(deviceName: String?) {
        this.deviceName = deviceName
    }

    fun getBasePath(): String? {
        return basePath
    }

    fun setBasePath(basePath: String?) {
        this.basePath = basePath
    }

    fun getProductImage(): String? {
        return productImage
    }

    fun setProductImage(productImage: String?) {
        this.productImage = productImage
    }


}