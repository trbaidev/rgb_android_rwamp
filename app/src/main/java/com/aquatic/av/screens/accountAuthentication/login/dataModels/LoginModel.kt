package com.aquatic.av.screens.accountAuthentication.login.dataModels

data class LoginModel(
    var user_email: String = "",
    var password: String = "",
    var device_val: String = "",
    var device_info: String = "AOS",
    var is_login: Int = 1
)