package com.aquatic.av.screens.setting.myEnvironments.addNewDeviceName

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentAddNewDeviceNameBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice.dataModels.AddNewSelectDeviceResponse
import com.aquatic.av.screens.setting.myEnvironments.addNewUploadReceipt.AddDeviceUploadReceiptFragment
import com.aquatic.av.screens.setting.myEnvironments.dataModels.EnvironmentReferenceModel
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView

class AddNewDeviceNameFragment :
    BaseFragment<FragmentAddNewDeviceNameBinding>(R.layout.fragment_add_new_device_name) {

    lateinit var addNewDeviceResponse: AddNewSelectDeviceResponse
lateinit var     environment_ref: EnvironmentReferenceModel
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            dataItem = addNewDeviceResponse
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnNext.setOnClickListener {
                var deviceName = edtDeviceName.text.trim().toString()
                if (!deviceName.isNullOrEmpty()) {
                    environment_ref.deviceName = deviceName
                    mainActivity.navigateTo(
                        AddDeviceUploadReceiptFragment.getInstance(
                           environment_ref,
                            addNewDeviceResponse,
                        )
                    )
                }else{
                    showToast("Device name require.")
                }
            }
            edtDeviceName.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable?) {
                    tvDeviceName.text = s.toString()
                }

            })
        }
    }


    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }


    companion object {
        fun getInstance(
            userRegSelectDeviceResponse: AddNewSelectDeviceResponse,
            environment_ref: EnvironmentReferenceModel,
        ): AddNewDeviceNameFragment {
            val deviceNameScreen = AddNewDeviceNameFragment()
            deviceNameScreen.addNewDeviceResponse = userRegSelectDeviceResponse
            deviceNameScreen.environment_ref=environment_ref
            return deviceNameScreen
        }
    }
}