package com.aquatic.av.screens.bluetooth.dataModels

class UpdateDeviceConnectedResponse( var responseCode: Int? = -1,
                                     var responseMessage: String? = "",
                                     var responseData: Int? = -1)