package com.aquatic.av.screens.setting.myEnvironments.binder

import android.content.Context
import android.text.TextUtils
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.MyEnvironmentRowItemBinding
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentModel


class MyEnvironmentBinder(private var itemClickLister: (entity: MyEnvironmentModel, position: Int) -> Unit) :
    BaseRecyclerViewBinder<MyEnvironmentModel, MyEnvironmentRowItemBinding>(R.layout.my_environment_row_item) {

    lateinit var context: Context

    override fun bindView(
        entity: MyEnvironmentModel,
        position: Int,
        viewHolder: BaseViewHolder<MyEnvironmentRowItemBinding>?,
        context: Context,
        collections: MutableList<MyEnvironmentModel>
    ) {
        viewHolder?.viewBinding?.apply {
            dataItem = entity
            this@MyEnvironmentBinder.context = context
            var myEnvironmentChildBinder = MyChildEnvironmentBinder { entity, position ->

            }
            var myChildEnvironmentAdapter = BaseRecyclerViewAdapter(
                arrayListOf(),
                myEnvironmentChildBinder,
                context
            )
            val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            rvDevices.layoutManager = layoutManager
            rvDevices.adapter = myChildEnvironmentAdapter
            entity.getDevices()?.let { myChildEnvironmentAdapter.addAll(it) }
            viewHolder.itemView.setOnClickListener {
                if(entity.getDevices()?.size!!>0) {
                    viewHolder?.viewBinding?.isvisible = !viewHolder?.viewBinding?.isvisible!!
                }
            }
            ivArrowLeft.setOnClickListener {
                if (layoutManager.findFirstVisibleItemPosition() > 0) {
                    rvDevices.smoothScrollToPosition(layoutManager.findFirstVisibleItemPosition() - 1)
                } else {
                    rvDevices.smoothScrollToPosition(0)
                }
            }
            ivArrowRight.setOnClickListener {
                rvDevices.smoothScrollToPosition(
                    layoutManager.findLastVisibleItemPosition() + 1
                )
            }
            btnEdit.setOnClickListener {
                if (isEdiable) {
                    var name = firstLine.text.toString().trim()
                    if (!TextUtils.isEmpty(name)) {
                        isEdiable = !isEdiable
                        entity.setEnvName(firstLine.text.toString().trim())
                        itemClickLister.invoke(entity, position)
                    } else {
                        Toast.makeText(context, "Please Enter Environment Name", Toast.LENGTH_SHORT)
                            .show()
                    }
                } else {
                    isEdiable = !isEdiable
                }
            }
        }
    }


}