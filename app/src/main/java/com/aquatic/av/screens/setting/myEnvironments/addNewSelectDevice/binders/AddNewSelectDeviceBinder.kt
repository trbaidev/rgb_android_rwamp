package com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice.binders

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.AddNewSelectDeviceRowItemBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.setting.myEnvironments.addNewSelectDevice.dataModels.AddNewSelectDeviceResponse

class AddNewSelectDeviceBinder(private var itemClickLister: (entity: AddNewSelectDeviceResponse, position: Int) -> Unit) :
    BaseRecyclerViewBinder<AddNewSelectDeviceResponse, AddNewSelectDeviceRowItemBinding>(
        R.layout.add_new_select_device_row_item
    ) {
    override fun bindView(
        entity: AddNewSelectDeviceResponse,
        position: Int,
        viewHolder: BaseViewHolder<AddNewSelectDeviceRowItemBinding>?,
        context: Context,
        collections: MutableList<AddNewSelectDeviceResponse>
    ) {
        viewHolder?.viewBinding?.let {
            it.dataItem = entity
            viewHolder.itemView.setOnClickListener {
                itemClickLister.invoke(entity, position)
            }
        }
    }
}