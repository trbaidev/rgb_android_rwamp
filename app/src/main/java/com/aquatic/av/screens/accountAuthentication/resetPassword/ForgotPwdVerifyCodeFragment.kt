package com.aquatic.av.screens.accountAuthentication.resetPassword

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceUtil
import com.aquatic.av.databinding.FragmentForgotPwdCodeBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.accountAuthentication.resetPassword.viewModel.ForgotPwdViewModel
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.aquatic.av.views.dialogs.DialogFactory.showDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotPwdVerifyCodeFragment :
    BaseFragment<FragmentForgotPwdCodeBinding>(R.layout.fragment_forgot_pwd_code) {

    lateinit var resetCode: String
    lateinit var email: String
    private val viewModel by viewModels<ForgotPwdViewModel>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        binding.apply {
            btnContinue.setOnClickListener {
                if (!edtVerifyCode.text.trim().toString().isNullOrEmpty()) {
                    if (resetCode == edtVerifyCode.text.trim().toString()) {
                        mainActivity.navigateTo(ResetPasswordFragment.getInstance(email))
                    } else {
                        showDialog("Invalid Code", "Validation code has been expired!", true)
                    }
                } else {
                    showDialog("Invalid Code", "Please enter code", true)
                }
            }
            ivBack.setOnClickListener {
                mainActivity.popFragment()
            }
            resend.setOnClickListener {
                viewModel.verifyUserEmail(email)
            }
        }
    }

    private fun setListeners() {
        viewModel.emailPwdResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        val splitData = it.body.user_exist.split("~").toTypedArray()
                        if (splitData[0].equals("true", true)) {
                            resetCode=splitData[1]
                            PreferenceUtil.getInstance()
                                .setStringValue(splitData[1], "forget_password")
                        }
                    }
                }
            }
        })
    }

    companion object {
        fun getInstance(resetCode: String, email: String): ForgotPwdVerifyCodeFragment {
            val forgotPwdVerifyCodeFragment = ForgotPwdVerifyCodeFragment()
            forgotPwdVerifyCodeFragment.resetCode = resetCode
            forgotPwdVerifyCodeFragment.email = email
            return forgotPwdVerifyCodeFragment
        }
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }
}