package com.aquatic.av.screens.rgb

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences
import com.aquatic.av.bluetoothScanner.ble.preference.getInt
import com.aquatic.av.databinding.FragmentFavoriteColorBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.rgb.binder.LocalColorBinder
import com.aquatic.av.screens.rgb.binder.ServerColorBinder
import com.aquatic.av.screens.rgb.dataModel.FavColorData
import com.aquatic.av.screens.rgb.dataModel.RGBColorModel
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.DEFAULT_CELL_COLOR
import com.aquatic.av.utils.showToast
import com.aquatic.av.utils.toHexARGB
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoriteColorFragment :
    BaseFragment<FragmentFavoriteColorBinding>(R.layout.fragment_favorite_color) {
    var fragCalled = false
    private val mRGBViewModel by lazy {
        (parentFragment as ColorPickerFragment).mRGBViewModel
    }
    val deleteFavColor = ArrayList<Int>()
    private val localItemAdapter by lazy {
        BaseRecyclerViewAdapter(
            arrayListOf(
                FavColorData(
                    device_id = 841,
                    color = toHexARGB(
                        RgbSharedPreferences.fav1Value.getInt(
                            Color.parseColor(DEFAULT_CELL_COLOR)
                        )
                    )
                ),
                FavColorData(
                    device_id = 841,
                    color = toHexARGB(
                        RgbSharedPreferences.fav2Value.getInt(
                            Color.parseColor(DEFAULT_CELL_COLOR)
                        )
                    )
                ),
                FavColorData(
                    device_id = 841,
                    color = toHexARGB(
                        RgbSharedPreferences.fav3Value.getInt(
                            Color.parseColor(DEFAULT_CELL_COLOR)
                        )
                    )
                ),
                FavColorData(
                    device_id = 841,
                    color = toHexARGB(
                        RgbSharedPreferences.fav4Value.getInt(
                            Color.parseColor(DEFAULT_CELL_COLOR)
                        )
                    )
                ),
                FavColorData(
                    device_id = 841,
                    color = toHexARGB(
                        RgbSharedPreferences.fav5Value.getInt(
                            Color.parseColor(DEFAULT_CELL_COLOR)
                        )
                    )
                )
            ),
            LocalColorBinder(itemClickListener = { _, position ->
                mRGBViewModel.onLocalFavoriteItemSelected.value = position
                (parentFragment as ColorPickerFragment).navigateColorPicker()
            }),
            requireContext()
        )
    }
    private val serverTopItemAdapter by lazy {
        BaseRecyclerViewAdapter(
            arrayListOf(),
            serverColorBinder,
            requireContext()
        )
    }
    private val serverMoreItemAdapter by lazy {
        BaseRecyclerViewAdapter(
            arrayListOf(),
            serverMoreColorBinder,
            requireContext()
        )
    }
    private val serverColorBinder by lazy {
        ServerColorBinder(
            itemClickListener = { item, position ->
                mRGBViewModel.onServerFavoriteItemSelected.value =
                    item.apply {
                        localItemPosition = position
                        is_top = true
                    }
                (parentFragment as ColorPickerFragment).navigateColorPicker()
            },
            itemLongClickListener = { item, position ->
                binding.btnSaveCnt.visibility = View.VISIBLE
                binding.btnCancelChanges.visibility = View.VISIBLE
                binding.showInstructions = false
                showItemSelector(item, position)
                return@ServerColorBinder true
            },
            itemSelectClickListener = { item, position ->
                item.dcid?.let { deleteFavColor.add(it) }
            },
            itemUnSelectClickListener = { item, position ->
                deleteFavColor.remove(item.dcid)
            })
    }
    private val serverMoreColorBinder by lazy {
        ServerColorBinder(
            itemClickListener = { item, position ->
                mRGBViewModel.onServerFavoriteItemSelected.value =
                    item.apply {
                        localItemPosition = (position + 5)
                        is_top = false
                    }
                (parentFragment as ColorPickerFragment).navigateColorPicker()
            },
            itemLongClickListener = { item, position ->
                binding.btnSaveCnt.visibility = View.VISIBLE
                binding.btnCancelChanges.visibility = View.VISIBLE
                binding.showInstructions = false
                showItemSelector(item, position)
                return@ServerColorBinder true
            },
            itemSelectClickListener = { item, position ->
                item.dcid?.let { deleteFavColor.add(it) }
            },
            itemUnSelectClickListener = { item, position ->
                deleteFavColor.remove(item.dcid)
            })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun showItemSelector(item: RGBColorModel.ResponseData, position: Int) {
        setItems(true)
    }

    private fun setItems(rest: Boolean) {
        serverColorBinder.showItemSelector = rest
        serverMoreColorBinder.showItemSelector = rest
        serverTopItemAdapter.notifyDataSetChanged()
        serverMoreItemAdapter.notifyDataSetChanged()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        // isDataLoaded is to prevent multiple calls
        if (isVisibleToUser && fragCalled) {
            init()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragCalled = true
        init()
    }

    private fun init() {
        binding.apply {
            fragCalled = true
            showRGBInstructions = mainActivity.isRGBMode
            showInstructions = !mainActivity.isRGBMode
            btnSave.setOnClickListener {
                setItems(false)
                deleteFavColor.clear()
                btnSaveCnt.visibility = View.GONE
                btnCancelChanges.visibility = View.GONE
                showInstructions = true
            }
            btnDelete.setOnClickListener {
                mRGBViewModel.removeDeviceFavoritesColor(
                    requireContext().getAccessToken(),
                    deleteFavColor
                )
            }
            btnCancelChanges.setOnClickListener {
                setItems(false)
                deleteFavColor.clear()
                btnSaveCnt.visibility = View.GONE
                btnCancelChanges.visibility = View.GONE
                showInstructions = true
            }
            if (mainActivity.isRGBMode) {
                moreFavoritesText = "Save More Favorites"
                rvTopFavorites.adapter = localItemAdapter
                rvTopFavorites.layoutManager = GridLayoutManager(requireContext(), 5)
            } else {
                rvTopFavorites.adapter = serverTopItemAdapter
                rvTopFavorites.layoutManager = GridLayoutManager(requireContext(), 5)
                moreFavoritesText = "More Favorites"
                rvMoreFavorites.adapter = serverMoreItemAdapter
                rvMoreFavorites.layoutManager = GridLayoutManager(requireContext(), 5)
                mRGBViewModel.getUserFavoritesColor(
                    requireContext().getAccessToken(),
                    mainActivity.device_id
                )
                mRGBViewModel.deleteColorsResponse.observe(viewLifecycleOwner, EventObserver {
                    when (it) {
                        is State.Error -> showToast(it.message)
                        is State.Exception -> {
                            it.exception.printStackTrace()
                            showToast(it.exception.message)
                        }
                        is State.Loading -> mainActivity.showLoader(it.isLoading)
                        is State.Success -> {
                            it.body?.let {
                                if (it.responseMessage.equals("success", true)) {
                                    setItems(false)
                                    deleteFavColor.clear()
                                    btnSaveCnt.visibility = View.GONE
                                    btnCancelChanges.visibility = View.GONE
                                    showInstructions = true
                                    mRGBViewModel.getUserFavoritesColor(
                                        requireContext().getAccessToken(),
                                        mainActivity.device_id
                                    )
                                }
                            }
                        }
                    }
                })
                mRGBViewModel.allColorsFavoritesResponse.observe(viewLifecycleOwner, EventObserver {
                    when (it) {
                        is State.Error -> showToast(it.message)
                        is State.Exception -> {
                            it.exception.printStackTrace()
                            showToast(it.exception.message)
                        }
                        is State.Loading -> mainActivity.showLoader(it.isLoading)
                        is State.Success -> {
                            it.body?.let {
                                it.responseData?.let {
                                    var sortedList =
                                        it.sortedByDescending { !it.color.equals("#808080") }
                                    serverTopItemAdapter.addAll(sortedList.subList(0, 5))
                                    serverMoreItemAdapter.addAll(sortedList.subList(5, 25))
                                }
                            }
                        }
                    }
                })

            }
        }
    }
}