package com.aquatic.av.screens.setting.myAccount.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import java.util.ArrayList

class ViewPagerAdapter(fm: FragmentActivity) : FragmentStateAdapter(fm) {

    private val mFragmentList: MutableList<Fragment> = ArrayList()
    private val mFragmentTitleList: MutableList<String> = ArrayList()

    override fun getItemCount(): Int {
        return mFragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return mFragmentList.get(position)
    }

    fun addFragment(fragment: Fragment?, title: String?) {
        fragment?.let { mFragmentList.add(it) }
        title?.let { mFragmentTitleList.add(it) }
    }
}
