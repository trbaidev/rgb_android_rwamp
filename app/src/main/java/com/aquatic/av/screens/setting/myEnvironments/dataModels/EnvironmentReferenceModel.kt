package com.aquatic.av.screens.setting.myEnvironments.dataModels

data class EnvironmentReferenceModel(
    var product_type_id:Int?=-1,
    var product_type_img:String?="",
    var env_id:Int?=-1,
    var env_name:String?="",
    var env_icon:String?="",
    var device_type:String?="",
    var product_id:Int=-1,
    var product_code:String="",
    var product_img:String="",
    var deviceName:String=""
)