package com.aquatic.av.screens.setting.productRegistration

import android.os.Bundle
import android.view.View
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentProductRegistrationBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.dashboard.dataModel.DashboardItemModel
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView

class ProductRegistrationFragment :
    BaseFragment<FragmentProductRegistrationBinding>(R.layout.fragment_product_registration) {

    lateinit var deviceItemModel: DashboardItemModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            binding.dataItem = deviceItemModel
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnViewReceipt.setOnClickListener {
                isvisible=!isvisible
            }
        }
    }


    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        titleBar?.hideButtons()
    }

    companion object {
        fun getInstance(
            deviceItemModel: DashboardItemModel
        ): ProductRegistrationFragment {
            val productRegistrationFragment = ProductRegistrationFragment()
            productRegistrationFragment.deviceItemModel = deviceItemModel
            return productRegistrationFragment
        }
    }
}