package com.aquatic.av.screens.setting.myAccount

import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentMyAccountNewBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.setting.accountInformation.AccountInformationFragment
import com.aquatic.av.screens.setting.myAccount.adapter.ViewPagerAdapter
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayoutMediator

class MyAccountNewFragment :
    BaseFragment<FragmentMyAccountNewBinding>(R.layout.fragment_my_account_new) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            setupViewPager(pager)
        }

    }

    private fun setupViewPager(viewpager: ViewPager2) {
        val adapter = activity?.let { ViewPagerAdapter(it) }
        adapter?.addFragment(AccountInformationFragment(), "ACCOUNT INFORMATION")
        adapter?.addFragment(AccountInformationFragment(), "DEVICES")
        adapter?.addFragment(AccountInformationFragment(), "ENVIRONMENTS")
        viewpager.adapter = adapter
        TabLayoutMediator(
            binding.tabLayout, viewpager
        ) { tab, position ->
            if (position == 0) {
                tab.text = "ACCOUNT INFORMATION"
            } else if (position==1) {
                tab.text = "DEVICES"
            } else {
                tab.text = "ENVIRONMENTS"
            }
        }.attach()
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        titleBar?.hideButtons()
    }
}