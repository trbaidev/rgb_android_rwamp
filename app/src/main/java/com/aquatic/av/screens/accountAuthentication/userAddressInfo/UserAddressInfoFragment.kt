package com.aquatic.av.screens.accountAuthentication.userAddressInfo

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentUserAddressInfoBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.viewMosdel.UserAddressInfoViewModel
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.onBoardingFlow.dataModels.DevicesInformation
import com.aquatic.av.screens.onBoardingFlow.userEnvironment.UserEnvironmentFragment
import com.aquatic.av.utils.BasePreferenceHelper.putAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.putUserAddressModel
import com.aquatic.av.utils.BasePreferenceHelper.putUserDetailModel
import com.aquatic.av.utils.BasePreferenceHelper.putUserEmail
import com.aquatic.av.utils.Utils
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserAddressInfoFragment :
        BaseFragment<FragmentUserAddressInfoBinding>(R.layout.fragment_user_address_info) {

    var userAddressModel: UserAddressModel? = null
    private val viewModel by viewModels<UserAddressInfoViewModel>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        binding.apply {
            edtPhone.addTextChangedListener(PhoneNumberFormattingTextWatcher())
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnNext.setOnClickListener {
                if (checkFieldsValidations()) {
                    userAddressModel?.address = binding.edtUserAddress.text.toString()
                    userAddressModel?.city = binding.edtCity.text.trim().toString()
                    userAddressModel?.state = binding.edtState.text.trim().toString()
                    userAddressModel?.zip_code = binding.edtZipCode.text.trim().toString()
                    userAddressModel?.country = binding.edtCountry.text.trim().toString()
                    userAddressModel?.phone = binding.edtPhone.text.trim().toString()
                    userAddressModel?.device_info = Utils.getDeviceId(mainActivity)
                    viewModel.createUser(userAddressModel)
//                    mainActivity.navigateTo(UserEnvironmentFragment())
                }
            }
            LayoutTerms.setOnClickListener {
                val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("http://aquaticav.com/terms/"))
                startActivity(browserIntent)
            }
        }
    }

    private fun setListeners() {
        viewModel.userAddressResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        val devicesInfo = DevicesInformation()
                        devicesInfo.user_id = it.body.responseData.user_info.user_id
                        devicesInfo.token = it.body.responseData.tokens.token
                        devicesInfo.user_email = userAddressModel?.user_email ?: ""
                    //    mainActivity.putUserAddressModel(it.body)
                        mainActivity.putUserDetailModel(it.body.responseData.user_info)
                        mainActivity.putUserEmail(it.body.responseData.user_info.user_email)
                        requireContext().putAccessToken("Bearer ${it.body.responseData.tokens.token}")
                        showToast("User created successfully")
                        mainActivity.navigateTo(
                                UserEnvironmentFragment.getInstance(
                                        it.body,
                                        userAddressModel,
                                        devicesInfo
                                )
                        )
                    }
                }
            }
        })
    }

    private fun checkFieldsValidations(): Boolean {
        val address = binding.edtUserAddress.text.trim().toString()
        val city = binding.edtCity.text.trim().toString()
        val state = binding.edtState.text.trim().toString()
        val postalCode = binding.edtZipCode.text.trim().toString()
        val country = binding.edtCountry.text.trim().toString()
        val phone = binding.edtPhone.text.trim().toString()
        val termsAgreementCheck = binding.checkboxIsAgree.isChecked
        when {
            TextUtils.isEmpty(country) -> {
                binding.edtCountry.error = "Please provide country"
                return false
            }
            TextUtils.isEmpty(city) -> {
                binding.edtCity.error = "Please provide city"
                return false
            }
            TextUtils.isEmpty(state) -> {
                binding.edtState.error = "Please provide state"
                return false
            }
            TextUtils.isEmpty(postalCode) -> {
                binding.edtZipCode.error = "Please provide zip code"
                return false
            }
            TextUtils.isEmpty(address) -> {
                binding.edtUserAddress.error = "Please provide address"
                return false
            }
            TextUtils.isEmpty(phone) -> {
                binding.edtPhone.error = "Please provide phone number"
                return false
            }
            phone.length < 14 -> {
                binding.edtPhone.error = "Please enter correct phone number"
                return false
            }
            !termsAgreementCheck -> {
                Toast.makeText(mainActivity, "Please Accept Term And Condition", Toast.LENGTH_SHORT)
                        .show()
                return false
            }
            else -> {
                return true
            }
        }
    }

    override fun setHeaderFooterViews(
            titleBar: TitleBar?,
            bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
    }


    companion object {
        fun getInstance(userAddressModel: UserAddressModel?): UserAddressInfoFragment {
            val userAddressInfoFragment = UserAddressInfoFragment()
            userAddressInfoFragment.userAddressModel = userAddressModel
            return userAddressInfoFragment
        }
    }
}