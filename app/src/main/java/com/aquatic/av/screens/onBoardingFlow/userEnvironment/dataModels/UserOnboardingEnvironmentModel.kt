package com.aquatic.av.screens.onBoardingFlow.userEnvironment.dataModels

data class UserOnboardingEnvironmentModel(
    var userId: Int? = 0,
    var types: Array<String>
)