package com.aquatic.av.screens.bluetooth.viewModel;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import com.aquatic.av.bluetoothScanner.ble.adapter.RGBBluetoothDevice;

import java.util.ArrayList;
import java.util.List;

import no.nordicsemi.android.support.v18.scanner.ScanResult;

/**
 * This class keeps the current list of discovered Bluetooth LE devices matching filter.
 * If a new device has been found it is added to the list and the LiveData i observers are
 * notified. If a packet from a device that's already in the list is found, the RSSI and name
 * are updated and observers are also notified. Observer may check {@link #getUpdatedDeviceIndex()}
 * to find out the index of the updated device.
 */
@SuppressWarnings("unused")
public class ScannerLiveData extends LiveData<ScannerLiveData> {
    private final List<RGBBluetoothDevice> mDevices = new ArrayList<>();
    private SCANSTATE mScanState;
    private Integer mUpdatedDeviceIndex;
    private boolean mScanningStarted;
    private boolean mBluetoothEnabled;
    private boolean mLocationEnabled;

    ScannerLiveData(final boolean bluetoothEnabled, final boolean locationEnabled) {
        mScanningStarted = false;
        mScanState = SCANSTATE.NONE;
        mBluetoothEnabled = bluetoothEnabled;
        mLocationEnabled = locationEnabled;
        postValue(this);
    }

    void refresh() {
        postValue(this);
    }

    void scanningStarted() {
        mScanningStarted = true;
        mScanState = SCANSTATE.STARTED;
        postValue(this);
    }

    void scanningStopped() {
        mScanState = SCANSTATE.STOPPED;
        mScanningStarted = false;
        postValue(this);
    }

    void bluetoothEnabled() {
        mBluetoothEnabled = true;
        postValue(this);
    }

    void bluetoothDisabled() {
        mBluetoothEnabled = false;
        mUpdatedDeviceIndex = null;
        mDevices.clear();
        postValue(this);
    }

    void deviceDiscovered(final ScanResult result) {
        RGBBluetoothDevice device = null;

        final int index = indexOf(result);
        if (index == -1) {
            if (!TextUtils.isEmpty(result.getDevice().getName()) && result.getDevice().getName().contains("RGB")) {
                device = new RGBBluetoothDevice(result);
                mDevices.add(device);
                mUpdatedDeviceIndex = null;
            }
        } else {
            device = mDevices.get(index);
            mUpdatedDeviceIndex = index;
        }
        // Update RSSI and name
        if (device != null) {
            device.setRssi(result.getRssi());
            device.setName(result.getScanRecord().getDeviceName());
            postValue(this);
        }
    }

    /**
     * Returns the list of devices.
     *
     * @return current list of devices discovered
     */
    @NonNull
    public List<RGBBluetoothDevice> getDevices() {
        return mDevices;
    }

    /**
     * Returns null if a new device was added, or an index of the updated device.
     */
    @Nullable
    public Integer getUpdatedDeviceIndex() {
        final Integer i = mUpdatedDeviceIndex;
        mUpdatedDeviceIndex = null;
        return i;
    }

    /**
     * Returns whether the list is empty.
     */
    public boolean isEmpty() {
        return mDevices.isEmpty();
    }

    /**
     * Returns whether scanning is in progress.
     */
    public boolean isScanning() {
        return mScanningStarted;
    }

    public SCANSTATE getScanState() {
        return mScanState;
    }

    /**
     * Returns whether Bluetooth adapter is enabled.
     */
    public boolean isBluetoothEnabled() {
        return mBluetoothEnabled;
    }

    /**
     * Returns whether Location is enabled.
     */
    public boolean isLocationEnabled() {
        return mLocationEnabled;
    }

    /* package */ void setLocationEnabled(final boolean enabled) {
        mLocationEnabled = enabled;
        postValue(this);
    }

    /**
     * Finds the index of existing devices on the scan results list.
     *
     * @param result scan result
     * @return index of -1 if not found
     */
    private int indexOf(final ScanResult result) {
        int i = 0;
        for (final RGBBluetoothDevice device : mDevices) {
            if (device.matches(result))
                return i;
            i++;
        }
        return -1;
    }

    /**
     *
     */
    public void restartScanner() {
        mScanningStarted = false;
        mScanState = SCANSTATE.NONE;
        postValue(this);
    }

    public enum SCANSTATE {
        NONE, STARTED, STOPPED
    }
}
