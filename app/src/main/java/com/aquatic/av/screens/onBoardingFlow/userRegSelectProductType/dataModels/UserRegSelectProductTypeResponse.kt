package com.aquatic.av.screens.onBoardingFlow.userRegSelectProductType.dataModels

data class UserRegSelectProductTypeResponse(
    var product_type_id: Int,
    var product_type: String,
    var base_path: String,
    var product_type_image: String
)