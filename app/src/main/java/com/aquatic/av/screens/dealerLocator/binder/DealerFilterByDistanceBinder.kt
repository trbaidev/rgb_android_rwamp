package com.aquatic.av.screens.dealerLocator.binder

import android.annotation.SuppressLint
import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.RowItemFilterByBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.dealerLocator.model.DealerFilterResponse

class DealerFilterByDistanceBinder(var lastSelectedItem: Int, private val itemClickListener: (item: DealerFilterResponse.MilesData, position: Int) -> Unit) : BaseRecyclerViewBinder<DealerFilterResponse.MilesData, RowItemFilterByBinding>(R.layout.row_item_filter_by) {
    @SuppressLint("NotifyDataSetChanged")
    override fun bindView(entity: DealerFilterResponse.MilesData, position: Int, viewHolder: BaseViewHolder<RowItemFilterByBinding>?, context: Context, collections: MutableList<DealerFilterResponse.MilesData>) {
        viewHolder?.viewBinding?.apply {
            val item = entity.value.toIntOrNull()
            txtItemName.text = if (item == 0) "Any Distance" else "${entity.value} Miles"
            txtItemName.isSelected = item == lastSelectedItem
            txtItemName.setOnClickListener {
                lastSelectedItem = item ?: 0
                itemClickListener.invoke(entity, position)
            }
        }
    }
}