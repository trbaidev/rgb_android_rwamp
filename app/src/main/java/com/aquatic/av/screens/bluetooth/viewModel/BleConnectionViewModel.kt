package com.aquatic.av.screens.bluetooth.viewModel

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.FileUtils
import com.aquatic.av.bluetoothScanner.ble.RGBManager
import com.aquatic.av.bluetoothScanner.ble.adapter.RGBBluetoothDevice
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceKey
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceUtil
import com.aquatic.av.bluetoothScanner.ble.profile.RGBBLEManagerCallbacks
import com.aquatic.av.bluetoothScanner.ble.profile.RGBUUIDS
import com.aquatic.av.bluetoothScanner.ble.utils.BTUtil
import com.aquatic.av.bluetoothScanner.ble.utils.DataUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject


@SuppressLint("StaticFieldLeak")
@HiltViewModel
class BleConnectionViewModel @Inject constructor(@ApplicationContext private val context: Context) : ViewModel(), RGBBLEManagerCallbacks {
    private val mBleManager  by lazy {
        RGBManager.getInstance().getBLEManager(context)
    }
    init {
        mBleManager.setGattCallbacks(this@BleConnectionViewModel)
    }

    // Connection states Connecting, Connected, Disconnecting, Disconnected etc.
    private val mConnectionState = MutableLiveData<String>()

    // Flag to determine if the device is connected
    val mIsConnected = MutableLiveData<Boolean>()

    // Flag to determine if the device has required services
    private val mIsSupported = SingleLiveEvent<Boolean>()

    // Flag to determine if the device is ready
    private val mOnDeviceReady = MutableLiveData<Void?>()
    private val dataStatusMessage = MutableLiveData<String?>()
    val isDeviceReady: LiveData<Void?>
        get() = mOnDeviceReady
    val connectionState: LiveData<String>
        get() = mConnectionState
    val isConnected: LiveData<Boolean>
        get() = mIsConnected
    val isSupported: LiveData<Boolean>
        get() = mIsSupported
    private val preferences by lazy {
        context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    }
    /**
     * Connect to peripheral
     */
    fun connect(device: RGBBluetoothDevice) {
//        final LogSession logSession = Logger.newSession(getApplication(), null, device.getAddress(), device.getName());
//        mBleManager.setLogger(logSession);
        FileUtils.addlogs("SCANNER: INITIATE CONNECTION")
        mBleManager.connect(device.device)
    }

    /**
     * Disconnect from peripheral
     * @return
     */
    fun disconnect(): Boolean {
        return mBleManager.disconnect()
    }

    override fun onCleared() {
        super.onCleared()
        mBleManager.removeGattCallbacks()
    }

    override fun onDeviceConnecting(device: BluetoothDevice) {
        mConnectionState.postValue(context.getString(R.string.state_connecting))
    }

    override fun onDeviceConnected(device: BluetoothDevice) {
        Log.d(TAG, "onDeviceConnected")
        FileUtils.addlogs("BLECONNECTION: onDeviceConnected")
        mIsConnected.postValue(true)
        mConnectionState.postValue(context.getString(R.string.state_discovering_services))
        readDataFromController()
        val editor = preferences.edit()
        editor.putString("connected_device_name", device.name)
        editor.putInt("connected_device_id", -100)
        editor.apply()
        PreferenceUtil.getInstance().setStringValue(PreferenceKey.CONNECTED_BT_DEVICE_ADDRESS, device.address)
        PreferenceUtil.getInstance().setStringValue(PreferenceKey.CONNECTED_BT_DEVICE_NAME, device.name)
    }

    private fun readDataFromController() {
        mBleManager.readData(RGBUUIDS.CHARACTERISTIC_FW_VERSION)
        mBleManager.readData(RGBUUIDS.CHARACTERISTIC_ON_OFF_CONTROL)
    }

    override fun onDeviceDisconnecting(device: BluetoothDevice) {
        mIsConnected.postValue(false)
    }

    override fun onDeviceDisconnected(device: BluetoothDevice) {
        Log.d(TAG, "onDeviceDisconnected")
        FileUtils.addlogs("BLECONNECTION: onDeviceDisconnected")
        mIsConnected.postValue(false)
        RGBManager.getInstance().notifyDeviceConnection(device, false)
    }

    override fun onLinklossOccur(device: BluetoothDevice) {
        mIsConnected.postValue(false)
    }

    override fun onServicesDiscovered(device: BluetoothDevice, optionalServicesFound: Boolean) {
        Log.d(TAG, "onServicesDiscovered")
        mConnectionState.postValue(context.getString(R.string.state_initializing))
        mBleManager.requestBLEConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_HIGH)
    }

    override fun onDeviceReady(device: BluetoothDevice) {
        Log.d(TAG, "onDeviceReady")
        FileUtils.addlogs("BLECONNECTION: onDeviceReady")
        mIsSupported.postValue(true)
        //        mConnectionState.postValue(getApplication().getString(R.string.state_discovering_services_completed, device.getName()));
        mOnDeviceReady.postValue(null)
        readDataFromController()
        RGBManager.getInstance().notifyDeviceConnection(device, true)
    }

    override fun shouldEnableBatteryLevelNotifications(device: BluetoothDevice): Boolean {
        return false
    }

    override fun onBatteryValueReceived(device: BluetoothDevice, value: Int) {}
    override fun onBondingRequired(device: BluetoothDevice) {}
    override fun onBonded(device: BluetoothDevice) {}
    override fun onError(device: BluetoothDevice, message: String, errorCode: Int) {
        FileUtils.addlogs("BLECONNECTION: onError message: $message")
    }

    override fun onDeviceNotSupported(device: BluetoothDevice) {
        mIsSupported.postValue(false)
    }

    override fun onDataSent(characteristic: BluetoothGattCharacteristic) {}
    override fun onSuccess(data: String) {}
    override fun onFailed(data: String) {}
    override fun onDataReceived(characteristic: BluetoothGattCharacteristic, bytes: ByteArray) {
        val data = DataUtil.bytesToHex(bytes)
        dataStatusMessage.postValue(data)
        FileUtils.addlogs("BLECONNECTION: onDataReceived")
        RGBManager.getInstance().notifyOnDataReceived(characteristic, bytes)
    }

    fun getDeviceConnectionState(context: Context?, devices: MutableList<RGBBluetoothDevice>): MutableList<RGBBluetoothDevice> {
        var connectedBluetoothDevice: BluetoothDevice? = null
        val updateConnectionDevices: MutableList<RGBBluetoothDevice> = ArrayList()
        val connectedBluetoothDevices = BTUtil.getConnectedBLEDevices(context)
        if (connectedBluetoothDevices != null && connectedBluetoothDevices.size > 0) {
            connectedBluetoothDevice = connectedBluetoothDevices[0]
        }
        if (connectedBluetoothDevice == null) {
            return devices
        }
        for (device in devices) {
            device.isConnectionState = device.address.equals(connectedBluetoothDevice.address, ignoreCase = true)
            updateConnectionDevices.add(device)
        }
        return updateConnectionDevices
    }

    companion object {
        private val TAG = BleConnectionViewModel::class.java.simpleName
    }

}