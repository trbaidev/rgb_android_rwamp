package com.aquatic.av.screens.setting.personalInformation.dataModels

class PersonalInfoModel {
    var first_name: String? = ""
    var last_name: String? = ""
    var user_email: String? = ""
    var user_password: String? = ""
    var isEmailModified: String? = ""
    var new_email: String? = ""
}