package com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.SerialVerificationModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserRegUploadReceiptSerialVerificationViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _userRegUploadReceiptSerialVerificationModelResponseResults = MutableLiveData<Event<State<SerialVerificationModel>>>()
    val userRegUploadReceiptSerialVerificationModelResponseResults: LiveData<Event<State<SerialVerificationModel>>> get() = _userRegUploadReceiptSerialVerificationModelResponseResults

    fun getAllProductTypes(serial_no:String) {
        viewModelScope.launch {
            authRepository.verifySerialNo(serial_no).collect {
                _userRegUploadReceiptSerialVerificationModelResponseResults.value = (Event(it))
            }
        }
    }
}