package com.aquatic.av.screens.rgb.dataModel

/**
 * Project           : rgb-android
 * File Name         : ModeType
 * Description       :
 * Revision History  : version 1
 * Date              : 2020-01-07
 * Original author   : Kannappan
 * Description       : Initial version
 */
object ModeType {
    const val MODE_DISABLE = 0
    const val MODE_ACTIVATE = 1
    const val MODE_CONFIGURE = 2
    const val MODE_SPEED_MIN = 10
    const val MODE_SPEED_MAX = 250
}