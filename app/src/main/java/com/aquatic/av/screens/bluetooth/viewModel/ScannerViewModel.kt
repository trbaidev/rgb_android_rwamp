package com.aquatic.av.screens.bluetooth.viewModel

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.os.Handler
import android.os.Looper
import android.os.ParcelUuid
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.bluetoothScanner.ble.profile.RGBUUIDS
import com.aquatic.av.bluetoothScanner.ble.utils.BLEUtils
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedModel
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import no.nordicsemi.android.support.v18.scanner.*
import javax.inject.Inject

@SuppressLint("StaticFieldLeak")
@HiltViewModel
class ScannerViewModel @Inject constructor(@ApplicationContext private val context: Context,
                                           private val authRepository: AuthRepository
) : ViewModel() {
    /**
     * MutableLiveData containing the scanner state to notify MainActivity.
     */
    val scannerState: ScannerLiveData = ScannerLiveData(BLEUtils.isBleEnabled(context), BLEUtils.isLocationEnabled(context))
    private val scanCallback: ScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            // If the packet has been obtained while Location was disabled, mark Location as not required
            if (BLEUtils.isLocationRequired(context) && !BLEUtils.isLocationEnabled(context)) BLEUtils.markLocationNotRequired(context)
            scannerState.deviceDiscovered(result)
        }

        override fun onBatchScanResults(results: List<ScanResult>) {
            // Batch scan is disabled (report delay = 0)
        }

        override fun onScanFailed(errorCode: Int) {
            // TODO This should be handled
            scannerState.scanningStopped()
        }
    }

    /**
     * Broadcast receiver to monitor the changes in the location provider
     */
    private val mLocationProviderChangedReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val enabled = BLEUtils.isLocationEnabled(context)
            scannerState.isLocationEnabled = enabled
        }
    }

    /**
     * Broadcast receiver to monitor the changes in the bluetooth adapter
     */
    private val mBluetoothStateBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)
            val previousState = intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_STATE, BluetoothAdapter.STATE_OFF)
            when (state) {
                BluetoothAdapter.STATE_ON -> scannerState.bluetoothEnabled()
                BluetoothAdapter.STATE_TURNING_OFF, BluetoothAdapter.STATE_OFF -> if (previousState != BluetoothAdapter.STATE_TURNING_OFF && previousState != BluetoothAdapter.STATE_OFF) {
                    stopScan()
                    scannerState.bluetoothDisabled()
                }
            }
        }
    }
    private val mHandler = Handler(Looper.getMainLooper())
    override fun onCleared() {
        super.onCleared()
        context.unregisterReceiver(mBluetoothStateBroadcastReceiver)
        if (BLEUtils.isMarshmallowOrAbove()) {
            context.unregisterReceiver(mLocationProviderChangedReceiver)
        }
    }

    fun refresh() {
        scannerState.refresh()
    }

    /**
     * Start scanning for Bluetooth devices.
     */
    fun startScan() {
        if (scannerState.isScanning) {
            return
        }

        // Scanning settings
        val settings = ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY) // Refresh the devices list every second
                .setReportDelay(0) // Hardware filtering has some issues on selected devices
                .setUseHardwareFilteringIfSupported(false) // Samsung S6 and S6 Edge report equal value of RSSI for all devices. In this app we ignore the RSSI.
                /*.setUseHardwareBatchingIfSupported(false)*/
                .build()

        // Let's use the filter to scan only for ingo electric devices
        val primaryServiceUuid = ParcelUuid(RGBUUIDS.PRIMARY_LED_RGB_BLE_SERVICE)
        val filters: List<ScanFilter> = ArrayList()
        //        filters.add(new ScanFilter.Builder().setServiceUuid(primaryServiceUuid).build());
        val scanner = BluetoothLeScannerCompat.getScanner()
        scanner.startScan(filters, settings, scanCallback)
        scannerState.scanningStarted()
        mHandler.postDelayed({ stopScan() }, SCAN_PERIOD)
    }

    /**
     * stop scanning for bluetooth devices.
     */
    fun stopScan() {
        val scanner = BluetoothLeScannerCompat.getScanner()
        scanner.stopScan(scanCallback)
        scannerState.scanningStopped()
    }

    /**
     * Register for required broadcast receivers.
     */
    private fun registerBroadcastReceivers(application: Context) {
        application.registerReceiver(mBluetoothStateBroadcastReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
        if (BLEUtils.isMarshmallowOrAbove()) {
            application.registerReceiver(mLocationProviderChangedReceiver, IntentFilter(LocationManager.MODE_CHANGED_ACTION))
        }
    }

    /**
     * Restart the scanner
     */
    fun restartScan() {
        scannerState.restartScanner()
    }

    companion object {
        private const val SCAN_PERIOD: Long = 6000
    }

    init {
        registerBroadcastReceivers(context)
    }

    private val _updateDeviceConnectedResults = MutableLiveData<Event<State<UpdateDeviceConnectedResponse>>>()
    val updateDeviceConnectedResults: LiveData<Event<State<UpdateDeviceConnectedResponse>>> get() = _updateDeviceConnectedResults


    fun updateDeviceConnectedStatus(token: String, body: UpdateDeviceConnectedModel) {
        viewModelScope.launch {
            authRepository.updateDeviceConnectedStatus(token, body).collect {
                _updateDeviceConnectedResults.value = Event(it)
            }
        }
    }
}