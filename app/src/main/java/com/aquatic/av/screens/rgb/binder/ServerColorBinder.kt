package com.aquatic.av.screens.rgb.binder

import android.content.Context
import android.graphics.Color
import com.aquatic.av.R
import com.aquatic.av.databinding.RowItemFavoriteColorBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.rgb.dataModel.RGBColorModel

class ServerColorBinder(
    private val itemClickListener: (item: RGBColorModel.ResponseData, position: Int) -> Unit,
    private val itemLongClickListener: (item: RGBColorModel.ResponseData, position: Int) -> Boolean,
    private val itemSelectClickListener: (item: RGBColorModel.ResponseData, position: Int) -> Unit,
    private val itemUnSelectClickListener: (item: RGBColorModel.ResponseData, position: Int) -> Unit
) : BaseRecyclerViewBinder<RGBColorModel.ResponseData, RowItemFavoriteColorBinding>(R.layout.row_item_favorite_color) {
    public var showItemSelector = false

    override fun bindView(
        entity: RGBColorModel.ResponseData,
        position: Int,
        viewHolder: BaseViewHolder<RowItemFavoriteColorBinding>?,
        context: Context,
        collections: MutableList<RGBColorModel.ResponseData>
    ) {
        viewHolder?.viewBinding?.let {
            it.showItemSelector = showItemSelector
            it.chkAddFavorite.setOnCheckedChangeListener({
                    buttonView, isChecked ->
                if (isChecked){
                    itemSelectClickListener.invoke(entity, position)
                }else{
                    itemUnSelectClickListener.invoke(entity, position)
                }
            })
            it.imgItem.setColorFilter(
                Color.parseColor(
                    if (entity.getColorItem()
                            .contains("#")
                    ) entity.getColorItem() else "#" + entity.getColorItem()
                )
            )
            it.root.setOnClickListener {
                if (!showItemSelector)
                    itemClickListener.invoke(entity, position)
                else {
                    viewHolder.viewBinding!!.chkAddFavorite.isChecked = !viewHolder.viewBinding!!.chkAddFavorite.isChecked
                }
            }
            it.root.setOnLongClickListener { itemLongClickListener.invoke(entity, position) }
        }
    }
}