package com.aquatic.av.screens.dealerLocator.binder

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.RowItemCheckboxBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.dealerLocator.model.DealerFilterResponse

class DealerTypeBinder : BaseRecyclerViewBinder<DealerFilterResponse.DealerTypeData, RowItemCheckboxBinding>(R.layout.row_item_checkbox) {
    val selectedItems = mutableListOf<String>()

    override fun bindView(entity: DealerFilterResponse.DealerTypeData, position: Int, viewHolder: BaseViewHolder<RowItemCheckboxBinding>?,
                          context: Context, collections: MutableList<DealerFilterResponse.DealerTypeData>) {
        viewHolder?.viewBinding?.apply {
            btnItem.text = entity.value
            btnItem.setOnCheckedChangeListener(null)
            btnItem.isChecked = selectedItems.contains(entity.value)
            btnItem.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    selectedItems.add(entity.value)
                } else {
                    selectedItems.remove(entity.value)
                }
            }
        }
    }
}