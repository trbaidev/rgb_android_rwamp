package com.aquatic.av.screens.setting.myEnvironments.addNewUploadReceipt.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.AddDeviceModel
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.AddDeviceResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UploadReceiptRegDeviceViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _userRegUploadReceiptRegDeviceResponseResults =MutableLiveData<Event<State<AddDeviceResponse>>>()
    val userRegUploadReceiptRegDeviceResponseResults:LiveData<Event<State<AddDeviceResponse>>>  = _userRegUploadReceiptRegDeviceResponseResults

    fun registerDevice(token:String,addDeviceModel: AddDeviceModel) {
        viewModelScope.launch {
            authRepository.registerDevice(token,addDeviceModel).collect {
                _userRegUploadReceiptRegDeviceResponseResults.value = (Event(it))
            }
        }
    }
}