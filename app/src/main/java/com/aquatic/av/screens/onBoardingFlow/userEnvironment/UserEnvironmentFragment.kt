package com.aquatic.av.screens.onBoardingFlow.userEnvironment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentUserEnvironmentBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressResponse
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.onBoardingFlow.dataModels.DevicesInformation
import com.aquatic.av.screens.onBoardingFlow.userEnvironment.binders.UserEnvironmentBinder
import com.aquatic.av.screens.onBoardingFlow.userEnvironment.dataModels.UserCustomEnvironmentModel
import com.aquatic.av.screens.onBoardingFlow.userEnvironment.dataModels.UserCustomEnvironmentResponse
import com.aquatic.av.screens.onBoardingFlow.userEnvironment.dataModels.UserEnvironmentResponse
import com.aquatic.av.screens.onBoardingFlow.userEnvironment.dataModels.UserOnboardingEnvironmentModel
import com.aquatic.av.screens.onBoardingFlow.userEnvironment.viewModel.UserCustomEnvironmentViewModel
import com.aquatic.av.screens.onBoardingFlow.userEnvironment.viewModel.UserEnvironmentViewModel
import com.aquatic.av.screens.onBoardingFlow.userRegSelectProductType.UserRegSelectProductTypeFragment
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.EditTextExtension.mapToObject
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.aquatic.av.views.dialogs.DialogFactory.showDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserEnvironmentFragment :
        BaseFragment<FragmentUserEnvironmentBinding>(R.layout.fragment_user_environment) {

    private var environmentResponse: UserEnvironmentResponse? = null
    private var userAddressResponse: UserAddressResponse? = null
    private var userAddressModel: UserAddressModel? = null
    private var devicesInformation: DevicesInformation? = DevicesInformation()
    private var environment: UserEnvironmentResponse.Environment? = null
    private val viewModel by viewModels<UserEnvironmentViewModel>()
    private val customEnvViewModel by viewModels<UserCustomEnvironmentViewModel>()

    private val userEnvironmentItemAdapter by lazy {
        BaseRecyclerViewAdapter(
                arrayListOf(),
                userEnvironmentBinder,
                requireContext()
        )
    }

    private val userEnvironmentBinder by lazy {
        UserEnvironmentBinder { entity, position ->
            updateData(entity, position)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun updateData(
            selectedEnvironment: UserEnvironmentResponse.Environment?,
            position: Int
    ) {
        binding.customEnvironmentName = ""
        binding.isCustomEnvironment = false
        val env = environmentResponse?.responseData?.static_env?.find { it.selected }
        env?.selected = false
        environmentResponse?.responseData?.static_env?.get(position)?.selected = true
        updateEnvironment(selectedEnvironment)
        userEnvironmentItemAdapter.notifyDataSetChanged()

    }


    private fun resetEnvirSelection(){
        val env = environmentResponse?.responseData?.static_env?.find { it.selected }
        env?.selected = false
        userEnvironmentBinder.lastSelectedIndex=-1
        userEnvironmentItemAdapter.notifyDataSetChanged()
    }
    private fun updateEnvironment(selectedEnvironment: UserEnvironmentResponse.Environment?) {
        environment = selectedEnvironment
        devicesInformation?.env_name = environment?.environment_name
        devicesInformation?.env_icon_name = environment?.env_icon_name
        devicesInformation?.env_id = environment?.environment_id ?: 0
        devicesInformation?.env_type = environment?.environment_type
//        showDialog(
//                "",
//                "Environment " + "\"" + environment?.environment_name + "\"" + " has been created",
//                true
//        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        seCustomEnvListeners()
        binding.btnBack.isEnabled = false
        binding.rvEnvironments.adapter = userEnvironmentItemAdapter
        binding.rvEnvironments.layoutManager = LinearLayoutManager(mainActivity)
        if (environmentResponse?.responseData?.static_env != null) {
            environmentResponse?.let {
                userEnvironmentItemAdapter.addAll(it.responseData.static_env ?: arrayListOf()
                )
            }
        }
        binding.apply {
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnNext.setOnClickListener {
                if (environment == null)
                    showToast("Please select an environment.")
                else if (devicesInformation?.env_icon_name.isNullOrEmpty())
                    showToast("Please select an icon for the environment.")
                else
                    mainActivity.navigateTo(
                            UserRegSelectProductTypeFragment.getInstance(
                                    userAddressResponse,
                                    userAddressModel,
                                    devicesInformation
                            )
                    )
            }
            tvCustomEnv.setOnClickListener {
                isCustomEnvironment = !isCustomEnvironment
                resetEnvirSelection()
            }
            ivOne.setOnClickListener {
                devicesInformation?.env_icon_name = customEnvironmentName
                resetEnvirSelection()
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_0")
            }
            ivTwo.setOnClickListener {
                devicesInformation?.env_icon_name = customEnvironmentName
                resetEnvirSelection()
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_1")
            }
            ivThree.setOnClickListener {
                devicesInformation?.env_icon_name = customEnvironmentName
                resetEnvirSelection()
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_2")
            }
            ivFour.setOnClickListener {
                devicesInformation?.env_icon_name = customEnvironmentName
                resetEnvirSelection()
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_3")
            }
            ivFive.setOnClickListener {
                devicesInformation?.env_icon_name = customEnvironmentName
                resetEnvirSelection()
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_4")
            }
            ivSix.setOnClickListener {
                devicesInformation?.env_icon_name = customEnvironmentName
                resetEnvirSelection()
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_5")
            }
            ivSeven.setOnClickListener {
                devicesInformation?.env_icon_name = customEnvironmentName
                resetEnvirSelection()
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_6")
            }
            ivEight.setOnClickListener {
                devicesInformation?.env_icon_name = customEnvironmentName
                resetEnvirSelection()
                setCustomEnvironment(
                        edtEnv.text.trim().toString(),
                        "vector_7"
                )
            }
        }
        if (environmentResponse == null) {
            viewModel.getAllEnvironments(
                    UserOnboardingEnvironmentModel(
                            devicesInformation?.user_id,
                            arrayOf("S")
                    )
            )
        }
    }

    private fun setCustomEnvironment(envName: String, selectedEnvironmentName: String?) {
        if (envName.isEmpty()) {
            showToast("PLease enter Environment name")
        } else {
            binding.customEnvironmentName = selectedEnvironmentName
            val cusEnv = UserCustomEnvironmentModel()
            cusEnv.env_Icon_Name = binding.customEnvironmentName
            cusEnv.environmentId = environment?.environment_id ?: 0
            cusEnv.environmentName = envName
            cusEnv.environmentType = "d"
            cusEnv.isActive = environment?.is_active ?: 1
            cusEnv.userIds = arrayOf(userAddressResponse?.responseData?.user_info?.user_id ?: -1)
            customEnvViewModel.addUpdateEnvironments(
                    requireContext().getAccessToken(),
                    cusEnv
            )
        }
    }


    override fun setHeaderFooterViews(
            titleBar: TitleBar?,
            bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }

    private fun setListeners() {
        viewModel.userEnvironmentResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        val enviResponse = Gson().mapToObject(
                                it.body as Map<String, Any?>?,
                                UserEnvironmentResponse::class.java
                        )
                        if (enviResponse != null) {
                            environmentResponse = enviResponse
                        }
                        userEnvironmentItemAdapter.addAll(
                                environmentResponse?.responseData?.static_env ?: arrayListOf()
                        )
                    }
                }
            }
        })
    }


    private fun seCustomEnvListeners() {
        customEnvViewModel.userCustomEnvironmentResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        val userCustomEnvironmentResponse = Gson().mapToObject(
                                it.body as Map<String, Any?>?,
                                UserCustomEnvironmentResponse::class.java
                        )
                        if (userCustomEnvironmentResponse?.responseCode == 1) {
                            showToast("Environment updated")
                            val cusEnv = UserEnvironmentResponse.Environment(
                                    environment_id = userCustomEnvironmentResponse.responseData,
                                    environment_name = binding.edtEnv.text.trim().toString(),
                                    environment_type = "d",
                                    is_active = 1,
                                    env_icon_name = binding.customEnvironmentName ?: "",
                                    selected = true
                            )
                            updateEnvironment(cusEnv)
                        }
                    }
                }
            }
        })
    }

    companion object {
        fun getInstance(
                userAddressResponse: UserAddressResponse?,
                userAddressModel: UserAddressModel?,
                devicesInformation: DevicesInformation?
        ): UserEnvironmentFragment {
            val userEnvironmentFragment = UserEnvironmentFragment()
            userEnvironmentFragment.userAddressResponse = userAddressResponse
            userEnvironmentFragment.userAddressModel = userAddressModel
            userEnvironmentFragment.devicesInformation = devicesInformation
            return userEnvironmentFragment
        }
    }
}