package com.aquatic.av.screens.setting.myEnvironments.addnewdevice.dataNodel

data class AddDeviceDataModel(
    var userId: Int,
    var types: Array<String>
)