package com.aquatic.av.screens.setting.myEnvironments.addNewDeviceProductType.binders

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.AddNewDeviceSeletctProductTypeRowItemBinding


import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.setting.myEnvironments.addNewDeviceProductType.dataModels.AddNewDeviceProductTypeResponse


class AddNewDeviceProductTypeBinder(private var itemClickLister: (entity: AddNewDeviceProductTypeResponse, position: Int) -> Unit) :
    BaseRecyclerViewBinder<AddNewDeviceProductTypeResponse, AddNewDeviceSeletctProductTypeRowItemBinding>(
        R.layout.add_new_device_seletct_product_type_row_item
    ) {
    override fun bindView(
        entity: AddNewDeviceProductTypeResponse,
        position: Int,
        viewHolder: BaseViewHolder<AddNewDeviceSeletctProductTypeRowItemBinding>?,
        context: Context,
        collections: MutableList<AddNewDeviceProductTypeResponse>
    ) {
        viewHolder?.viewBinding?.let {
            it.dataItem = entity
            viewHolder
            viewHolder.itemView.setOnClickListener {
                itemClickLister.invoke(entity, position)
            }
        }
    }
}