package com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyEnvironmentViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _userEnvironmentResults = MutableLiveData<Event<State<Any>>>()
    val userEnvironmentResults: LiveData<Event<State<Any>>> get() = _userEnvironmentResults

    fun getAllEnvironments(userOnboardingEnvironmentModel: Any) {
        viewModelScope.launch {
            authRepository.getAllEnvironments(userOnboardingEnvironmentModel).collect {
                _userEnvironmentResults.value = (Event(it))
            }
        }
    }
}