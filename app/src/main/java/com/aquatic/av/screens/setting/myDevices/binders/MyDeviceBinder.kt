package com.aquatic.av.screens.setting.myDevices.binders

import android.content.Context
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.aquatic.av.R
import com.aquatic.av.databinding.MyDeviceRowItemBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.dashboard.dataModel.DashboardItemModel
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentModel


class MyDeviceBinder(
    private val connectedDeviceName: String,
    private val statusConnectedItemClickListener: (item: DashboardItemModel) -> Unit,
    private var itemClickLister: (entity: DashboardItemModel, position: Int) -> Unit,
    private var itemBtnClickLister: (entity: DashboardItemModel, position: Int) -> Unit,
    private var envBtnClickLister: (entity: MyEnvironmentModel, position: Int) -> Unit
) :
    BaseRecyclerViewBinder<DashboardItemModel, MyDeviceRowItemBinding>(R.layout.my_device_row_item),
    AdapterView.OnItemSelectedListener {

    var environmentList: List<MyEnvironmentModel>? = null

    var environmentModel: MyEnvironmentModel? = null

    lateinit var context: Context


    @JvmName("setEnvironmentList1")
    fun setEnvironmentList(envList: List<MyEnvironmentModel>?) {
        environmentList = envList
    }

    override fun bindView(
        entity: DashboardItemModel,
        position: Int,
        viewHolder: BaseViewHolder<MyDeviceRowItemBinding>?,
        context: Context,
        collections: MutableList<DashboardItemModel>
    ) {
        this.context = context
        viewHolder?.itemView?.setOnClickListener {
            itemClickLister.invoke(entity, position)
        }
        viewHolder?.viewBinding?.apply {
            dataItem = entity
            btnConnectBT.setOnClickListener {
                statusConnectedItemClickListener.invoke(entity)
            }
            spEnv.setOnItemSelectedListener(this@MyDeviceBinder)
            if (environmentList != null) {
                val mAdapter = EnvironmentsSpinner(environmentList!!)
                spEnv.setAdapter(mAdapter)
                if (!entity.envName.isNullOrEmpty()) {
                    var item = environmentList?.find { myEnvironmentModel: MyEnvironmentModel -> myEnvironmentModel.getEnvName() == entity.envName }
                    var index=  environmentList?.indexOf(item)
                    spEnv.setSelection(if(index!=-1)index!! else 0)
                }
            }
            if (entity.device_uuid.equals(connectedDeviceName,true)
                && !(entity.device_uuid.isNullOrEmpty() && connectedDeviceName.isNullOrEmpty())){
                entity.deviceConnectImage = R.drawable.ic_status_vector_enabled
                imgStatus.setBackgroundResource(R.drawable.ic_status_vector_enabled)
            } else {
                entity.deviceConnectImage = R.drawable.ic_status_vector_disabled
                imgStatus.setBackgroundResource(R.drawable.ic_status_vector_disabled)
            }
            btnNameEdit.setOnClickListener {
                if (isEditable) {
                    if (environmentModel != null) {
                        envBtnClickLister.invoke(environmentModel!!, position)
                    }
                    spEnv.visibility = View.GONE
                    txtHolder.visibility = View.VISIBLE
                    var name = firstLine.text.toString().trim()
                    if (!TextUtils.isEmpty(name)) {
                        isEditable = !isEditable
                        entity.deviceName = firstLine.text.toString().trim()
                        itemBtnClickLister.invoke(entity, position)
                    } else {
                        Toast.makeText(context, "Please Enter Environment Name", Toast.LENGTH_SHORT)
                            .show()
                    }
                } else {
                    isEditable = !isEditable
                    spEnv.visibility = View.VISIBLE
                    txtHolder.visibility = View.GONE
                }
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (environmentList != null) {
            environmentModel = environmentList?.get(position)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }


}