package com.aquatic.av.screens.dashboard.binder

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.RowItemDashboardBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.dashboard.dataModel.DashboardItemModel

class DashboardItemBinder(private val isDeviceConnected: Boolean, private val connectedDeviceID: Int, private val connectedDeviceName: String, private val itemClickListener: (item: DashboardItemModel) -> Unit,
                          private val statusConnectedItemClickListener: (item: DashboardItemModel) -> Unit) : BaseRecyclerViewBinder<DashboardItemModel, RowItemDashboardBinding>(R.layout.row_item_dashboard) {

    override fun bindView(entity: DashboardItemModel, position: Int, viewHolder: BaseViewHolder<RowItemDashboardBinding>?,
                          context: Context, collections: MutableList<DashboardItemModel>) {
        viewHolder?.viewBinding?.apply {
            // TODO: 01/12/2021 set last connected days 
            itemData = entity
            if (isDeviceConnected) {
                if (entity.device_uuid.equals(connectedDeviceName,true)
                    && !(entity.device_uuid.isNullOrEmpty() && connectedDeviceName.isNullOrEmpty())) {
                    entity.deviceConnectImage=R.drawable.ic_status_vector_enabled
                    entity.deviceConnectImage = R.drawable.ic_status_vector_enabled
                    ivStatus.setBackgroundResource(R.drawable.ic_status_vector_enabled)
                    tvConnectStatus.text = "Connected"
                }else {
                    entity.deviceConnectImage=R.drawable.ic_status_vector_disabled
                    ivStatus.setBackgroundResource(R.drawable.ic_status_vector_disabled)
                    tvConnectStatus.text = "Not Connected"
                }
            } else {
                entity.deviceConnectImage=R.drawable.ic_status_vector_disabled
                entity.deviceConnectImage = R.drawable.aquatic_av_off
                ivStatus.setBackgroundResource(R.drawable.ic_status_vector_disabled)
                tvConnectStatus.text = "Not Connected"
            }
            root.setOnClickListener {
                itemClickListener.invoke(entity)
            }
            llConnectStatus.setOnClickListener {
                statusConnectedItemClickListener.invoke(entity)
            }
        }
    }
}