package com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.DealerModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserRegUploadReceiptDealerViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _userRegUploadReceiptDealerResponseResults = MutableLiveData<Event<State<ArrayList<DealerModel>>>>()
    val userRegUploadReceiptDealerResponseResults: LiveData<Event<State<ArrayList<DealerModel>>>> get() = _userRegUploadReceiptDealerResponseResults

    fun getAllProductTypes() {
        viewModelScope.launch {
            authRepository.getAllDealers().collect {
                _userRegUploadReceiptDealerResponseResults.value = (Event(it))
            }
        }
    }
}