package com.aquatic.av.screens.setting.myDevices

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentMyDevicesBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.bluetooth.ConnectedDeviceFragment
import com.aquatic.av.screens.bluetooth.ScannerFragment
import com.aquatic.av.screens.dashboard.dataModel.DashboardItemModel
import com.aquatic.av.screens.setting.myDevices.binders.MyDeviceBinder
import com.aquatic.av.screens.setting.myDevices.dataModels.MyDeviceModel
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentModel
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentResponse
import com.aquatic.av.screens.setting.myDevices.viewModel.MyDeviceUpdateViewModel
import com.aquatic.av.screens.setting.myDevices.viewModel.MyDeviceViewModel
import com.aquatic.av.screens.setting.myDevices.viewModel.UpdateEnvironmentViewModel
import com.aquatic.av.screens.setting.myEnvironments.addnewdevice.AddNewDeviceFragment
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentModel
import com.aquatic.av.screens.setting.myEnvironments.viewModel.MyEnvironmentsViewModel
import com.aquatic.av.screens.setting.productRegistration.ProductRegistrationFragment
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.getUserDetailModel
import com.aquatic.av.utils.isDeviceConnected
import com.aquatic.av.utils.showToast
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MyDevicesFragment : BaseFragment<FragmentMyDevicesBinding>(R.layout.fragment_my_devices) {

    val userInfoModel by lazy { requireContext().getUserDetailModel() }
    private val myDeviceViewModel by viewModels<MyDeviceViewModel>()
    private val myEnvironmentsViewModel by viewModels<MyEnvironmentsViewModel>()
    private val myUpdateDeviceViewModel by viewModels<MyDeviceUpdateViewModel>()
    private val myEnvironmentUpdateViewModel by viewModels<UpdateEnvironmentViewModel>()
    var selectedEnvironmentModel: MyEnvironmentModel? = null
    private val preferences by lazy {
        requireContext().getSharedPreferences(mainActivity.packageName, Context.MODE_PRIVATE)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setEnvironmentsListeners()
        setUpdateListeners()
        setUpdateEnvListeners()
        binding.myRecyclerView.adapter = myDeviceAdapter
        binding.myRecyclerView.layoutManager = LinearLayoutManager(mainActivity)

        binding.apply {
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnAddNewDevice.setOnClickListener {
                mainActivity.navigateTo(
                    AddNewDeviceFragment.getInstance(
                        "Add New Device",
                        "NEW CUSTOM ENVIRONMENT"
                    )
                )
            }
        }
        userInfoModel?.user_email?.let {
            myDeviceViewModel.getDevicesInfoDash(
                requireContext().getAccessToken(),
                it,
                ""
            )
        }
        userInfoModel?.user_id?.let {
            var jObj = JsonObject()
            jObj.addProperty("user_id", it)
            myEnvironmentsViewModel.getMyEnvironmentInfo(
                requireContext().getAccessToken(),
                jObj
            )
        }

    }

    private val myDeviceAdapter by lazy {
        BaseRecyclerViewAdapter(
            arrayListOf(),
            myDeviceBinder,
            requireContext()
        )
    }

    private val myDeviceBinder by lazy {
        MyDeviceBinder(
            connectedDeviceName = preferences.getString("connected_device_name", "") ?: "",
            statusConnectedItemClickListener={
                if (isDeviceConnected()) {
                    it.deviceId?.let { it1 ->
                        preferences.getString("connected_device_name", "")?.let { it2 ->
                            ConnectedDeviceFragment.newInstance(
                                it2,
                                it1
                            )
                        }
                    }?.let { it2 -> mainActivity.navigateTo(it2) }
                } else {
                    it.deviceId?.let { it1 ->
                        preferences.getString("connected_device_name", "")?.let { it2 ->
                            ScannerFragment.newInstance(
                                it2,
                                it1
                            )
                        }
                    }?.let { it2 -> mainActivity.navigateTo(it2) }
                }
            },
            itemBtnClickLister = { entity: DashboardItemModel, position: Int ->
                myUpdateDeviceViewModel.updateDeviceName(
                    requireContext().getAccessToken(),
                    MyDeviceModel(
                        device_id = entity.deviceId!!,
                        device_name = entity.deviceName!!,
                        modified_by = userInfoModel?.user_email ?: ""
                    )
                )
                if(selectedEnvironmentModel!=null){
                    myEnvironmentUpdateViewModel.updateEnvironment(requireContext().getAccessToken(),
                        UpdateEnvironmentModel(device_id = entity.deviceId!!,
                        env_name = selectedEnvironmentModel?.getEnvName()!!,
                        env_icon_name = selectedEnvironmentModel?.getEnvIconName()!!,
                        modified_by = userInfoModel?.user_email!!,
                        user_email = userInfoModel?.user_email!!,
                        user_id = userInfoModel?.user_id!!,
                        env_id = selectedEnvironmentModel?.getEnvId()!!)
                    )
                }
            },
            itemClickLister = { entity: DashboardItemModel, position: Int ->
                mainActivity.navigateTo(ProductRegistrationFragment.getInstance(entity))
            },
            envBtnClickLister = {entity: MyEnvironmentModel, position: Int ->
                selectedEnvironmentModel=entity
            }
        )
    }

    private fun setEnvironmentsListeners() {
        myEnvironmentsViewModel.myEnvironmentResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        it.body.getResponseData()?.let { it1 ->
                            myDeviceBinder.setEnvironmentList(it1)
                            myDeviceAdapter.notifyDataSetChanged()
                        }
                    }
                }
            }
        })
    }

    private fun setUpdateEnvListeners() {
        myEnvironmentUpdateViewModel.envResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        if (it.body.responseMessage.equals("Success", true)) {
                            userInfoModel?.user_id?.let {
                                var jObj = JsonObject()
                                jObj.addProperty("user_id", it)
                                myEnvironmentsViewModel.getMyEnvironmentInfo(
                                    requireContext().getAccessToken(),
                                    jObj
                                )
                            }
                        }
                    }
                }
            }
        })
    }

    private fun setListeners() {
        myDeviceViewModel.myDeviceResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        if (it.body.responseCode == 1) {
                            it.body.responseData?.let { it1 ->
                                myDeviceAdapter.addAll(
                                    it1
                                )
                            }
                        }
                    }
                }
            }
        })
    }

    private fun setUpdateListeners() {
        myUpdateDeviceViewModel.myUpdateDeviceResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        userInfoModel?.user_email?.let {
                            myDeviceViewModel.getDevicesInfoDash(
                                requireContext().getAccessToken(),
                                it,
                                ""
                            )
                        }
                    }
                }
            }
        })
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        titleBar?.hideButtons()
        bottomNavigationView?.visible()
    }


}