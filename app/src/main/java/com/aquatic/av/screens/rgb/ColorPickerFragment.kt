package com.aquatic.av.screens.rgb

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentColorPickerBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.rgb.viewModel.RGBViewModel
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint





@AndroidEntryPoint
class ColorPickerFragment : BaseFragment<FragmentColorPickerBinding>(R.layout.fragment_color_picker) {
    val mRGBViewModel by viewModels<RGBViewModel>()

    override fun setHeaderFooterViews(titleBar: TitleBar?, bottomNavigationView: BottomNavigationView?) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.showTitleBar()
        bottomNavigationView?.visible()
        titleBar?.hideButtons()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)
        binding.viewPager.adapter = sectionsPagerAdapter
        binding.viewPager.setPagingEnabled(false)
        binding.tabs.setupWithViewPager(binding.viewPager)
   //     hideSystemUI()
    }
//    private fun hideSystemUI() {
//        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
//            val v: View = mainActivity.getWindow().getDecorView()
//            v.systemUiVisibility = View.GONE
//        } else if (Build.VERSION.SDK_INT >= 19) {
//            //for new api versions.
//            val decorView: View = mainActivity.getWindow().getDecorView()
//            val uiOptions =
//                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//            decorView.systemUiVisibility = uiOptions
//        }
//    }
    fun navigateColorPicker(): Unit {
        binding.viewPager.setCurrentItem(0, true)

    }

    @Suppress("DEPRECATION")
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getCount(): Int = 3
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> AdjustColorFragment()
                1 -> ModeSelectionFragment()
                else -> FavoriteColorFragment()
            }
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "Adjust"
                1 -> "Modes"
                else -> "Favorites"
            }
        }


    }


}