package com.aquatic.av.screens.setting

import android.os.Bundle
import android.view.View
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentAboutUsBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView

class AboutUsFragment : BaseFragment<FragmentAboutUsBinding>(R.layout.fragment_about_us) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnBack.setOnClickListener { mainActivity.onBackPressed() }

        }
    }

     override fun setHeaderFooterViews(titleBar: TitleBar?, bottomNavigationView: BottomNavigationView?) {
        super.setHeaderFooterViews(titleBar,bottomNavigationView)
        titleBar?.hideTitleBar()
    }
}