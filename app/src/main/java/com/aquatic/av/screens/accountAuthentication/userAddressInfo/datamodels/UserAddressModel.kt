package com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels


data class UserAddressModel(
    var first_name: String = "",
    var last_name: String = "",
    var user_email: String = "",
    var user_password: String = "",
    var user_password_plain: String = "",
    var address: String = "",
    var city: String = "",
    var state: String = "",
    var zip_code: String = "",
    var phone: String = "",
    var country: String = "",
    var device_info: String = "",
    var created_by: String = user_email,
    var modified_by: String = user_email,
    var user_id: Int = 0,
    var user_type_id: Int = 3,
    var user_role_id: Int = 3,
    var device_source: String = "Mobile",
    var is_terms_accepted: Boolean = true,
    var is_notification_enabled: Boolean = true
)
