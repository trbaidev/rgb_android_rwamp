package com.aquatic.av.screens.setting.myDevices.dataModels

data class UpdateEnvironmentModel(
    var device_id: Int,
    var env_name: String,
    var env_icon_name: String,
    var modified_by: String,
    var user_email: String,
    var user_id: Int,
    var env_id: Int
)