package com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserRegSelectDeviceViewModel @Inject constructor(private val authRepository: AuthRepository) :
    ViewModel() {
    private val _userRegSelectDeviceResults =
        MutableLiveData<Event<State<ArrayList<Any>>>>()
    val userRegSelectDeviceResults: LiveData<Event<State<ArrayList<Any>>>> get() = _userRegSelectDeviceResults

    fun getAllProductsByTypeId(userRegSelectedDeviceModel: Any) {
        viewModelScope.launch {
            authRepository.getAllProductsByTypeId(userRegSelectedDeviceModel).collect {
                _userRegSelectDeviceResults.value = (Event(it))
            }
        }
    }




}