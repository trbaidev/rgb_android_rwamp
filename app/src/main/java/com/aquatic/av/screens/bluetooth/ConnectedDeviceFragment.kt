package com.aquatic.av.screens.bluetooth

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.RGBManager
import com.aquatic.av.bluetoothScanner.ble.adapter.ConnectedDeviceAdapter
import com.aquatic.av.bluetoothScanner.ble.adapter.OnItemClickListener
import com.aquatic.av.bluetoothScanner.ble.adapter.RGBBluetoothDevice
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceKey
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceUtil
import com.aquatic.av.bluetoothScanner.ble.profile.BTConnectionListener
import com.aquatic.av.bluetoothScanner.ble.utils.BTCon
import com.aquatic.av.bluetoothScanner.ble.utils.BTUtil
import com.aquatic.av.screens.bluetooth.viewModel.BleConnectionViewModel
import com.aquatic.av.databinding.FragmentConnectedDeviceBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedModel
import com.aquatic.av.screens.bluetooth.viewModel.ScannerViewModel
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ConnectedDeviceFragment : BaseFragment<FragmentConnectedDeviceBinding>(R.layout.fragment_connected_device), OnItemClickListener, BTConnectionListener {
    private val viewModel by viewModels<BleConnectionViewModel>()
    private val mScannerViewModel by viewModels<ScannerViewModel>()
    private var adapter: ConnectedDeviceAdapter? = null
    private var deviceName: String = ""
    private var deviceID: Int = -1
    private val preferences by lazy {
        requireContext().getSharedPreferences(mainActivity.packageName, Context.MODE_PRIVATE)
    }

    override fun setHeaderFooterViews(titleBar: TitleBar?, bottomNavigationView: BottomNavigationView?) {
        super.setHeaderFooterViews(titleBar,bottomNavigationView)
        titleBar?.hideTitleBar()
    }

    companion object {
        fun newInstance(deviceName: String, deviceID: Int): ConnectedDeviceFragment {

            val fragment = ConnectedDeviceFragment()
            fragment.deviceID = deviceID
            fragment.deviceName = deviceName
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()

        initConnectedDevices()
        RGBManager.getInstance().registerConnectionListener(this)
        binding.btnScanDevice.setOnClickListener {
            mainActivity.navigateTo(ScannerFragment.newInstance(deviceName, deviceID))
        }
        binding.connectionStateLayout.gone()
        binding.dmgContainer.setOnClickListener { mainActivity.onBackPressed() }
    }


    private fun initObservers() {
        viewModel.isConnected.observe(viewLifecycleOwner, { connected: Boolean? ->
            if (!connected!!) {
                BTCon.setConnectionState(false)
                val editor = preferences.edit()
                editor.putString("connected_device_name", "")
                editor.putInt("connected_device_id", -1)
                editor.apply()
                Toast.makeText(requireContext(), R.string.state_disconnected, Toast.LENGTH_SHORT).show()
            } else {
                BTCon.setConnectionState(true)
                val editor = preferences.edit()
                editor.putString("connected_device_name", deviceName)
                editor.putInt("connected_device_id", deviceID)
                editor.apply()
                Toast.makeText(requireContext(), R.string.state_connected, Toast.LENGTH_SHORT).show()
            }
            dismissProgressBar()
        })
    }

    private fun initConnectedDevices() {
        val name = PreferenceUtil.getInstance().getStringValue(PreferenceKey.CONNECTED_BT_DEVICE_NAME, null)
        val address = PreferenceUtil.getInstance().getStringValue(PreferenceKey.CONNECTED_BT_DEVICE_ADDRESS, null)
        if (TextUtils.isEmpty(address)) {
            binding.recyclerViewConnectedDevices.gone()
            return
        }
        binding.recyclerViewConnectedDevices.visible()
        binding.recyclerViewConnectedDevices.layoutManager = LinearLayoutManager(requireContext())
        val dividerItemDecoration = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        binding.recyclerViewConnectedDevices.addItemDecoration(dividerItemDecoration)
        val device = BTUtil.getRemoteDevice(requireContext(), address)
        val rgbBluetoothDevice = RGBBluetoothDevice(name, device)
        var devices: MutableList<RGBBluetoothDevice> = mutableListOf()
        devices.add(rgbBluetoothDevice)
        devices = viewModel.getDeviceConnectionState(requireContext(), devices).toMutableList()
        adapter = ConnectedDeviceAdapter(requireContext(), devices)
        adapter?.setOnItemClickListener(this)
        binding.recyclerViewConnectedDevices.adapter = adapter
    }

    override fun onItemClick(device: RGBBluetoothDevice) {
        if (device.isConnectionState) {
            showProgressBar(getString(R.string.state_disconnecting))
            val isDisconnect = viewModel.disconnect()
            device.isConnectionState = !isDisconnect
            adapter?.updateConnectedDevice(device)
        } else {
            showProgressBar(getString(R.string.state_connecting))
            deviceName=device.name
            viewModel.connect(device)
            mScannerViewModel.updateDeviceConnectedStatus(
                requireContext().getAccessToken(),
                UpdateDeviceConnectedModel(
                    device_id = deviceID,
                    uuid = device.name
                )
            )
        }
    }

    override fun onDeviceConnected(device: BluetoothDevice) {
        updateAdapterDevice(device, true)
    }

    private fun updateAdapterDevice(device: BluetoothDevice?, b: Boolean) {
        viewModel.mIsConnected.postValue(b)
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            dismissProgressBar()
            val rgbBluetoothDevice: RGBBluetoothDevice = if (device != null) {
                RGBBluetoothDevice(device.name, device)
            } else {
                val name = PreferenceUtil.getInstance().getStringValue(PreferenceKey.CONNECTED_BT_DEVICE_NAME, null)
                val address = PreferenceUtil.getInstance().getStringValue(PreferenceKey.CONNECTED_BT_DEVICE_ADDRESS, null)
                val remoteDevice = BTUtil.getRemoteDevice(requireContext(), address)
                RGBBluetoothDevice(name, remoteDevice)
            }
            rgbBluetoothDevice.isConnectionState = b
            adapter?.updateConnectedDevice(rgbBluetoothDevice)
        }
    }

    override fun onDeviceDisconnected(device: BluetoothDevice) {
        updateAdapterDevice(device, false)
    }

    override fun onDataSent() {}
    override fun onSuccess(data: String) {}
    override fun onFailed(data: String) {}
    override fun onDataReceived(characteristic: BluetoothGattCharacteristic, bytes: ByteArray) {}
    override fun onDestroy() {
        super.onDestroy()
        RGBManager.getInstance().unregisterConnectionListener(this)
    }

    private fun showProgressBar(message: String) {
        binding.connectionStateLayout.visible()
        binding.connectionState.text = message
    }

    private fun dismissProgressBar() {
        binding.connectionStateLayout.gone()
    }
}