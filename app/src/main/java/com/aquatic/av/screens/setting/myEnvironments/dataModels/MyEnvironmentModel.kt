package com.aquatic.av.screens.setting.myEnvironments.dataModels

import com.aquatic.av.R
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MyEnvironmentModel(
    @SerializedName("env_id")
    @Expose
    private var envId: Int? = -1,

    @SerializedName("env_name")
    @Expose
    private var envName: String? = "",

    @SerializedName("env_icon_name")
    @Expose
    private var envIconName: String? = "",

    @SerializedName("env_type")
    @Expose
    private var envType: String? = "",

    @SerializedName("devices")
    @Expose
    private var devices: List<Device>? = null,

    private var isExpanded: Boolean = false
) {


    fun getEnvId(): Int? {
        return envId
    }

    fun setEnvId(envId: Int?) {
        this.envId = envId
    }

    fun getEnvName(): String? {
        return envName
    }

    fun setEnvName(envName: String?) {
        this.envName = envName
    }

    fun getEnvIconName(): String? {
        return envIconName
    }

    fun setEnvIconName(envIconName: String?) {
        this.envIconName = envIconName
    }

    fun getDevices(): List<Device>? {
        return devices
    }

    fun setDevices(devices: List<Device>?) {
        this.devices = devices
    }


    fun isExpanded(): Boolean {
        return isExpanded
    }

    fun setExpanded(expanded: Boolean) {
        isExpanded = expanded
    }

    fun getEnvType(): String? {
        return envType
    }

    fun setEnvType(envType: String?) {
        this.envType = envType
    }

    fun getDeviceCount(): String =
        devices?.size.toString() + if (devices?.size!! > 1) " devices" else " device"


    fun getEnvironmentImages(text: String): Int {
        return when (text) {
            "vector_0" -> R.drawable.vector_0
            "vector_1" -> R.drawable.vector_1
            "vector_2" -> R.drawable.vector_2
            "vector_3" -> R.drawable.vector_3
            "vector_4" -> R.drawable.vector_4
            "vector_5" -> R.drawable.vector_5
            "vector_6" -> R.drawable.vector_6
            "vector_7" -> R.drawable.vector_7
            else ->
                R.drawable.vector_0
        }
    }

}