package com.aquatic.av.screens.accountAuthentication

import android.os.Bundle
import android.view.View
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentAccountBinding
import com.aquatic.av.screens.accountAuthentication.createAccount.CreateAccountFragment
import com.aquatic.av.screens.accountAuthentication.login.LoginFragment
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.dealerLocator.DealerLocatorFragment
import com.aquatic.av.screens.dialogs.FeaturesBottomSheetDialogFragment
import com.aquatic.av.utils.gone
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView


class AccountFragment : BaseFragment<FragmentAccountBinding>(R.layout.fragment_account) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {

            btnLogin.setOnClickListener {
                mainActivity.navigateTo(LoginFragment())
            }
            btnRegister.setOnClickListener {
                mainActivity.navigateTo(CreateAccountFragment())
            }
            tvWhyAcc.setOnClickListener {
                openFeatureBottomSheet()
            }
            btnDealerLocation.setOnClickListener {
                mainActivity.navigateTo(DealerLocatorFragment())
            }
            btnRgbmodeOnly.setOnClickListener {
                mainActivity.initApp(isRGBMode = true)
            }
        }
    }


    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }

    private fun openFeatureBottomSheet() {
        val modalBottomSheet = FeaturesBottomSheetDialogFragment()
        modalBottomSheet.show(parentFragmentManager, FeaturesBottomSheetDialogFragment.TAG)
    }

}

