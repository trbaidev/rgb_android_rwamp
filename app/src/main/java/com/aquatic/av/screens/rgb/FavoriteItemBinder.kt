package com.aquatic.av.screens.rgb

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.RowItemFavoriteColorBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder

class FavoriteItemBinder(private val itemClickListener: (item: String) -> Unit,
                         private val itemLongClickListener: (item: String) -> Boolean) : BaseRecyclerViewBinder<String, RowItemFavoriteColorBinding>(R.layout.row_item_favorite_color) {
    var showItemSelector: Boolean = false
    var addedColorList = listOf<String>()
    override fun bindView(entity: String, position: Int, viewHolder: BaseViewHolder<RowItemFavoriteColorBinding>?, context: Context, collections: MutableList<String>) {
        viewHolder?.viewBinding?.let {
            it.showItemSelector = showItemSelector
            it.imgItem.setOnLongClickListener { itemLongClickListener.invoke(entity) }
            it.imgItem.setOnClickListener { itemClickListener.invoke(entity) }
        }
    }
}