package com.aquatic.av.screens.onBoardingFlow.dataModels

class DevicesInformation {
    var user_id:Int=-1
    var token = ""
    var env_name: String? = ""
    var env_type: String? = ""
    var env_icon_name: String? = ""
    var env_id: Int= -1
    var device_name: String? = ""
    var user_email:String=""
    var created_by:String=""
    var product_id:Int? = -1
    var product_type_id:Int? = -1
    var purchase_location_dealer_id:Int? = -1
    var serial_no = ""
    var purchase_location:String? = ""
    var purchase_date = ""
    var receipt:String?=""

}