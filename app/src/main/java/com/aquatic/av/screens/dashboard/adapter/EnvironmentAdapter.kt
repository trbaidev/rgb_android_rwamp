package com.aquatic.av.screens.dashboard.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.aquatic.av.R
import com.aquatic.av.databinding.RowItemEnvironmentSpinnerBinding
import com.aquatic.av.activities.model.UserEnvironmentModel
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentModel
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentResponse
import com.aquatic.av.utils.capitalizeWords

class EnvironmentAdapter(private val mContext: Context) : BaseAdapter() {
    private val dealerCollection: MutableList<MyEnvironmentModel> = mutableListOf()


    override fun getCount(): Int {
        return dealerCollection.size
    }

    override fun getItem(position: Int): MyEnvironmentModel {
        return dealerCollection[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun getEnvironmentIcon(iconName: String): Int {
        return when (iconName) {
            "vector_0" -> R.drawable.vector_0
            "vector_1" -> R.drawable.vector_1
            "vector_2" -> R.drawable.vector_2
            "vector_3" -> R.drawable.vector_3
            "vector_4" -> R.drawable.vector_4
            "vector_5" -> R.drawable.vector_5
            "vector_6" -> R.drawable.vector_6
            "vector_7" -> R.drawable.vector_7
            else -> R.drawable.vector_0
        }
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val itemView = RowItemEnvironmentSpinnerBinding.inflate(LayoutInflater.from(mContext), parent, false)
        itemView.txtEnvironmentName.text = getItem(position).getEnvName()?.capitalizeWords()
        itemView.imgEnvironmentIcon.setImageResource(getEnvironmentIcon(iconName = getItem(position).getEnvIconName() ?: ""))
        return itemView.root
    }

    fun setData(entityList: List<MyEnvironmentModel>) {
        dealerCollection.clear()
        dealerCollection.addAll(entityList)
        notifyDataSetChanged()
    }

}