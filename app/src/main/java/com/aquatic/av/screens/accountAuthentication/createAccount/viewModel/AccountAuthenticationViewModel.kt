package com.aquatic.av.screens.accountAuthentication.createAccount.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.screens.accountAuthentication.createAccount.dataModels.EmailCheckModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AccountAuthenticationViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel() {

    private val _emailCheckResults = MutableLiveData<Event<State<EmailCheckModel>>>()
    val emailCheckResults: LiveData<Event<State<EmailCheckModel>>> get() = _emailCheckResults

    fun emailCheck(query: String) {
        viewModelScope.launch {
            authRepository.emailCheck(query).collect {
                _emailCheckResults.value = (Event(it))
            }
        }
    }
}