package com.aquatic.av.screens.setting.myEnvironments.addnewdevice

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentAddNewDevicesBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.setting.myEnvironments.addNewDeviceProductType.AddNewDeviceProductTypeFragment
import com.aquatic.av.screens.setting.myEnvironments.addnewdevice.binder.ExpandableListViewAdapter
import com.aquatic.av.screens.setting.myEnvironments.addnewdevice.dataNodel.AddDeviceCustomEnvironmentModel
import com.aquatic.av.screens.setting.myEnvironments.addnewdevice.dataNodel.AddDeviceCustomEnvironmentResponse
import com.aquatic.av.screens.setting.myEnvironments.addnewdevice.dataNodel.AddDeviceDataModel
import com.aquatic.av.screens.setting.myEnvironments.addnewdevice.dataNodel.AddDeviceEnvironmentReponse
import com.aquatic.av.screens.setting.myEnvironments.addnewdevice.viewModel.DeviceCustomEnvironmentViewModel
import com.aquatic.av.screens.setting.myEnvironments.addnewdevice.viewModel.DeviceEnvironmentViewModel
import com.aquatic.av.screens.setting.myEnvironments.dataModels.EnvironmentReferenceModel
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.getUserDetailModel
import com.aquatic.av.utils.EditTextExtension.mapToObject
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.aquatic.av.views.dialogs.DialogFactory.showDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import java.util.ArrayList
import java.util.HashMap

@AndroidEntryPoint
class AddNewDeviceFragment :
    BaseFragment<FragmentAddNewDevicesBinding>(R.layout.fragment_add_new_devices) {

    private var env_id: Int? = null
    private var env_name: String? = null
    private var env_icon: String? = null
    private val viewModel by viewModels<DeviceEnvironmentViewModel>()
    private val deviceCustomeviewModel by viewModels<DeviceCustomEnvironmentViewModel>()
    private var listDataChild2: HashMap<String, ArrayList<AddDeviceEnvironmentReponse.Environment>>? = null
    lateinit var environment: AddDeviceCustomEnvironmentModel
  lateinit var  header_title:String
   lateinit var env_btn_name:String
    private var listDataGroup: ArrayList<String>? = null

    private var expandableListViewAdapter: ExpandableListViewAdapter? = null
    val userModel by lazy {
        requireContext().getUserDetailModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            headerTitle=header_title
            customEnvName=env_btn_name
            setListeners()
            seCustomEnvListeners()
            tvCustomEnv.setOnClickListener {
                isCustomEnvironment = !isCustomEnvironment
            }
            btnNext.setOnClickListener {
                if (TextUtils.isEmpty(env_icon)) {
                    showToast("Please choose an icon for the environment.")
                } else {
                    var env_model = EnvironmentReferenceModel(
                        env_name = env_name,
                        env_icon = env_icon,
                        env_id = env_id
                    )
                    mainActivity.navigateTo(AddNewDeviceProductTypeFragment.getInstance(env_model))
                }
            }
            btnCancel.setOnClickListener {
                mainActivity.popFragment()
            }
            ivOne.setOnClickListener {
                env_icon = customEnvironmentName
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_0")
            }
            ivTwo.setOnClickListener {
                env_icon = customEnvironmentName
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_1")
            }
            ivThree.setOnClickListener {
                env_icon = customEnvironmentName
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_2")
            }
            ivFour.setOnClickListener {
                env_icon = customEnvironmentName
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_3")
            }
            ivFive.setOnClickListener {
                env_icon = customEnvironmentName
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_4")
            }
            ivSix.setOnClickListener {
                env_icon = customEnvironmentName
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_5")
            }
            ivSeven.setOnClickListener {
                env_icon = customEnvironmentName
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_6")
            }
            ivEight.setOnClickListener {
                env_icon = customEnvironmentName
                setCustomEnvironment(edtEnv.text.trim().toString(), "vector_7")
            }
        }
        viewModel.getAllEnvironments(
            AddDeviceDataModel(
                userModel?.user_id!!,
                arrayOf("S", "D")
            )
        )
    }


    private fun setCustomEnvironment(envName: String, selectedEnvironmentName: String?) {
        if (envName.isEmpty()) {
            showToast("Please Enter environment name")
        } else {
            binding.customEnvironmentName=selectedEnvironmentName
            environment = AddDeviceCustomEnvironmentModel(
                environmentId = env_id ?: 0,
                environmentName = envName,
                environmentType = "D",
                isActive = 0,
                env_Icon_Name = binding.customEnvironmentName,
                userIds = arrayOf(requireContext().getUserDetailModel()?.user_id!!),
            )
            deviceCustomeviewModel.addUpdateEnvironments(
                requireContext().getAccessToken(),
                environment
            )
        }
    }

    private fun setListeners() {
        viewModel.deviceEnvironmentResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        var environmentResponse = Gson().mapToObject(
                            it.body as Map<String, Any?>?,
                            AddDeviceEnvironmentReponse::class.java
                        )
                        initListeners()
                        initListAdapter()
                        initListData()
                        listDataChild2?.put(
                            listDataGroup?.get(0)!!,
                            environmentResponse?.responseData?.dynamic_env!!
                        )
                        listDataChild2?.put(
                            listDataGroup?.get(1)!!,
                            environmentResponse?.responseData?.static_env!!
                        )

                        expandableListViewAdapter?.notifyDataSetChanged()
                    }
                }
            }
        })
    }


    private fun seCustomEnvListeners() {
        deviceCustomeviewModel.deviceCustomEnvironmentResults.observe(
            viewLifecycleOwner,
            EventObserver {
                when (it) {
                    is State.Error -> showToast(it.message)
                    is State.Exception -> {
                        it.exception.printStackTrace()
                        showToast(it.exception.message)
                    }
                    is State.Loading -> mainActivity.showLoader(it.isLoading)
                    is State.Success -> {
                        it.body?.apply {
                            var environmentResponse = Gson().mapToObject(
                                it.body as Map<String, Any?>?,
                                AddDeviceCustomEnvironmentResponse::class.java
                            )
                            var devenv = AddDeviceEnvironmentReponse.Environment(
                                environment_id = environment.environmentId,
                                environment_name = environment.environmentName,
                                environment_type = environment.environmentType,
                                is_active = environment.isActive,
                                env_icon_name = environment.env_Icon_Name!!,
                                selected = false
                            )
                            env_id = environmentResponse?.responseData!!
                            env_name = environment.environmentName
                            env_icon = environment.env_Icon_Name
                            devenv.environment_id = environmentResponse?.responseData!!
                            val dynEnvs: ArrayList<AddDeviceEnvironmentReponse.Environment>? =
                                listDataChild2!![listDataGroup!![0]]
                            dynEnvs!!.add(devenv)
                            listDataChild2!![listDataGroup!![0]] = dynEnvs
                            expandableListViewAdapter!!.notifyDataSetChanged()
                            showToast("Environment Saved")
                            showDialog(
                                "",
                                "Environment " + "\"" + env_name + "\"" + " has been created",
                                true
                            )
                        }
                    }
                }
            })
    }

    private fun initListAdapter() {
        listDataGroup = ArrayList()
        listDataChild2 = HashMap<String, ArrayList<AddDeviceEnvironmentReponse.Environment>>()
        expandableListViewAdapter =
            ExpandableListViewAdapter(mainActivity, listDataGroup, listDataChild2)
        binding.expandableListView.setAdapter(expandableListViewAdapter)
    }

    private fun initListData() {
        listDataGroup?.add(getString(R.string.text_my_env))
        listDataGroup?.add(getString(R.string.text_new_env))
        val existing_envs: ArrayList<AddDeviceEnvironmentReponse.Environment> =
            ArrayList<AddDeviceEnvironmentReponse.Environment>()
        val new_envs: ArrayList<AddDeviceEnvironmentReponse.Environment> =
            ArrayList<AddDeviceEnvironmentReponse.Environment>()
        listDataChild2!![listDataGroup!![0]] = existing_envs
        listDataChild2!![listDataGroup!![1]] = new_envs
        expandableListViewAdapter!!.notifyDataSetChanged()
    }

    private fun initListeners() {
        binding.expandableListView.setOnChildClickListener(ExpandableListView.OnChildClickListener { parent, v, groupPosition, childPosition, id ->
            env_id =
                listDataChild2!![listDataGroup!![groupPosition]]!![childPosition].environment_id
            env_name =
                listDataChild2!![listDataGroup!![groupPosition]]!![childPosition].environment_name
            env_icon =
                listDataChild2!![listDataGroup!![groupPosition]]!![childPosition].env_icon_name
            if(listDataChild2!![listDataGroup!![0]]!=null && listDataChild2!![listDataGroup!![0]]?.size!!>0){
                var env = listDataChild2!![listDataGroup!![0]]?.find { it.selected }
                env?.selected = false
            }
            if(listDataChild2!![listDataGroup!![1]]!=null && listDataChild2!![listDataGroup!![1]]?.size!!>0){
                var env = listDataChild2!![listDataGroup!![1]]?.find { it.selected }
                env?.selected = false
            }
            setSelectedItem(groupPosition,childPosition)
             expandableListViewAdapter?.notifyDataSetChanged()
            false
        })
        binding.expandableListView.setOnGroupClickListener(ExpandableListView.OnGroupClickListener { parent, v, groupPosition, id ->
            setListViewHeight(parent, groupPosition)
            false
        })
    }

    private fun setSelectedItem(groupPosition:Int,childPosition:Int){
        listDataChild2!![listDataGroup!![groupPosition]]!![childPosition].selected = true
    }
    private fun setListViewHeight(
        listView: ExpandableListView,
        group: Int
    ) {
        val listAdapter = listView.expandableListAdapter as ExpandableListAdapter
        var totalHeight = 0
        val desiredWidth = View.MeasureSpec.makeMeasureSpec(
            listView.width,
            View.MeasureSpec.EXACTLY
        )
        for (i in 0 until listAdapter.groupCount) {
            val groupItem = listAdapter.getGroupView(i, false, null, listView)
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
            totalHeight += groupItem.measuredHeight
            if (listView.isGroupExpanded(i) && i != group
                || !listView.isGroupExpanded(i) && i == group
            ) {
                for (j in 0 until listAdapter.getChildrenCount(i)) {
                    val listItem = listAdapter.getChildView(
                        i, j, false, null,
                        listView
                    )
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
                    totalHeight += listItem.measuredHeight
                }
            }
        }
        val params = listView.layoutParams
        var height = (totalHeight
                + listView.dividerHeight * (listAdapter.groupCount - 1))
        if (height < 10) height = 200
        params.height = height
        listView.layoutParams = params
        listView.requestLayout()
    }


    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        titleBar?.hideButtons()
        bottomNavigationView?.gone()
    }

    companion object {
        fun getInstance(
            header_title:String,
            env_name:String
        ): AddNewDeviceFragment {
            val addnewDevicefragment = AddNewDeviceFragment()
            addnewDevicefragment.header_title = header_title
            addnewDevicefragment.env_btn_name=env_name
            return addnewDevicefragment
        }
    }
}