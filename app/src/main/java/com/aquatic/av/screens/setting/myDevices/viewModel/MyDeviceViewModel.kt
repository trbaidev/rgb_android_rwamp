package com.aquatic.av.screens.setting.myDevices.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.setting.myDevices.dataModels.MyDeviceResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyDeviceViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _myDeviceResults = MutableLiveData<Event<State<MyDeviceResponse>>>()
    val myDeviceResults: LiveData<Event<State<MyDeviceResponse>>> get() = _myDeviceResults

    fun getDevicesInfoDash(token:String,email:String, env_name: String) {
        viewModelScope.launch {
            authRepository.getDevicesInfoDash(token,email,env_name).collect {
                _myDeviceResults.value = (Event(it))
            }
        }
    }
}