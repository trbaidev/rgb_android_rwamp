package com.aquatic.av.screens.rgb.binder

import android.content.Context
import android.graphics.Color
import com.aquatic.av.R
import com.aquatic.av.databinding.RowItemFavoriteColorBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.rgb.dataModel.FavColorData

class LocalColorBinder(private val itemClickListener: (item: FavColorData, position: Int) -> Unit) : BaseRecyclerViewBinder<FavColorData, RowItemFavoriteColorBinding>(R.layout.row_item_favorite_color) {

    override fun bindView(entity: FavColorData, position: Int, viewHolder: BaseViewHolder<RowItemFavoriteColorBinding>?,
                          context: Context, collections: MutableList<FavColorData>) {
        viewHolder?.viewBinding?.apply {
            showItemSelector = false
            imgItem.setColorFilter(Color.parseColor(entity.color))
            root.setOnClickListener {
                itemClickListener.invoke(entity, position)
            }
        }
    }
}