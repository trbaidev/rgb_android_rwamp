package com.aquatic.av.screens.setting.deviceinformation

import android.content.Context
import android.os.Bundle
import android.view.View
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentDeviceDetailBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.bluetooth.ConnectedDeviceFragment
import com.aquatic.av.screens.bluetooth.ScannerFragment
import com.aquatic.av.screens.dashboard.dataModel.DashboardItemModel
import com.aquatic.av.screens.setting.productRegistration.ProductRegistrationFragment
import com.aquatic.av.utils.isDeviceConnected
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView

class DeviceDetailFragment :
    BaseFragment<FragmentDeviceDetailBinding>(R.layout.fragment_device_detail) {

    lateinit var deviceItemModel: DashboardItemModel

    private val preferences by lazy {
        requireContext().getSharedPreferences(mainActivity.packageName, Context.MODE_PRIVATE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            binding.dataItem = deviceItemModel
            if (deviceItemModel.device_uuid.equals(preferences.getString("connected_device_name", ""),true)
                && !(deviceItemModel.device_uuid.isNullOrEmpty() && preferences.getString("connected_device_name", "").isNullOrEmpty())) {
                ivStatus.setBackgroundResource(R.drawable.ic_status_vector_enabled)
                tvStatus.text="Connected"
            }else {
                ivStatus.setBackgroundResource(R.drawable.ic_status_vector_disabled)
                tvStatus.text="Not Connected"
            }
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnConnectBT.setOnClickListener {
                if (isDeviceConnected()) {
                    deviceItemModel.deviceId?.let { it1 ->
                        preferences.getString("connected_device_name", "")?.let { it2 ->
                            ConnectedDeviceFragment.newInstance(
                                it2,
                                it1
                            )
                        }
                    }?.let { it2 -> mainActivity.navigateTo(it2) }
                } else {
                    deviceItemModel.deviceId?.let { it1 ->
                        preferences.getString("connected_device_name", "")?.let { it2 ->
                            ScannerFragment.newInstance(
                                it2,
                                it1
                            )
                        }
                    }?.let { it2 -> mainActivity.navigateTo(it2) }
                }

            }
            tvView.setOnClickListener {
                mainActivity.navigateTo(ProductRegistrationFragment.getInstance(deviceItemModel))
            }
        }

    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        titleBar?.hideButtons()
    }

    companion object {
        fun getInstance(
            deviceItemModel: DashboardItemModel
        ): DeviceDetailFragment {
            val devicesInformationFragment = DeviceDetailFragment()
            devicesInformationFragment.deviceItemModel = deviceItemModel
            return devicesInformationFragment
        }
    }
}