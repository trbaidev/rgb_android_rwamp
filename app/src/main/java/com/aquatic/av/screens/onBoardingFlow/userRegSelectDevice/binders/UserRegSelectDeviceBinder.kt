package com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice.binders

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.UserRegSeletctDeviceRowItemBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice.dataModels.UserRegSelectDeviceResponse

class UserRegSelectDeviceBinder(private var itemClickLister: (entity: UserRegSelectDeviceResponse, position: Int) -> Unit) :
    BaseRecyclerViewBinder<UserRegSelectDeviceResponse, UserRegSeletctDeviceRowItemBinding>(
        R.layout.user_reg_seletct_device_row_item
    ) {
    override fun bindView(
        entity: UserRegSelectDeviceResponse,
        position: Int,
        viewHolder: BaseViewHolder<UserRegSeletctDeviceRowItemBinding>?,
        context: Context,
        collections: MutableList<UserRegSelectDeviceResponse>
    ) {
        viewHolder?.viewBinding?.let {
            it.dataItem = entity
            viewHolder.itemView.setOnClickListener {
                itemClickLister.invoke(entity, position)
            }
        }
    }
}