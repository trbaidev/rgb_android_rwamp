package com.aquatic.av.screens.setting.myEnvironments.addnewdevice.binder

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import com.aquatic.av.databinding.MyEnviListRowChildBinding
import com.aquatic.av.databinding.MyEnviListRowGroupBinding
import com.aquatic.av.screens.setting.myEnvironments.addnewdevice.dataNodel.AddDeviceEnvironmentReponse
import java.util.HashMap

class ExpandableListViewAdapter(val context: Context?,
                                val listDataGroup: ArrayList<String>?,
                                val listChildData: HashMap<String, ArrayList<AddDeviceEnvironmentReponse.Environment>>?) : BaseExpandableListAdapter() {

    private val inflater = LayoutInflater.from(context)

    override fun getGroupCount(): Int = listDataGroup!!.size

    override fun getChildrenCount(groupPosition: Int): Int = listChildData!![listDataGroup!![groupPosition]]?.size!!

    override fun getGroup(groupPosition: Int): String = listDataGroup!![groupPosition]

    override fun getChild(groupPosition: Int, childPosition: Int) = listChildData!![listDataGroup!![groupPosition]]?.get(childPosition)!!

    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()

    override fun getChildId(groupPosition: Int, childPosition: Int): Long = childPosition.toLong()

    override fun hasStableIds(): Boolean = false

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean = true

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View? {
        val binding = MyEnviListRowChildBinding.inflate(inflater, parent, false)
        binding.apply {
            val temp: AddDeviceEnvironmentReponse.Environment =
                getChild(groupPosition, childPosition) as AddDeviceEnvironmentReponse.Environment
            dataItem = temp
        }
        return binding.root
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View? {
        val binding = MyEnviListRowGroupBinding.inflate(inflater, parent, false)
        binding?.apply {
            textViewGroup.setTypeface(null, Typeface.BOLD)
            val temp = getGroup(groupPosition) as String?
            name = temp
            arrowStatus = isExpanded
        }
        return binding?.root
    }
}