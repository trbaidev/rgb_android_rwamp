package com.aquatic.av.screens.dealerLocator.model

data class GoogleAutocompleteSearch (var results: ArrayList<GoogleAutoCompleteResult>? = arrayListOf(),
                                     var status: String? = "")