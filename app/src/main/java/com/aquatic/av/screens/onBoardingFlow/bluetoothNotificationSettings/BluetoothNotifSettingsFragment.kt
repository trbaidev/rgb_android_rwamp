package com.aquatic.av.screens.onBoardingFlow.bluetoothNotificationSettings

import android.os.Bundle
import android.view.View
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.utils.BTCon
import com.aquatic.av.bluetoothScanner.ble.utils.BTCon.isConnected
import com.aquatic.av.callBacks.ButtonListners
import com.aquatic.av.databinding.FragmentBluetoothSettingsBinding
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressResponse
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.onBoardingFlow.RegisterScanner.RegisterScannerFragment
import com.aquatic.av.screens.onBoardingFlow.dataModels.DevicesInformation
import com.aquatic.av.screens.onBoardingFlow.userRegPermisson.UserRegPermissionFragment
import com.aquatic.av.screens.onBoardingFlow.userRegSelectDevice.dataModels.UserRegSelectDeviceResponse
import com.aquatic.av.utils.BasePreferenceHelper.setLoginStatus
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.aquatic.av.views.dialogs.DialogFactory.showDialog
import com.google.android.material.bottomnavigation.BottomNavigationView

class BluetoothNotifSettingsFragment :
    BaseFragment<FragmentBluetoothSettingsBinding>(R.layout.fragment_bluetooth_settings) {

    private  var userAddressResponse: UserAddressResponse? = null
    private  var userRegSelectDeviceResponse: UserRegSelectDeviceResponse? = null
    private  var devicesInformation: DevicesInformation? = null
    private  var userAddressModel: UserAddressModel? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnBack.setOnClickListener(View.OnClickListener {
                showDialog(
                    "",
                    "Do you want to skip bluetooth connection?",
                    true,
                    object : ButtonListners {
                        override fun positiveCall() {
                            requireContext().setLoginStatus(true)
                            bottomNavigation?.visible()
                            mainActivity.initApp(isRGBMode = false)
                        }

                        override fun negavtivecall() {

                        }
                    })
            })

            btnNext.setOnClickListener {
                if (!BTCon.isConnected) {
                    mainActivity.navigateTo(
                        RegisterScannerFragment.getInstance(
                            devicesInformation?.device_name,
                            -100
                        )
                    )
                } else {
                    mainActivity.navigateTo(UserRegPermissionFragment())
                }
            }
        }
    }


    companion object {
        fun getInstance(
            userAddressResponse: UserAddressResponse?,
            userRegSelectDeviceResponse: UserRegSelectDeviceResponse?,
            userAddressModel: UserAddressModel?,
            devicesInformation: DevicesInformation?
        ): BluetoothNotifSettingsFragment {
            val bluetoothNotifSettingsFragment = BluetoothNotifSettingsFragment()
            bluetoothNotifSettingsFragment.userAddressResponse = userAddressResponse
            bluetoothNotifSettingsFragment.userAddressModel = userAddressModel
            bluetoothNotifSettingsFragment.userRegSelectDeviceResponse = userRegSelectDeviceResponse
            bluetoothNotifSettingsFragment.devicesInformation = devicesInformation
            return bluetoothNotifSettingsFragment
        }
    }

    override fun onResume() {
        super.onResume()
        if (isConnected) {
            binding.linLayoutInner.visibility = View.VISIBLE
        }
    }


    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }

}