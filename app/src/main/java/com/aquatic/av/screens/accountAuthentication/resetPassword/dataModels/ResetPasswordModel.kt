package com.aquatic.av.screens.accountAuthentication.resetPassword.dataModels

data class ResetPasswordModel(
    var user_email: String = "",
    var password: String
)