package com.aquatic.av.screens.rgb

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SeekBar
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.PREF_SPEED
import com.aquatic.av.bluetoothScanner.ble.preference.getInt
import com.aquatic.av.bluetoothScanner.ble.preference.putInt
import com.aquatic.av.databinding.FragmentModeSelectionBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.rgb.viewModel.RGBViewModel
import com.makeramen.roundedimageview.RoundedImageView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ModeSelectionFragment : BaseFragment<FragmentModeSelectionBinding>(R.layout.fragment_mode_selection) {

    private var selected = ""
    var TAG = "ModeSelectionFragment"
    private var mSpeed: Int = 50
    private var mProgressMax: Int = 100
    private val mRGBViewModel by viewModels<RGBViewModel>()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val listItem: MutableList<String> = getModeList()

        selected = listItem[0]

        binding.apply {

            pulsatingPurple.setOnClickListener(clickListener)
            pulsatingWhite.setOnClickListener(clickListener)
            pulsatingGreen.setOnClickListener(clickListener)
            pulsatingRed.setOnClickListener(clickListener)
            pulsatingBlue.setOnClickListener(clickListener)
            pulsatingYellow.setOnClickListener(clickListener)
            pulsatingRedGreen.setOnClickListener(clickListener)
            pulsatingOrange.setOnClickListener(clickListener)
            pulsatingGreenBlue.setOnClickListener(clickListener)
            pulsatingRedBlue.setOnClickListener(clickListener)
            partyMode.setOnClickListener(clickListener)


            partyModeRainbow.setOnClickListener(clickListener)
            rainbowStrobe.setOnClickListener(clickListener)
            redStrobe.setOnClickListener(clickListener)
            greenStrobe.setOnClickListener(clickListener)
            yellowStrobe.setOnClickListener(clickListener)
            blueStrobe.setOnClickListener(clickListener)
            cyanStrobe.setOnClickListener(clickListener)
            purpleStrobe.setOnClickListener(clickListener)
            whiteStrobe.setOnClickListener(clickListener)

        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            binding.speedSeekBar.min = 0
        }
        binding.speedSeekBar.max = mProgressMax
        binding.speedSeekBar.progress = PREF_SPEED.getInt(mSpeed)
        mSpeed = binding.speedSeekBar.progress

        binding.speedSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                mSpeed = progress
                PREF_SPEED.putInt(progress)
                Log.d(TAG, "Speed: $mSpeed")
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                Log.d(TAG, "Speed: onStartTrackingTouch")
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                Log.d(TAG, "Speed : $mSpeed")
                mRGBViewModel.sendSpeedData(mSpeed)
            }
        })
    }

    private fun removeBorderRadius(selectedView: View) {
        binding.apply {
            pulsatingPurple.borderWidth = 0f
            pulsatingWhite.borderWidth = 0f
            pulsatingGreen.borderWidth = 0f
            pulsatingRed.borderWidth = 0f
            pulsatingBlue.borderWidth = 0f
            pulsatingYellow.borderWidth = 0f
            pulsatingRedGreen.borderWidth = 0f
            pulsatingOrange.borderWidth = 0f
            pulsatingGreenBlue.borderWidth = 0f
            pulsatingRedBlue.borderWidth = 0f
            partyMode.borderWidth = 0f


            partyModeRainbow.borderWidth = 0f
            rainbowStrobe.borderWidth = 0f
            redStrobe.borderWidth = 0f
            greenStrobe.borderWidth = 0f
            yellowStrobe.borderWidth = 0f
            blueStrobe.borderWidth = 0f
            cyanStrobe.borderWidth = 0f
            purpleStrobe.borderWidth = 0f
            whiteStrobe.borderWidth = 0f

            if (selectedView is RoundedImageView) {
                selectedView.borderWidth = 15f
            }
        }
    }

    private val clickListener = View.OnClickListener { v ->
        removeBorderRadius(v)
        when (v.id) {
            R.id.pulsatingPurple -> {
                mRGBViewModel.sendActivateModeCodeData(4)
            }
            R.id.pulsatingWhite -> {
                mRGBViewModel.sendActivateModeCodeData(5)
            }
            R.id.pulsatingGreen -> {
                mRGBViewModel.sendActivateModeCodeData(1)
            }
            R.id.pulsatingRed -> {
                mRGBViewModel.sendActivateModeCodeData(0)
            }
            R.id.pulsatingBlue -> {
                mRGBViewModel.sendActivateModeCodeData(12)
            }
            R.id.pulsatingYellow -> {
                mRGBViewModel.sendActivateModeCodeData(2)
            }
            R.id.pulsatingRedGreen -> {
                mRGBViewModel.sendActivateModeCodeData(6)
            }
            R.id.pulsatingOrange -> {
                mRGBViewModel.sendActivateModeCodeData(3)
            }
            R.id.partyMode -> {
                mRGBViewModel.sendActivateModeCodeData(20)
            }

            R.id.pulsatingGreenBlue -> {
                mRGBViewModel.sendActivateModeCodeData(8)
            }
            R.id.pulsatingRedBlue -> {
                mRGBViewModel.sendActivateModeCodeData(7)
            }
            R.id.partyModeRainbow -> {
                mRGBViewModel.sendActivateModeCodeData(22)
            }

            R.id.rainbowStrobe -> {
                mRGBViewModel.sendActivateModeCodeData(9)
            }
            R.id.redStrobe -> {
                mRGBViewModel.sendActivateModeCodeData(10)
            }
            R.id.greenStrobe -> {
                mRGBViewModel.sendActivateModeCodeData(11)
            }
            R.id.yellowStrobe -> {
                mRGBViewModel.sendActivateModeCodeData(13)
            }
            R.id.blueStrobe -> {
                mRGBViewModel.sendActivateModeCodeData(12)
            }
            R.id.cyanStrobe -> {
                mRGBViewModel.sendActivateModeCodeData(14)
            }
            R.id.purpleStrobe -> {
                mRGBViewModel.sendActivateModeCodeData(15)
            }
            R.id.whiteStrobe -> {
                mRGBViewModel.sendActivateModeCodeData(16)
            }
        }
    }

    private fun getModeList(): MutableList<String> {
        val listItem: MutableList<String> = arrayListOf()
        listItem.add("Party Mode")
        listItem.add("Gradual Rainbow Transition")
        listItem.add("Pulsating Red")
        listItem.add("Pulsating Green")
        listItem.add("Pulsating Yellow")
        listItem.add("Pulsating Cyan")
        listItem.add("Pulsating Purple")
        listItem.add("Pulsating White")
        listItem.add("Pulsating Red/Green")
        listItem.add("Pulsating Red/Blue")
        listItem.add("Pulsating Green/Blue")
        listItem.add("Rainbow Strobe")
        listItem.add("Red Strobe")
        listItem.add("Green Strobe")
        listItem.add("Blue Strobe")
        listItem.add("Yellow Strobe")
        listItem.add("Cyan Strobe")
        listItem.add("Purple Strobe")
        listItem.add("White Strobe")
        listItem.add("Rapid Rainbow Transition")
        return listItem
    }

}
