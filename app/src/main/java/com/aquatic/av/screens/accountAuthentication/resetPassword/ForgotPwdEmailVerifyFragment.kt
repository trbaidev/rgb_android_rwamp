package com.aquatic.av.screens.accountAuthentication.resetPassword

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceUtil
import com.aquatic.av.databinding.FragmentForgotPwdEmailVerifyFragmentBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.accountAuthentication.resetPassword.viewModel.ForgotPwdViewModel
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.views.TitleBar
import com.aquatic.av.views.dialogs.DialogFactory.showDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotPwdEmailVerifyFragment :
    BaseFragment<FragmentForgotPwdEmailVerifyFragmentBinding>(R.layout.fragment_forgot_pwd_email_verify_fragment) {


    private val viewModel by viewModels<ForgotPwdViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        binding.apply {
            btnContinue.setOnClickListener {
                if (!edtEmailVerify.text.trim().toString().isNullOrEmpty())
                    viewModel.verifyUserEmail(edtEmailVerify.text.trim().toString())
                else
                    showDialog("Invalid email", "Please enter email",true)
            }
            ivBack.setOnClickListener {
                mainActivity.popFragment()
            }
        }
    }

    private fun setListeners() {
        viewModel.emailPwdResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        val splitData = it.body.user_exist.split("~").toTypedArray()
                        if (splitData[0].equals("true", true)) {
                            PreferenceUtil.getInstance()
                                .setStringValue(splitData[1], "forget_password")
                            mainActivity.navigateTo(
                                ForgotPwdVerifyCodeFragment.getInstance(
                                    splitData[1],
                                    binding.edtEmailVerify.text.trim().toString()
                                )
                            )
                        } else {
                            showDialog(
                                "Invalid email",
                                "Email address doesn't exists in our database. Please create an account.",true
                            )
                        }
                    }
                }
            }
        })
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }
}