package com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.dataModels

import com.aquatic.av.R

data class MyEnvironmentResponse(
    var responseCode: Int,
    var responseMessage: String?,
    var responseData: ResponseData
) {

    data class ResponseData(
        var static_env: List<Environment>?,
        var dynamic_env: List<Environment>?
    )

    data class Environment(
        var environment_id: Int,
        var environment_name: String,
        var environment_type: String,
        var is_active: Int,
        var env_icon_name: String,
        var selected: Boolean
    ) {
        fun getEnvironmentImages(text: String): Int {
            return when (text) {
                "vector_0" -> R.drawable.vector_0
                "vector_1" -> R.drawable.vector_1
                "vector_2" -> R.drawable.vector_2
                "vector_3" -> R.drawable.vector_3
                "vector_4" -> R.drawable.vector_4
                "vector_5" -> R.drawable.vector_5
                "vector_6" -> R.drawable.vector_6
                "vector_7" -> R.drawable.vector_7
                else ->
                    R.drawable.vector_0
            }
        }
    }

}