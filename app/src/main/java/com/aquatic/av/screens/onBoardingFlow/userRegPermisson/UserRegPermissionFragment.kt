package com.aquatic.av.screens.onBoardingFlow.userRegPermisson

import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import android.widget.TextView
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentUserRegPermissionBinding
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.utils.BasePreferenceHelper.setLoginStatus
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView

class UserRegPermissionFragment :
    BaseFragment<FragmentUserRegPermissionBinding>(R.layout.fragment_user_reg_permission) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnNext.setOnClickListener {
                requireContext().setLoginStatus(true)
                bottomNavigation?.visible()
                mainActivity.initApp(isRGBMode = false)
            }
            notifSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    setSwitchButtonStatus(View.GONE, View.VISIBLE,notifTextOff,notifTextOn)
                } else {
                    setSwitchButtonStatus(View.VISIBLE, View.GONE,notifTextOff,notifTextOn)
                }
            })
        }
    }

    private fun setSwitchButtonStatus(offStatus: Int, onnStatus: Int,switch_off_text: TextView,switch_on_text:TextView) {
        switch_off_text.visibility = offStatus
        switch_on_text.visibility = onnStatus
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
    }
}