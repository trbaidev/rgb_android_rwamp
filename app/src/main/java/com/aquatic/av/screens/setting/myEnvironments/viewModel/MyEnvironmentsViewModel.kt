package com.aquatic.av.screens.setting.myEnvironments.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentResponse
import com.google.gson.JsonObject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyEnvironmentsViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel() {
    private val _myEnvironmentsResults = MutableLiveData<Event<State<MyEnvironmentResponse>>>()
    val myEnvironmentResults: LiveData<Event<State<MyEnvironmentResponse>>> get() = _myEnvironmentsResults

    fun getMyEnvironmentInfo(token: String, user_id: JsonObject) {
        viewModelScope.launch {
            authRepository.getMyEnvironmentInfo(token, user_id).collect {
                _myEnvironmentsResults.value = (Event(it))
            }
        }
    }
}