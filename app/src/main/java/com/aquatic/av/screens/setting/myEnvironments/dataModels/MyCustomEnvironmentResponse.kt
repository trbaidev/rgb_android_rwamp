package com.aquatic.av.screens.setting.myEnvironments.dataModels


data class MyCustomEnvironmentResponse(var responseCode: Int,
                                       var responseData: Int,
                                       var responseMessage: String)