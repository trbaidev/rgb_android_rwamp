package com.aquatic.av.screens.accountAuthentication.login

import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import androidx.fragment.app.viewModels
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentLoginBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginModel
import com.aquatic.av.screens.accountAuthentication.login.viewModel.LoginViewModel
import com.aquatic.av.screens.accountAuthentication.resetPassword.ForgotPwdEmailVerifyFragment
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.utils.BasePreferenceHelper.putAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.putUserDetailModel
import com.aquatic.av.utils.BasePreferenceHelper.putUserEmail
import com.aquatic.av.utils.BasePreferenceHelper.setLoginStatus
import com.aquatic.av.utils.EditTextExtension.getEncryptPassword
import com.aquatic.av.utils.Utils
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.showToast
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>(R.layout.fragment_login) {
    var hideBackButton = false
    private val viewModel by viewModels<LoginViewModel>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUi()
        bindObservers()
        binding.apply {

            btnLogin.setOnClickListener {
                if (checkFieldsValidations()) {
                    val loginModel = LoginModel(
                            binding.edtEmail.text.trim().toString(),
                            binding.edtPwd.getEncryptPassword(),
                            Utils.getDeviceId(mainActivity)
                    )
                    viewModel.loginUser(loginModel)
                }
            }
            tvRgbModeOnly.setOnClickListener {
                mainActivity.initApp(true)
            }
            tvForgot.setOnClickListener {
                mainActivity.navigateTo(ForgotPwdEmailVerifyFragment())
            }
            ivBack.setOnClickListener {
                mainActivity.popFragment()
            }
        }

    }

    private fun setUi() {
        binding.ivBack.visibility = if (hideBackButton) View.GONE else View.VISIBLE
    }

    private fun bindObservers() {
        viewModel.loginResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        it.body.user_info.user_pwd=binding.edtPwd.text.trim().toString()
                        requireContext().putUserDetailModel(it.body.user_info)
                        mainActivity.putUserEmail(it.body.user_info.user_email)
                        requireContext().putAccessToken("Bearer ${it.body.user_info.token}")
                        requireContext().setLoginStatus(true)
                        bottomNavigation?.visible()
                        mainActivity.initApp(isRGBMode = false)
                    }
                }
            }
        })
    }

    private fun checkFieldsValidations(): Boolean {
        val email: String = binding.edtEmail.text.trim().toString()
        val password: String = binding.edtPwd.text.trim().toString()
        when {
            TextUtils.isEmpty(email) -> {
                binding.edtEmail.error = "Please enter email"
                return false
            }
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                binding.edtEmail.error = "Please enter valid email"
                return false
            }
            TextUtils.isEmpty(password) -> {
                binding.edtPwd.error = "Please provide password"
                return false
            }
        }
        return true
    }

    override fun setHeaderFooterViews(
            titleBar: TitleBar?,
            bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        bottomNavigationView?.gone()
        titleBar?.hideButtons()
    }

    companion object {

        fun getInstance(hideBackButton: Boolean): LoginFragment {
            val loginFragment = LoginFragment()
            loginFragment.hideBackButton = hideBackButton
            return loginFragment
        }

    }
}