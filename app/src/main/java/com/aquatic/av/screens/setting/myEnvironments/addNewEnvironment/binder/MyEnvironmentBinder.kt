package com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.binder

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.MyEnvironmentsRowItemBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.dataModels.MyEnvironmentResponse

class MyEnvironmentBinder(var lastSelectedIndex: Int = -1, private var itemClickLister: (entity: MyEnvironmentResponse.Environment, position: Int) -> Unit) :
        BaseRecyclerViewBinder<MyEnvironmentResponse.Environment, MyEnvironmentsRowItemBinding>(R.layout.my_environments_row_item) {

    override fun bindView(
            entity: MyEnvironmentResponse.Environment,
            position: Int,
            viewHolder: BaseViewHolder<MyEnvironmentsRowItemBinding>?,
            context: Context,
            collections: MutableList<MyEnvironmentResponse.Environment>
    ) {
        viewHolder?.viewBinding?.let {
            it.dataItem = entity
            it.isItemSelected = position == lastSelectedIndex
            viewHolder.itemView.setOnClickListener {
                lastSelectedIndex = position
                itemClickLister.invoke(entity, position)
            }
        }
    }
}