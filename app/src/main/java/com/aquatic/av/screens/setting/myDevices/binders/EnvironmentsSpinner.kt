package com.aquatic.av.screens.setting.myDevices.binders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.aquatic.av.R
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentModel

class EnvironmentsSpinner(envList: List<MyEnvironmentModel>) : BaseAdapter() {
    private  var envList: List<MyEnvironmentModel>

    init {
        this.envList = envList
    }

    override fun getCount(): Int {
        return envList.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view: View
        var inflater: LayoutInflater = LayoutInflater.from(parent?.context)
        view = inflater.inflate(R.layout.spinner_environment_row, null)
        var text = view.findViewById<TextView>(R.id.tvEnvNameList)
        text.text = envList.get(position).getEnvName()
        return view
    }
}