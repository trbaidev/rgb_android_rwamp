package com.aquatic.av.screens.dealerLocator.model


import com.google.gson.annotations.SerializedName

data class DealerSuggestionRequest(
    var dealertype: List<String> = listOf(),
    var desitnationlong: Int = 0,
    var destinationlat: Int = 0,
    var distance: Int = 0,
    var input: String = "",
    var product: List<String> = listOf(),
    var sourcelat: Int = 0,
    var sourcelong: Int = 0
)