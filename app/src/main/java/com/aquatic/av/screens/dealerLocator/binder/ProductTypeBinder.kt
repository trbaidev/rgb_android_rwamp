package com.aquatic.av.screens.dealerLocator.binder

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.RowItemCheckboxBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.dealerLocator.model.DealerFilterResponse

class ProductTypeBinder : BaseRecyclerViewBinder<DealerFilterResponse.ProductsData, RowItemCheckboxBinding>(R.layout.row_item_checkbox) {
    val selectedItems = mutableListOf<String>()
    override fun bindView(entity: DealerFilterResponse.ProductsData, position: Int, viewHolder: BaseViewHolder<RowItemCheckboxBinding>?, context: Context, collections: MutableList<DealerFilterResponse.ProductsData>) {
        viewHolder?.viewBinding?.apply {
            btnItem.text = entity.value
            btnItem.setOnCheckedChangeListener(null)
            btnItem.isChecked = selectedItems.contains(entity.value)
            btnItem.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    selectedItems.add(entity.value)
                } else {
                    selectedItems.remove(entity.value)
                }
            }
        }
    }
}