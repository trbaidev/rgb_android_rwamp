package com.aquatic.av.screens.onBoardingFlow.userEnvironment.dataModels

class UserCustomEnvironmentModel {
    var environmentId: Int = -1
    var environmentName: String = ""
    var environmentType: String = ""
    var env_Icon_Name: String? = ""
    var isActive: Int = 0
    var userIds: Array<Int>? = null
}