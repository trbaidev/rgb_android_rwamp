package com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.binders

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.aquatic.av.R
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.DealerModel


class AutoCompleteAdapter(context: Context, objects: ArrayList<DealerModel>) :
    ArrayAdapter<DealerModel>(context, 0, objects) {


    private var dealersModel: List<DealerModel>

    init {
        this.dealersModel = objects
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view: View
        var inflater: LayoutInflater = LayoutInflater.from(parent?.context)
        view = inflater.inflate(R.layout.spinner_single_item_custom, null)
        var text = view.findViewById<TextView>(R.id.tvEnvNameList)
        text.text = dealersModel.get(position).dealer_name
        return view
    }


}