package com.aquatic.av.screens.dealerLocator

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentDealerLocatorBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.dealerLocator.adapter.DealerSuggestionAdapter
import com.aquatic.av.screens.dealerLocator.binder.*
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionRequest
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionResponse
import com.aquatic.av.utils.*
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.isLogin
import com.aquatic.av.utils.EditTextExtension.doAfterTextChangedDelayed
import com.aquatic.av.utils.EditTextExtension.getGenericAdapter
import com.aquatic.av.utils.EditTextExtension.hideKeyboard
import com.aquatic.av.views.TitleBar
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import dagger.hilt.android.AndroidEntryPoint
import java.lang.reflect.Method
import java.util.*


@SuppressLint("NotifyDataSetChanged")
@AndroidEntryPoint
class DealerLocatorFragment :
    BaseFragment<FragmentDealerLocatorBinding>(R.layout.fragment_dealer_locator) {
    private val viewModel by viewModels<DealerLocatorViewModel>()
    private var googleMap: GoogleMap? = null
    private var markers: MutableMap<Int, Marker> = mutableMapOf()
    private val dealerBottomSheetBehavior by lazy { BottomSheetBehavior.from(binding.incBSDealer.bottomSheetDealer) }
    private val filterBottomSheetBehavior by lazy { BottomSheetBehavior.from(binding.incBSFilter.bottomSheetFilter) }
    private val distanceItemBinder by lazy {
        DistanceItemBinder(-1) { _, position ->
            binding.incBSFilter.rgDistance.apply {
                post {
                    adapter?.notifyDataSetChanged()
                }
            }
        }
    }
    private val filterByDistanceItemBinder by lazy {
        DealerFilterByDistanceBinder(-1) { item, _ ->
            binding.incBSDealer.rvDistance.apply {
                post {
                    getFilterSuggestions(item.value.toIntOrNull() ?: 0)
                    adapter?.notifyDataSetChanged()
                }
            }
        }
    }
    private val dealerItemBinder by lazy { DealerTypeBinder() }
    private val productItemBinder by lazy { ProductTypeBinder() }

    private val dealerSuggestionAdapter by lazy {
        DealerSuggestionAdapter(requireContext()) { item, _ ->
            binding.edtSuggestion.setText("")
            binding.edtSuggestion.hideKeyboard()
            try {
                val method: Method = Spinner::class.java.getDeclaredMethod("onDetachedFromWindow")
                method.isAccessible = true
                method.invoke(binding.spnSuggestion)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            googleMap?.let {
                item.geometry.location.lat?.let { it1 ->
                    item.geometry.location.lng?.let { it2 ->
                        LatLng(
                            it1,
                            it2
                        )
                    }
                }?.let { it2 ->
                    animateMarkerCamera(it2)
                    it?.addMarker(
                        MarkerOptions()
                            .position(it2)
                            .title(item?.name)
                            .snippet(item.formatted_address)
                    )
                }
                //   createDealerMarker(item)
            }

            item?.geometry?.location?.lat?.toInt()?.let {
                item?.geometry?.location?.lng?.toInt()?.let { it1 ->
                    DealerSuggestionRequest(
                        sourcelong = it1,
                        sourcelat = it
                    )
                }
            }?.let {
                viewModel.getDealerSuggestions(
                    requireContext().getAccessToken(),
                    it
                )
            }
        }
    }

    private val onMapReadyCallback = OnMapReadyCallback {
        googleMap = it
        googleMap?.setOnMarkerClickListener(GoogleMap.OnMarkerClickListener { marker ->
            if (marker.tag == null) return@OnMarkerClickListener false
            val dealer = marker.tag as DealerSuggestionResponse.DealerItem
            dealer.distance = 0f
            val dealers = arrayListOf<DealerSuggestionResponse.DealerItem>()
            val selectedDealer = Location("SelectedDealer")
            selectedDealer.latitude = dealer.lat
            selectedDealer.longitude = dealer.longitude
            markers.values.forEach {
                if (it.tag != null && it.tag is DealerSuggestionResponse.DealerItem) {
                    if (dealer == it.tag) {
                        dealers.add(0, it.tag as DealerSuggestionResponse.DealerItem)
                    } else {
                        (it.tag as DealerSuggestionResponse.DealerItem).distance =
                            selectedDealer.distanceTo(getLocationObject(it.tag as DealerSuggestionResponse.DealerItem))
                        dealers.add(it.tag as DealerSuggestionResponse.DealerItem)
                    }
                }
            }
            dealers.sortBy { it.distance }
            showItemDetail(dealers)
            true
        })
        checkLocationPermission()
    }

    private fun getLocationObject(dealers: DealerSuggestionResponse.DealerItem): Location {
        val location = Location("location")
        location.latitude = dealers.lat
        location.longitude = dealers.longitude
        return location
    }

    private val locationHelper: LocationHelper by lazy {
        LocationHelper(
            mainActivity,
            1,
            1500,
            1000,
            LocationRequest.PRIORITY_HIGH_ACCURACY,
            object : LocationHelper.LocationListener {
                override fun onLocationResult(location: Location) {
                    animateMarkerCamera(LatLng(location.latitude, location.longitude))
                    viewModel.getFilteredDealerSuggestion(
                        requireContext().getAccessToken(),
                        DealerSuggestionRequest(
                            sourcelong = location.longitude.toInt(),
                            sourcelat = location.latitude.toInt()
                        )
                    )
                }

                override fun onLocationAvailability(availability: LocationAvailability?) {}
            })
    }


    private fun initMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(onMapReadyCallback)
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMap()
        viewModel.getDealerFilters(requireContext().getAccessToken())
        prepareViews()
        bindObservers()
        binding.incBSDealer.rvDealers.setOnTouchListener { v, motionEvent ->
            v.parent.requestDisallowInterceptTouchEvent(true)
            v.onTouchEvent(motionEvent)
            return@setOnTouchListener true
        }

    }

    private fun prepareViews() {
        binding.apply {
            btnBack.setOnClickListener { mainActivity.onBackPressed() }
            incBSFilter.btnApplyFilters.setOnClickListener {
                getFilterSuggestions(distanceItemBinder.lastSelectedItem)
                filterBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

            }
            ivFilter.setOnClickListener {
                dealerBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                filterBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }

            spnSuggestion.adapter = dealerSuggestionAdapter
            spnSuggestion.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {}
                override fun onNothingSelected(p0: AdapterView<*>?) {}
            }
            edtSuggestion.doAfterTextChangedDelayed {
                if (it.isNotEmpty()) {
                    viewModel.getNearbyDealers(
                        it,
                        "regions",
                        "AIzaSyBs-mc1PB3k9Yh0qsNpU2Mjm35nhKXw_uk"
                    )
                }
            }
        }
        dealerBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        filterBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun getFilterSuggestions(position: Int) {
        filterByDistanceItemBinder.lastSelectedItem = position
        distanceItemBinder.lastSelectedItem = position
        binding.incBSFilter.rgDistance.apply { post { adapter?.notifyDataSetChanged() } }
        binding.incBSDealer.rvDistance.apply { post { adapter?.notifyDataSetChanged() } }

        viewModel.getFilteredDealerSuggestion(
            requireContext().getAccessToken(),
            DealerSuggestionRequest(
                desitnationlong = locationHelper.currentLastLocation?.longitude?.toInt() ?: 0,
                destinationlat = locationHelper.currentLastLocation?.latitude?.toInt() ?: 0,
                distance = distanceItemBinder.lastSelectedItem,
                dealertype = dealerItemBinder.selectedItems,
                product = productItemBinder.selectedItems
            )
        )
    }

    private fun createDealerMarker(dealer: DealerSuggestionResponse.DealerItem) {
        if (!markers.containsKey(dealer.dealerId)) {
            val marker = googleMap?.addMarker(
                MarkerOptions()
                    .position(LatLng(dealer.lat, dealer.longitude))
                    .title(dealer.dealerName)
                    .snippet(dealer.fulladdress)
            )
            marker?.let {
                it.tag = dealer
                markers[dealer.dealerId] = it
            }
        }
    }

    private fun bindObservers() {
        viewModel.dealerLocatorResponse.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> {
                    mainActivity.showLoader(it.isLoading)
                }
                is State.Success -> {
                    it.body?.apply {
                        //        dealerSuggestionAdapter.setData(it.body.responseData ?: arrayListOf())
//                        binding.spnSuggestion.visible()
//                          viewModel.getDealerSuggestions(requireContext().getAccessToken(),results.)
                        //      binding.spnSuggestion.performClick()
                        it.body.responseData?.filter { it.lat.toInt() != 0 }?.forEach { item ->
                            createDealerMarker(item)
                        }
                        showToast(it.body.responseMessage)
                    }
                }
            }
        })
        viewModel.dealerLocatorFilteredResponse.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        markers = mutableMapOf()
                        it.body.responseData?.filter { it.lat.toInt() != 0 }?.forEach { item ->
                            createDealerMarker(item)
                        }
                    }
                }
            }
        })
        viewModel.dealerFilterResponse.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        binding.incBSFilter.rgDistance.adapter = getGenericAdapter(
                            collection = it.body.responseData?.milesdata ?: arrayListOf(),
                            viewBinder = distanceItemBinder
                        )
                        binding.incBSDealer.rvDistance.adapter = getGenericAdapter(
                            collection = it.body.responseData?.milesdata ?: arrayListOf(),
                            viewBinder = filterByDistanceItemBinder
                        )
                        binding.incBSDealer.rvDistance.layoutManager = LinearLayoutManager(
                            requireContext(),
                            LinearLayoutManager.HORIZONTAL,
                            false
                        )
                        binding.incBSFilter.llDealers.adapter = getGenericAdapter(
                            collection = it.body.responseData?.dealertypedata ?: arrayListOf(),
                            viewBinder = dealerItemBinder
                        )
                        binding.incBSFilter.llProducts.adapter = getGenericAdapter(
                            collection = it.body.responseData?.productssdata ?: arrayListOf(),
                            viewBinder = productItemBinder
                        )
                    }
                }
            }
        })
        viewModel.googlResult.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> {
                    mainActivity.showLoader(it.isLoading)
                }
                is State.Success -> {
                    it.body?.apply {
                        dealerSuggestionAdapter.setData(results ?: arrayListOf())
                        binding.spnSuggestion.performClick()
                    }
                }
            }
        })
    }

    private fun animateMarkerCamera(latLng: LatLng) {
        googleMap?.let {
            val cu = CameraUpdateFactory.newLatLngZoom(latLng, APP_ZOOM_LEVEL)
            it.animateCamera(cu)
        }
    }

    private fun checkLocationPermission() {
        Dexter.withContext(requireContext())
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                @SuppressLint("MissingPermission")
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
//                        googleMap?.isMyLocationEnabled = true
                    locationHelper.getLocationAsync()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    Toast.makeText(
                        requireContext(),
                        "LocationService is required",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                }
            }).check()
    }

    private fun showItemDetail(dealerCollections: ArrayList<DealerSuggestionResponse.DealerItem>) {
        binding.incBSDealer.rvDealers.apply {
            adapter = getGenericAdapter(dealerCollections, DealerItemBinder())
            layoutManager = LinearLayoutManager(requireContext())
        }
        dealerBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        filterBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    override fun setHeaderFooterViews(
        titleBar: TitleBar?,
        bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        titleBar?.hideButtons()
        if (titleBar?.context?.isLogin() == true)
            bottomNavigationView?.gone()
    }

}