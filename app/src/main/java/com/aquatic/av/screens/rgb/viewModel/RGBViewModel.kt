package com.aquatic.av.screens.rgb.viewModel

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.AquaticAVApplication
import com.aquatic.av.bluetoothScanner.ble.FileUtils
import com.aquatic.av.bluetoothScanner.ble.RGBManager
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceKey
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceUtil
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.PREF_BRIGHTNESS
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.PREF_ON_OFF_CONTROL
import com.aquatic.av.bluetoothScanner.ble.preference.getInt
import com.aquatic.av.bluetoothScanner.ble.preference.putBoolean
import com.aquatic.av.bluetoothScanner.ble.profile.BTConnectionListener
import com.aquatic.av.bluetoothScanner.ble.profile.RGBBLEManager
import com.aquatic.av.bluetoothScanner.ble.profile.RGBBLEManagerImpl
import com.aquatic.av.bluetoothScanner.ble.profile.RGBUUIDS
import com.aquatic.av.bluetoothScanner.ble.utils.BLEUtils
import com.aquatic.av.bluetoothScanner.ble.utils.DataUtil
import com.aquatic.av.bluetoothScanner.ble.utils.Transporter
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.RGBRepository
import com.aquatic.av.screens.rgb.dataModel.RGBColorModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.nio.charset.StandardCharsets
import javax.inject.Inject

@SuppressLint("StaticFieldLeak")
@HiltViewModel
class RGBViewModel @Inject constructor(@ApplicationContext private val context: Context,
                                       private val rgbRepository: RGBRepository) : ViewModel() {
    val firmwareVersion = MutableLiveData<String>()
    val onOFFStatus = MutableLiveData<Int>()
    val isConnected: LiveData<Boolean>
        get() = mIsConnected
    val isONOFF: LiveData<Int>
        get() = onOFFStatus
    val isDeviceConnected: Boolean
        get() = BLEUtils.isBleEnabled(context) && mBleManager.isConnected

    private val mBleManager: RGBBLEManager = RGBManager.getInstance().getBLEManager(context)

    // Flag to determine if the device is connected
    private val mIsConnected = MutableLiveData<Boolean>()
    private val bluetoothState = MutableLiveData<Boolean>()
    val onLocalFavoriteItemSelected = MutableLiveData<Int>()
    val onServerFavoriteItemSelected = MutableLiveData<RGBColorModel.ResponseData>()

    /**
     * Broadcast receiver to monitor the changes in the bluetooth adapter
     */
    private val mBluetoothStateBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF)
            val previousState = intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_STATE, BluetoothAdapter.STATE_OFF)
            when (state) {
                BluetoothAdapter.STATE_ON -> {
                    FileUtils.addlogs("HOME: ACTION_BT_STATE_ON ")
                    Log.d(TAG, "HOME: ACTION_BT_STATE_ON ")
                    bluetoothState.value = true
                    startAutoConnection()
                }
                BluetoothAdapter.STATE_TURNING_OFF, BluetoothAdapter.STATE_OFF -> {
                    Log.d(TAG, "HOME: ACTION_BT_STATE_OFF ")
                    FileUtils.addlogs("HOME: ACTION_BT_STATE_OFF")
                    bluetoothState.setValue(false)
                }
            }
        }
    }
    private val btConnectionListener: BTConnectionListener = object : BTConnectionListener {
        override fun onDeviceConnected(device: BluetoothDevice) {
            FileUtils.addlogs("HOME: onDeviceConnected : " + device.address)
            mIsConnected.postValue(true)
            sendModeConfigurationData()
            readData()
            //After connection send brightness value to controller
            val brightness: Int = PREF_BRIGHTNESS.getInt(Transporter.BRIGHTNESS_DEFAULT_VALUE)
            sendBrightnessData(brightness)
        }

        override fun onDeviceDisconnected(device: BluetoothDevice) {
            FileUtils.addlogs("HOME: onDevice Disconnected : ")
            mIsConnected.postValue(false)
        }

        override fun onDataSent() {}
        override fun onSuccess(data: String) {}
        override fun onFailed(data: String) {}
        override fun onDataReceived(characteristic: BluetoothGattCharacteristic, data: ByteArray) {
            if (characteristic.uuid == RGBUUIDS.CHARACTERISTIC_FW_VERSION) {
                val fwVersion = String(characteristic.value, StandardCharsets.UTF_8)
                PreferenceUtil.getInstance().setStringValue(PreferenceKey.FW_VERSION, fwVersion)
                firmwareVersion.postValue(fwVersion)
            } else if (characteristic.uuid == RGBUUIDS.CHARACTERISTIC_BRIGHTNESS_CONTROL) {
                val brightness: Int = DataUtil.convertByteToInt(data, 1)
                Log.d(TAG, "Read BRIGHTNESS data: $brightness")
                FileUtils.addlogs("HOME: Brightness value: $brightness")
                //            RgbSharedPreferencesKt.putInt(PreferenceKeys.getPREF_BRIGHTNESS(), brightness);
            } else if (characteristic.uuid == RGBUUIDS.CHARACTERISTIC_ON_OFF_CONTROL) {
                val onOffState: Int = DataUtil.convertByteToInt(data, 1)
                FileUtils.addlogs("HOME: CHARACTERISTIC_ON_OFF_CONTROL onOffState  value: $onOffState")
                Log.d(TAG, "Read ON/OFF data:$onOffState")
                PREF_ON_OFF_CONTROL.putBoolean(onOffState == 1)
                onOFFStatus.postValue(onOffState)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        RGBManager.getInstance().unregisterConnectionListener(btConnectionListener)
    }

    fun sendColorAdjustment(brightness: Int, r: Int, g: Int, b: Int) {
        if (!Transporter.isMusicEnabled()) {
            sendDisableMode()
            sendRGBColor(brightness, r, g, b)
        }
    }

    private fun sendRGBColor(brightness: Int, r: Int, g: Int, b: Int) {
        Transporter.sendRGBColorData(mBleManager, brightness, r, g, b)
    }

    private fun sendDisableMode() {
        Transporter.sendDisableMode(mBleManager)
    }

    /**
     * @return
     */

    fun disconnect() {
        if (isDeviceConnected) {
            mBleManager.disconnect()
        }
    }

    /**
     * Register a broadcast receiver
     */
    fun registerBTStateReceiver(context: Context) {
        //Register Bluetooth state change receiver
        context.registerReceiver(mBluetoothStateBroadcastReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
        //        LocalBroadcastManager.getInstance(context).registerReceiver(mBTStateBroadcastReceiver,
//                new IntentFilter(BTConstant.ACTION_BT_STATE_ON));
//        LocalBroadcastManager.getInstance(context).registerReceiver(mBTStateBroadcastReceiver,
//                new IntentFilter(BTConstant.ACTION_BT_STATE_OFF));
    }

    /**
     * Unregister a broadcast receiver
     */
    fun unRegisterBTStateReceiver(context: Context) {
        context.unregisterReceiver(mBluetoothStateBroadcastReceiver)
    }

    private fun readData() {
        FileUtils.addlogs("HOME: Start reading ON/OFF control data")
        mBleManager.readData(RGBUUIDS.CHARACTERISTIC_ON_OFF_CONTROL)
        mBleManager.readData(RGBUUIDS.CHARACTERISTIC_FW_VERSION)
    }

    fun sendONOFFControlData(selected: Boolean) {
        Transporter.sendONOFFControlData(mBleManager, selected)
        if (isDeviceConnected) onOFFStatus.postValue(if (selected) 1 else 0)
    }

    fun sendBrightnessData(brightness: Int) {
        Transporter.sendBrightnessData(mBleManager, brightness)
    }

    fun sendSpeedData(mSpeed: Int) {
        Transporter.sendModeSpeed(mBleManager, mSpeed)
    }

    fun sendModeConfigurationData() {
        Transporter.sendModeConfig(context, mBleManager)
    }

    fun sendActivateModeCodeData(modeCode: Int) {
        FileUtils.addlogs("HOME: sendActivateModeCodeData $modeCode")
        Transporter.sendActivateMode(mBleManager, modeCode)
    }

    /**
     * Closes and releases resources.
     */
    fun close() {
        mBleManager.close()
    }

    fun startAutoConnection() {
        if (!isDeviceConnected) {
            val macAddress: String = PreferenceUtil.getInstance()
                    .getStringValue(PreferenceKey.CONNECTED_BT_DEVICE_ADDRESS, null)
            if (!TextUtils.isEmpty(macAddress)) {
                if (BLEUtils.isBleEnabled(context.applicationContext)) {
                    FileUtils.addlogs("HOME: startAutoConnect")
                    (context as AquaticAVApplication).startAutoConnect()
                }
            }
        }
    }

    companion object {
        private const val TAG = "HomeViewModel"
    }

    init {
        // Initialize the manager
        mBleManager.setGattCallbacks(RGBBLEManagerImpl())
        RGBManager.getInstance().registerConnectionListener(btConnectionListener)
    }

    private val _updateRGBColorResponse = MutableLiveData<Event<State<Any>>>()
    val updateRGBColorResponse: LiveData<Event<State<Any>>> get() = _updateRGBColorResponse
    private val _allColorsResponse = MutableLiveData<Event<State<RGBColorModel>>>()
    val allColorsResponse: LiveData<Event<State<RGBColorModel>>> get() = _allColorsResponse
    private val _allColorsFavoritesResponse = MutableLiveData<Event<State<RGBColorModel>>>()
    val allColorsFavoritesResponse: LiveData<Event<State<RGBColorModel>>> get() = _allColorsFavoritesResponse
    private val _deleteColorsResponse = MutableLiveData<Event<State<RGBColorModel>>>()
    val deleteColorsResponse: LiveData<Event<State<RGBColorModel>>> get() = _deleteColorsResponse
    fun updateRGBColor(token: String, dcid: Int, colorCode: String, isFavorite: Int, is_top: Int, device_id: Int) {
        viewModelScope.launch {
            rgbRepository.addUpdateColor(token, hashMapOf(
                    "device_id" to device_id,
                    "dcid" to dcid,
                    "color_code" to colorCode,
                    "is_favourite" to isFavorite,
                    "is_top" to is_top
            )).collect {
                _updateRGBColorResponse.value = Event(it)
            }
        }
    }

    fun getUserFavoritesColor(token: String, device_id: Int) {
        viewModelScope.launch {
            rgbRepository.getAllUserColors(token, hashMapOf(
                    "device_id" to device_id,
            )).collect {
                _allColorsResponse.value = Event(it)
                _allColorsFavoritesResponse.value = Event(it)
            }
        }
    }


    fun removeDeviceFavoritesColor(token: String, list: List<Int>) {
        viewModelScope.launch {
            rgbRepository.removeDeviceFavoritesColor(token, hashMapOf(
                    "dcid" to list
            )).collect {
                _deleteColorsResponse.value = Event(it)
            }
        }
    }
}