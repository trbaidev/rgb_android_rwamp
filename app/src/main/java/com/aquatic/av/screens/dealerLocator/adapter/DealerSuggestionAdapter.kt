package com.aquatic.av.screens.dealerLocator.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.aquatic.av.R
import com.aquatic.av.screens.dealerLocator.model.GoogleAutoCompleteResult
import com.aquatic.av.utils.capitalizeWords


class DealerSuggestionAdapter(private val mContext: Context, private val itemClickListener: (item: GoogleAutoCompleteResult, position: Int) -> Unit) :
        ArrayAdapter<GoogleAutoCompleteResult>(mContext, R.layout.row_item_spinner) {
    private val dealerCollection: MutableList<GoogleAutoCompleteResult> = mutableListOf()


    override fun getCount(): Int {
        return dealerCollection.size
    }

    override fun getItem(position: Int): GoogleAutoCompleteResult {
        return dealerCollection[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label = super.getView(position, convertView, parent) as TextView
        label.text = java.lang.String.format("%s ", getItem(position).formatted_address?.capitalizeWords())
        label.setOnClickListener { itemClickListener.invoke(getItem(position), position) }
        label.setPaddingRelative(25, 25, 25, 25)
        return label
    }


    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val itemView: View = (mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.row_item_spinner, parent, false)
        try {
            val department: GoogleAutoCompleteResult = getItem(position)
            val name = convertView?.findViewById(android.R.id.text1) as TextView
            name.text = department.formatted_address?.capitalizeWords()
            name.setOnClickListener { itemClickListener.invoke(department, position) }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return itemView
    }

//    fun setData(entityList: List<DealerSuggestionResponse.DealerItem>) {
//        dealerCollection.clear()
//        dealerCollection.addAll(entityList)
//        notifyDataSetChanged()
//    }

    fun setData(entityList: List<GoogleAutoCompleteResult>) {
        dealerCollection.clear()
        dealerCollection.addAll(entityList)
        notifyDataSetChanged()
    }

}