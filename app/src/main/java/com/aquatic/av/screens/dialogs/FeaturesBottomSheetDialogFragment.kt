package com.aquatic.av.screens.dialogs

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.aquatic.av.R
import com.aquatic.av.databinding.BottomSheetBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class FeaturesBottomSheetDialogFragment : BottomSheetDialogFragment() {

    lateinit var binding: BottomSheetBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet, null, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            setHtmlText(binding.tvLineOne, getString(R.string.line_one))
            setHtmlText(binding.tvLineTwo, getString(R.string.line_two))
            setHtmlText(binding.tvLineFive, getString(R.string.line_five))
            binding.ivClose.setOnClickListener(View.OnClickListener { dialog?.dismiss() })
        }
    }

    companion object {
        const val TAG = "FeaturesBottomSheetDialogFragment"
    }


    private fun setHtmlText(textView: TextView, html: String) {
        textView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(html)
        }
    }
}