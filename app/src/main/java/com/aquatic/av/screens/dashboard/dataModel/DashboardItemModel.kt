package com.aquatic.av.screens.dashboard.dataModel


import com.aquatic.av.R
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceKey
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceUtil
import com.google.gson.annotations.SerializedName

data class DashboardItemModel(
    @SerializedName("base_path")
    var basePath: String? = "",
    @SerializedName("dealer_name")
    var dealerName: String? = "",
    @SerializedName("device_id")
    var deviceId: Int? = 0,
    @SerializedName("device_name")
    var deviceName: String? = "",
    @SerializedName("env_icon_name")
    var envIconName: String? = "",
    @SerializedName("env_name")
    var envName: String? = "",
    @SerializedName("expiry_date")
    var expiryDate: String? = "",
    @SerializedName("is_ble_device")
    var isBleDevice: Boolean? = false,
    @SerializedName("is_mobile_app")
    var isMobileApp: Boolean? = false,
    @SerializedName("product_code")
    var productCode: String? = "",
    @SerializedName("product_name")
    var productName: String? = "",
    @SerializedName("product_image")
    var productImage: String? = "",
    @SerializedName("purchase_date")
    var purchaseDate: String? = "",
    var receipt: String? = "",
    @SerializedName("receipt_path")
    var receiptPath: String? = "",
    @SerializedName("receipt_status")
    var receiptStatus: String? = "",
    @SerializedName("remaining_warranty")
    var remainingWarranty: String? = "",
    @SerializedName("last_connected")
    var lastConnected: String? = "",
    @SerializedName("serial_no")
    var serial_no:String?="",
    @SerializedName("device_uuid")
    var device_uuid:String?="",
    var serialNo: String? = "",
    var fav_colors: List<String>,
    var user_name: String,
    var last_login_date: String,
    var purchase_location: String,
    var sku: String,
    var software_version: String ) {
    var deviceConnectImage: Int? = -1
    var fw_version = PreferenceUtil.getInstance().getStringValue(PreferenceKey.FW_VERSION, "")
    var connected_device_name = PreferenceUtil.getInstance().getStringValue("connected_device_name", "")
    var connected_device_id = PreferenceUtil.getInstance().getIntValue("connected_device_id", 0)

    fun getProductImageURL(): String = "$basePath/$productImage"

    fun showConnectedStatus(): Boolean = isBleDevice == true && isMobileApp == true

    fun deviceConnectedText(): String = if (!(device_uuid.isNullOrEmpty() && connected_device_name.isNullOrEmpty())&&(device_uuid.equals(connected_device_name, true)))
        "Connected"
    else
        "Not Connected"

    fun getStatusImage(): Int = if (deviceName.equals(connected_device_name, true) && (deviceId == connected_device_id || -100 == connected_device_id))
        R.drawable.ic_status_vector_enabled
    else
        R.drawable.ic_status_vector_disabled

    fun getWarrantyText(): String = if (remainingWarranty?.toInt() ?: 0 > 0)
        "In warranty"
    else
        "Warranty expired"

    fun getDeviceVersion(): String = if (isBleDevice == true && isMobileApp == true) {
        "SW: 1.2 | Updated"
//        "SW: $fw_version | Updated"
    } else {
        ""
    }
}