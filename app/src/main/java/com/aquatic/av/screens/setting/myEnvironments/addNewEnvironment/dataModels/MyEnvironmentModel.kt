package com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.dataModels

data class MyEnvironmentModel(
    var userId: Int? = 0,
    var types: Array<String>
)