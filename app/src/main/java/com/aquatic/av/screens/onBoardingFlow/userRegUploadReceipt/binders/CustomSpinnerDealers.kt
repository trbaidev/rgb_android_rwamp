package com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.binders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.aquatic.av.R
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.DealerModel

class CustomSpinnerDealers(dealersModel: List<DealerModel>) : BaseAdapter() {
    private  var dealersModel: List<DealerModel>

    init {
        this.dealersModel = dealersModel
    }

    override fun getCount(): Int {
        return dealersModel.size
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view: View
        var inflater: LayoutInflater = LayoutInflater.from(parent?.context)
        view = inflater.inflate(R.layout.spinner_single_item_custom, null)
        var text = view.findViewById<TextView>(R.id.tvEnvNameList)
        text.text = dealersModel.get(position).dealer_name
        return view
    }
}