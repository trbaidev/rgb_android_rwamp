package com.aquatic.av.screens.setting.myEnvironments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquatic.av.R
import com.aquatic.av.databinding.FragmentMyEnvironmentBinding
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.EventObserver
import com.aquatic.av.screens.base.BaseFragment
import com.aquatic.av.screens.base.BaseRecyclerViewAdapter
import com.aquatic.av.screens.setting.myEnvironments.addNewEnvironment.AddNewEnvironmentFragment
import com.aquatic.av.screens.setting.myEnvironments.addnewdevice.AddNewDeviceFragment
import com.aquatic.av.screens.setting.myEnvironments.binder.MyEnvironmentBinder
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyCustomEnvironmentModel
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyCustomEnvironmentResponse
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentModel
import com.aquatic.av.screens.setting.myEnvironments.viewModel.MyEnvironmentUpdateViewModel
import com.aquatic.av.screens.setting.myEnvironments.viewModel.MyEnvironmentsViewModel
import com.aquatic.av.utils.BasePreferenceHelper.getAccessToken
import com.aquatic.av.utils.BasePreferenceHelper.getUserDetailModel
import com.aquatic.av.utils.EditTextExtension.mapToObject
import com.aquatic.av.utils.showToast
import com.aquatic.av.utils.visible
import com.aquatic.av.views.TitleBar
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyEnvironmentsFragment :
        BaseFragment<FragmentMyEnvironmentBinding>(R.layout.fragment_my_environment) {

    val userInfoModel by lazy { requireContext().getUserDetailModel() }
    private val myEnvironmentsViewModel by viewModels<MyEnvironmentsViewModel>()
    private val myEnvironmentsUpdateViewModel by viewModels<MyEnvironmentUpdateViewModel>()

    private val myEnvironmentAdapter by lazy {
        BaseRecyclerViewAdapter(
                arrayListOf(),
                myEnvironmentBinder,
                requireContext()
        )
    }

    private val myEnvironmentBinder by lazy {
        MyEnvironmentBinder { entity, position ->
            updateData(entity)
        }
    }

    private fun updateData(myEnvironmentmodel: MyEnvironmentModel) {
        val cusEnv = MyCustomEnvironmentModel(
                environmentId = myEnvironmentmodel.getEnvId()!!,
                environmentName = myEnvironmentmodel.getEnvName()!!,
                env_Icon_Name = myEnvironmentmodel.getEnvName(),
                isActive = 1,
                userIds = arrayOf(requireContext().getUserDetailModel()?.user_id ?: 0),
                environmentType = myEnvironmentmodel.getEnvType()!!
        )
        myEnvironmentsUpdateViewModel.addUpdateEnvironments(
                requireContext().getAccessToken(),
                cusEnv
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        seMyEnvListeners()
        binding.myRecyclerView.adapter = myEnvironmentAdapter
        binding.myRecyclerView.layoutManager = LinearLayoutManager(mainActivity)
        binding.apply {
            btnBack.setOnClickListener {
                mainActivity.popFragment()
            }
            btnAddNewDevice.setOnClickListener {
             //   mainActivity.navigateTo(AddNewDeviceFragment.getInstance("Add New Environment", "Custom Environment"))
                mainActivity.navigateTo(AddNewEnvironmentFragment())
            }
        }
        userInfoModel?.user_id?.let {
            var jObj = JsonObject()
            jObj.addProperty("user_id", it)
            myEnvironmentsViewModel.getMyEnvironmentInfo(
                    requireContext().getAccessToken(),
                    jObj
            )
        }
    }

    private fun setListeners() {
        myEnvironmentsViewModel.myEnvironmentResults.observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is State.Error -> showToast(it.message)
                is State.Exception -> {
                    it.exception.printStackTrace()
                    showToast(it.exception.message)
                }
                is State.Loading -> mainActivity.showLoader(it.isLoading)
                is State.Success -> {
                    it.body?.apply {
                        it.body.getResponseData()?.let { it1 ->
                            myEnvironmentAdapter.addAll(
                                    it1
                            )
                        }
                    }
                }
            }
        })
    }


    private fun seMyEnvListeners() {
        myEnvironmentsUpdateViewModel.myEnvironmentResults.observe(
                viewLifecycleOwner,
                EventObserver {
                    when (it) {
                        is State.Error -> showToast(it.message)
                        is State.Exception -> {
                            it.exception.printStackTrace()
                            showToast(it.exception.message)
                        }
                        is State.Loading -> mainActivity.showLoader(it.isLoading)
                        is State.Success -> {
                            it.body?.apply {
                                var myCustomEnvironmentResponse = Gson().mapToObject(
                                        it.body as Map<String, Any?>?,
                                        MyCustomEnvironmentResponse::class.java
                                )
                                if (myCustomEnvironmentResponse?.responseCode == 1) {
                                    showToast("Environment updated")
                                } else {
                                    showToast(myCustomEnvironmentResponse?.responseMessage)
                                }
                            }
                        }
                    }
                })
    }


    override fun setHeaderFooterViews(
            titleBar: TitleBar?,
            bottomNavigationView: BottomNavigationView?
    ) {
        super.setHeaderFooterViews(titleBar, bottomNavigationView)
        titleBar?.hideTitleBar()
        titleBar?.hideButtons()
        bottomNavigationView?.visible()
    }
}