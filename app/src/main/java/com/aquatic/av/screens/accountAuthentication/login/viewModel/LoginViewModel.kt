package com.aquatic.av.screens.accountAuthentication.login.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginModel
import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _loginResults = MutableLiveData<Event<State<LoginResponse>>>()
    val loginResults: LiveData<Event<State<LoginResponse>>> get() = _loginResults

    fun loginUser(loginModel: LoginModel) {
        viewModelScope.launch {
            authRepository.loginUser(loginModel).collect {
                _loginResults.value = (Event(it))
            }
        }
    }
}