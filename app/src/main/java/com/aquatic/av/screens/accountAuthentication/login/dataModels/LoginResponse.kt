package com.aquatic.av.screens.accountAuthentication.login.dataModels

import com.aquatic.av.activities.model.UserEnvironmentModel

data class LoginResponse(
    var user_info: UserInfo,
    var devices_info: List<UserEnvironmentModel>
) {

    data class UserInfo(
        var first_name: String,
        var last_name: String,
        var user_email: String,
        var address: String,
        var city: String,
        var state: String,
        var zip_code: String,
        var phone: String,
        var user_exist: String,
        var country: String,
        var user_role_id: Int,
        var user_role: String,
        var user_type_id: Int,
        var user_type: String,
        var token: String,
        var user_id: Int,
        var user_pwd:String
    ){
        public fun getFullName():String = "$first_name $last_name"
    }

}