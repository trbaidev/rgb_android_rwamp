package com.aquatic.av.screens.onBoardingFlow.userEnvironment.binders

import android.content.Context
import com.aquatic.av.R
import com.aquatic.av.databinding.UserEnvironmentRowItemBinding
import com.aquatic.av.screens.base.BaseRecyclerViewBinder
import com.aquatic.av.screens.onBoardingFlow.userEnvironment.dataModels.UserEnvironmentResponse

class UserEnvironmentBinder(var lastSelectedIndex: Int = -1, private var itemClickLister: (entity: UserEnvironmentResponse.Environment, position: Int) -> Unit) :
        BaseRecyclerViewBinder<UserEnvironmentResponse.Environment, UserEnvironmentRowItemBinding>(R.layout.user_environment_row_item) {

    override fun bindView(
            entity: UserEnvironmentResponse.Environment,
            position: Int,
            viewHolder: BaseViewHolder<UserEnvironmentRowItemBinding>?,
            context: Context,
            collections: MutableList<UserEnvironmentResponse.Environment>
    ) {
        viewHolder?.viewBinding?.let {
            it.dataItem = entity
            it.isItemSelected = position == lastSelectedIndex
            viewHolder.itemView.setOnClickListener {
                lastSelectedIndex = position
                itemClickLister.invoke(entity, position)
            }
        }
    }
}