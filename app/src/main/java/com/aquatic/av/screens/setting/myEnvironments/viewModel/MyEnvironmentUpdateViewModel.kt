package com.aquatic.av.screens.setting.myEnvironments.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aquatic.av.network.State
import com.aquatic.av.network.baseModel.Event
import com.aquatic.av.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyEnvironmentUpdateViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {
    private val _myEnvironmentResults = MutableLiveData<Event<State<Any>>>()
    val myEnvironmentResults: LiveData<Event<State<Any>>> get() = _myEnvironmentResults

    fun addUpdateEnvironments(token:String,userCustomEnvironmentModel: Any) {
        viewModelScope.launch {
            authRepository.addUpdateEnvironments(token,userCustomEnvironmentModel).collect {
                _myEnvironmentResults.value = (Event(it))
            }
        }
    }
}