package com.aquatic.av.bluetoothScanner.ble.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.aquatic.av.R;
import com.aquatic.av.databinding.RowScannerDeviceBinding;

import java.util.List;

@SuppressWarnings("unused")
public class ConnectedDeviceAdapter extends RecyclerView.Adapter<ConnectedDeviceAdapter.ViewHolder> {
    private final Context mContext;
    private final List<RGBBluetoothDevice> mDevices;
    private OnItemClickListener mOnItemClickListener;

    public ConnectedDeviceAdapter(final Context context, List<RGBBluetoothDevice> devices) {
        mContext = context;
        mDevices = devices;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        RowScannerDeviceBinding scannerDeviceBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.row_scanner_device,
                parent, false);
        return new ViewHolder(scannerDeviceBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final RGBBluetoothDevice device = mDevices.get(position);
        final String deviceName = device.getName();

        if (!TextUtils.isEmpty(deviceName))
            holder.scannerDeviceBinding.deviceName.setText(deviceName);
        else
            holder.scannerDeviceBinding.deviceName.setText(R.string.unknown_device);

        holder.scannerDeviceBinding.deviceAddress.setText(device.getAddress());

        if (device.isConnectionState()) {
            holder.scannerDeviceBinding.txtConnectedStatus.setSelected(true);
            holder.scannerDeviceBinding.txtConnectedStatus.setText(R.string.connected);
        } else {
            holder.scannerDeviceBinding.txtConnectedStatus.setSelected(false);
            holder.scannerDeviceBinding.txtConnectedStatus.setText(R.string.connect);
        }
        if (device.isConnectionState()) {
            holder.scannerDeviceBinding.connectionStateIv.setColorFilter(ContextCompat.getColor(mContext, R.color.theme_blue), PorterDuff.Mode.SRC_ATOP);
        } else {
            holder.scannerDeviceBinding.connectionStateIv.setColorFilter(ContextCompat.getColor(mContext, R.color.gray), PorterDuff.Mode.SRC_ATOP);
        }

    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void updateConnectedDevice(RGBBluetoothDevice device) {
        if (mDevices != null) {
            mDevices.clear();
            mDevices.add(device);
            notifyDataSetChanged();
        }
    }

    final class ViewHolder extends RecyclerView.ViewHolder {
        private RowScannerDeviceBinding scannerDeviceBinding;

        private ViewHolder(final RowScannerDeviceBinding scannerDeviceBinding) {
            super(scannerDeviceBinding.getRoot());
            this.scannerDeviceBinding = scannerDeviceBinding;
            scannerDeviceBinding.getRoot().setOnClickListener(v -> {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(mDevices.get(getAdapterPosition()));
                }
            });
        }
    }
}
