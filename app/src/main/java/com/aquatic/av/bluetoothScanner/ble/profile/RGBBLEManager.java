package com.aquatic.av.bluetoothScanner.ble.profile;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;

import com.aquatic.av.bluetoothScanner.ble.FileUtils;
import com.aquatic.av.bluetoothScanner.ble.RGBManager;
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceKey;
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceUtil;
import com.aquatic.av.bluetoothScanner.ble.utils.DataUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Deque;
import java.util.LinkedList;
import java.util.UUID;

import no.nordicsemi.android.ble.BleManager;
import no.nordicsemi.android.ble.Request;
import no.nordicsemi.android.ble.utils.ParserUtils;

public class RGBBLEManager extends BleManager<RGBBLEManagerCallbacks> {

    private static final String TAG = RGBBLEManager.class.getSimpleName();
    private static RGBBLEManager ourInstance = null;
    private BluetoothGattCharacteristic mRGBControlCharacteristic;//mModeConfigCharacteristic, mRGBControlCharacteristic, mModeControlCharacteristic, mPinConfigCharacteristic;
    private ByteArrayOutputStream receivedBytes = new ByteArrayOutputStream();
    private BluetoothGatt mBluetoothGatt;
    private RGBBLEManagerCallbacks callbacks;
    private boolean isHandlerRunning;
    private BluetoothGattCharacteristic mBrightnessCharacteristic;
    private BluetoothGattCharacteristic mONOFFCharacteristic;
    private BluetoothGattCharacteristic mFWVersionCharacteristic;
    /**
     * BluetoothGatt callbacks for connection/disconnection, service discovery, receiving indication, etc
     */
    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {

        @Override
        protected Deque<Request> initGatt(final BluetoothGatt gatt) {
            Log.d(TAG, "initGatt");
            FileUtils.addlogs("BLE: initGatt");
            final LinkedList<Request> requests = new LinkedList<>();
//            requests.push(Request.newReadRequest(mModeControlCharacteristic));
//            requests.push(Request.newReadRequest(mModeConfigCharacteristic));
            requests.push(Request.newReadRequest(mRGBControlCharacteristic));
            requests.push(Request.newReadRequest(mONOFFCharacteristic));
            requests.push(Request.newReadRequest(mBrightnessCharacteristic));
            requests.push(Request.newReadRequest(mFWVersionCharacteristic));
            return requests;
        }

        @Override
        public boolean isRequiredServiceSupported(final BluetoothGatt gatt) {
            Log.d(TAG, "isRequiredServiceSupported");
            FileUtils.addlogs("BLE: isRequiredServiceSupported");
            mBluetoothGatt = gatt;
            final BluetoothGattService ledRGBService = gatt.getService(RGBUUIDS.PRIMARY_LED_RGB_BLE_SERVICE);
            if (ledRGBService != null) {
                mRGBControlCharacteristic = ledRGBService.getCharacteristic(RGBUUIDS.CHARACTERISTIC_RGB_CONTROL);
                mONOFFCharacteristic = ledRGBService.getCharacteristic(RGBUUIDS.CHARACTERISTIC_ON_OFF_CONTROL);
                mBrightnessCharacteristic = ledRGBService.getCharacteristic(RGBUUIDS.CHARACTERISTIC_BRIGHTNESS_CONTROL);
                mFWVersionCharacteristic = ledRGBService.getCharacteristic(RGBUUIDS.CHARACTERISTIC_FW_VERSION);
//                mModeControlCharacteristic = ledRGBService.getCharacteristic(RGBUUIDS.CHARACTERISTIC_MODE_CONTROL);
//                mModeConfigCharacteristic = ledRGBService.getCharacteristic(RGBUUIDS.CHARACTERISTIC_MODE_CONFIG);
//                mPinConfigCharacteristic = ledRGBService.getCharacteristic(RGBUUIDS.CHARACTERISTIC_PIN_CONFIG);
            }

            boolean writeRequest = false;
            if (ledRGBService != null) {
                writeRequest = isWritePropertyEnabled(mRGBControlCharacteristic);
            }

            return mRGBControlCharacteristic != null
                    && writeRequest;
        }

        @Override
        protected void onDeviceDisconnected() {
            Log.d(TAG, "onDeviceDisconnected");
//            mModeConfigCharacteristic = null;
//            mPinConfigCharacteristic = null;
            mRGBControlCharacteristic = null;
            mBluetoothGatt = null;
            FileUtils.addlogs("RGBBLE MANAGER: onDeviceDisconnected");
            RGBManager.getInstance().notifyDeviceConnection(null, false);
//            mModeControlCharacteristic = null;
        }

        @Override
        protected void onCharacteristicRead(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            byte[] bytes = characteristic.getValue();
            if (bytes != null) {
                Log.d(TAG, "onCharacteristicRead: bytes: " + bytes.length + "; data: " + DataUtil.bytesToHex(bytes)
                        + "; Characteristics:" + characteristic.getUuid());
                String hexString = DataUtil.bytesToHex(bytes);
                Log.d(TAG, "Read response Hex data: " + hexString);

                if (characteristic.getUuid().equals(RGBUUIDS.CHARACTERISTIC_FW_VERSION)) {
                    String fwVersion = new String(bytes, StandardCharsets.UTF_8);
                    PreferenceUtil.getInstance().setStringValue(PreferenceKey.FW_VERSION, fwVersion);
                }
                mCallbacks.onDataReceived(characteristic, bytes);
            }
        }

        @Override
        public void onCharacteristicWrite(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            byte[] bytes = characteristic.getValue();
            if (bytes != null) {
                Log.d(TAG, "onCharacteristicWrite: bytes: " + bytes.length + "; data: " + DataUtil.bytesToHex(bytes)
                        + "; Characteristics:" + characteristic.getUuid());
            }
            mCallbacks.onDataSent(characteristic);
        }

        @Override
        public void onCharacteristicNotified(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            if (!isHandlerRunning) {
                startHandler(characteristic);
            }
            byte[] bytes = characteristic.getValue();
            try {
                receivedBytes.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "onCharacteristicNotified: bytes: " + bytes.length + "; data: " + DataUtil.bytesToHex(bytes)
                    + "; Characteristics:" + characteristic.getUuid());
        }

        @Override
        protected void onBatteryValueReceived(BluetoothGatt gatt, int value) {
            Log.d(TAG, "battery level: " + value);
        }
    };

    private RGBBLEManager(final Context context) {
        super(context);
    }

    public static RGBBLEManager getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new RGBBLEManager(context);
        }
        return ourInstance;
    }

    private boolean isWritePropertyEnabled(BluetoothGattCharacteristic characteristic) {
        boolean writeRequest;
        final int rxProperties = characteristic.getProperties();
        writeRequest = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0;
        return writeRequest;
    }

    private void startHandler(BluetoothGattCharacteristic characteristic) {
        isHandlerRunning = true;
        android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isHandlerRunning = false;
                handleOnCharacteristicNotified(characteristic, receivedBytes.toByteArray());
            }
        }, 1000);
    }

    private void handleOnCharacteristicNotified(BluetoothGattCharacteristic characteristic, byte[] bytes) {
        try {
            receivedBytes.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        receivedBytes.reset();
        String data = ParserUtils.parse(bytes);
        Log.d(TAG, "Success response: " + data);
        mCallbacks.onDataReceived(characteristic, bytes);
    }

    @NonNull
    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattCallback;
    }

    @Override
    public void setGattCallbacks(@NonNull RGBBLEManagerCallbacks callbacks) {
        super.setGattCallbacks(callbacks);
        this.callbacks = callbacks;
    }

    public void removeGattCallbacks() {
        callbacks = null;
    }

    @Override
    protected boolean shouldAutoConnect() {
        // If you want to connect to the device using autoConnect flag = true, return true here.
        return true;
    }

    /**
     * Send data from App to BLE
     */
    public void sendData(UUID characteristicUUID, byte[] command) {
        Log.d(TAG, "Send Data UUID:" + characteristicUUID.toString());
        Log.d(TAG, "Send Data length:" + command.length);
        Log.d(TAG, "Send Data HEX:" + DataUtil.bytesToHex(command));

//        FileUtils.addlogs("Send Data HEX:" + DataUtil.bytesToHex(command));

        if (!isConnected() || mBluetoothGatt == null) {
            FileUtils.addlogs("Error in Send Data failed isConnected() " + isConnected() + "; mBluetoothGatt: "+ mBluetoothGatt);
            return;
        }
        BluetoothGattService ledRGBService =
                mBluetoothGatt.getService(RGBUUIDS.PRIMARY_LED_RGB_BLE_SERVICE);
        if (ledRGBService != null) {
            BluetoothGattCharacteristic characteristic
                    = ledRGBService.getCharacteristic(characteristicUUID);
            characteristic.setValue(command);
            boolean status = writeCharacteristic(characteristic, command, BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
            Log.d(TAG, "SEND DATA isWriteResponse :" + status);
            FileUtils.addlogs("SEND DATA isWriteResponse" + status);
        } else {
            FileUtils.addlogs("Error in Send Data  ledRGBService is null");
            Log.d(TAG, "ledRGBService is null");
        }
    }

    /**
     * Send data from App to BLE
     */
    public void readData(UUID characteristicUUID) {
        if (!isConnected() || mBluetoothGatt == null || characteristicUUID == null) {
            FileUtils.addlogs("readData isConnected():  "+ isConnected() + "; mBluetoothGatt" + mBluetoothGatt);
            return;
        }

        BluetoothGattService ledRGBService =
                mBluetoothGatt.getService(RGBUUIDS.PRIMARY_LED_RGB_BLE_SERVICE);
        if (ledRGBService != null) {
            BluetoothGattCharacteristic characteristic
                    = ledRGBService.getCharacteristic(characteristicUUID);
            boolean status = readCharacteristic(characteristic);
            FileUtils.addlogs("Reading characteristic status: "+ status);
            Log.d(TAG, "Reading characteristic " + characteristic.getUuid());
            Log.d(TAG, "gatt.readCharacteristic(" + characteristic.getUuid() + ")");
            Log.d(TAG, "Read Char Data :" + status);
        } else {
            FileUtils.addlogs("ledRGBService is null");
            Log.d(TAG, "ledRGBService is null");
        }
    }


    /**
     * convert byte into int value
     *
     * @param aByte
     * @return
     */
    private int getIntValue(byte aByte) {
        return Math.abs(aByte & 0xFF);
    }

    public void enableNotifications() {
        enableNotifications(mRGBControlCharacteristic);
    }

    public void requestBLEConnectionPriority(int priority) {
        requestConnectionPriority(priority);
    }

}
