package com.aquatic.av.bluetoothScanner.ble.profile;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;

/**
 * Project           : RGB
 * File Name         : BTConnectionListener
 * Description       :
 * Revision History  : version 1
 * Date              : 10/16/18
 * Original author   : Kannappan
 * Description       : Initial version
 */
public interface BTConnectionListener {
    void onDeviceConnected(final BluetoothDevice device);

    void onDeviceDisconnected(final BluetoothDevice device);

    /**
     * Called when the data has been sent to the connected device.
     */
    void onDataSent();

    /**
     * Called when the data has been sent to the connected device and received the response from Device.
     *
     * @param data
     */
    void onSuccess(String data);

    /**
     * Called when receiving a failed message from connected device
     *
     * @param data
     */
    void onFailed(String data);

    /**
     * Called when the characteristic is changed from connected device.
     *  @param characteristic
     * @param bytes
     */
    void onDataReceived(BluetoothGattCharacteristic characteristic, byte[] bytes);
}
