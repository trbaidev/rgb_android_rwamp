package com.aquatic.av.bluetoothScanner.ble;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;

import com.aquatic.av.bluetoothScanner.ble.profile.BTConnectionListener;
import com.aquatic.av.bluetoothScanner.ble.profile.RGBBLEManager;
import com.aquatic.av.bluetoothScanner.ble.profile.RGBBLEManagerImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Project           : RGB
 * File Name         : RGBManager
 * Description       :
 * Revision History  : version 1
 * Date              : 10/3/18
 * Original author   : Kannappan
 * Description       : Initial version
 */
public class RGBManager {
    private static final RGBManager ourInstance = new RGBManager();
    private List<BTConnectionListener> btConnectionListeners = new ArrayList<>();
    private Context appContext;

    private RGBManager() {
    }

    public static RGBManager getInstance() {
        return ourInstance;
    }

    public List<BTConnectionListener> getBtConnectionListeners() {
        return btConnectionListeners;
    }

    /**
     * @param btConnectionListener
     */
    public void registerConnectionListener(BTConnectionListener btConnectionListener) {
        if (!btConnectionListeners.contains(btConnectionListener)) {
            btConnectionListeners.add(btConnectionListener);
        }
    }

    /**
     * @param btConnectionListener
     */
    public void unregisterConnectionListener(BTConnectionListener btConnectionListener) {
        btConnectionListeners.remove(btConnectionListener);
    }

    /**
     * Notify device connection state
     *
     * @param bluetoothDevice
     * @param isConnected
     */
    public void notifyDeviceConnection(BluetoothDevice bluetoothDevice, boolean isConnected) {
        for (BTConnectionListener btConnectionListener : btConnectionListeners) {
            if (isConnected) {
                btConnectionListener.onDeviceConnected(bluetoothDevice);
            } else {
                btConnectionListener.onDeviceDisconnected(bluetoothDevice);
            }
        }
    }

    public RGBBLEManager getBLEManager(Context context) {
        RGBBLEManager rgbbleManager = RGBBLEManager.getInstance(context);
        rgbbleManager.setGattCallbacks(new RGBBLEManagerImpl());
        return rgbbleManager;
    }

    public void init(Context app) {
        this.appContext = app;
    }

    public Context getAppContext() {
        return appContext;
    }

    public void setAppContext(Context appContext) {
        this.appContext = appContext;
    }

    public void notifyOnDataReceived(BluetoothGattCharacteristic characteristic, byte[] bytes) {
        for (BTConnectionListener btConnectionListener : btConnectionListeners) {
            btConnectionListener.onDataReceived(characteristic, bytes);
        }
    }
}
