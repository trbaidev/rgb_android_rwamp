package com.aquatic.av.bluetoothScanner.ble.adapter;

@FunctionalInterface
public interface OnItemClickListener {
    void onItemClick(final RGBBluetoothDevice device);
}