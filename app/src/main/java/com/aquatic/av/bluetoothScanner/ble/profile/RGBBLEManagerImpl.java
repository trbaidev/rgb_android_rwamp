package com.aquatic.av.bluetoothScanner.ble.profile;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;

import androidx.annotation.Nullable;

import com.aquatic.av.bluetoothScanner.ble.RGBManager;

import java.util.List;

/**
 * Project           : RGB
 * File Name         : RGBBLEManagerImpl
 * Description       :
 * Revision History  : version 1
 * Date              : 9/29/18
 * Original author   : Kannappan
 * Description       : Initial version
 */
public class RGBBLEManagerImpl implements RGBBLEManagerCallbacks {

    @Override
    public void onDataSent(BluetoothGattCharacteristic characteristic) {
        List<BTConnectionListener> btConnectionListeners = getBtConnectionListeners();
        if (btConnectionListeners == null) return;
        for (BTConnectionListener btConnectionListener : btConnectionListeners) {
            btConnectionListener.onDataSent();
        }
    }

    @Nullable
    private List<BTConnectionListener> getBtConnectionListeners() {
        List<BTConnectionListener> btConnectionListeners = RGBManager.getInstance().getBtConnectionListeners();
        if (btConnectionListeners == null || btConnectionListeners.size() == 0) {
            return null;
        }
        return btConnectionListeners;
    }

    @Override
    public void onSuccess(String data) {
        List<BTConnectionListener> btConnectionListeners = getBtConnectionListeners();
        if (btConnectionListeners == null) return;
        for (BTConnectionListener btConnectionListener : btConnectionListeners) {
            btConnectionListener.onSuccess(data);
        }
    }

    @Override
    public void onFailed(String data) {
        List<BTConnectionListener> btConnectionListeners = getBtConnectionListeners();
        if (btConnectionListeners == null) return;
        for (BTConnectionListener btConnectionListener : btConnectionListeners) {
            btConnectionListener.onFailed(data);
        }
    }

    @Override
    public void onDataReceived(BluetoothGattCharacteristic characteristic, byte[] bytes) {
        List<BTConnectionListener> btConnectionListeners = getBtConnectionListeners();
        if (btConnectionListeners == null) return;
        for (BTConnectionListener btConnectionListener : btConnectionListeners) {
            btConnectionListener.onDataReceived(characteristic, bytes);
        }
    }

    @Override
    public void onDeviceConnecting(BluetoothDevice device) {

    }

    @Override
    public void onDeviceConnected(BluetoothDevice device) {
        RGBManager.getInstance().notifyDeviceConnection(device, true);
    }

    @Override
    public void onDeviceDisconnecting(BluetoothDevice device) {

    }

    @Override
    public void onDeviceDisconnected(BluetoothDevice device) {
        RGBManager.getInstance().notifyDeviceConnection(device, false);
    }

    @Override
    public void onLinklossOccur(BluetoothDevice device) {
        RGBManager.getInstance().notifyDeviceConnection(device, false);
    }

    @Override
    public void onServicesDiscovered(BluetoothDevice device, boolean optionalServicesFound) {

    }

    @Override
    public void onDeviceReady(BluetoothDevice device) {

    }

    @Override
    public boolean shouldEnableBatteryLevelNotifications(BluetoothDevice device) {
        return false;
    }

    @Override
    public void onBatteryValueReceived(BluetoothDevice device, int value) {

    }

    @Override
    public void onBondingRequired(BluetoothDevice device) {

    }

    @Override
    public void onBonded(BluetoothDevice device) {

    }

    @Override
    public void onError(BluetoothDevice device, String message, int errorCode) {

    }

    @Override
    public void onDeviceNotSupported(BluetoothDevice device) {

    }
}
