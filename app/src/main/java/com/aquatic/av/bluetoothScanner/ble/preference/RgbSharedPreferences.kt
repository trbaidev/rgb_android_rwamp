package com.aquatic.av.bluetoothScanner.ble.preference

import android.content.Context
import android.content.SharedPreferences

object RgbSharedPreferences {
    const val PREF_BRIGHTNESS = "BRIGHTNESS"
    const val PREF_ON_OFF_CONTROL = "ON_OFF_CONTROL"
    const val BLE_CONNECTION_INTERVAL_DELAY_TIME: Long = 10
    const val COLOR_WHEEL_INTERVAL_DELAY_TIME: Long = 100
    const val PREF_FLASH_MUSIC_ENABLED = "FLASH_MUSIC_ENABLED"
    const val PREF_MODE_ENABLED = "MODE_ENABLED"
    const val preset1Value = "preset1"
    const val preset2Value = "preset2"
    const val preset3Value = "preset3"
    const val preset4Value = "preset4"
    const val preset5Value = "preset5"

    const val fav1Value = "fav1"
    const val fav2Value = "fav2"
    const val fav3Value = "fav3"
    const val fav4Value = "fav4"
    const val fav5Value = "fav5"
    const val PREF_SPEED = "SPEED"

    lateinit var sharedPreferences: SharedPreferences

    fun init(context: Context, preferencesName: String = context.packageName) {
        init(context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE))
    }

    private fun init(sharedPreferences: SharedPreferences) {
        this.sharedPreferences = sharedPreferences
    }

    fun clear() {
        this.sharedPreferences.edit().clear().commit()
    }
}

fun String.getBoolean(defaultValue: Boolean = false): Boolean {
    return RgbSharedPreferences.sharedPreferences.getBoolean(this, defaultValue)
}

fun String.getInt(defaultValue: Int = 0): Int {
    return RgbSharedPreferences.sharedPreferences.getInt(this, defaultValue)
}

fun String.getFloat(defaultValue: Float = 0.0f): Float {
    return RgbSharedPreferences.sharedPreferences.getFloat(this, defaultValue)
}

fun String.getLong(defaultValue: Long = 0): Long {
    return RgbSharedPreferences.sharedPreferences.getLong(this, defaultValue)
}

fun String.getString(defaultValue: String = ""): String? {
    return RgbSharedPreferences.sharedPreferences.getString(this, defaultValue)
}

fun String.getStringSet(defaultValue: Set<String>): Set<String>? {
    return RgbSharedPreferences.sharedPreferences.getStringSet(this, defaultValue)
}

fun String.putBoolean(value: Boolean = false) {
    RgbSharedPreferences.sharedPreferences.edit()?.putBoolean(this, value)?.apply()
}

fun String.putInt(value: Int = 0) {
    RgbSharedPreferences.sharedPreferences.edit()?.putInt(this, value)?.apply()
}

fun String.putFloat(value: Float = 0.0f) {
    RgbSharedPreferences.sharedPreferences.edit()?.putFloat(this, value)?.apply()
}

fun String.putLong(value: Long = 0) {
    RgbSharedPreferences.sharedPreferences.edit()?.putLong(this, value)?.apply()
}

fun String.putString(value: String? = "") {
    RgbSharedPreferences.sharedPreferences.edit()?.putString(this, value)?.apply()
}

fun String.putStringSet(value: Set<String>) {
    RgbSharedPreferences.sharedPreferences.edit()?.putStringSet(this, value)?.apply()
}