package com.aquatic.av.bluetoothScanner.ble.utils;

/**
 * Project           : RGB
 * File Name         : BTConstant
 * Description       :
 * Revision History  : version 1
 * Date              : 10/16/18
 * Original author   : Kannappan
 * Description       : Initial version
 */
public interface BTConstant {

    //BT broadcast action
    String ACTION_BT_STATE_ON = "action_bt_state_on";
    String ACTION_BT_STATE_OFF = "action_bt_state_off";
}
