package com.aquatic.av.bluetoothScanner.ble.profile;

import java.util.UUID;

/**
 * Project           : RGB
 * File Name         : RGBUUIDS
 * Description       :
 * Revision History  : version 1
 * Date              : 9/23/18
 * Original author   : Kannappan
 * Description       : Initial version
 */
public class RGBUUIDS {

    public static UUID PRIMARY_LED_RGB_BLE_SERVICE = UUID.fromString("f3f70001-9e9a-4daf-a910-31264d01239d");

    //RGB-Control Characteristic - READ/WRITE
    public static UUID CHARACTERISTIC_RGB_CONTROL = UUID.fromString("f3f70002-9e9a-4daf-a910-31264d01239d");

    //Mode Control Characteristic - WRITE/READ - Mode Control Characteristic is used to activate already configured RGB control mode OR Deactivate OR Configure a new mode
    public static UUID CHARACTERISTIC_MODE_CONTROL = UUID.fromString("f3f70003-9e9a-4daf-a910-31264d01239d");

    //Mode Config Characteristic - WRITE/READ
    public static UUID CHARACTERISTIC_MODE_CONFIG = UUID.fromString("f3f70004-9e9a-4daf-a910-31264d01239d");

    //PIN Config Characteristic - WRITE/READ
//    public static UUID CHARACTERISTIC_PIN_CONFIG = UUID.fromString("443e9b29-c723-4050-b303-de11aa9c06f7");

    //RGB-Control Characteristic - READ/WRITE
    public static UUID CHARACTERISTIC_BRIGHTNESS_CONTROL = UUID.fromString("f3f70005-9e9a-4daf-a910-31264d01239d");

    //RGB-Control Characteristic - READ/WRITE
    public static UUID CHARACTERISTIC_ON_OFF_CONTROL = UUID.fromString("f3f70006-9e9a-4daf-a910-31264d01239d");

    //RGB-Control Characteristic - READ/WRITE
    public static UUID CHARACTERISTIC_FW_VERSION = UUID.fromString("f3f70007-9e9a-4daf-a910-31264d01239d");

    public static UUID CHARACTERISTIC_SPEED_CONTROL_CHAR = UUID.fromString("f3f70008-9e9a-4daf-a910-31264d01239d");

}
