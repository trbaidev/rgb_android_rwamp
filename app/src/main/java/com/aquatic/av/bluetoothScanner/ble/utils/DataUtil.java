package com.aquatic.av.bluetoothScanner.ble.utils;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class DataUtil {
    private static final char[] hexArray;
    private static final String TAG = DataUtil.class.getSimpleName();

    static {
        hexArray = "0123456789ABCDEF".toCharArray();
    }

    public static byte[] integerToBytes(int value, int byteCount) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            DataOutputStream dos = new DataOutputStream(baos);
            if (byteCount == 1) {
                dos.writeByte(value);//1 byte
            } else if (byteCount == 2) {
                dos.writeShort(value);//2 byte
            }
            dos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }

    /**
     * byte conversion of integers
     *
     * @param value
     * @param byteCount
     * @return
     */
    public static byte[] byteConversion(int value, int byteCount) {
        byte[] bytes = new byte[byteCount];

        bytes[byteCount - 1] = (byte) (value & 0xFF);

        if (byteCount == 1) {
            return bytes;
        }

        byteCount = byteCount - 2;

        while (byteCount >= 0) {
            //shift 8 bits on right
            value = value >> 8;
            bytes[byteCount] = (byte) ((value) & 0xFF);
            byteCount--;
        }
        return bytes;
    }

    /**
     * Int to byte as little endian order
     *
     * @param value
     * @return
     */
    public static byte[] littleEndianIntToByteArray(int value) {
        byte[] ret = new byte[4];
        ret[0] = (byte) (value & 0xFF);
        ret[1] = (byte) ((value >> 8) & 0xFF);
        ret[2] = (byte) ((value >> 16) & 0xFF);
        ret[3] = (byte) ((value >> 24) & 0xFF);
        Log.d(TAG, "litteleEndianIntToByteArray Little endian: " + bytesToHex(ret));
        return ret;
    }

    /**
     * Byte to int as Little endian order
     *
     * @param bytes
     * @return
     */
    public static int convertLittleEndianByteArray(byte[] bytes) {
        return (bytes[3] << 24) & 0xff000000 |
                (bytes[2] << 16) & 0x00ff0000 |
                (bytes[1] << 8) & 0x0000ff00 |
                (bytes[0] << 0) & 0x000000ff;
    }


    /**
     * COnvert string data into bytes with given length
     *
     * @param data
     * @param length
     * @return
     */
    @NonNull
    public static byte[] stringToBytesData(String data, int length) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(data);
        int len = data.length();
        if (len < length) {
            for (int i = length; i > len; i--) {
                stringBuilder.append(" ");
            }
        }
        return stringBuilder.toString().getBytes();
    }

    /**
     * @param hex
     * @return
     */
    public static String hexToString(String hex) {
        StringBuilder sb = new StringBuilder();
        char[] hexData = hex.toCharArray();
        for (int count = 0; count < hexData.length - 1; count += 2) {
            int firstDigit = Character.digit(hexData[count], 16);
            int lastDigit = Character.digit(hexData[count + 1], 16);
            int decimal = firstDigit * 16 + lastDigit;
            sb.append((char) decimal);
        }
        return sb.toString();
    }

    /**
     * converts bytes to Hex
     *
     * @param bytes
     * @return
     */
    @Nullable
    public static String bytesToHex(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        char[] hexChars = new char[(bytes.length * 2)];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 255;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[(j * 2) + 1] = hexArray[v & 15];
        }
        return new String(hexChars);
    }

    /**
     * remove /r and /n in byte data
     *
     * @param bytes
     * @return
     */
    public static byte[] removeCRLF(byte[] bytes) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];
            int bInt = b;
            if (bInt == 13) {
                if (i < bytes.length && (bytes[i + 1] == 10)) {
                    continue;
                } else {
                    byteArrayOutputStream.write(b);
                }
            } else {
                byteArrayOutputStream.write(b);
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static int convertByteToInt(byte[] bytes, int length) {
        if(bytes.length == 1 && length == 1){
            return bytes[0] & 0x01;
        }
        return  -1;
    }

}
