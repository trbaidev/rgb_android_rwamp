package com.aquatic.av.bluetoothScanner.ble.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BTUtil {
    public static final int BT_ENABLE = 102;
    private static final String TAG = BTUtil.class.getSimpleName();

    /**
     * // For API level 18 and above, get a reference to BluetoothAdapter through
     * // BluetoothManager.
     * get bluetooth manager
     *
     * @return
     */
    public static BluetoothManager getBLEManager(Context context) {
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        return bluetoothManager;
    }

    /**
     * get Bluetooth adapter
     *
     * @return
     */
    public static BluetoothAdapter getBLEAdapter(Context context) {
        BluetoothManager bleManager = getBLEManager(context);
        if (bleManager == null) {
            return null;
        }
        BluetoothAdapter adapter = bleManager.getAdapter();
        return adapter;
    }

    /**
     * get all bonded devices
     */
    public static List<BluetoothDevice> getBondedBTDevices(Context context, int type, String deviceName) {
        BluetoothAdapter mBluetoothAdapter = getBLEAdapter(context);
        List<BluetoothDevice> devices = new ArrayList<>();
        if (mBluetoothAdapter != null) {
            Set<BluetoothDevice> bluetoothDevices = mBluetoothAdapter.getBondedDevices();
            if (bluetoothDevices != null && bluetoothDevices.size() > 0) {
                for (BluetoothDevice device :
                        bluetoothDevices) {
                    if (!TextUtils.isEmpty(device.getName())) {
                        devices.add(device);
                    }
                }
            }
            return devices;
        }
        return null;
    }

    /**
     * get connected  BLE devices
     *
     * @return
     */
    public static List<BluetoothDevice> getConnectedBLEDevices(Context context) {
        List<BluetoothDevice> devices = new ArrayList<>();
        //Connected devices
        List<BluetoothDevice> connectedDevices = getBLEManager(context).getConnectedDevices(BluetoothProfile.GATT);
        for (BluetoothDevice device : connectedDevices) {
            if (device.getType() == BluetoothDevice.DEVICE_TYPE_LE) {
                //store connected device
                devices.add(device);
            }
        }
        return devices;
    }

    /**
     * check whether the device is bonded or not
     *
     * @return
     */
    public static boolean isBonded(BluetoothDevice bluetoothDevice) {
        int bondState = bluetoothDevice.getBondState();
        return bondState == BluetoothDevice.BOND_BONDED;
    }

    /**
     * create a bond
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static void createBond(BluetoothDevice bluetoothDevice) {
        boolean bondState;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            bondState = bluetoothDevice.createBond();
        } else {
            bondState = createBondApi18(bluetoothDevice);
        }
        if (bondState) {
            Log.d(TAG, "Bonded successfully...");
        } else {
            Log.d(TAG, "Error in bond process.");
        }
    }

    /**
     * create bond for api leve 18
     *
     * @param device
     * @return
     */
    private static boolean createBondApi18(final BluetoothDevice device) {
        /*
         * There is a createBond() method in BluetoothDevice class but for now it's hidden. We will call it using reflections. It has been revealed in KitKat (Api19)
         */
        try {
            final Method createBond = device.getClass().getMethod("createBond");
            if (createBond != null) {
                return (Boolean) createBond.invoke(device);
            }
        } catch (final Exception e) {
            Log.e(TAG, "An exception occurred while creating bond", e);
        }
        return false;
    }


    /**
     * Removes the bond information for the given device.
     *
     * @param device the device to unbound
     * @return <code>true</code> if operation succeeded, <code>false</code> otherwise
     */
    public static boolean removeBond(final BluetoothDevice device) {
        if (device.getBondState() == BluetoothDevice.BOND_NONE)
            return true;

        Log.d(TAG, "Removing bond information...");
        boolean result = false;
        /*
         * There is a removeBond() method in BluetoothDevice class but for now it's hidden. We will call it using reflections.
         */
        try {
            final Method removeBond = device.getClass().getMethod("removeBond");
            if (removeBond != null) {
                result = (Boolean) removeBond.invoke(device);
            }
        } catch (final Exception e) {
            Log.e(TAG, "An exception occurred while removing bond information", e);
        }
        return result;
    }

    /**
     * To check whether the device has BLE or not.
     *
     * @param context Application/Activity  context
     * @return Returns TRUE- device has a BLE, otherwise return FALSE
     */
    public static boolean deviceHasBle(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    /**
     * To check whether the device has BLUETOOTH or not.
     *
     * @param context Application/Activity  context
     * @return Returns TRUE- device has a BLUETOOTH, otherwise return FALSE
     */
    public static boolean deviceHasBT(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH);
    }

    /**
     * To check whether the BT is enabled or not
     *
     * @param context Application/Activity context
     * @return Returns TRUE-  BT is enabled, otherwise return FALSE
     */
    public static boolean isBluetoothEnabled(Context context) {
        BluetoothManager bluetoothManager = getBLEManager(context);
        BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();
        return (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled());
    }

    /**
     * @param context Application/Activity context
     */
    public static void startEnableBluetoothActivity(Context context) {
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        ((Activity) context).startActivityForResult(intent, BT_ENABLE);
    }

    /**
     * Get the remote BT device from mac address.
     *
     * @param context    Application/Activity context
     * @param macAddress String- Mac address of the BT device
     * @return {BluetoothDevice} - Returns the BluetoothDevice otherwise null.
     */
    public static BluetoothDevice getRemoteDevice(Context context, String macAddress) {
        BluetoothManager bluetoothManager = getBLEManager(context);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        return bluetoothAdapter.getRemoteDevice(macAddress);
    }

    /**
     * To check whether the Location is enabled or not
     *
     * @param context Application/Activity context
     * @return Returns TRUE-  Location is enabled, otherwise return FALSE
     */
    public static boolean isLocationPermissionOn(Context context) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean result = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return result;
    }

}
