package com.aquatic.av.bluetoothScanner.ble.profile;

import android.bluetooth.BluetoothGattCharacteristic;

import no.nordicsemi.android.ble.BleManagerCallbacks;

public interface RGBBLEManagerCallbacks extends BleManagerCallbacks {

    /**
     * Called when the data has been sent to the connected device.
     * @param characteristic
     */
    void onDataSent(BluetoothGattCharacteristic characteristic);

    /**
     * Called when the data has been sent to the connected device and received the response from Device.
     *
     * @param data
     */
    void onSuccess(String data);

    /**
     * Called when receiving a failed message from connected device
     *
     * @param data
     */
    void onFailed(String data);

    /**
     * Called when the characteristic is changed from connected device.
     *
     * @param bytes
     */
    void onDataReceived(BluetoothGattCharacteristic characteristic, byte[] bytes);
}
