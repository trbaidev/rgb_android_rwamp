package com.aquatic.av.bluetoothScanner.ble.preference;

/**
 * These unique keys are used to store value into shared preferences
 */
public interface PreferenceKey {

    String FW_VERSION = "FW_VERSION";
    String CONNECTED_BT_DEVICE_ADDRESS = "CONNECTED_BT_DEVICE_ADDRESS";
    String CONNECTED_BT_DEVICE_NAME = "CONNECTED_BT_DEVICE_NAME";
}
