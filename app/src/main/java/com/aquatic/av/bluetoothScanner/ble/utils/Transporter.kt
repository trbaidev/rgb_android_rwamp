package com.aquatic.av.bluetoothScanner.ble.utils

import android.content.Context
import android.util.Log
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.PREF_FLASH_MUSIC_ENABLED
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences.PREF_MODE_ENABLED
import com.aquatic.av.bluetoothScanner.ble.preference.getBoolean
import com.aquatic.av.bluetoothScanner.ble.profile.RGBBLEManager
import com.aquatic.av.bluetoothScanner.ble.profile.RGBUUIDS
import com.aquatic.av.player.AAVPlayer
import com.aquatic.av.screens.rgb.dataModel.ModeType
import java.io.ByteArrayOutputStream
import kotlin.math.min

object Transporter {
    private const val TAG = "Transporter"

    fun sendONOFFControlData(mBleManager: RGBBLEManager, selected: Boolean) {
//        if (PREF_MODE_ENABLED.getBoolean(false)) {
//            sendDisableMode(mBleManager)
//        }
        val onOff = if (selected) 1 else 0
        val byteArrayOutputStream = ByteArrayOutputStream()
        try {
            byteArrayOutputStream.write(DataUtil.integerToBytes(onOff, 1))
            mBleManager.sendData(
                    RGBUUIDS.CHARACTERISTIC_ON_OFF_CONTROL,
                    byteArrayOutputStream.toByteArray()
            )
            byteArrayOutputStream.close()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            byteArrayOutputStream.close()
        }
    }

    fun sendBrightnessData(mBleManager: RGBBLEManager, brightness: Int) {
        Log.d(TAG, "sendBrightnessData: " + min(brightness * 15, 100))
        val byteArrayOutputStream = ByteArrayOutputStream()
        try {
            byteArrayOutputStream.write(DataUtil.integerToBytes(
                    min(brightness * 15, 100), 1))
            mBleManager.sendData(
                    RGBUUIDS.CHARACTERISTIC_BRIGHTNESS_CONTROL,
                    byteArrayOutputStream.toByteArray()
            )
            byteArrayOutputStream.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            byteArrayOutputStream.close()
        }
    }

    fun sendDisableMode(mBleManager: RGBBLEManager) {
        val byteArrayOutputStream = ByteArrayOutputStream()
        try {
            byteArrayOutputStream.write(DataUtil.integerToBytes(ModeType.MODE_DISABLE, 1))
            byteArrayOutputStream.write(DataUtil.integerToBytes(0, 1))
            mBleManager.sendData(
                    RGBUUIDS.CHARACTERISTIC_MODE_CONTROL,
                    byteArrayOutputStream.toByteArray()
            )
            byteArrayOutputStream.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            byteArrayOutputStream.close()
        }

    }

    fun sendActivateMode(mBleManager: RGBBLEManager, modeCode: Int) {
        val byteArrayOutputStream = ByteArrayOutputStream()
        try {
            byteArrayOutputStream.write(DataUtil.integerToBytes(ModeType.MODE_ACTIVATE, 1))
            byteArrayOutputStream.write(DataUtil.integerToBytes(modeCode, 1))
            mBleManager.sendData(
                    RGBUUIDS.CHARACTERISTIC_MODE_CONTROL,
                    byteArrayOutputStream.toByteArray()
            )
            byteArrayOutputStream.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            byteArrayOutputStream.close()
        }
    }

    fun sendConfigMode(mBleManager: RGBBLEManager, modeCode: Int) {
        val byteArrayOutputStream = ByteArrayOutputStream()
        try {
            byteArrayOutputStream.write(DataUtil.integerToBytes(ModeType.MODE_CONFIGURE, 1))
            byteArrayOutputStream.write(DataUtil.integerToBytes(modeCode, 1))
            mBleManager.sendData(
                    RGBUUIDS.CHARACTERISTIC_MODE_CONTROL,
                    byteArrayOutputStream.toByteArray()
            )
            byteArrayOutputStream.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            byteArrayOutputStream.close()
        }
    }

    const val BRIGHTNESS_DEFAULT_VALUE: Int = 3

    fun sendRGBColorData(mBleManager: RGBBLEManager, brightness: Int, r: Int, g: Int, b: Int) {
        if (PREF_MODE_ENABLED.getBoolean(false)) {
            sendDisableMode(mBleManager)
        }
        val byteArrayOutputStream = ByteArrayOutputStream()
        try {
            byteArrayOutputStream.write(DataUtil.integerToBytes(0, 1))
            //brightness
            byteArrayOutputStream.write(DataUtil.integerToBytes(
                    min(
                            brightness * 15,
                            100
                    ), 1))
            //color codes
            byteArrayOutputStream.write(DataUtil.integerToBytes(r, 1))
            byteArrayOutputStream.write(DataUtil.integerToBytes(g, 1))
            byteArrayOutputStream.write(DataUtil.integerToBytes(b, 1))
            mBleManager.sendData(
                    RGBUUIDS.CHARACTERISTIC_RGB_CONTROL,
                    byteArrayOutputStream.toByteArray()
            )
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            byteArrayOutputStream.close()
        }
    }

    fun sendModeSpeed(mBleManager: RGBBLEManager, modeSpeed: Int) {
        val byteArrayOutputStream = ByteArrayOutputStream()
        try {
            byteArrayOutputStream.write(DataUtil.integerToBytes(modeSpeed, 1))
            byteArrayOutputStream.write(DataUtil.integerToBytes(ModeType.MODE_SPEED_MIN, 1))
            byteArrayOutputStream.write(DataUtil.integerToBytes(ModeType.MODE_SPEED_MAX, 1))
            mBleManager.sendData(
                    RGBUUIDS.CHARACTERISTIC_SPEED_CONTROL_CHAR,
                    byteArrayOutputStream.toByteArray()
            )
            byteArrayOutputStream.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            byteArrayOutputStream.close()
        }
    }

    fun sendModeSpeed(mBleManager: RGBBLEManager, modeSpeed: Int, speedMin: Int, speedMax: Int) {
        val byteArrayOutputStream = ByteArrayOutputStream()
        try {
            byteArrayOutputStream.write(DataUtil.integerToBytes(modeSpeed, 1))
            byteArrayOutputStream.write(DataUtil.integerToBytes(speedMin, 1))
            byteArrayOutputStream.write(DataUtil.integerToBytes(speedMax, 1))
            mBleManager.sendData(
                    RGBUUIDS.CHARACTERISTIC_SPEED_CONTROL_CHAR,
                    byteArrayOutputStream.toByteArray()
            )
            byteArrayOutputStream.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            byteArrayOutputStream.close()
        }
    }

    fun isMusicEnabled(): Boolean {
        val isFlashMusic = false
        if (AAVPlayer.isPlaying() && PREF_FLASH_MUSIC_ENABLED.getBoolean(isFlashMusic)) {
            return true
        }
        return isFlashMusic
    }

    fun sendModeConfig(context: Context, mBleManager: RGBBLEManager) {
//        val modeConfiguration = getModeConfig(context)
//        if (modeConfiguration != null) {
//            for (mode in modeConfiguration.modes) {
//                when (mode.mode_number) {
//                    0 -> {
//                        sendModeData(mBleManager, mode)
//                    }
//                    1 -> {
//                        sendModeData(mBleManager, mode)
//                    }
//                    else -> {
//                        return
//                    }
//                }
//            }
//        }
    }
}