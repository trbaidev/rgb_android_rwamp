package com.aquatic.av.bluetoothScanner.ble.adapter;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;

import no.nordicsemi.android.support.v18.scanner.ScanResult;

public class RGBBluetoothDevice implements Parcelable {

    public static final Creator<RGBBluetoothDevice> CREATOR = new Creator<RGBBluetoothDevice>() {
        @Override
        public RGBBluetoothDevice createFromParcel(final Parcel source) {
            return new RGBBluetoothDevice(source);
        }

        @Override
        public RGBBluetoothDevice[] newArray(final int size) {
            return new RGBBluetoothDevice[size];
        }
    };

    private final BluetoothDevice device;
    private String name;
    private boolean connectionState;
    private int rssi;

    public RGBBluetoothDevice(final ScanResult scanResult) {
        this.device = scanResult.getDevice();
        this.name = scanResult.getScanRecord().getDeviceName();
        this.rssi = scanResult.getRssi();
        this.connectionState = false;
    }

    private RGBBluetoothDevice(final Parcel in) {
        this.device = in.readParcelable(BluetoothDevice.class.getClassLoader());
        this.name = in.readString();
        this.rssi = in.readInt();
        this.connectionState = in.readBoolean();
    }

    public RGBBluetoothDevice(String name, BluetoothDevice device) {
        this.name = name;
        this.device = device;
        this.rssi = 0;
        this.connectionState = false;
    }
    public RGBBluetoothDevice(String name, BluetoothDevice device,Boolean connectionState) {
        this.name = name;
        this.device = device;
        this.rssi = 0;
        this.connectionState = connectionState;
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public String getAddress() {
        return device.getAddress();
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(final int rssi) {
        this.rssi = rssi;
    }

    // Parcelable implementation

    public boolean matches(final ScanResult scanResult) {
        return device.getAddress().equals(scanResult.getDevice().getAddress());
    }

    @Override
    public boolean equals(final Object o) {
        if (o instanceof RGBBluetoothDevice) {
            final RGBBluetoothDevice that = (RGBBluetoothDevice) o;
            return device.getAddress().equals(that.device.getAddress());
        }
        return super.equals(o);
    }

    @Override
    public void writeToParcel(final Parcel parcel, final int flags) {
        parcel.writeParcelable(device, flags);
        parcel.writeString(name);
        parcel.writeInt(rssi);
        parcel.writeBoolean(connectionState);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public boolean isConnectionState() {
        return connectionState;
    }

    public void setConnectionState(boolean connectionState) {
        this.connectionState = connectionState;
    }
}
