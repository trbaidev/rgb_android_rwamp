
package com.aquatic.av.bluetoothScanner.ble.receiver;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.aquatic.av.bluetoothScanner.ble.utils.BTConstant;

public class BTStateChangeReceiver extends BroadcastReceiver {
    private static final String TAG = BTStateChangeReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "bluetooth state change receiver...");

        final String action = intent.getAction();
        if (!TextUtils.isEmpty(action) && action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    onBluetoothStateOFF(context);
                    Log.d(TAG, "bluetooth state STATE_OFF");
                    break;
                case BluetoothAdapter.STATE_ON:
                    Log.d(TAG, "bluetooth state STATE_ON");
                    onBluetoothStateON(context);
                    break;
            }
        }
    }

    private void onBluetoothStateON(Context context) {
        Intent intent = new Intent(BTConstant.ACTION_BT_STATE_ON);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private void onBluetoothStateOFF(Context context) {
        Intent intent = new Intent(BTConstant.ACTION_BT_STATE_OFF);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
