package com.aquatic.av.bluetoothScanner.ble.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.aquatic.av.R;
import com.aquatic.av.screens.bluetooth.viewModel.ScannerLiveData;
import com.aquatic.av.databinding.RowScannerDeviceBinding;

import java.util.List;

@SuppressWarnings("unused")
public class ScannerDeviceAdapter extends RecyclerView.Adapter<ScannerDeviceAdapter.ViewHolder> {
    private final Context mContext;
    public List<RGBBluetoothDevice> mDevices;
    private OnItemClickListener mOnItemClickListener;

    @SuppressLint("NotifyDataSetChanged")
    public ScannerDeviceAdapter(final Context activity, LifecycleOwner lifecycleOwner, final ScannerLiveData scannerLiveData) {
        mContext = activity;
        mDevices = scannerLiveData.getDevices();
        scannerLiveData.observe(lifecycleOwner, devices -> {
            final Integer i = devices.getUpdatedDeviceIndex();
            if (i != null)
                notifyItemChanged(i);
            else
                notifyDataSetChanged();
        });
    }

    public void setOnItemClickListener(final OnItemClickListener context) {
        mOnItemClickListener = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        RowScannerDeviceBinding scannerDeviceBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.row_scanner_device,
                parent, false);
        return new ViewHolder(scannerDeviceBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final RGBBluetoothDevice device = mDevices.get(position);
        final String deviceName = device.getName();

        if (!TextUtils.isEmpty(deviceName))
            holder.scannerDeviceBinding.deviceName.setText(deviceName);
        else
            holder.scannerDeviceBinding.deviceName.setText(R.string.unknown_device);
        holder.scannerDeviceBinding.deviceAddress.setText(device.getAddress());
        if (device.isConnectionState()) {
            holder.scannerDeviceBinding.txtConnectedStatus.setSelected(true);
            holder.scannerDeviceBinding.txtConnectedStatus.setText(R.string.connected);
        } else {
            holder.scannerDeviceBinding.txtConnectedStatus.setSelected(false);
            holder.scannerDeviceBinding.txtConnectedStatus.setText(R.string.connect);
        }
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    final class ViewHolder extends RecyclerView.ViewHolder {
        private final RowScannerDeviceBinding scannerDeviceBinding;

        private ViewHolder(final RowScannerDeviceBinding scannerDeviceBinding) {
            super(scannerDeviceBinding.getRoot());
            this.scannerDeviceBinding = scannerDeviceBinding;
            scannerDeviceBinding.deviceContainer.setOnClickListener(v -> {
                if (mOnItemClickListener != null && getAdapterPosition() != -1) {
                    mOnItemClickListener.onItemClick(mDevices.get(getAdapterPosition()));
                }
            });
        }
    }
}
