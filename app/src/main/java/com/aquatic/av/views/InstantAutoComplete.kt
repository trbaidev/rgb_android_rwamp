package com.aquatic.av.views

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatAutoCompleteTextView

class InstantAutoComplete @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : AppCompatAutoCompleteTextView(context, attrs) {

    fun constructor(context: Context?) {
        constructor(context)
    }

    fun constructor(arg0: Context?, arg1: AttributeSet?) {
        constructor(arg0, arg1)
    }

    fun constructor(arg0: Context?, arg1: AttributeSet?, arg2: Int) {
        constructor(arg0, arg1, arg2)
    }

    override fun enoughToFilter(): Boolean {
        return true
    }

    override fun onFocusChanged(
        focused: Boolean, direction: Int,
        previouslyFocusedRect: Rect?
    ) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        if (focused && getFilter() != null) {
            performFiltering(getText(), 0)
        }
    }
}
