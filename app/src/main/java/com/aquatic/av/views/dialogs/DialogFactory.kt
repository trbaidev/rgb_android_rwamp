package com.aquatic.av.views.dialogs

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.aquatic.av.R
import com.aquatic.av.callBacks.ButtonListners

object DialogFactory {


    fun createQuitDialog(
        activity: Activity?,
        dialogPositive: DialogInterface.OnClickListener?, messageId: Int
    ): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setIcon(R.mipmap.ic_launcher)
            .setTitle(R.string.dialog_alert_title)
            .setMessage(messageId)
            .setCancelable(true)
            .setPositiveButton(R.string.yes, dialogPositive)
            .setNegativeButton(
                R.string.no
            ) { dialog, _ -> dialog.dismiss() }
        return builder.create()
    }

    fun createSimpleDialog(
        activity: Activity?,
        dialogPositive: DialogInterface.OnClickListener?,
        dialogNegative: DialogInterface.OnClickListener?,
        messageId: Int,
        titleId: Int,
        yesButtonText: Int,
        noButtonText: Int
    ): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setIcon(R.mipmap.ic_launcher)
            .setTitle(titleId)
            .setMessage(messageId)
            .setCancelable(true)
            .setPositiveButton(yesButtonText, dialogPositive)
            .setNegativeButton(noButtonText, dialogNegative)
        return builder.create()
    }

    fun createMessageDialog(
        activity: Activity?,
        dialogPositive: DialogInterface.OnClickListener?,
        message: CharSequence?, titleId: String?
    ): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setIcon(R.mipmap.ic_launcher)
            .setTitle(titleId)
            .setCancelable(false)
            .setMessage(message)
            .setCancelable(false)
            .setPositiveButton(R.string.ok, dialogPositive)
        return builder.create()
    }

    fun createMessageDialog2(
        activity: Activity?,
        dialogPositive: DialogInterface.OnClickListener?,
        message: CharSequence?, titleId: String?
    ): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setIcon(R.mipmap.ic_launcher)
            .setTitle(R.string.dialog_alert_title)
            .setMessage(message)
            .setCancelable(true)
            .setPositiveButton("Yes", dialogPositive)
            .setNegativeButton(
                "No"
            ) { dialog, _ -> dialog.dismiss() }
        return builder.create()
    }

    fun createInputDialog(
        activity: Activity?,
        dialogPositive: DialogInterface.OnClickListener?,
        dialogNegative: DialogInterface.OnClickListener?, title: String?,
        message: String?
    ): Dialog {
        val alert = AlertDialog.Builder(activity)
        alert.setIcon(R.mipmap.ic_launcher)
        alert.setTitle(title)
        alert.setMessage(message)
        val input = EditText(activity)
        alert.setView(input)
        alert.setPositiveButton("Yes", dialogPositive)
        alert.setNegativeButton("No", dialogNegative)
        return alert.create()
    }

    fun Fragment.showDialog(
        title: String,
        message: String,
        cancelAble: Boolean = false,
        customDialogClickListener: DialogInterface.OnClickListener = DialogInterface.OnClickListener { dialog, _ -> dialog?.cancel() }
    ): AlertDialog {

        val dialog = AlertDialog.Builder(requireContext(), R.style.MyDialogTheme)
        dialog.setTitle(title)
        dialog.setMessage(message)
        dialog.setCancelable(cancelAble)
        dialog.setNegativeButton("Ok", customDialogClickListener)
        return dialog.show()
    }

    fun Fragment.showDialog(title: String, message: String, cancelAble: Boolean = false, buttonListners: ButtonListners): AlertDialog {
        val dialog = AlertDialog.Builder(requireContext(), R.style.MyDialogTheme)
        dialog.setTitle(title)
        dialog.setMessage(message)
        dialog.setCancelable(cancelAble)
        dialog.setNegativeButton("Cancel") { dialog, which ->
            if (buttonListners != null) {
                buttonListners.negavtivecall()
            }
            dialog.cancel()
        }
        dialog.setPositiveButton(
            "Skip"
        ) { dialog, which ->
            if (buttonListners != null) {
                buttonListners.positiveCall()
            }
        }
       return dialog.show()
    }

    fun Fragment.showDialog(
        title: String,
        message: String,
        cancelAble: Boolean = false
    ): AlertDialog {

        val dialog = AlertDialog.Builder(requireContext(), R.style.MyDialogTheme)
        dialog.setTitle(title)
        dialog.setMessage(message)
        dialog.setCancelable(cancelAble)
        return dialog.show()
    }

    fun Activity.showDialog(
        title: String,
        message: String,
        customDialogClickListener: DialogInterface.OnClickListener = DialogInterface.OnClickListener { dialog, _ -> dialog?.cancel() }
    ): AlertDialog {
        val dialog = AlertDialog.Builder(this, R.style.MyDialogTheme)
        dialog.setTitle(title)
        dialog.setMessage(message)
        dialog.setNegativeButton("Ok", customDialogClickListener)
        return dialog.show()
    }

    fun Context.showDialog(
        title: String,
        message: String,
        customDialogClickListener: DialogInterface.OnClickListener = DialogInterface.OnClickListener { dialog, _ -> dialog?.cancel() }
    ): AlertDialog {
        val dialog = AlertDialog.Builder(this, R.style.MyDialogTheme)
        dialog.setTitle(title)
        dialog.setMessage(message)
        dialog.setNegativeButton("Ok", customDialogClickListener)
        return dialog.show()
    }
}