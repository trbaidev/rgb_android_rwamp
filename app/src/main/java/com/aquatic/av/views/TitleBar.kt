package com.aquatic.av.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.FrameLayout
import com.aquatic.av.databinding.HeaderMainBinding
import com.aquatic.av.screens.dashboard.adapter.EnvironmentAdapter
import com.aquatic.av.activities.model.UserEnvironmentModel
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentModel
import com.aquatic.av.utils.gone
import com.aquatic.av.utils.invisible
import com.aquatic.av.utils.visible

class TitleBar : FrameLayout {
    var binding: HeaderMainBinding? = null
    val adapter = EnvironmentAdapter(context)

    private var backButtonListener: OnClickListener? = null

    constructor(context: Context) : super(context) {
        initLayout(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initLayout(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        initLayout(context)
    }


    private fun initLayout(context: Context) {
        this.binding = HeaderMainBinding.inflate(LayoutInflater.from(context), this, true)
        this.binding?.spnEnvironment?.adapter = adapter

    }

    fun hideButtons() {
        this.binding?.apply {
            btnLeft.invisible()
//            txtHeading.gone()
        }
    }

    fun showBackButton() {
        this.binding?.apply {
            btnLeft.visible()
            btnLeft.setOnClickListener(backButtonListener)
        }
    }


    fun setHeading(resID: Int, onClickListener: OnClickListener) {
        this.binding?.apply {
            txtHeading.visible()
            txtHeading.setImageResource(resID)
            txtHeading.setOnClickListener(onClickListener)
//            txtHeading.text = heading
        }
    }

    fun showEnvironments(itemSelectedListener: (item: MyEnvironmentModel) -> Unit) {
        this.binding?.apply {
            spnEnvironment.visible()
            spnEnvironment.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//                override fun onUserItemSelected(position: Int) {
//                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                    itemSelectedListener.invoke(adapter.getItem(position = position))
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
        }
    }

    fun showTitleBar() {
        this.binding?.containerTitleBar?.visible()
    }

    fun hideTitleBar() {
        this.binding?.containerTitleBar?.gone()
    }


    fun setBackButtonListener(listener: OnClickListener?) {
        this.backButtonListener = listener
    }


}