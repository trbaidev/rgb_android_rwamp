package com.aquatic.av.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.widget.AppCompatDrawableManager
import androidx.core.graphics.ColorUtils
import com.aquatic.av.R


class CircleImage @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val indicatorPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val circlePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val bitmapPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var mBitmapShader: Shader? = null
    private val mShaderMatrix: Matrix = Matrix()

    private val mBitmapDrawBounds: RectF = RectF()

    private var outerRadius = 0.0f
    private var innerRadius = 0.0f
    private var indicatorItemRadius = 0.0f

    private var centerX = 0f
    private var centerY = 0f

    private var actualColor: FloatArray = floatArrayOf(0f, 1f, 0.5f)

    private var isColorSelected = false
    private var isColorImage = false
    private var isMultiColorSelected = false

    var bitmap: Bitmap? = null

    init {

        indicatorPaint.style = Paint.Style.STROKE
        indicatorPaint.strokeWidth = dp(2f)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            indicatorPaint.color = resources.getColor(R.color.theme_blue, null)
        } else {
            indicatorPaint.color = resources.getColor(R.color.theme_blue)
        }

        circlePaint.strokeCap = Paint.Cap.ROUND
        circlePaint.style = Paint.Style.FILL

        indicatorItemRadius = dp(4f)
    }

    override fun onSizeChanged(width: Int, height: Int, oldW: Int, oldH: Int) {
        val minimumSize = if (width > height) height else width
        val padding = (paddingLeft + paddingRight + paddingTop + paddingBottom) / 4f

        updateCircleDrawBounds(mBitmapDrawBounds)
        outerRadius = minimumSize.toFloat() / 2f - padding

        innerRadius = outerRadius - indicatorItemRadius / 3

        centerX = width / 2f
        centerY = height / 2f
        setupBitmap()
    }

    @SuppressLint("RestrictedApi")
    private fun setupBitmap() {

        bitmap = getBitmapFromDrawable(
            AppCompatDrawableManager.get().getDrawable(
                context,
                R.drawable.palette
            )
        )
        if (bitmap == null) {
            return
        }

        mBitmapShader = BitmapShader(bitmap!!, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        bitmapPaint.setShader(mBitmapShader)

        updateBitmapSize()
    }

    fun multiColorSelecetd(isSelecetd: Boolean) {
        isMultiColorSelected = isSelecetd
    }

    private fun dp(value: Float): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value,
            resources.displayMetrics
        )
    }

    @SuppressLint("RestrictedApi")
    fun drawColorImage() {
        isColorImage = true
        bitmap = getBitmapFromDrawable(
            AppCompatDrawableManager.get().getDrawable(
                context,
                R.drawable.palette
            )
        )
        invalidate()
    }

    private fun updateBitmapSize() {
        if (bitmap == null) return

        bitmap?.let { bitmap ->
            val dx: Float
            val dy: Float
            val scale: Float

            // scale up/down with respect to this view size and maintain aspect ratio
            // translate bitmap position with dx/dy to the center of the image
            if (bitmap.getWidth() < bitmap.getHeight()) {
                scale = mBitmapDrawBounds.width() / bitmap.getWidth()
                dx = mBitmapDrawBounds.left
                dy =
                    mBitmapDrawBounds.top - bitmap.getHeight() * scale / 2f + mBitmapDrawBounds.width() / 2f
            } else {
                scale = mBitmapDrawBounds.height() / bitmap.getHeight()
                dx =
                    mBitmapDrawBounds.left - bitmap.getWidth() * scale / 2f + mBitmapDrawBounds.width() / 2f
                dy = mBitmapDrawBounds.top
            }
            mShaderMatrix.setScale(scale, scale)
            mShaderMatrix.postTranslate(dx, dy)
            mBitmapShader!!.setLocalMatrix(mShaderMatrix)
        }


    }

    protected fun updateCircleDrawBounds(bounds: RectF) {
        val contentWidth = (width - paddingLeft - paddingRight).toFloat()
        val contentHeight = (height - paddingTop - paddingBottom).toFloat()

        var left = paddingLeft.toFloat()
        var top = paddingTop.toFloat()
        if (contentWidth > contentHeight) {
            left += (contentWidth - contentHeight) / 2f
        } else {
            top += (contentHeight - contentWidth) / 2f
        }

        val diameter = Math.min(contentWidth, contentHeight)
        bounds.set(left, top, left + diameter, top + diameter)
    }

    private fun getBitmapFromDrawable(drawable: Drawable?): Bitmap? {
        if (drawable == null) {
            return null
        }

        if (drawable is BitmapDrawable) {
            return drawable.bitmap
        }

        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (isColorImage) {
            bitmap?.let {
                println("it isz ${it.height}")
                canvas.drawOval(mBitmapDrawBounds, bitmapPaint)

                if (isMultiColorSelected) {
                    circlePaint.color = Color.WHITE
                    canvas.drawCircle(centerX, centerY, innerRadius, circlePaint)
                }
            }

            return
        }

        if (isColorSelected) {
            canvas.drawCircle(centerX, centerY, outerRadius, indicatorPaint)
        }

        circlePaint.strokeCap = Paint.Cap.ROUND
        circlePaint.style = Paint.Style.FILL
        val color = ColorUtils.HSLToColor(actualColor)

        if (color == Color.WHITE && !isColorSelected) {
            circlePaint.color = Color.BLACK
            val blackRadius = innerRadius + dp(1f)
            canvas.drawCircle(centerX, centerY, blackRadius, circlePaint)
        }

        circlePaint.color = color
        canvas.drawCircle(centerX, centerY, innerRadius, circlePaint)
    }

    fun setColor(color: Int) {
        isColorSelected = false

        postDelayed({
            ColorUtils.colorToHSL(color, this.actualColor)
            invalidate()
        }, 300)
    }

    fun selectedColor(color: Int) {
        setColor(color)
        isColorSelected = true
    }

    /**
     * Extract the Bitmap from a Drawable and resize it to the expectedSize conserving the ratio.
     *
     * @param drawable   Drawable used to extract the Bitmap. Can be null.
     * @param expectSize Expected size for the Bitmap. Use [.DEFAULT_DRAWABLE_SIZE] to
     * keep the original [Drawable] size.
     * @return The Bitmap associated to the Drawable or null if the drawable was null.
     * @see <html>[Stackoverflow answer](https://stackoverflow.com/a/10600736/1827254)</html>
     */
    fun getBitmapFromDrawable(@Nullable drawable: Drawable?, expectSize: Int): Bitmap? {
        val bitmap: Bitmap

        if (drawable == null) {
            println("Drwawe nlll")
            return null
        }

        if (drawable is BitmapDrawable) {
            val bitmapDrawable = drawable as BitmapDrawable?
            if (bitmapDrawable!!.bitmap != null) {
                return bitmapDrawable.bitmap
            }
        }

        if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            bitmap = Bitmap.createBitmap(
                1,
                1,
                Bitmap.Config.ARGB_8888
            ) // Single color bitmap will be created of 1x1 pixel
        } else {
            val ratio = if (expectSize != 50)
                calculateRatio(drawable.intrinsicWidth, drawable.intrinsicHeight, expectSize)
            else
                1f

            val width = (drawable.intrinsicWidth * ratio).toInt()
            val height = (drawable.intrinsicHeight * ratio).toInt()

            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        }

        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }

    /**
     * Calculate the ratio to multiply the Bitmap size with, for it to be the maximum size of
     * "expected".
     *
     * @param height   Original Bitmap height
     * @param width    Original Bitmap width
     * @param expected Expected maximum size.
     * @return If height and with equals 0, 1 is return. Otherwise the ratio is returned.
     * The ration is base on the greatest side so the image will always be the maximum size.
     */
    fun calculateRatio(height: Int, width: Int, expected: Int): Float {
        if (height == 0 && width == 0) {
            return 1f
        }
        return if (height > width)
            expected / width.toFloat()
        else
            expected / height.toFloat()
    }

}