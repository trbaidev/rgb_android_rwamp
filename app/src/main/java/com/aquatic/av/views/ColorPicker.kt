package com.aquatic.av.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import androidx.core.graphics.ColorUtils

interface ColorChangeListener {
    fun onColorUpdated(newColor: Int)
    fun onStopTrackingTouch()
}

class ColorPicker @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val TAG = "ColorPicker"
    val shaderPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val indicatorPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val storkePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val indicatorShaderPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val fillIndicatorShaderPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var centerCircleRadius = 0.0f
    private var outerRadius = 0.0f
    private var hueRadius = 0.0f
    private var indicatorRadius = 0.0f
    private var fillIndicatorRadius = 0.0f
    private var indicatorItemRadius = 0.0f

    private val noOfHueColors = 360
    private val hueColors = IntArray(noOfHueColors)
    private val indicatorColors = IntArray(11)
    private val hueColorsPosition = FloatArray(noOfHueColors)

    private var centerX = 0f
    private var centerY = 0f

    private var indicatorX: Float = 0f
    private var indicatorY: Float = 0f

    private var actualColor: FloatArray = floatArrayOf(0f, 1f, 0.5f)

    private var hueCircleRecF: RectF? = null
    private var indicatorCircleRecF: RectF? = null
    private var fillIndicatorCircleRecF: RectF? = null

    private lateinit var shader: Shader

    private var selectedColor: Float = 0.0f

    init {
        shaderPaint.style = Paint.Style.STROKE
        shaderPaint.strokeCap = Paint.Cap.ROUND

        indicatorShaderPaint.style = Paint.Style.STROKE
        indicatorShaderPaint.strokeCap = Paint.Cap.ROUND

        fillIndicatorShaderPaint.style = Paint.Style.STROKE
        fillIndicatorShaderPaint.strokeCap = Paint.Cap.ROUND
        fillIndicatorShaderPaint.strokeWidth = dp(1f)

        indicatorPaint.style = Paint.Style.FILL
        indicatorPaint.strokeWidth = dp(2f) // The size of the border of the head circle

        storkePaint.color = Color.parseColor("#F2F2F2")
        storkePaint.style = Paint.Style.STROKE
        storkePaint.strokeCap = Paint.Cap.ROUND
        storkePaint.strokeWidth = dp(1f)

        indicatorItemRadius = dp(13f) //Changed the size of head circle
    }

    fun fillHueColorArray(color: FloatArray): IntArray {
        color[1] = 1f
        color[2] = 0.5f
        for (i in 0 until noOfHueColors) {
            color[0] = i.toFloat()
            hueColors[i] = ColorUtils.HSLToColor(color)
        }
        return hueColors
    }

    fun fillIndicatorColorArray(color: FloatArray): IntArray {
        for (i in 0 until 11) {
            color[2] = 0.0f
            indicatorColors[i] = ColorUtils.HSLToColor(color)
        }
        return indicatorColors
    }

    var fillWithColor: Int = 0
    fun fillWithColor(fill: Int) {
        fillWithColor = fill
        invalidate()
    }

    override fun onSizeChanged(width: Int, height: Int, oldW: Int, oldH: Int) {
        val minimumSize = if (width > height) height else width
        val padding = (paddingLeft + paddingRight + paddingTop + paddingBottom) / 4f
        outerRadius = minimumSize.toFloat() / 2f - padding

        centerCircleRadius = minimumSize.toFloat() / 6f
        hueRadius = centerCircleRadius * 2
        indicatorRadius = hueRadius //+ dp(20f) made same radius as hue, a requirement of new design
        fillIndicatorRadius = dp(2f)

        centerX = width / 2f
        centerY = height / 2f
    }

    private fun dp(value: Float): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value,
            resources.displayMetrics
        )
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)


        drawHue(canvas)
        drawIndicator(canvas)

        if (fillWithColor == 1) {
            return
        }

        indicatorPaint.style = Paint.Style.FILL
        indicatorPaint.color = ColorUtils.HSLToColor(actualColor)
        canvas.drawCircle(centerX, centerY, centerCircleRadius, indicatorPaint)
        canvas.drawCircle(centerX, centerY, centerCircleRadius, storkePaint)
    }

    private fun drawHue(canvas: Canvas) {
        shaderPaint.style = Paint.Style.STROKE
        shaderPaint.strokeCap = Paint.Cap.ROUND

        if (fillWithColor == 1) {
            hueCircleRecF = RectF(
                centerX - (hueRadius / 2),
                centerY - (hueRadius / 2),
                centerX + (hueRadius / 2),
                centerY + (hueRadius / 2)
            )

            indicatorPaint.style = Paint.Style.FILL
            indicatorPaint.color = ColorUtils.HSLToColor(actualColor)
            canvas.drawCircle(centerX, centerY, hueRadius + dp(8f), indicatorPaint)

            canvas.drawCircle(centerX, centerY, hueRadius + dp(8f), storkePaint)
        } else {
            hueCircleRecF = RectF(
                centerX - hueRadius,
                centerY - hueRadius,
                centerX + hueRadius,
                centerY + hueRadius
            )
        }

        hueCircleRecF?.let {
            shaderPaint.strokeWidth =
                if (fillWithColor == 1) hueRadius else 80f // The width of outer circle.
            shaderPaint.shader = getShader()
            canvas.drawArc(it, 0f, 360f, false, shaderPaint)
        }
    }

    private fun drawIndicator(canvas: Canvas) {

        if (fillWithColor == 1) {
            val touchRadius = Math.sqrt(
                Math.pow(
                    touchX - centerX.toDouble(),
                    2.0
                ) + Math.pow(touchY - centerY.toDouble(), 2.0)
            ).toFloat()

            if (touchRadius > (centerX - (hueRadius / 2))) {
                touchX =
                    (centerX + (centerX - (hueRadius / 2)) * Math.cos(Math.toRadians(angle))).toFloat()
                touchY =
                    (centerX + (centerX - (hueRadius / 2)) * Math.sin(Math.toRadians(angle))).toFloat()
            }

            canvas.drawCircle(touchX, touchY, fillIndicatorRadius * 2, fillIndicatorShaderPaint)

            return
        }
        indicatorX = (centerX + indicatorRadius * Math.cos(Math.toRadians(angle))).toFloat()
        indicatorY = (centerY + indicatorRadius * Math.sin(Math.toRadians(angle))).toFloat()

        if (indicatorCircleRecF == null) {
            indicatorCircleRecF = RectF(
                centerX - indicatorRadius,
                centerY - indicatorRadius,
                centerX + indicatorRadius,
                centerY + indicatorRadius
//                centerX - hueRadius,
//                centerY - hueRadius,
//                centerX + hueRadius,
//                centerY + hueRadius
            )
        }

        indicatorCircleRecF?.let {

            for (count in 0 until 180) {
                val stockWidth = (count * 2 * 4f) / 180
                val alpha = (200 * count) / 200

                if (stockWidth < 0.5f) {
                    indicatorShaderPaint.strokeWidth = 0.0f
                } else {
                    indicatorShaderPaint.strokeWidth = stockWidth
                }

                indicatorShaderPaint.color = Color.parseColor(
                    "#${
                        String.format(
                            "%02X",
                            0xFF and alpha
                        )
                    }000000"
                )

//                val newAngle = (angle.toFloat() - 180f + (count * 1)) % 360
//                canvas.drawArc(
//                    it,
//                    newAngle,
//                    2f,
//                    false,
//                    indicatorShaderPaint
//                )
            }
        }



        indicatorPaint.style = Paint.Style.STROKE //FILL
        indicatorPaint.color = Color.WHITE
        canvas.drawCircle(
            indicatorX,
            indicatorY,
            indicatorItemRadius,
            indicatorPaint
        ) // The head of the indicator
    }


    private fun getShader(): Shader {
        fillHueColorArray(actualColor.copyOf())
        getColorPositionArray()
        shader = SweepGradient(centerX, centerY, hueColors, hueColorsPosition)
        shader.setLocalMatrix(matrix)

        return shader
    }

    private fun getColorPositionArray(): FloatArray {
        for (i in 0 until noOfHueColors) {
            hueColorsPosition[i] = i * (360 / (noOfHueColors - 1)) / 360f
        }
        return hueColorsPosition
    }

    operator fun contains(point: PointF): Boolean {
        val touchRadius = indicatorItemRadius + indicatorItemRadius * 0.2


        if (fillWithColor == 1) {

            return point.x in (centerX - (hueRadius - fillIndicatorRadius * 2))..(centerX + (hueRadius - fillIndicatorRadius * 2)) && point.y in (centerY - (hueRadius - fillIndicatorRadius * 2))..(centerY + (hueRadius - fillIndicatorRadius * 2))
        }

        return (point.x in (centerX - hueRadius)..(centerX + hueRadius)
                && point.y in (centerY - hueRadius)..(centerY + hueRadius))
                || (point.x in (indicatorX - touchRadius)..(indicatorX + touchRadius)
                && point.y in (indicatorY - touchRadius)..(indicatorY + touchRadius))
    }

    var touchX: Float = 0f
    var touchY: Float = 0f

    var isTouched = false
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                Log.d(TAG, "ACTION_DOWN")

                if (PointF(x, y) in this) {
                    touchX = x
                    touchY = y

                    isTouched = true
                    calculateAngle(x, y)
                    updateComponent(angle)
                }
            }

            MotionEvent.ACTION_MOVE -> {
                Log.d(TAG, "ACTION_MOVE")

                if (isTouched) {
                    touchX = x
                    touchY = y

                    calculateAngle(x, y)
                    updateComponent(angle)
                }
            }

            MotionEvent.ACTION_UP -> {
                isTouched = false
                Log.d(TAG, "ACTION_UP")
                colorChangeListener?.let {
                    it.onStopTrackingTouch()
                }
            }
        }

        invalidate()

        return isTouched
    }

    fun updateComponent(angle: Double) {
        var relativeAngle = angle
        if (angle < 0) {
            relativeAngle += 360f
        }

        val baseAngle = relativeAngle - 0
        val component = (baseAngle / 360) * 360

        actualColor[0] = component.toFloat()
        actualColor[1] = 1.0f
        actualColor[2] = 0.5f

        colorChangeListener?.let {
            val newColor = ColorUtils.HSLToColor(actualColor)
            it.onColorUpdated(newColor)
        }


    }

    var angle: Double = 0.0

    fun calculateAngle(x1: Float, y1: Float) {
        val x = x1 - centerX
        val y = y1 - centerY
        val c = Math.sqrt((x * x + y * y).toDouble())

        angle = Math.toDegrees(Math.acos(x / c))
        if (y < 0) {
            angle = 360 - angle
        }
    }

    fun setColor(color: Int) {
        postDelayed({
            ColorUtils.colorToHSL(color, this.actualColor)
            updateAngle(actualColor[0])
            invalidate()
        }, 300)
    }

    fun updateAngle(component: Float) {
        val baseAngle = component / 360 * 360
        val relativeAngle = baseAngle + 0

        angle = relativeAngle.toDouble()
    }

    lateinit var colorPickerSq: View

    fun setColorPicketSq(colorPickerSq: View) {
        this.colorPickerSq = colorPickerSq
    }

    var colorChangeListener: ColorChangeListener? = null
}