package com.aquatic.av.network

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectivityInterceptor @Inject constructor(@ApplicationContext private val context: Context) :Interceptor{
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!ConnectionService(context).isOnline()) {
            throw IOException("Please Check Your Internet Connection")
        } else {
            return chain.proceed(chain.request())
        }
    }
}