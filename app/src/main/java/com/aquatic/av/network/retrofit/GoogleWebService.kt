package com.aquatic.av.network.retrofit

import com.aquatic.av.screens.dealerLocator.model.GoogleAutocompleteSearch
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GoogleWebService {

    @GET("maps/api/place/textsearch/json")
    suspend fun getNearbyDealers(
        @Query("input") input: String,
        @Query("types") types: String,
        @Query("key") key: String,
    ): Response<GoogleAutocompleteSearch>

}