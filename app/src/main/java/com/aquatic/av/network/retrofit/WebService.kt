package com.aquatic.av.network.retrofit


import com.aquatic.av.activities.model.UserEnvironmentModel
import com.aquatic.av.screens.accountAuthentication.createAccount.dataModels.EmailCheckModel
import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginModel
import com.aquatic.av.screens.accountAuthentication.login.dataModels.LoginResponse
import com.aquatic.av.screens.accountAuthentication.resetPassword.dataModels.ResetPasswordModel
import com.aquatic.av.screens.accountAuthentication.resetPassword.dataModels.ResetPasswordResponse
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressModel
import com.aquatic.av.screens.accountAuthentication.userAddressInfo.datamodels.UserAddressResponse
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedModel
import com.aquatic.av.screens.bluetooth.dataModels.UpdateDeviceConnectedResponse
import com.aquatic.av.screens.dashboard.dataModel.DashboardItemModel
import com.aquatic.av.screens.dealerLocator.model.DealerFilterResponse
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionRequest
import com.aquatic.av.screens.dealerLocator.model.DealerSuggestionResponse
import com.aquatic.av.screens.onBoardingFlow.userRegUploadReceipt.dataModels.*
import com.aquatic.av.screens.rgb.dataModel.RGBColorModel
import com.aquatic.av.screens.setting.contactInformation.dataModels.ContactInfoModel
import com.aquatic.av.screens.setting.contactInformation.dataModels.ContactInfoResponse
import com.aquatic.av.screens.setting.myDevices.dataModels.MyDeviceModel
import com.aquatic.av.screens.setting.myDevices.dataModels.MyDeviceResponse
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentModel
import com.aquatic.av.screens.setting.myDevices.dataModels.UpdateEnvironmentResponse
import com.aquatic.av.screens.setting.myEnvironments.dataModels.MyEnvironmentResponse
import com.aquatic.av.screens.setting.personalInformation.dataModels.PersonalInfoModel
import com.aquatic.av.screens.setting.personalInformation.dataModels.PersonalInfoResponse
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface WebService {

    @GET("verifyemail")
    suspend fun emailCheck(@Query("user_email") name: String?): Response<EmailCheckModel>

    @GET("reset")
    suspend fun verifyUserEmail(@Query("user_email") name: String?): Response<EmailCheckModel>

    @POST("updatepinfo")
    suspend fun resetPassword(@Body resetPasswordModel: ResetPasswordModel): Response<ResetPasswordResponse>

    @POST("api/User/AddUpdateUsers_MB")

    suspend fun createUser(@Body userAddressModel: UserAddressModel?): Response<UserAddressResponse>

    @POST("userinfoV4")
    suspend fun loginUser(@Body loginModel: LoginModel): Response<LoginResponse>

    @POST("api/Environment/GetAllEnvironments")
    suspend fun getAllEnvironments(@Body userEnvironmentModel: Any): Response<Any>

    @GET("getallproducttypes")
    suspend fun getAllProductTypes(): Response<ArrayList<Any>>

    @POST("getallproductsbytypeid")
    suspend fun getAllProductsByTypeId(@Body body: Any): Response<ArrayList<Any>>

    @GET("getAllDealers")
    suspend fun getAllDealers(): Response<ArrayList<DealerModel>>

    @GET("verifyserial")
    suspend fun verifySerialNo(@Query("serial_no") serial_no: String): Response<SerialVerificationModel>

    @Multipart
    @POST("api/Generic/UploadImage")
    suspend fun postReceipt(
        @Header("Authorization") token: String,
        @Part image: MultipartBody.Part
    ): Response<ReceiptUploadResponse>

    @POST("api/Devices/deviceaddv2")
    suspend fun registerDevice(
        @Header("Authorization") token: String,
        @Body addDeviceModel: AddDeviceModel
    ): Response<AddDeviceResponse>


    @POST("api/Dealer/GetDealersSuggestion")
    suspend fun getDealerSuggestions(
        @Header("Authorization") token: String,
        @Body dealerSuggestionRequest: DealerSuggestionRequest
    ): Response<DealerSuggestionResponse>

    @POST("api/Dealer/GetFilterDataForDealersLocation")
    suspend fun getDealerFilters(
        @Header("Authorization") token: String
    ): Response<DealerFilterResponse>

    @POST("/api/Environment/AddUpdateEnvironments")
    suspend fun addUpdateEnvironments(
        @Header("Authorization") token: String,
        @Body objects: Any
    ): Response<Any>

    @GET("devicesinfo")
    suspend fun getUserEnvironments(
        @Header("Authorization") token: String,
        @Query("user_email") user_email: String
    ): Response<ArrayList<UserEnvironmentModel>>

    @GET("devicesinfodash")
    suspend fun getUserDashboard(
        @Header("Authorization") token: String,
        @Query("user_email") userEmail: String,
        @Query("env_name") environmentName: String
    ): Response<ArrayList<DashboardItemModel>>

    @POST("updateuserinfo")
    suspend fun updateUserPersonalInfo(
        @Header("Authorization") token: String,
        @Body personalInfoModel: PersonalInfoModel
    ): Response<PersonalInfoResponse>

    @POST("updateusercontactinfo")
    suspend fun updateUserContactInfo(
        @Header("Authorization") token: String,
        @Body contactInfoModel: ContactInfoModel
    ): Response<ContactInfoResponse>

    @POST("api/Environment/GetMyEnvironments")
    suspend fun getMyEnvironmentInfo(
        @Header("Authorization") token: String,
        @Body user_id: JsonObject
    ): Response<MyEnvironmentResponse>

    @GET("api/Devices/api/Devices/devicesinfodash")
    suspend fun getDevicesInfoDash(
        @Header("Authorization") token: String,
        @Query("user_email") email: String,
        @Query("env_name") env_name: String
    ): Response<MyDeviceResponse>

    @POST("updatedevicename")
    suspend fun updateDeviceName(
        @Header("Authorization") token: String,
        @Body my_device: MyDeviceModel
    ): Response<Any>


    @POST("api/Devices/AddUpdateDeviceFavouriteColors")
    suspend fun addUpdateDeviceColor(
        @Header("Authorization") token: String,
        @Body param: HashMap<String, Any>
    ): Response<Any>

    @POST("/api/Devices/RemoveDeviceFavColor_MB")
    suspend fun removeFavoritesColors(
        @Header("Authorization") token: String,
        @Body param: HashMap<String, Any>
    ): Response<Any>

    @POST("api/Devices/GetDevicesColors_MB")
    suspend fun getAllUserColors(
        @Header("Authorization") token: String,
        @Body param: HashMap<String, Int>
    ): Response<RGBColorModel>

    @POST("/api/Devices/RemoveDeviceFavColor_MB")
    suspend fun removeDeviceFavoritesColor(
        @Header("Authorization") token: String,
        @Body param: HashMap<String, List<Int>>
    ): Response<RGBColorModel>

    @POST("api/Devices/UpdateDeviceEnv")
    suspend fun updateEnvironment(
        @Header("Authorization") token: String,
        @Body body: UpdateEnvironmentModel
    ): Response<UpdateEnvironmentResponse>

    @POST("api/Devices/UpdateDeviceConnectedStatus")
    suspend fun updateDeviceConnectedStatus(
        @Header("Authorization") token: String,
        @Body body: UpdateDeviceConnectedModel
    ): Response<UpdateDeviceConnectedResponse>
}
