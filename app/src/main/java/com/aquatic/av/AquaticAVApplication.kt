package com.aquatic.av

import android.app.Application
import android.graphics.Bitmap
import android.text.TextUtils
import com.aquatic.av.bluetoothScanner.ble.FileUtils
import com.aquatic.av.bluetoothScanner.ble.RGBManager
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceKey
import com.aquatic.av.bluetoothScanner.ble.preference.PreferenceUtil
import com.aquatic.av.bluetoothScanner.ble.preference.RgbSharedPreferences
import com.aquatic.av.bluetoothScanner.ble.profile.RGBBLEManagerImpl
import com.aquatic.av.bluetoothScanner.ble.utils.BLEUtils
import com.aquatic.av.bluetoothScanner.ble.utils.BTUtil
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.ImageScaleType
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer
import dagger.hilt.android.HiltAndroidApp
import io.intercom.android.sdk.Intercom

@HiltAndroidApp
class AquaticAVApplication : Application() {
    val TAG: String = AquaticAVApplication::javaClass.name
    override fun onCreate() {
        super.onCreate()
        initImageLoader()
        ctx = this
        PreferenceUtil.init(this, TAG)
        RgbSharedPreferences.init(this)
        RGBManager.getInstance().init(this)
        Intercom.initialize(this, BuildConfig.INTERCOM_API_KEY, BuildConfig.INTERCOM_APP_ID)
        startAutoConnect()
    }

    companion object {
        val TAG: String = AquaticAVApplication::class.java
            .simpleName
        lateinit var ctx: AquaticAVApplication

        fun getAppContext(): AquaticAVApplication {
            return ctx
        }
    }

    private fun initImageLoader() {
        val options = DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.image_placeholder)
                .showImageOnFail(R.drawable.image_placeholder)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .displayer(FadeInBitmapDisplayer(850))
                .bitmapConfig(Bitmap.Config.RGB_565).build()
        val config = ImageLoaderConfiguration.Builder(
                applicationContext).defaultDisplayImageOptions(options)
                .build()
        ImageLoader.getInstance().init(config)
    }

    fun startAutoConnect() {
        FileUtils.addlogs("APP: startAutoConnect")
        val macAddress = PreferenceUtil.getInstance()
                .getStringValue(PreferenceKey.CONNECTED_BT_DEVICE_ADDRESS, null)
        if (!TextUtils.isEmpty(macAddress)) {
            if (BLEUtils.isBleEnabled(this)) {
                val bleManager = RGBManager.getInstance().getBLEManager(this)
                if (!bleManager.isConnected) {
                    val bluetoothDevice = BTUtil.getRemoteDevice(this, macAddress)
                    if (bluetoothDevice != null) {
                        // Initialize the manager
                        bleManager.setGattCallbacks(RGBBLEManagerImpl())
                        bleManager.connect(bluetoothDevice)
                    }
                }
            }
        }
    }
}